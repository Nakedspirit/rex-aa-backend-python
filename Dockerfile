# To run locally
# 1. docker build -t rex-gig-app-backend .
# 2. docker run --rm --detach  --env-file .env --publish 3000:80 --name reg-gig-app --tty --interactive rex-gig-app-backend

FROM python:3.7

LABEL version="1.0"
LABEL description="Rex Gig Mobile Api"

ARG INSTALL_LINTERS=0

ENV LC_ALL="en_US.UTF-8" LANGUAGE="en_US.UTF-8" LANG="en_US.UTF-8"
ENV LIB_PATH /app

RUN mkdir -p ${LIB_PATH}

WORKDIR ${LIB_PATH}
COPY ./requirements.txt ${LIB_PATH}/requirements.txt
COPY ./requirements-lint.txt /app/requirements-lint.txt
RUN /usr/local/bin/pip3 install -r ${LIB_PATH}/requirements.txt
RUN [ "$INSTALL_LINTERS" = "1" ] && /usr/local/bin/pip3 install -r /app/requirements-lint.txt || true

COPY . .

EXPOSE 80

CMD ["bash", "./entrypoint.sh"]