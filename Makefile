# Project configuration
PROJECT_NAME = rex-gig-mobile-api

# Makefile parameters.
MAKE_MAJOR = $(shell echo $(MAKE_VERSION) | cut -f1 -d.)
MAKE_MINOR = $(shell echo $(MAKE_VERSION) | cut -f2 -d.)
MODERN_MAKE = 0
ifeq (4, $(MAKE_MAJOR))
	MODERN_MAKE = 1
endif
ifeq (3, $(MAKE_MAJOR))
	ifeq (81, $(MAKE_MINOR))
		MODERN_MAKE = 1
	endif
endif
ifeq (1, $(MODERN_MAKE))
	TAG := $(shell git describe --exact-match 2>&1)
    ifneq ($(.SHELLSTATUS), 0)
		TAG := $(shell git rev-parse --abbrev-ref HEAD | tr '/' '-')-$(shell git rev-parse HEAD)
	endif
else
	TAG ?= $(shell git describe)
endif

# General.
SHELL = /bin/bash
TOPDIR = $(shell git rev-parse --show-toplevel)

# Docker.
# TODO replace with Rex ECR
ECS_REGISTRY = 355508092300.dkr.ecr.us-west-2.amazonaws.com
DOCKER_ORG = rex
DOCKER_REPO = $(DOCKER_ORG)/$(PROJECT_NAME)
DOCKER_IMG = $(DOCKER_REPO):$(TAG)

# Helm Charts.
DEPLOY_EXTRA_OPTS = "--set image.tag=$(TAG)"

default: help

help: # Display help
	@awk -F ':|##' \
		'/^[^\t].+?:.*?##/ {\
			printf "\033[36m%-30s\033[0m %s\n", $$1, $$NF \
		}' $(MAKEFILE_LIST) | sort

build-docker: ## Build the docker image
	docker build -t $(DOCKER_IMG) .
	docker tag $(DOCKER_IMG) $(ECS_REGISTRY)/$(DOCKER_IMG)

dist-clean: dist-clean-docker dist-clean-local ## Clean everything (!DESTRUCTIVE!)

dist-clean-code: ## Remove unwanted files in this project (!DESTRUCTIVE!)
	cd $(TOPDIR) && git clean -ffdx

dist-clean-docker: ## Remove all docker artifacts for this project (!DESTRUCTIVE!)
	docker image rm -f $(shell docker image ls --filter reference='$(DOCKER_REPO)' -q) || true

dist-clean-local: ## Remove all the Kubernetes objects associated to this project
	helm delete --purge $(PROJECT_NAME) || true

deploy-local: ## Deploy the application to local
	HELM_ENV=local bash tools/helm_apply.sh $(PROJECT_NAME) $(DEPLOY_EXTRA_OPTS)

deploy-prod: ## Deploy the application to QA
	HELM_ENV=prod bash tools/helm_apply.sh $(PROJECT_NAME) $(DEPLOY_EXTRA_OPTS)

deploy-qa: ## Deploy the application to QA
	HELM_ENV=qa bash tools/helm_apply.sh $(PROJECT_NAME) $(DEPLOY_EXTRA_OPTS)

deploy-qa2: ## Deploy the application to QA
	HELM_ENV=qa2 bash tools/helm_apply.sh $(PROJECT_NAME) $(DEPLOY_EXTRA_OPTS)

deploy-qa3: ## Deploy the application to QA
	HELM_ENV=qa3 bash tools/helm_apply.sh $(PROJECT_NAME) $(DEPLOY_EXTRA_OPTS)

deploy-qa4: ## Deploy the application to QA
	HELM_ENV=qa4 bash tools/helm_apply.sh $(PROJECT_NAME) $(DEPLOY_EXTRA_OPTS)

deploy-stage: ## Deploy the application to QA
	HELM_ENV=stage bash tools/helm_apply.sh $(PROJECT_NAME) $(DEPLOY_EXTRA_OPTS)

push-docker: ## Push a Docker image to ECR
	@eval $(shell aws ecr get-login --no-include-email --region us-west-2) #  --profile rex-admin)
	docker push $(ECS_REGISTRY)/$(DOCKER_IMG)

push-release: ## Push a release upstream
	git checkout develop
	git pull
	git push
	git checkout master
	git pull
	git push
	git push --tags
	git checkout develop

run-dev: ## runs flask dev server
	FLASK_APP=rex-mobile-api FLASK_ENV=development flask run

view-tag: ## show the tag that will be used for the container
	echo $(TAG)

.PHONY: help build-docker ci ci-linters ci-doc ci-tests dist-clean dist-clean-code dist-clean-docker dist-clean-local code deploy-local deploy-prod deploy-qa deploy-qa2 deploy-qa3 deploy-qa4 deploy-stage dist docs prepare push-docker setup