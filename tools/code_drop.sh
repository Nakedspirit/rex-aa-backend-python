#!/bin/bash

PATH_TO_REX_REPO="../rex-agents-app"

# Copying folders to REX repo
folders_to_copy=('rex_backend' 'charts')
for folder in ${folders_to_copy[*]}
do
    rm -rf $PATH_TO_REX_REPO/backend/$folder/*
    cp -r ./$folder/* $PATH_TO_REX_REPO/backend/$folder/
    echo "Copied ${folder} folder"
done


# Copying files to REX repo
files_to_copy=('alembic.ini' 'Dockerfile' 'entrypoint.sh' 'Makefile' 'requirements.txt' 'requirements-lint.txt' 'server.py' 'console.py' 'setup.cfg')
for file in ${files_to_copy[*]}
do
    rm -rf $PATH_TO_REX_REPO/backend/$file
    cp -r ./$file $PATH_TO_REX_REPO/backend/
    echo "Copied ${file} file"
done

echo 'Done!'