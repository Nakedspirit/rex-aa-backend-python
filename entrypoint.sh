#!/bin/bash
echo 'Waiting for db start up'
sleep 10
echo 'Run migrations'
alembic upgrade head
echo 'Start app server'
uwsgi --http 0.0.0.0:80 --processes 4 --callable app --wsgi-file server.py --enable-threads --master