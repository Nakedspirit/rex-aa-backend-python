# Intro

The process of code drop is not so obvious in our case and it's important to note that it's the part of deploy process.

## Preparation

We need to clone REX repo to the local machine, below are steps to complete this

1. You need the access to [REX repository](https://bitbucket.org/rexdev/rex-agents-app)
2. If you have not the access, take **REX bitbucket account** from [there](https://docs.google.com/spreadsheets/d/1AjGUcBqKmazOEtp53lHy8revdLM3yLzLCHALPqlIC6A/edit#gid=564453859) and then login
3. Clone repository to the local machine
4. Switch to qa branch
5. That's all for REX repo for now

Ok, now we need to configure the code drop for our working repo

1. Open your working repo
2. Open `tools/code_drop.sh`
3. Assign path of your REX repo to `PATH_TO_REX_REPO` variable

## The Code Drop itself

1. Open REX repo in terminal
2. Checkout to needed branch, `qa` in most cases
3. Open your working repo in terminal
4. Run `bash tools/lint.sh` in your working repo
5. Come back to REX repo
6. Use git to commit new changes, hope you know how to use git

## Deploy

The table tells us about which branch going to which environment

| REX Branch | Deploying to | Additional info |
| --- | --- | --- |
| [qa](https://bitbucket.org/rexdev/rex-agents-app/src/qa/) | QA |  |
| [develop](https://bitbucket.org/rexdev/rex-agents-app/src/develop/) | REX engineers will create PR to `master` branch and then deploy it to Production |

1. Go to Code Drop and pass through it
2. Contact to REX engineers and ask them to deploy needed branch, for example `qa` branch to QA
