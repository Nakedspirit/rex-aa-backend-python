# Intro

Welcome to Rex AA App. This is a Python Flask application with a GraphQL API that on a high level can:

- query data from _autoschedule_ DB
- call _autoschedule api_ to perform business actions such as "claim a showing"
- return and store _post showing questions & answers_

# What is what

The app's main purpose is to query data from Rex DB and mutate it via Rex API (autoschedule API). Certain features of the mobile AA app are not supported by Rex DB and API. To implement them we provision our own PostgresDB called `gig-app` on Rex DB cluster.

# Deployments

There is no SVI deployment of Rex AA app because Rex DB and autoschedule API exist inside Rex's VPN. In other words, this app can function only in 2 cases:

1. When deployed to Rex k8s as a service
2. When ran locally by a developer with an active VPN connection

## Setting up VPN Connection & Rex DBs

To work on this app you'll need to set up a Rex VPN. It will allow you to access Rex DBs primarily autoschedule DB.

All necessary credentials are stored in [Infrastructure Table](https://docs.google.com/spreadsheets/d/1AjGUcBqKmazOEtp53lHy8revdLM3yLzLCHALPqlIC6A/edit#gid=564453859). If you don't find what you need ask in #rex slack channel

### Step 1

To connect to DBs you will need an OpenVPN profile. You can use [Tunnelblick](https://tunnelblick.net/) for OSX or any other OpenVPN client.

> For (1) above the PM will need to contact REX engineers, so it'll take some time. Now you can go to Development section and return here later.

1. Ask PM for an `@rexhomes.com` email and lastpass account
2. Open [lastpass](https://www.lastpass.com/) and login
3. Open the item, there should be lint to https://vpn.rexchange.com/
4. Go to https://vpn.rexchange.com/ and login with credentials from lastpass
5. Download `.ovpn` file from https://vpn.rexchange.com/
6. Open `.ovpn` in Tunnelblick

### Step 2

You can use [TablePlus](https://tableplus.io/) to access Rex DBs. An importable TablePlus-dump file is provided. When imported to TablePlus it will set up all necessary connections.

[Secured dump file](https://drive.google.com/file/d/1LfX5mrbwD5CP-eg1CS3dBgzieJ3QdUI0/view?usp=sharing)
Password: `svi`

# Development

The app is designed to run as a Docker container. Running without Docker is not supported and not advised.

1. Get a [local .env](https://docs.google.com/document/d/1npOXHYyNZs9YaiJmmxE9EKFHhkneHp2HbM56eNo1zcI/edit)
2. Make sure your Rex VPN is up and you can connect to DBs via TablePlus (or any other DB viewer)
3. Run `docker-compose build`
4. Run `docker-compose run --rm rex_backend alembic upgrade head` to migrate database
5. Run `docker-compose up` to start the app
4. Open `localhost:5005/playground`. If you see GraphQL schema and docs most likely everything is good 
5. You can run tests with `docker-compose run --rm rex_backend py.test`
6. You can run code linters with `docker-compose run --rm rex_backend bash tools/lint.sh`
7. You can also open the container of the app with `docker-compose run --rm rex_backend bash` and run commads inside the container

## IDE

If plan to use VSCode to write code [read our guide](https://github.com/SiliconValleyInsight/rex-aa-backend-python/blob/development/docs/VSCODE_GUIDE.md).
