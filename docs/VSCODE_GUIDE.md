# VSCode Python Guide

This guide will walk you through the necessary set up that will allow you:

1. Use VSCode built-in debugger to debug docker/docker-compose python app
2. Run Python unit tests

## Debugging

### Preliminary

This has already been done for Rex. If you have been referred here to set up for another project, please, read on.

VSCode internally uses [ptvsd](https://github.com/microsoft/ptvsd) to debug Python apps. With Docker based development you need to run `ptvsd` on a some port that is not your main app's port. For example, if app runs on 3000 you should configure `ptvsd` on 3005.

Add the following snippet to your app's entry point (application.py for Rex)

```python
is_debugging_env = os.environ.get('DEBUG') is not None
if is_debugging_env:
    import ptvsd

    # Allow VSCode attach a debugger on this port
    # make sure docker-compose opens this port:
    # - ports: 3005:3005
    ptvsd.enable_attach(address=('0.0.0.0', 3005), redirect_output=True)

    # Pause the program until a remote debugger is attached
    # ptvsd.wait_for_attach()
```

If your project uses requirements.txt to manage dependencies add the following:

```
# VSCode debugging
ptvsd==4.2.1
```

Make sure docker-compose exposes `ptvsd` outside. Add this to your service configuration

```yml
services:
  my_backend:
    ports:
      - "3000:80" # main app
      - "3005:3005" # debugger port
```

You should set env `DEBUG=1` for the above snippet to kick-in

### VSCode Settings

The setup below is based on [official documentation](https://code.visualstudio.com/docs/python/debugging#_remote-debugging). Consult it for additional information.

1. Install VSCode Python [plugin](https://code.visualstudio.com/docs/languages/python)
2. Add the following to `.vscode/launch.json`

```json
{
  "configurations": [
    {
      "name": "Python: Remote Attach",
      "type": "python",
      "request": "attach",
      "port": 3005, // THIS IS THE PORT YOU CONFIGURED ptvsd ON!
      "host": "localhost",
      "pathMappings": [
        {
          "localRoot": "${workspaceFolder}",
          "remoteRoot": "/app" // THIS IS WORKDIR from Dockerfile
        }
      ]
    }
  ]
}
```

3. Make sure `DEBUG=1` env is present (for Rex this is true, see docker-compose)
4. Run the app as usual (e.g. `docker-compose up -d`)
5. Open VSCode, click on Debugger icon, select `Python: Remote Attach` and click green run icon
6. Enjoy debugging from VSCode

## Testing

// TODO
