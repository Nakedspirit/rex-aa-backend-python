# REX APIs

Here's the document is about REX APIs we are using in the project.
The reason for this document is that we need to collect our knowledge about REX APIs.

## REX Paperless Signin API

The API for Open House Showings to work with visitors, phone number, photos.

| Environment | URL | Comment |
| --- | --- | --- |
| Local | https://qa-paperless-signin.rexhomes.com | - |
| QA | http://paperless-signin | inside REX infrastructure |
| PROD | http://paperless-signin | inside REX infrastructure |

**Default Params**:
```yaml
PASSWORD: poa0HVpIi
```

### Photos Section

#### (1) Get Photos

The endpoint for getting Open House photos links

```yaml
method: GET
path: api/photos

params:
    required:
        password: default PASSWORD
        open_house_guid: autoschedule db, open_houses table, dino_open_house_guid column
```

**Returns**:
```json
{
    "links": [
        "https://cdn.rexhomes.com/svi/qa/b5bef4d363394247b2ea0a1fba2ebde9/0.jpg"
    ],
    "success": true
}
```

#### (2) Upload Photos

The endpoint for uploading photos for Open House

```yaml
method: POST
path: api/photos

params:
    required:
        password: default PASSWORD
        open_house_guid: autoschedule db, open_houses table, dino_open_house_guid column

    photo1: image file
    photo2: image file
    photo3: image file
    ...
    photon: image file
```

**Returns**:
```json
{
  "links": [
    "https://cdn.rexhomes.com/svi/qa/b5bef4d363394247b2ea0a1fba2ebde9/0.jpg"
  ],
  "success": true
}
```

### Visitors Section

To add visitor to Open House you need to perform:
1. call Add User endpoint
2. call Sign User endpoint

#### (1) Add User

```yaml
method: POST
path: api/add-user

params:
    required:
        password: default PASSWORD
        open_house_guid: autoschedule db, open_houses table, dino_open_house_guid column
        open_house_phone_number: From Phone Number endpoint
        phone_number: phone number of Visitor
        email: email of Visitor
        name: name of Visitor
```

**Returns**:
```json
{
  "id": 1,
  "success": true
}
```

#### (2) Sign User

```yaml
method: POST
path: api/add-user

params:
    required:
        password: default PASSWORD
        open_house_guid: autoschedule db, open_houses table, dino_open_house_guid column
        open_house_phone_number: From Phone Number endpoint
        phone_number: phone number of Visitor
        email: email of Visitor

    name: name of Visitor
```

**Returns**:
```json
{
  "id": 41,
  "success": true
}
```

#### (3) Get Visitors

```yaml
method: GET
path: api/signins

params:
    required:
        password: default PASSWORD
        open_house_guid: autoschedule db, open_houses table, dino_open_house_guid column
```

**Returns**:

```json
{
  "signins": [
    {
      "id": 1,
      "created": "Wed, 27 Nov 2019 04:42:23 GMT",
      "email": "test+1@test.test",
      "name": "Test1",
      "phone_number": "+11111111111"
    },
    {
      "id": 2,
      "created": "Sun, 01 Dec 2019 09:50:43 GMT",
      "email": "test@test.test",
      "name": "Test",
      "phone_number": "+11111111112"
    }
  ]
}
```

## REX Services API

The API which provides access for different services under the one namespace such as questions.

| Environment | URL | Comment |
| --- | --- | --- |
| Local | https://qa-services.rexagents.com | - |
| QA | http://service-name-here | inside REX infrastructure |
| PROD | http://service-name-here | inside REX infrastructure |

### Questions Section

#### (1) Get Questions

```yaml
method: GET
path: questions/get-questions-survey

params:
    required:
        tool:
          - sellersurvey (For comments about Listing)
          - aasurvey (For comments about Listing)
          - rexlisting
          - postshowings (For Showing Report of regular Showings)
          - saguaro
          - openhouse (For Showing Report of Open House Showing)
          - openhouseleads (For Open House Visitors)
          - openhouseattendee
          - enhancedinspection
```

**Returns** with `tool:"postshowings"`:

```json
{
  "success": true,
  "result": {
    "pages": [
      {
        "title": "Post Showing Feedback",
        "elements": [
          {
            "name": "ps_buyer_likes",
            "title": "Main Buyer likes (Select all that apply):",
            "valueName": "0abdcd3711c34dd78f119ca928bb3ecd",
            "isRequired": true,
            "choices": [
              {
                "value": "Location (school district/neighborhood/nearby amenities)",
                "text": "Location (school district/neighborhood/nearby amenities)"
              },
              {
                "value": "N/A",
                "text": "N/A"
              }
            ],
            "type": "checkbox"
          },
          {
            "name": "ps_price_feedback",
            "title": "What are the main thoughts on price?",
            "valueName": "b58b4c80e01b493bbe26c015470d2493",
            "isRequired": true,
            "choices": [
              {
                "value": "Underpriced",
                "text": "Underpriced"
              },
              {
                "value": "About Right",
                "text": "About Right"
              },
              {
                "value": "Somewhat Overpriced",
                "text": "Somewhat Overpriced"
              },
              {
                "value": "Substantially Overpriced",
                "text": "Substantially Overpriced"
              }
            ],
            "type": "radiogroup"
          }
        ]
      }
    ]
  }
}
```

#### (2) Get Answers

```yaml
method: GET
path: questions/get-answers

params:
    required:
        tool:
          - sellersurvey (For comments about Listing)
          - aasurvey (For comments about Listing)
          - rexlisting
          - postshowings (For Showing Report of regular Showings)
          - saguaro
          - openhouse (For Showing Report of Open House Showing)
          - openhouseleads (For Open House Visitors)
          - openhouseattendee
          - enhancedinspection
        listing_guid: from MySQL, rex_db database, listing table, guid column
        source: everything can be placed here, for visitors i use such string "gig-app;showing_id:1;signin_id:1"
```

**Returns**:

```json
{
  "success": true,
  "result": [
    {
      "question_guid": "42761b79b5364185a9b9efe0a4ce6026",
      "name": "ohl_guest_name",
      "question": "Guest Name",
      "stage_needed_for": "SHOWINGS",
      "type": "text",
      "enum_csv": "",
      "is_multi": 0,
      "question_source": "aa",
      "validator_regex": null,
      "validator_error_message": null,
      "current": 1,
      "required": 1,
      "question_id": 240,
      "tool_name": "openhouseleads",
      "set_id": 54093,
      "answer": [
        {
          "answer_text": "test@test.test",
          "answer_int": null,
          "answer_date": null,
          "answer_bool": null,
          "source": "gig-app;showing_id:1;visitor_id:1",
          "recorded_by": "kzvonov@rexhomes.com",
          "guid": "588411a71fdf401eb6cccf99133cc626"
        }
      ]
    }
  ]
}
```

#### (3) Add Answers

```yaml
method: POST
path: questions/add-multiple-answers
```

**Data** to send in `json` format:

```json
{
  "listing_guid": "3e8f44aa948d42f5b9050f3c21aeee4d", # from MySQL, rex_db database, listing table, guid column
  "recorded_by": "kzvonov@rexhomes.com", # user's email
  "replace": true, # flag to rewrite answers
  "source": "gig-app;showing_id:1;visitor_id:1", # everything can be placed here, but it's better to use something meaningful
  "values": {
    "{question_guid_variable}": ["{answer_1_variable}"],
    "{question_guid_2_variable}": ["{answer_2_variable}"],
  }
}

```

**Returns**:

```json
{
  "success": true,
  "result": true
}
```
