# Python Local Development

This is WIP guide to running Rex up locally.

While Rex app is configured to run inside Docker ONLY, one might find this guide useful. Maybe for other projects, maybe to troubleshoot something really weird.

## Setting up local environment

1. Make sure you have the following packages installed

```
brew install readline openssl xz
brew install mysql # for Rex only
```

2. Use installer to install pyenv and some of its plugins `curl https://pyenv.run | bash` https://github.com/pyenv/pyenv-installer Pyenv is like rbenv and helps you manage python versions. More here [Managing Multiple Python Versions With pyenv – Real Python](https://realpython.com/intro-to-pyenv/)
3. Append to your `.bash_profile`

```
# Load pyenv automatically by adding
# the following to ~/.bashrc:

export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
```

3. Restart shell
4. Create `~/.env-exporter`

```
# about zlib
export CFLAGS="-I$(xcrun --show-sdk-path)/usr/include"
# about readline
export CFLAGS="-I$(brew --prefix readline)/include $CFLAGS"
export LDFLAGS="-L$(brew --prefix readline)/lib $LDFLAGS"
# about openssl
export CFLAGS="-I$(brew --prefix openssl)/include $CFLAGS"
export LDFLAGS="-L$(brew --prefix openssl)/lib $LDFLAGS"
```

3. Run `source ~/.env-exporter`
4. Run inside project directory `pyenv local 3.7.0`, restart the shell and verify the installation by running `python -V` it should say 3.7.0
5. Create a virtualenv via `pyenv virtualenv 3.7.0 rex`
6. Activate it `pyenv local rex`
7. Now you can install dependencies from requirements.txt `pip install -r requirements.txt`

## Resources

[Managing Multiple Python Versions With pyenv – Real Python](https://realpython.com/intro-to-pyenv/#virtual-environments-and-pyenv)
