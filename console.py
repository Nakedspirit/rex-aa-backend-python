# coding: utf-8
import IPython
from rex_backend.application import app


def console():
    with app.app_context():
        IPython.start_ipython(argv=[])
        print('Done')


if __name__ == '__main__':
    console()
