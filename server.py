import argparse
from rex_backend.application import app
from rex_backend import views


assert views


if __name__ == '__main__':
    ARGS = argparse.ArgumentParser(description="Run http server.")
    ARGS.add_argument(
        '--host', action="store", dest='host',
        default='0.0.0.0', help='Bind address in format: host[:port]')

    args = ARGS.parse_args()
    port = 5000
    if ':' in args.host:
        args.host, port = args.host.split(':', 1)
        port = int(port)

    app.run(args.host, port)
