# REX Python Flask Backend

First time here? Read our [Getting Started Guide](https://github.com/SiliconValleyInsight/rex-aa-backend-python/blob/development/docs/GETTING_STARTED.md).

Do you want to perform a code drop? Check out [Code Drop Guide](https://github.com/SiliconValleyInsight/rex-aa-backend-python/blob/development/docs/CODE_DROP.md).

## Environments

| Development                                                         | Staging | Production                                                       |
| ------------------------------------------------------------------- | ------- | ---------------------------------------------------------------- |
| [Playground](https://qa-rex-gig-mobile-api.rexhomes.com/playground) | [Playground](https://stage-rex-gig-mobile-api.rexhomes.com/playground) | [Playground](https://rex-gig-mobile-api.rexhomes.com/playground) |
| [API Endpoint](https://qa-rex-gig-mobile-api.rexhomes.com/graphql)  | [API Endpoint](https://stage-rex-gig-mobile-api.rexhomes.com/graphql) | [API Endpoint](https://rex-gig-mobile-api.rexhomes.com/graphql)  |

## Tests

To run all tests

`docker-compose run --rm rex_backend py.test`

To run specific test

`docker-compose run --rm rex_backend py.test rex_backend/tests/test_views.py::test_status_handler_200`

## Code linters

`docker-compose run --rm rex_backend bash tools/lint.sh`

## REPL console

`docker-compose run --rm rex_backend python console.py`

## Database migrations

Migrate to latest

`docker-compose run --rm rex_backend alembic upgrade head`

Create new migration (based on ORM models)

`docker-compose run --rm rex_backend alembic revision --autogenerate -m "new migration name"`

More about alembic: https://www.compose.com/articles/schema-migrations-with-alembic-python-and-postgresql/

## GraphQL playground

Define `ENABLE_PLAYGROUND` environment variable to have playground in production.

    http://localhost:5000/playground
