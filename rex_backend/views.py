import json
from rex_backend.application import app
from flask import Response, render_template
from graphene_file_upload.flask import FileUploadGraphQLView

from rex_backend.graphql.schema import rex_schema
from rex_backend.graphql.middlewares import JWTAuthorizationMiddleware, SentryMiddleware


@app.route('/status')
def status_handler() -> str:
    return json.dumps({"status": "ok"})


def playground_handler():
    print(f"HEY HANDLER {app.config['ENABLE_PLAYGROUND']} ARE YOU UPDATING?")
    if app.config['ENABLE_PLAYGROUND']:
        return render_template('playground.html', endpoint='/graphql')
    return Response("Not Found", status=404)


app.add_url_rule(
    '/graphql',
    view_func=FileUploadGraphQLView.as_view(
        'graphql',
        schema=rex_schema,
        graphiql=False,
        middleware=[SentryMiddleware(), JWTAuthorizationMiddleware()]
    )
)
app.add_url_rule('/playground', view_func=playground_handler)
