from datetime import datetime, timedelta


SHOWING_ATTRS = [
    'id',
    'status',
    'data',
    'final_meeting_time',
    'timezone',
    'created_at',
]

ADDITIONAL_SHOWING_ATTRS = [
    'claimed_by_id',
    'claimed_at',
]

OPEN_HOUSE_ATTRS = [
    'open_house_id',
    'open_house_guid',
    'open_house_created_at',
    'open_house_css_user_id',
    'open_house_listing_id',
    'open_house_showing_id',
    'open_house_duration',
]


def assign_attrs_from(from_object, to_object, attr_names, default_value=None):
    for attr_name in attr_names:
        attr_value = default_value

        if hasattr(from_object, attr_name):
            attr_value = getattr(from_object, attr_name)

        setattr(to_object, attr_name, attr_value)


class ExtendedRexShowing:
    __repr_attrs__ = ['id', 'status', 'claimed_by_id', 'open_house_id']

    @classmethod
    def init_from_list(cls, rex_showings):
        return [cls(rex_showing) for rex_showing in rex_showings]

    def __init__(self, rex_showing):
        assign_attrs_from(rex_showing, self, SHOWING_ATTRS)
        assign_attrs_from(rex_showing, self, ADDITIONAL_SHOWING_ATTRS)
        assign_attrs_from(rex_showing, self, OPEN_HOUSE_ATTRS)
        self._update_status()

    def __repr__(self):
        data = {attr_name: getattr(self, attr_name) for attr_name in self.__repr_attrs__}
        return f'{self.__class__}: {data}'

    def _update_status(self):
        self.status = self.status.upper()

        if self.status == 'PENDING' and not self.is_claimed:
            self.status = 'INITIALIZED'

    @property
    def type(self):
        if self.open_house_id:
            return 'OPEN_HOUSE'

        return 'REGULAR'

    @property
    def is_claimed(self):
        return bool(self.claimed_by_id)

    @property
    def rex_listing_id(self):
        rex_listing_id = self.data.get('listingId')
        return int(rex_listing_id) if rex_listing_id else None

    @property
    def rex_listing(self):
        if not hasattr(self, '_rex_listing'):
            self._rex_listing = None
        return self._rex_listing

    def is_claimed_by(self, user):
        return self.is_claimed and int(self.claimed_by_id) in user.rex_users_ids

    def is_claimed_recently(self, days=2):
        if not self.claimed_at:
            return False

        days_to_show = timedelta(days=days)
        difference_between_now_and_when_claimed = datetime.now() - self.claimed_at
        return days_to_show > difference_between_now_and_when_claimed
