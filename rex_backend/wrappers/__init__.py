class BaseWrapper:
    def __init__(self, origin):
        self.origin = origin

    def __repr__(self):
        return (
            f'{self.__class__}: '
            f'{self.origin!r}'
        )

    def __getattr__(self, attr):
        return getattr(self.origin, attr)
