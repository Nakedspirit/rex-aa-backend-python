from abc import ABCMeta, abstractmethod
from datetime import datetime, timedelta

from sqlalchemy import and_, or_

from . import BaseWrapper
from rex_backend.repositories.rex_showing_repo import RexShowingRepo
from rex_backend.repositories.user_action_repo import UserActionRepo
from rex_backend.models.rex_showings import RexBlock, RexBlockAttribute, RexShowing
from rex_backend.models.base import ShowingReport


class BaseUserWrapper(BaseWrapper, metaclass=ABCMeta):
    """
    This is the base for wrappers around User model
    Wrappers extends User model functionality and add a special behavior
    """
    def __init__(self, user):
        super().__init__(user)
        self.current_identity = None
        self.current_device = None

    @property
    @abstractmethod
    def is_demo(self):
        pass

    @property
    @abstractmethod
    def is_ready_to_make_queries(self):
        pass

    @abstractmethod
    def unclaimed_showings_query(self):
        pass

    @abstractmethod
    def upcoming_showings_query(self):
        pass

    @abstractmethod
    def completed_showings_query(self):
        pass

    @abstractmethod
    def gigs_query(self, **kwargs):
        pass

    @abstractmethod
    def showing_query(self, showing_id):
        pass

    @abstractmethod
    def showing_report_query(self, rex_showing_id):
        pass


class Agent(BaseUserWrapper):
    def __init__(self, user, hours_to_show_showings=12):
        super().__init__(user)
        self.hours_to_show_showings = hours_to_show_showings

    @staticmethod
    def validate_rex_users_ids(rex_users_ids):
        if not rex_users_ids or len(rex_users_ids) == 0:
            raise ValueError(f"rex_users_ids can't be empty, rex_users_ids: {rex_users_ids}")

    @property
    def stop_displaying_at(self):
        return datetime.now() - timedelta(hours=self.hours_to_show_showings)

    @property
    def is_demo(self):
        return False

    @property
    def is_ready_to_make_queries(self):
        return bool(self.origin.rex_users_ids)

    def apply_hide_showings_condition(self, query):
        actions = UserActionRepo.load_hide_showing_actions_by_user_id(self.id)
        ids_to_hide = [action.object_id for action in actions]

        if not ids_to_hide:
            return query

        return query.filter(RexShowing.id.notin_(ids_to_hide))

    def unclaimed_showings_query(self):
        self.validate_rex_users_ids(self.rex_users_ids)

        return RexShowingRepo.query_unclaimed_showings_by_rex_users_ids(self.origin.rex_users_ids)

    def upcoming_showings_query(self):
        self.validate_rex_users_ids(self.rex_users_ids)

        if len(self.rex_users_ids) > 1:
            condition = RexBlockAttribute.value.in_([str(id) for id in self.rex_users_ids])
        else:
            condition = RexBlockAttribute.value == str(self.rex_users_ids[0])

        return RexShowingRepo.query_showings().filter(
            or_(
                RexShowing.status.in_(['Pending', 'Scheduled']),
                and_(
                    RexShowing.status == 'Cancelled',
                    RexShowing.created_at > self.stop_displaying_at
                )
            )
        ).filter(condition)

    def completed_showings_query(self):
        self.validate_rex_users_ids(self.rex_users_ids)

        ids = [str(id) for id in self.origin.rex_users_ids]
        return RexShowingRepo.query_completed_showings().filter(RexBlockAttribute.value.in_(ids))

    def gigs_query(self, max_meeting_datetime_of_showing, max_meeting_datetime_of_open_house):
        self.validate_rex_users_ids(self.rex_users_ids)

        ids = [str(id) for id in self.origin.rex_users_ids]
        return RexShowingRepo.query_gigs(
            max_meeting_datetime_of_showing,
            max_meeting_datetime_of_open_house
        ).filter(RexBlockAttribute.value.in_(ids))

    def showing_query(self, showing_id):
        self.validate_rex_users_ids(self.rex_users_ids)
        ids = [str(id) for id in self.origin.rex_users_ids]

        return RexShowingRepo.query_showings() \
            .filter(RexShowing.id == int(showing_id)) \
            .filter(RexBlock.block_type == 'firstcomefirstserve') \
            .filter(RexBlockAttribute.key == 'first_user_id') \
            .filter(RexBlockAttribute.value.in_(ids))

    def showing_report_query(self, rex_showing_id):
        return ShowingReport.query.filter(ShowingReport.rex_showing_id == rex_showing_id) \
                            .filter(ShowingReport.user_id == self.origin.id)


class DemoUser(BaseUserWrapper):
    def __init__(self, user, hours_to_show_showings=12):
        super().__init__(user)
        self.hours_to_show_showings = hours_to_show_showings

    @property
    def rex_users_ids(self):
        return ()

    @property
    def stop_displaying_at(self):
        return datetime.now() - timedelta(hours=self.hours_to_show_showings)

    @property
    def is_demo(self):
        return True

    @property
    def is_ready_to_make_queries(self):
        return True

    def apply_hide_showings_condition(self, query):
        actions = UserActionRepo.load_hide_showing_actions_by_user_id(self.id)
        ids_to_hide = [action.object_id for action in actions]

        if not ids_to_hide:
            return query

        return query.filter(RexShowing.id.notin_(ids_to_hide))

    def unclaimed_showings_query(self):
        query = RexShowingRepo.rex_unclaimed_showings_query() \
                              .filter(RexBlockAttribute.value == None) \
                              .filter(RexShowing.created_at > self.stop_displaying_at) # noqa E711

        query_failed = RexShowingRepo.rex_failed_or_claimed_showings_query() \
                                     .filter(RexShowing.created_at > self.stop_displaying_at)
        return query.union_all(query_failed)

    def upcoming_showings_query(self):
        return RexShowingRepo.rex_upcoming_showings_query().filter(RexShowing.created_at > self.stop_displaying_at)

    def completed_showings_query(self):
        data = ShowingReport.query.with_entities(
            ShowingReport.rex_showing_id,
        ).filter(ShowingReport.is_questions_completed == True).all()  # noqa: E712
        completed_ids = [record.rex_showing_id for record in data]

        query = RexShowingRepo.query_completed_showings()

        if len(completed_ids) > 0:
            query = query.filter(
                RexShowing.id.notin_(completed_ids)
            )

        return query

    def gigs_query(self, max_meeting_datetime_of_showing, max_meeting_datetime_of_open_house):
        return RexShowingRepo.query_gigs(
            max_meeting_datetime_of_showing,
            max_meeting_datetime_of_open_house
        )

    def showing_query(self, showing_id):
        return RexShowingRepo.rex_showing_query(showing_id)

    def showing_report_query(self, rex_showing_id):
        return ShowingReport.query.filter(ShowingReport.rex_showing_id == rex_showing_id)
