import re
import types
import functools
import operator
from collections import defaultdict


from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError


EMAIL_REGEXP = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"


class SimpleObject(object):
    '''
    Just an object that holds attributes
    You can assign any attrs
    You're allowed to modify attrs
    '''
    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

    def __repr__(self):
        return f'{self.__class__}: {self.__dict__}'


def is_valid_email(email):
    if not email:
        return False

    email = str(email)
    return bool(re.match(EMAIL_REGEXP, email))


def flat_list(l):
    return functools.reduce(operator.iconcat, l, [])


def copy_attrs_to_simple_object(obj):
    internal_attr_regexp = re.compile('^(_{1,2}\w+)|(\w+_{1,2})$')  # noqa: W605
    attrs_dict = {}
    for attr_name in dir(obj):
        if not internal_attr_regexp.match(attr_name):
            attr_value = getattr(obj, attr_name)
            if not isinstance(attr_value, types.MethodType):
                attrs_dict[attr_name] = attr_value

    return SimpleObject(**attrs_dict)


def to_dict_by_attr(objs, attr_name):
    result = defaultdict(list)
    for obj in objs:
        needed_attr = getattr(obj, attr_name)
        result[needed_attr].append(obj)
    return result


def get_name_from_database_url(database_url):
    splitted_database_url = database_url.split('/')
    return splitted_database_url[-1]


def change_name_for_database_url(database_url, database_name):
    splitted_database_url = database_url.split('/')
    splitted_database_url[-1] = database_name
    return '/'.join(splitted_database_url)


def env_var_to_bool(value):
    if value is None:
        return False

    return str(value).lower() in ('t', '1', 'true')


def env_to_list(value):
    return [item.strip() for item in (value or '').split(',') if item]


def camel_to_snake(s):
    return re.sub("([A-Z])", "_\\1", s).lower().lstrip("_")


def snake_to_camel(s):
    parts = s.lower().split('_')
    return ''.join([part.title() for part in parts])


def make_connection_string(driver: str, user: str, password: str, host: str, port: str, database: str):
    url = '{driver}://{user}:{password}@{host}:{port}/{database}'.format(
        driver=driver,
        user=user,
        password=password,
        host=host,
        port=port,
        database=database)
    return url


def make_rex_showing_address(rex_showing):
    address_points = [
        rex_showing.data.get('streetAddress'),
        rex_showing.data.get('city'),
        rex_showing.data.get('state')
    ]
    filtered_address = [p for p in address_points if p]
    return ', '.join(filtered_address)


def create_database_if_not_exists(database_url, default_database_name='postgres'):
    engine = create_engine(database_url)
    default_database_url = change_name_for_database_url(database_url, default_database_name)
    database_name = get_name_from_database_url(database_url)

    # Create db if not exists
    try:
        conn = engine.connect()
    except OperationalError as exc:
        expected_err_msg = f'database "{database_name}" does not exist'
        if expected_err_msg not in exc._message():
            raise exc
        engine = create_engine(default_database_url)
        conn = engine.connect()
        conn.execute('commit')
        conn.execute(f"create database {database_name}")
    finally:
        conn.close()
