from .interactor import Interactor
from rex_backend.models.base import ReportGroup, ReportGroupStatus, ShowingReport


class CreateGroupsForShowingReport(Interactor):
    METHODS_TO_RUN = [
        'load',
        'delete',
        'create',
    ]
    REQUIRED_ARGS = [
        'showing_report_id',
        'groups_titles',
    ]

    def load(self, **kwargs):
        showing_report_id = kwargs.get('showing_report_id')
        groups = ReportGroup.query.filter(ReportGroup.showing_report_id == showing_report_id).all()
        self.context.groups = groups

    def delete(self, showing_report_id, groups_titles):
        groups = self.context.groups

        if len(groups):
            session = ReportGroup.query.session
            for group in groups:
                session.delete(group)
            session.commit()

    def create(self, showing_report_id, groups_titles):
        status = ReportGroupStatus.initialized

        showing_report = ShowingReport.query.filter(ShowingReport.id == showing_report_id).first()
        if showing_report.is_questions_completed:
            status = ReportGroupStatus.completed

        new_groups = [ReportGroup(title=title, showing_report_id=showing_report_id, status=status)
                      for title in groups_titles]

        session = ReportGroup.query.session
        for group in new_groups:
            session.add(group)
        session.commit()

        return new_groups
