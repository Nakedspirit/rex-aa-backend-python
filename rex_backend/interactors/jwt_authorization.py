import logging

from rex_backend.interactors.interactor import Interactor
from rex_backend.application import app, jwt_service
from rex_backend.exceptions import AuthException
from rex_backend.models.base import User, GoogleIdentity, Device
from rex_backend.wrappers.user import Agent, DemoUser


logger = logging.getLogger('rex_backend')


class JwtAuthorization(Interactor):
    DISABLED_MUTATIONS = [
        "claimShowing",
        "optOutShowing",
        "cancelShowing",
        "finishShowing",
        "submitReport",
        "submitForPayment",
        "answerReportQuestions",
        "completeCallBuyer",
        "answerVisitorQuestions",
        "uploadOpenHousePhotos",
        "addVisitor",
        "answerReportGroupQuestions",
        "answerReportQuestions",
    ]
    METHODS_TO_RUN = [
        'extract_jwt_payload',
        'check_restrictions',
        'load_device',
        'load_user'
    ]

    def extract_jwt_payload(self, **kwargs):
        self.context.jwt = kwargs.get('jwt')
        self.context.field_name = kwargs.get('field_name')

        if not self.context.jwt:
            raise AuthException("JWT payload is invalid")

        try:
            self.context.jwt_payload = jwt_service.decode(self.context.jwt)
        except Exception:
            raise AuthException("JWT payload is invalid")

        if not jwt_service.is_valid_payload(self.context.jwt_payload):
            raise AuthException("JWT payload is invalid")

    def check_restrictions(self, **kwargs):
        if self.context.jwt_payload.get("demo_mode") and self.context.field_name in self.DISABLED_MUTATIONS:
            raise AuthException("Demo Mode does not have permission for this action.")

        if self.context.jwt_payload.get("disable_mutations") and self.context.field_name in self.DISABLED_MUTATIONS:
            raise AuthException("You don't have permission for this action. \nContact REX operations.")

    def load_device(self, **kwargs):
        self.context.user_id = self.context.jwt_payload.get("user_id")
        self.context.device_id = self.context.jwt_payload.get('device_id')
        self.context.identity_id = self.context.jwt_payload.get("identity_id")
        self.context.is_demo_mode = self.context.jwt_payload.get("demo_mode")

        device = Device.query.filter(Device.id == self.context.device_id).first()
        if not device or device.user_id != self.context.user_id:
            raise AuthException("Invalid device")

        self.context.device = device

    def load_user(self, **kwargs):
        user = User.query.filter(User.id == self.context.user_id).first()

        if self.context.is_demo_mode:
            hours_to_show = int(app.config['HOURS_TO_SHOW_SHOWINGS_IN_DEMO_MODE'])
            user = DemoUser(user, hours_to_show_showings=hours_to_show)
            logger.info(
                'Initialized `hours_to_show_showings_in_demo_mode` for DemoUser'
                f' with value: {user.hours_to_show_showings}'
            )
        else:
            hours_to_show = int(app.config['DAYS_TO_SHOW_ALREADY_CLAIMED']) * 24
            user = Agent(user, hours_to_show_showings=hours_to_show)

        user = user
        identity = GoogleIdentity.query.filter(GoogleIdentity.id == self.context.identity_id).first()

        if not (user and identity):
            raise AuthException("Can't find user")

        user.current_identity = identity
        user.current_device = self.context.device

        return {'user': user, 'is_demo_mode': self.context.is_demo_mode}
