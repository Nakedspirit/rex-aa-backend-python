import logging


from .interactor import Interactor
from rex_backend.exceptions import NotFoundException
from rex_backend.models.base import ReportAnswer, ReportQuestion
from rex_backend.models.rex_listings import RexListing
from rex_backend.apis.rex_questions import rex_questions_api


logger = logging.getLogger('rex_backend')


class SendAnswersToRex(Interactor):
    METHODS_TO_RUN = [
        'load_showing',
        'load_listing',
        'load_answers',
        'send_to_api'
    ]
    REQUIRED_ARGS = [
        'current_user',
        'showing_report',
    ]

    def check_showing_report(self, **kwargs):
        showing_report = kwargs.get('showing_report')

        if not showing_report.is_questions_completed:
            logger.info(f"showing_report({showing_report.id}) is not finished yet, won't send answers to API")
            self.finish(False)

    def load_showing(self, **kwargs):
        current_user = kwargs.get('current_user')
        showing_report = kwargs.get('showing_report')

        showing = current_user.showing_query(showing_report.rex_showing_id).first()
        if not showing:
            raise NotFoundException('Showing', 'Showing not found')

        self.context.showing = showing

    def load_listing(self, **kwargs):
        showing = self.context.showing

        listing = RexListing.query.filter(RexListing.id == showing.data.get('listingId')).first()
        if not listing:
            raise NotFoundException('Listing', 'Listing not found')

        self.context.listing = listing

    def load_answers(self, **kwargs):
        showing_report = kwargs.get('showing_report')

        answers = ReportAnswer.query.filter(ReportAnswer.showing_report_id == showing_report.id).all()
        if not answers:
            raise NotFoundException('Answers', 'Answers not found')

        guids = [answer.rex_question_guid for answer in answers]
        questions = ReportQuestion.query.filter(ReportQuestion.rex_question_guid.in_(guids)).all()

        self.context.answers = answers
        self.context.questions = questions

    def send_to_api(self, **kwargs):
        current_user = kwargs.get('current_user')
        showing = self.context.showing
        listing = self.context.listing

        questions_dict = dict([(question.rex_question_guid, question) for question in self.context.questions])

        answers_to_send = {}
        for answer in self.context.answers:
            guid = answer.rex_question_guid
            question = questions_dict.get(guid)
            if not question:
                continue

            question_type = question.original_data.get('type')
            answer_to_send = answer.values

            if question_type != 'checkbox':
                answer_to_send = answer_to_send[0] if len(answer_to_send) > 0 else None

            if answer_to_send:
                answers_to_send[guid] = answer_to_send

        data = {
            'listing_guid': listing.guid,
            'recorded_by': current_user.contact_email,
            'replace': True,  # always delete previous answers
            'source': str(showing.id),
            'values': answers_to_send
        }
        result = rex_questions_api.call_add_answers(json=data)
        is_success = result.get('content', {}).get('result', False)
        logger.info(f"Answers sent to API, result: {result}")

        return is_success
