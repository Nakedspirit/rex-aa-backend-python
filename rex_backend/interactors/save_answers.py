from .interactor import Interactor
from rex_backend.models.base import ReportAnswer


class SaveAnswers(Interactor):
    METHODS_TO_RUN = [
        'extract_questions_guids',
        'delete_prev_answers',
        'save_new_answers'
    ]
    REQUIRED_ARGS = [
        'showing_report',
        'answers_data',
    ]

    def extract_questions_guids(self, **kwargs):
        answers_data = kwargs.get('answers_data')
        questions_guids = [answer_data.get('question_id') for answer_data in answers_data]
        assert questions_guids
        self.context.questions_guids = questions_guids

    def delete_prev_answers(self, **kwargs):
        showing_report = kwargs.get('showing_report')
        questions_guids = self.context.questions_guids

        query = ReportAnswer.query.filter(ReportAnswer.showing_report_id == showing_report.id)
        if len(questions_guids) > 1:
            query = query.filter(ReportAnswer.rex_question_guid.in_(questions_guids))
        else:
            query = query.filter(ReportAnswer.rex_question_guid == questions_guids[0])

        query.delete(synchronize_session='fetch')
        query.session.commit()

    def save_new_answers(self, **kwargs):
        showing_report = kwargs.get('showing_report')
        answers_data = kwargs.get('answers_data')

        new_answers = []
        session = ReportAnswer.query.session

        for answer_data in answers_data:
            answer = ReportAnswer(
                showing_report=showing_report,
                rex_question_guid=answer_data.get('question_id'),
                data={'values': answer_data.get('answers')}
            )
            new_answers.append(answer)
            session.add(answer)

        session.commit()

        return new_answers
