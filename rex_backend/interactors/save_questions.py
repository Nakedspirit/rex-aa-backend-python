from .interactor import Interactor
from rex_backend.models.base import ReportQuestion
from rex_backend.application import db


class SaveQuestions(Interactor):
    METHODS_TO_RUN = [
        'load_questions',
        'make_dict_from_questions_by_guid',
        'update_or_create_questions'
    ]
    REQUIRED_ARGS = [
        'rex_questions',
    ]

    def load_questions(self, rex_questions):
        query = ReportQuestion.query

        questions_guids = [rex_question.guid for rex_question in rex_questions]
        if len(questions_guids) > 1:
            query = query.filter(ReportQuestion.rex_question_guid.in_(questions_guids))
        else:
            guid = questions_guids[0]
            query = query.filter(ReportQuestion.rex_question_guid == guid)

        questions = query.all()
        self.context.questions = questions

    def make_dict_from_questions_by_guid(self, rex_questions):
        questions = self.context.questions
        questions_dict = {}

        for question in questions:
            questions_dict[question.rex_question_guid] = question

        self.context.questions_dict = questions_dict

    def update_or_create_questions(self, rex_questions):
        questions_dict = self.context.questions_dict
        result_questions = []

        for rex_question in rex_questions:
            question = questions_dict.get(rex_question.guid)
            if not question:
                question = ReportQuestion(
                    rex_question_guid=rex_question.guid
                )

            question.title = rex_question.title
            question.data = {
                'type': rex_question.type,
                'is_required': rex_question.is_required,
                'options': rex_question.options,
                'original_data': rex_question.original_data,
            }
            result_questions.append(question)
            db.session.add(question)

        db.session.commit()

        return result_questions
