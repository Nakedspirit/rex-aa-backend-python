from datetime import timedelta


from rex_backend.exceptions import RestAPIWrongResponseException
from rex_backend.interactors.interactor import Interactor
from rex_backend.apis.rex_bonobono import rex_bonobono_api


stat_name_to_rex_api_stat = {
    'COMPLETED_SHOWINGS_AMOUNT': 'numShowingsTotal',
    'ESCROW_SHOWINGS_AMOUNT': 'numShowingsEscrow'
}


class Leaderboard(Interactor):
    METHODS_TO_RUN = [
        'initialize',
        'load_data_for_current_period',
        'load_data_for_prev_period',
        'count_positions',
        'to_app_format',
        'mark_current_user_stat'
    ]

    def initialize(self, **kwargs):
        self.context.stat_name = kwargs.get('stat_name')
        self.context.rex_api_stat_name = stat_name_to_rex_api_stat.get(self.context.stat_name)

    def load_data_for_current_period(self, **kwargs):
        current_month_result = rex_bonobono_api.call_get_leaderboard_stats(params={
            'sort_by': self.context.rex_api_stat_name,
            'date_start': kwargs.get('date_start'),
            'date_end': kwargs.get('date_end')
        })

        content = current_month_result.get('content')
        if not content:
            self.finish([])

        errors = content.get('error')
        if errors:
            raise RestAPIWrongResponseException(errors)

        self.context.current_month_agents_stats = content.get('agents')

    def load_data_for_prev_period(self, **kwargs):
        days_in_period = (kwargs.get('date_end') - kwargs.get('date_start')).days
        end_of_the_prev_period = kwargs.get('date_start') - timedelta(days=1)
        start_of_the_prev_period = end_of_the_prev_period - timedelta(days=days_in_period - 1)

        prev_month_result = rex_bonobono_api.call_get_leaderboard_stats(params={
            'sort_by': self.context.rex_api_stat_name,
            'date_start': start_of_the_prev_period,
            'date_end': end_of_the_prev_period,
        })
        self.context.prev_month_agents_stats = prev_month_result.get('content', {}).get('agents', [])

    def count_positions(self, **kwargs):
        self.context.current_month_agents_stats = self._count_positions_for_stats(
            self.context.current_month_agents_stats,
            self.context.rex_api_stat_name
        )
        self.context.prev_month_agents_stats = self._count_positions_for_stats(
            self.context.prev_month_agents_stats,
            self.context.rex_api_stat_name
        )

    def to_app_format(self, **kwargs):
        stat_name = self.context.stat_name
        rex_api_stat_name = self.context.rex_api_stat_name

        current_month_agents_stats = self.context.current_month_agents_stats
        prev_month_agents_stats_dict = dict([(stat.get('id'), stat) for stat in self.context.prev_month_agents_stats])

        def format_obj(rex_user_stat):
            id = rex_user_stat.get('id')
            current_position = rex_user_stat.get('position')
            prev_position = prev_month_agents_stats_dict.get(id, {}).get('position')
            return {
                'is_current_user': False,
                'rex_user_id': id,
                'user_name': rex_user_stat.get('name'),
                'name': stat_name,
                'value': rex_user_stat.get(rex_api_stat_name),
                'position': rex_user_stat.get('position'),
                'position_change': prev_position - current_position if prev_position else 0
            }

        stats = list(map(format_obj, current_month_agents_stats))
        self.context.stats = stats
        return stats

    def mark_current_user_stat(self, **kwargs):
        current_user = kwargs.get('current_user')
        stats = self.context.stats

        if not current_user:
            return stats

        for stat in stats:
            if int(stat.get('rex_user_id')) in current_user.rex_users_ids:
                stat['is_current_user'] = True
                break

        return stats

    def _count_positions_for_stats(self, rex_users_stats, rex_user_stat_name):
        if len(rex_users_stats) == 0:
            return []

        positions = {}
        for rex_user_stat in rex_users_stats:
            value = rex_user_stat.get(rex_user_stat_name)
            if positions.get(value):
                positions[value].append(rex_user_stat)
            else:
                positions[value] = [rex_user_stat]

        result = []
        positions = sorted(positions.items(), key=lambda position: position[0], reverse=True)
        for index, position in enumerate(positions):
            elems = position[1]
            for elem in elems:
                elem['position'] = index + 1
            result += elems

        return result
