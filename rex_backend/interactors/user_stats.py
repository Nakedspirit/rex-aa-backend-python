from datetime import datetime, timedelta
from rex_backend.interactors.interactor import Interactor


from rex_backend.models.base import UserStat
from rex_backend.repositories.user_stat_repo import UserStatRepo


class UserStatsInteractor(Interactor):
    METHODS_TO_RUN = [
        'count_stats_for_today',
        'count_position_for_user_stat'
    ]

    def count_stats_for_today(self, **kwargs):
        self.context.stat_name = kwargs.get('stat_name')
        today = datetime.date(datetime.utcnow())
        start_of_the_current_month = today - timedelta(days=today.day - 1)
        agregated_user_stats_query = UserStatRepo.query_user_stats_for_period(
            self.context.stat_name,
            start_of_the_current_month,
            today
        )

        current_user_id = kwargs.get('current_user_id')
        if current_user_id:
            agregated_user_stats_query = agregated_user_stats_query.filter(UserStat.user_id == current_user_id)

        agregated_user_stats = agregated_user_stats_query.limit(3).all()

        self.context.today = today
        self.context.start_of_the_current_month = start_of_the_current_month
        self.context.agregated_user_stats = agregated_user_stats

    def count_position_for_user_stat(self, **kwargs):
        end_of_the_prev_month = self.context.start_of_the_current_month - timedelta(days=1)
        start_of_the_prev_month = end_of_the_prev_month - timedelta(days=end_of_the_prev_month.day - 1)

        complete_user_stats = []
        for user_stat in self.context.agregated_user_stats:
            current_position = UserStatRepo.count_user_position_in_period(
                user_stat.user_id,
                self.context.stat_name,
                self.context.start_of_the_current_month,
                self.context.today
            )
            prev_month_position = UserStatRepo.count_user_position_in_period(
                user_stat.user_id,
                self.context.stat_name,
                start_of_the_prev_month,
                end_of_the_prev_month
            )
            complete_user_stats.append({
                'user_id': user_stat.user_id,
                'value': user_stat.value,
                'position': current_position,
                'position_change': prev_month_position - current_position if prev_month_position else None
            })

        return complete_user_stats
