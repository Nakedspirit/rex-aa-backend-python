import logging


logger = logging.getLogger('rex_backend')


class InteractorException(Exception):
    pass


class NoMethodsToRunException(InteractorException):
    pass


class ArgumentNotFoundException(InteractorException):
    pass


class InteractorFinishException(InteractorException):
    def __init__(self, to_return):
        self.to_return = to_return


class Context(object):
    pass


class Interactor:
    METHODS_TO_RUN = []
    REQUIRED_ARGS = []

    def __init__(self, need_logs=False):
        self.context = Context()
        self.need_logs = need_logs

    def __call__(self, **kwargs):
        return self.call(**kwargs)

    def call(self, **kwargs):
        self._check_methods_to_run()
        self._check_arguments(**kwargs)

        result = None
        for index, method_name in enumerate(self.METHODS_TO_RUN, start=1):
            func_to_run = getattr(self, method_name)

            try:
                result = func_to_run(**kwargs)
            except InteractorFinishException as exc:
                logger.warn(
                    f'"{self._class_name}" execution was finished'
                    f' in "{method_name}" method'
                    f' and returns "{exc.to_return!r}"'
                )
                return exc.to_return

            if self.need_logs:
                msg = self._make_log_message(index, method_name, result)
                logger.info(msg)

        return result

    def finish(self, to_return=None):
        raise InteractorFinishException(to_return)

    @property
    def _class_name(self):
        return (str(self.__class__).split('.')[-1])[0:-2]

    def _check_methods_to_run(self):
        if len(self.METHODS_TO_RUN) == 0:
            raise NoMethodsToRunException(f'no methods to run for {self.__class__}')

    def _check_arguments(self, **kwargs):
        if len(self.REQUIRED_ARGS) == 0:
            return

        for required_arg_name in self.REQUIRED_ARGS:
            arg_value = kwargs.get(required_arg_name)
            if arg_value is None:
                raise ArgumentNotFoundException(f'{required_arg_name} is required, please pass the arg to the interactor')

    def _make_log_message(self, index, method_name, result):
        return f'\nCLASS: {self.__class__}\nSTEP: {index} | {method_name}\nCONTEXT: {self.context.__dict__}\nRETURNS: {type(result)} | {result}\n'
