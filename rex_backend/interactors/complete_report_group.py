from .interactor import Interactor
from rex_backend.models.base import ShowingReport, ReportGroup, ReportGroupStatus


class CompleteReportGroup(Interactor):
    METHODS_TO_RUN = [
        'status_to_complete',
        'count_completed_groups',
        'count_all_groups',
        'update_showing_report',
    ]
    REQUIRED_ARGS = [
        'report_group',
    ]

    def status_to_complete(self, **kwargs):
        group = kwargs.get('report_group')
        group.status = ReportGroupStatus.completed
        ReportGroup.query.session.add(group)
        ReportGroup.query.session.commit()
        self.context.group = group

    def count_completed_groups(self, **kwargs):
        showing_report_id = self.context.group.showing_report_id
        self.context.completed_groups_num = ReportGroup.query \
                                                       .filter(ReportGroup.showing_report_id == showing_report_id) \
                                                       .filter(ReportGroup.status == ReportGroupStatus.completed) \
                                                       .count()

    def count_all_groups(self, **kwargs):
        showing_report_id = self.context.group.showing_report_id
        self.context.all_groups_num = ReportGroup.query.filter(ReportGroup.showing_report_id == showing_report_id).count()

    def update_showing_report(self, **kwargs):
        if self.context.completed_groups_num != self.context.all_groups_num:
            return

        group = self.context.group
        report = group.showing_report
        report.is_questions_completed = True
        ShowingReport.query.session.add(report)
        ShowingReport.query.session.commit()

        return group
