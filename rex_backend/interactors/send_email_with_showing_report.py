import logging

import jinja2

from rex_backend.helpers import to_dict_by_attr
from .interactor import Interactor
from rex_backend.models.base import ReportAnswer, ReportQuestion
from rex_backend.helpers import make_rex_showing_address
from rex_backend.services import aws_ses_service, rest_api_service
from rex_backend.application import app
from rex_backend.models.rex_listings import RexListing
from rex_backend.exceptions import NotFoundException
from rex_backend.repositories.rex_listing_repo import RexListingRepo
from rex_backend.utils.email_message import EmailMessage
from rex_backend.utils.rest_api import RestAPIException


logger = logging.getLogger('rex_backend')

EMAILS_TO_IGNORE_AVOID_REAL_USERS_EMAILS = ['ascheglov@rexhomes.com', 'jys@rexhomes.com', 'jys+1@rexhomes.com']

IGNORE_QUESTIONS_NAMES_FOR_SELLER = [
    "ps_aa_pre_qualified",
    "ps_aa_selling",
    "ps_aa_make_offer",
    "ps_aa_visted_homes",
    "ps_aa_time_shopping",
    "ps_aa_time_purchase",
    "ps_aa_interest_level",
    "ps_aa_services",
    "ohl_pre_qualified",
    "ohl_selling",
    "ohl_make_offer",
    "ohl_visted_homes",
    "ohl_time_shopping",
    "ohl_time_purchase",
    "ohl_interest_level",
    "ohl_aa_services",
]


class SendEmailWithShowingReport(Interactor):
    METHODS_TO_RUN = [
        'check_is_showing_report_ready_for_send',
        'load_questions_and_answers',
        'combine_questions_and_answers',
        'make_content_for_email',
        'load_rex_listing',
        'load_recipient_emails',
        'make_email_message_for_css',
        'send_email_message_to_css',
        'make_email_message_for_seller',
        'send_email_message_to_seller',
    ]
    REQUIRED_ARGS = [
        'showing_report',
    ]

    def check_is_showing_report_ready_for_send(self, showing_report):
        logger.info(f'Start sending email for Showing Report - {showing_report}')
        if not showing_report.is_questions_completed:
            logger.info('STOP, showing_report questions is not completed')
            self.finish()

    def load_questions_and_answers(self, showing_report):
        rex_showing_id = showing_report.rex_showing_id
        email = showing_report.user.contact_email

        answers = ReportAnswer.query.filter(ReportAnswer.showing_report_id == showing_report.id).all()
        if len(answers) == 0:
            logger.info(f'STOP, answers not found')
            self.finish()

        answers_dict = to_dict_by_attr(answers, 'rex_question_guid')
        questions = ReportQuestion.query.filter(ReportQuestion.rex_question_guid.in_(answers_dict.keys())).all()

        if len(questions) == 0:
            logger.info(f'STOP, questions not found')
            self.finish()

        self.context.answers_dict = answers_dict
        self.context.questions = questions

    def combine_questions_and_answers(self, **kwargs):
        for question in self.context.questions:
            answers = self.context.answers_dict.get(question.rex_question_guid) or []
            answer = answers[0]
            if len(question.options):
                for option in question.options:
                    if option.value in answer.values:
                        option.selected = True
            else:
                question.answer_value = answer.values[0]

    def make_content_for_email(self, showing_report):
        user = showing_report.user
        rex_showing = showing_report.rex_showing
        showing_type_name = 'Open House' if rex_showing.open_house_id else 'Showing'

        address = make_rex_showing_address(rex_showing)
        meeting_time = f'{rex_showing.final_meeting_time} {rex_showing.timezone}'

        html_for_mail = jinja2.Template(
            open('rex_backend/templates/mail/showing_report.html').read()
        ).render(
            showing_type_name=showing_type_name,
            user=user,
            address=address,
            meeting_time=meeting_time,
            questions=self.context.questions
        )

        self.context.subject = f'{showing_type_name} Feedback for {address} at {meeting_time}. Agent {user.fullname} contact is {user.national_phone_number}'
        self.context.html_for_mail = html_for_mail

    def load_rex_listing(self, showing_report):
        showing = showing_report.rex_showing
        listing_id = showing.data.get('listingId')

        listing = RexListing.query.filter(RexListing.id == listing_id).first()
        if not listing:
            raise NotFoundException('Listing', 'Listing not found')

        logger.info(f'Loaded listing {listing}')
        self.context.listing = listing

    def load_recipient_emails(self, showing_report):
        listing_guid = self.context.listing.guid
        url = app.config['REX_SVC_RESPONSIBILITIES_URL']
        token = app.config['REX_SVC_RESPONSIBILITIES_INTERNAL_TOKEN']
        try:
            json_response = rest_api_service.get(
                url,
                params={'listingId': listing_guid},
                headers={'X-Admin-Rex-Token': token}
            )
            logger.info(f'Loaded data with CSS, response - {json_response}')
        except RestAPIException as e:
            json_response = {}
            logger.error(f'REX Responsibilities API returned: {e}')

        team = json_response.get('content', {}).get('teams', {}).get(listing_guid, [])
        recipient_dict = {}
        listing_agent_dict = {}
        for member in team:
            role = member.get('responsibility')
            if role == 'CSS':
                recipient_dict = member
                continue
            elif role in ('LA', 'LISTING_AGENT'):
                listing_agent_dict = member
                continue

        logger.info(f'Extracted CSS: {recipient_dict} and LISTING AGENT: {listing_agent_dict}')
        emails = [
            recipient_dict.get('email', None),
            listing_agent_dict.get('email', None)
        ]
        self.context.emails = [email for email in emails if email]

    def make_email_message_for_css(self, showing_report):
        emails = self.context.emails

        if app.config['AVOID_REAL_USERS_EMAILS']:
            logger.info(f'AVOID_REAL_USERS_EMAILS is True')
            emails = [e for e in emails if e in EMAILS_TO_IGNORE_AVOID_REAL_USERS_EMAILS]

        email_message = EmailMessage(
            recipients=emails,
            subject=self.context.subject,
            html=self.context.html_for_mail,
            default_recipients=[app.config['DEFAULT_RECEIVER']],
            bcc=app.config['EMAILS_BCC']
        )
        print(f'Created message for CSS, message - {email_message}')

        self.context.email_message_for_css = email_message

    def send_email_message_to_css(self, showing_report):
        result = aws_ses_service.send_email(self.context.email_message_for_css)
        logger.info(f'Sent message to CSS, result - {result.response}')

    def make_email_message_for_seller(self, showing_report):
        user = showing_report.user
        rex_listing = self.context.listing
        rex_showing = showing_report.rex_showing
        showing_type_name = 'Open House' if rex_showing.open_house_id else 'Showing'

        seller = RexListingRepo.load_seller(rex_listing.id)
        logger.info(f'Loaded SELLER - {seller}')

        email = None
        if seller and seller.email:
            email = seller.email

        if app.config['AVOID_REAL_USERS_EMAILS'] and (email not in EMAILS_TO_IGNORE_AVOID_REAL_USERS_EMAILS):
            logger.info('AVOID_REAL_USERS_EMAILS is True')
            email = None

        logger.info(f'SELLER email - {email}')

        address = make_rex_showing_address(rex_showing)
        meeting_time = f'{rex_showing.final_meeting_time} {rex_showing.timezone}'
        subject = f'{showing_type_name} Feedback for {address} at {meeting_time}.'

        questions = [q for q in self.context.questions if q.options]
        questions = [question
                     for question in questions
                     if question.original_data.get('name') not in IGNORE_QUESTIONS_NAMES_FOR_SELLER]

        html_for_mail = jinja2.Template(
            open('rex_backend/templates/mail/showing_report.html').read()
        ).render(
            showing_type_name=showing_type_name,
            user=user,
            address=address,
            meeting_time=meeting_time,
            questions=questions
        )

        email_message = EmailMessage(
            recipients=email,
            subject=subject,
            html=html_for_mail,
            default_recipients=[app.config['DEFAULT_RECEIVER']],
            bcc=app.config['EMAILS_BCC']
        )
        logger.info(f'Created message for SELLER, message - {email_message}')

        self.context.email_message_for_seller = email_message

    def send_email_message_to_seller(self, showing_report):
        result = aws_ses_service.send_email(self.context.email_message_for_seller)
        logger.info(f'Sent message to SELLER, result - {result.response}')
        return result
