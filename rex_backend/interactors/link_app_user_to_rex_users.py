from rex_backend.interactors.interactor import Interactor


from rex_backend.models.base import LinkToRexUser
from rex_backend.repositories.rex_user_repo import RexUserRepo
from rex_backend.application import db


class LinkUsersInteractor(Interactor):
    METHODS_TO_RUN = [
        'load_rex_users',
        'load_existed_links',
        'filter_rex_users',
        'link_app_user_to_rex_users',
    ]
    REQUIRED_ARGS = [
        'user',
        'phone_number',
        'email',
    ]

    def load_rex_users(self, **kwargs):
        all_rex_users = RexUserRepo.load_rex_users_by_phone_number_or_email(
            kwargs.get('phone_number'),
            kwargs.get('email')
        )
        if len(all_rex_users) == 0:
            self.finish()

        self.context.all_rex_users = all_rex_users

    def load_existed_links(self, **kwargs):
        rex_users_ids = [rex_user.id for rex_user in self.context.all_rex_users]
        if len(rex_users_ids) == 1:
            rex_user_id = rex_users_ids[0]
            condition = LinkToRexUser.rex_user_id == rex_user_id
        else:
            condition = LinkToRexUser.rex_user_id.in_(rex_users_ids)

        existing_links = LinkToRexUser.query.filter(condition).all()
        self.context.existing_links = existing_links

    def filter_rex_users(self, **kwargs):
        rex_users = self.context.all_rex_users
        already_linked_ids = [link.rex_user_id for link in self.context.existing_links]

        for rex_user in list(rex_users):
            if rex_user.id in already_linked_ids:
                rex_users.remove(rex_user)

        self.context.rex_users_to_link = rex_users

    def link_app_user_to_rex_users(self, **kwargs):
        current_user = kwargs.get('user')
        rex_users_to_link = self.context.rex_users_to_link

        if len(rex_users_to_link) == 0:
            self.finish(current_user.links_to_rex_users)

        new_links = []
        for rex_user in rex_users_to_link:
            new_link = LinkToRexUser(
                rex_user_id=rex_user.id,
                user_id=current_user.id,
                email=rex_user.email_address,
                phone_number=rex_user.phone_number
            )
            new_links.append(new_link)
            db.session.add(new_link)

        if len(new_links) > 0:
            db.session.commit()

        return current_user.links_to_rex_users + new_links
