import logging
import json
from sqlalchemy import text


from rex_backend.interactors.interactor import Interactor
from rex_backend.interactors.link_app_user_to_rex_users import LinkUsersInteractor
from rex_backend.application import app, google_table_service, google_oauth_service, db, jwt_service
from rex_backend.models.base import User, GoogleIdentity, Device

from rex_backend.models.rex_showings import RexAssociatedAgent
from rex_backend.exceptions import NotFoundRexAssociatedAgentException
from rex_backend.utils.tz import tznow
from rex_backend.utils.developer_emails import get_developer_emails


logger = logging.Logger(__name__)


class SignInOauthInteractor(Interactor):
    METHODS_TO_RUN = [
        'make_identity_payload',
        'check_oauth_tokens',
        'check_email_in_whitelist',
        'load_rex_agent',
        'load_app_user_and_identity',
        'update_app_user_and_identity',
        'update_or_create_device',
        'link_app_user_to_rex_users',
        'make_jwt',
        'prepare_response'
    ]

    def make_identity_payload(self, **kwargs):
        self.context.identity_payload = {
            'oauth_user_id': kwargs.get('oauth_user_id'),
            'oauth_access_token': kwargs.get('oauth_access_token'),
            'oauth_id_token': kwargs.get('oauth_id_token'),
            'expiration_date': kwargs.get('expiration_date')
        }

    def check_oauth_tokens(self, **kwargs):
        identity_payload = self.context.identity_payload

        oauth_id_token = identity_payload.get('oauth_id_token')
        if oauth_id_token:
            token_info = google_oauth_service.get_id_token_info(oauth_id_token)
        else:
            token_info = google_oauth_service.get_token_info(identity_payload.get('oauth_access_token'))

        self.context.identity_payload['oauth_user_id'] = token_info['user_id']
        self.context.token_info = token_info
        return token_info

    def check_email_in_whitelist(self, **kwargs):
        if not hasattr(self.context, 'other_jwt_attributes'):
            self.context.other_jwt_attributes = {}

        email = self.context.token_info['email']
        whitelisted_emails = google_table_service.get_whitelisted_emails()
        if email not in whitelisted_emails:
            logger.debug(f'mutations disabled for: {email}')
            self.context.other_jwt_attributes['disable_mutations'] = True

    def load_rex_agent(self, **kwargs):
        if not hasattr(self.context, 'other_jwt_attributes'):
            self.context.other_jwt_attributes = {}

        email = self.context.token_info['email']
        agent = self.load_rex_agent_by_email(email)

        if agent:
            self.context.is_demo_mode = False
        else:
            self.context.is_demo_mode = True
            self.context.other_jwt_attributes['demo_mode'] = True
            self.context.other_jwt_attributes['disable_mutations'] = True
            agent = self.load_demo_rex_agent_by_email()

        if agent is None:
            logger.debug(f'record not foundre in REX associate_agents table for: {email}')
            raise NotFoundRexAssociatedAgentException('email', email)

        self.context.rex_agent = agent
        return agent

    def load_app_user_and_identity(self, **kwargs):
        identity_payload = self.context.identity_payload
        identity = self.load_identity_by_payload(identity_payload)

        if identity:
            user = identity.user
        else:
            identity = GoogleIdentity()
            user = User()
            identity.user = user

        self.context.identity = identity
        self.context.user = user
        return identity

    def update_app_user_and_identity(self, **kwargs):
        user = self.context.user
        identity = self.context.identity
        attributes = self.context.rex_agent.get_attrs_for_app_user()

        if self.context.is_demo_mode:
            del attributes['rex_agent_id']

        for attr_name, attr_value in attributes.items():
            setattr(user, attr_name, attr_value)

        identity.payload = self.context.identity_payload
        identity.sign_in_at = tznow()

        session = db.session
        session.add(identity)
        session.add(user)
        session.commit()
        return identity

    def update_or_create_device(self, **kwargs):
        if not hasattr(self.context, 'other_jwt_attributes'):
            self.context.other_jwt_attributes = {}

        user = self.context.user
        device_id = kwargs.get('device_id')

        device = Device.query.filter(Device.id == device_id).first()
        if not device:
            device = Device(id=device_id)

        device.user_id = user.id

        session = db.session
        session.add(device)
        session.commit()

        self.context.device = device
        self.context.other_jwt_attributes['device_id'] = device.id

        return device

    def link_app_user_to_rex_users(self, **kwargs):
        if self.context.is_demo_mode:
            return None
        user = self.context.user
        rex_agent = self.context.rex_agent
        LinkUsersInteractor(need_logs=True).call(
            user=user,
            phone_number=rex_agent.phone_number,
            email=rex_agent.email
        )

    def make_jwt(self, **kwargs):
        self.context.jwt = jwt_service.jwt_for_user(
            self.context.user,
            self.context.identity,
            self.context.other_jwt_attributes
        )

    def prepare_response(self, **kwargs):
        jwt = self.context.jwt
        other_jwt_attributes = self.context.other_jwt_attributes
        user = self.context.user

        is_developer = self.get_is_developer(user.contact_email)
        disable_mutations = bool(other_jwt_attributes.get('disable_mutations'))
        demo_mode = bool(other_jwt_attributes.get('demo_mode'))

        return {'jwt': jwt,
                'is_developer': is_developer,
                'disable_mutations': disable_mutations,
                'demo_mode': demo_mode}

    def get_is_developer(self, email):
        try:
            dev_emails = get_developer_emails()
            return email in dev_emails
        except BaseException:
            return False

    def load_demo_rex_agent_by_email(self):
        email = app.config['DEMO_REX_USER_EMAIL']
        return self.load_rex_agent_by_email(email)

    def load_rex_agent_by_email(self, email):
        agent = RexAssociatedAgent.query \
                                  .filter(RexAssociatedAgent.email == email) \
                                  .first()
        return agent

    def load_identity_by_payload(self, payload):
        oauth_user_id = payload.get('oauth_user_id')
        condition = text("{}.payload @> :payload".format(GoogleIdentity.__tablename__))
        payload_for_condition = json.dumps({'oauth_user_id': oauth_user_id})
        return GoogleIdentity.query \
                             .join(User) \
                             .filter(condition) \
                             .params(payload=payload_for_condition) \
                             .first()
