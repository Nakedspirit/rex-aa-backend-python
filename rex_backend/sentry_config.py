from rex_backend.exceptions import AuthException, InvalidOauthToken
from rex_backend.utils.rest_api import RestAPIException


EXCEPTIONS_TO_IGNORE = (AuthException, InvalidOauthToken, RestAPIException)


def sentry_before_send_hook(event, hint):
    if 'exc_info' in hint:
        exc_type, exc_value, tb = hint['exc_info']
        if isinstance(exc_value, EXCEPTIONS_TO_IGNORE):
            return None

        return event
