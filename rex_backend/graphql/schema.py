import graphene

from .query import Query
from .mutation import Mutation

rex_schema = graphene.Schema(query=Query, mutation=Mutation)
