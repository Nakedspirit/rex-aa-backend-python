Queries examples
================

MUTATIONS
----------

```graphql
  mutation {
    signInOauth(oauthProvider: "google", oauthUserId: "user-id", oauthAccessToken: "token-should-be-here") {
      result {
        ... on SignInOk {
          __typename
          jwt
        }
        ... on ValidationProblem {
          __typename
          field
          messages
        }
      }
    }
  }
```