import graphene


from .mutations import SignInOauth, ClaimShowing, OptOutShowing, CancelShowing, FinishShowing, SubmitReport, SubmitForPayment, AnswerReportQuestions, CompleteCallBuyer, RegisterEvent, AssignPushNotificationsToken, SignOut, ClearUnreadNotifications, CloseApp, AnswerReportGroupQuestions, AddVisitor, HideShowing, UploadOpenHousePhotos, AnswerVisitorQuestions


class Mutation(graphene.ObjectType):
    sign_in_oauth = SignInOauth.Field()
    claim_showing = ClaimShowing.Field()
    opt_out_showing = OptOutShowing.Field()
    cancel_showing = CancelShowing.Field()
    finish_showing = FinishShowing.Field()
    submit_report = SubmitReport.Field()
    submit_for_payment = SubmitForPayment.Field()
    answer_report_questions = AnswerReportQuestions.Field()
    complete_call_buyer = CompleteCallBuyer.Field()
    register_event = RegisterEvent.Field()
    assign_push_notifications_token = AssignPushNotificationsToken.Field()
    sign_out = SignOut.Field()
    clear_unread_notifications = ClearUnreadNotifications.Field()
    close_app = CloseApp.Field()
    answer_report_group_questions = AnswerReportGroupQuestions.Field()
    add_visitor = AddVisitor.Field()
    hide_showing = HideShowing.Field()
    upload_open_house_photos = UploadOpenHousePhotos.Field()
    answer_visitor_questions = AnswerVisitorQuestions.Field()
