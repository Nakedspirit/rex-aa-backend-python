import graphene
from graphene import relay


from rex_backend.application import db


class AssignPushNotificationsToken(relay.ClientIDMutation):
    class Input:
        token = graphene.String(required=True)

    result = graphene.Boolean()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        device = info.context.device
        device.push_notification_token = input.get('token')
        session = db.session
        session.add(device)
        session.commit()

        return AssignPushNotificationsToken(result=True)
