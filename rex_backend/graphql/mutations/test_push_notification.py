import json


import graphene
from graphene import relay


from rex_backend.services import firebase_cm_service


class TestPushNotification(relay.ClientIDMutation):
    class Input:
        title = graphene.String(required=True)
        body = graphene.String(required=True)
        data = graphene.String(description="Should be a json converted to string")

    result = graphene.Boolean()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        device = info.context.device
        if not device.push_notification_token:
            return TestPushNotification(result=False)

        title = input.get('title')
        body = input.get('body')

        try:
            data = json.loads(input.get('data'))
        except: # noqa E722
            data = {}

        token = device.push_notification_token
        firebase_cm_service.send_message(token, title, body, data=data)
        return TestPushNotification(result=True)
