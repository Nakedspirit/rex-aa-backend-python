import graphene
from graphene import relay


from rex_backend.graphql.types import NotFoundProblem, ValidationProblem
from rex_backend.models.base import ShowingReport
from rex_backend.models.rex_listings import RexListing
from rex_backend.apis.rex_questions import rex_questions_api


class AnswerInput(graphene.InputObjectType):
    question_id = graphene.ID(required=True)
    answers = graphene.List(graphene.String)


class Result(graphene.ObjectType):
    success = graphene.Boolean()


class AnswerReportQuestionsResult(graphene.Union):
    class Meta:
        types = (Result, NotFoundProblem, ValidationProblem)


class AnswerReportQuestions(relay.ClientIDMutation):
    class Input:
        showing_report_id = graphene.ID(required=True)
        answers_to_questions = graphene.List(AnswerInput, required=True)

    result = AnswerReportQuestionsResult(deprecation_reason='We moved to REX API')

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        current_user = info.context.user
        showing_report_id = input.get('showing_report_id')

        showing_report = ShowingReport.query.filter(ShowingReport.user_id == current_user.id) \
                                            .filter(ShowingReport.id == showing_report_id) \
                                            .first()
        if not showing_report:
            problem = NotFoundProblem(entity='ShowingReport', message='Report not found')
            return AnswerReportQuestions(result=problem)

        showing = current_user.showing_query(showing_report.rex_showing_id).first()
        if not showing:
            problem = NotFoundProblem(entity='Showing', message='Showing not found')
            return AnswerReportQuestions(result=problem)

        listing = RexListing.query.filter(RexListing.id == showing.data.get('listingId')).first()
        if not listing:
            problem = NotFoundProblem(entity='Listing', message='Listing not found')
            return AnswerReportQuestions(result=problem)

        answers = input.get('answers_to_questions')
        if len(answers) == 0:
            problem = ValidationProblem(field='answers', message='You did not provide answers')
            return AnswerReportQuestions(result=problem)

        answers = dict([(answer.get('question_id'), answer.get('answers')[0]) for answer in answers])
        data = {
            'listing_guid': listing.guid,
            'recorded_by': current_user.contact_email,
            'replace': True,  # always delete previous answers
            'source': str(showing.id),
            'values': answers
        }
        api_call_result = rex_questions_api.call_add_answers(json=data)
        is_success = api_call_result.get('content', {}).get('result', False)

        return AnswerReportQuestions(result=Result(success=is_success))
