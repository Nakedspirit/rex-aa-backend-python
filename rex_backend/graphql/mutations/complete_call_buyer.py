import graphene
from graphene import relay


from rex_backend.application import db
from rex_backend.models.base import ShowingReport as ShowingReportModel, ShowingReportStatus
from rex_backend.graphql.resolvers import resolve_showing_report
from rex_backend.graphql.types import ShowingReport as ShowingReportGraphql


class CompleteCallBuyer(relay.ClientIDMutation):
    class Input:
        showing_report_id = graphene.ID(required=True)

    result = graphene.Field(ShowingReportGraphql)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        current_user = info.context.user
        showing_report_id = int(input.get('showing_report_id'))

        showing_report = ShowingReportModel.query.filter(ShowingReportModel.id == showing_report_id) \
                                           .filter(ShowingReportModel.user_id == current_user.id) \
                                           .filter(ShowingReportModel.status == ShowingReportStatus.ok) \
                                           .first()

        if not showing_report:
            return CompleteCallBuyer(result=None)

        showing_report.is_call_buyer_completed = True
        db.session.add(showing_report)
        db.session.commit()

        showing_report_graphql_obj = resolve_showing_report(None, info, showing_report.rex_showing_id)
        return CompleteCallBuyer(result=showing_report_graphql_obj)
