from graphene import relay
from graphene.types.datetime import DateTime


from rex_backend.application import db
from rex_backend.utils.tz import tznow


class CloseApp(relay.ClientIDMutation):
    result = DateTime()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        device = info.context.device
        device.closed_at = tznow()

        session = db.session
        session.add(device)
        session.commit()

        return CloseApp(result=device.closed_at)
