import graphene
from graphene import relay


from rex_backend.application import rex_scheduler_api_service
from rex_backend.repositories.rex_showing_repo import RexShowingRepo
from .claim_showing import make_rex_event
from rex_backend.application import sentry_sdk
from .hide_showing import HideShowing


class OptOutShowing(relay.ClientIDMutation):
    class Input:
        showing_id = graphene.ID(required=True)

    result = graphene.Boolean()
    showing_id = graphene.ID()

    @staticmethod
    def mutate_and_get_payload(root, info, **input):
        current_user = info.context.user
        rex_showing_id = input.get('showing_id')

        rex_member = RexShowingRepo.load_rex_member(rex_showing_id, current_user.rex_users_ids)
        if rex_member is None:
            return OptOutShowing(result=False)

        if rex_member.is_active is False:
            HideShowing.mutate_and_get_payload(root, info, **input)
            return OptOutShowing(result=True, showing_id=rex_showing_id)

        rex_event_to_send = make_rex_event(rex_member, None)
        rex_api_result = rex_scheduler_api_service.call_main_endpoint(rex_event_to_send)

        content = rex_api_result.get('content', {})
        if content.get('success') is False:
            msg = 'Bad REX API response, message:{}'.format(content.get('message'))
            sentry_sdk.capture_message(msg)
            return OptOutShowing(result=False, showing_id=rex_showing_id)

        HideShowing.mutate_and_get_payload(root, info, **input)
        return OptOutShowing(result=True, showing_id=rex_showing_id)
