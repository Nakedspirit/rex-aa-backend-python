import graphene
from graphene import relay


from rex_backend.graphql.types import NotFoundProblem, ValidationProblem, Question
from rex_backend.models.rex_listings import RexListing
from .answer_report_questions import AnswerInput
from rex_backend.apis.rex_questions import rex_questions_api, make_source_for_visitors_questions, RexQuestionsContainer
from rex_backend.graphql.resolvers.visitor_questions import get_questions, BadAPIResponse


class AnswerVisitorQuestionsOk(graphene.ObjectType):
    success = graphene.Boolean()
    questions = graphene.List(Question)


class AnswerVisitorQuestionsNotOk(graphene.ObjectType):
    message = graphene.NonNull(graphene.String)


class AnswerVisitorQuestionsResult(graphene.Union):
    class Meta:
        types = (AnswerVisitorQuestionsOk, AnswerVisitorQuestionsNotOk, NotFoundProblem, ValidationProblem)


class AnswerVisitorQuestions(relay.ClientIDMutation):
    class Input:
        showing_id = graphene.ID(required=True)
        visitor_id = graphene.ID(required=True)
        answers_to_questions = graphene.List(AnswerInput, required=True)

    result = graphene.NonNull(AnswerVisitorQuestionsResult)

    @classmethod
    def mutate_and_get_payload(cls, root, info, showing_id, visitor_id, answers_to_questions):
        current_user = info.context.user

        rex_showing = current_user.showing_query(showing_id).first()
        if not rex_showing or not rex_showing.open_house_guid:
            problem = NotFoundProblem(
                entity='Open House',
                message='Open House not found.'
            )
            return AnswerVisitorQuestions(result=problem)

        rex_listing_id = int(rex_showing.data.get('listingId'))
        rex_listing = RexListing.query.filter(RexListing.id == rex_listing_id).first()
        if not rex_listing:
            problem = NotFoundProblem(
                entity='Listing',
                message='Listing not found.'
            )
            return AnswerVisitorQuestions(result=problem)

        if len(answers_to_questions) == 0:
            problem = ValidationProblem(
                field='answersToQuestions',
                message='You did not provide answers'
            )
            return AnswerVisitorQuestions(result=problem)

        questions_api_response = rex_questions_api.call_get_questions(params={
            'tool': 'openhouseleads',
        })
        questions_container = RexQuestionsContainer(questions_api_response)
        answers = questions_container.format_answers_to_send_to_rex(answers_to_questions)

        api_result = rex_questions_api.call_add_answers(json={
            'listing_guid': rex_listing.guid,
            'recorded_by': current_user.contact_email,
            'replace': True,  # always delete previous answers
            'source': make_source_for_visitors_questions(showing_id, visitor_id),
            'values': answers,
        })

        try:
            questions = get_questions(rex_listing.guid, rex_showing.id, visitor_id)
        except BadAPIResponse as exc:
            return AnswerVisitorQuestions(
                result=AnswerVisitorQuestionsNotOk(
                    message=exc.message
                )
            )

        return AnswerVisitorQuestions(
            result=AnswerVisitorQuestionsOk(
                success=True,
                questions=[question.to_graphql() for question in questions]
            )
        )
