from datetime import datetime

import graphene
from graphene import relay
from graphene_file_upload.scalars import Upload

from rex_backend.graphql.types import Showing, NotFoundProblem, ValidationProblem
from rex_backend.apis.rex_paperless_signin import rex_paperless_signin_api, PASSWORD
from rex_backend.graphql.adapters import to_graphql_showing
from rex_backend.graphql.data_loaders import init_data_loaders


class UploadOpenHousePhotosNotOk(graphene.ObjectType):
    message = graphene.NonNull(graphene.String)


class UploadOpenHousePhotosResult(graphene.Union):
    class Meta:
        types = (Showing, UploadOpenHousePhotosNotOk, NotFoundProblem, ValidationProblem)


class UploadOpenHousePhotos(relay.ClientIDMutation):
    class Input:
        showing_id = graphene.ID(required=True)
        files = graphene.List(Upload, required=True)

    result = UploadOpenHousePhotosResult()

    @classmethod
    @init_data_loaders
    def mutate_and_get_payload(cls, root, info, showing_id, files):
        if not files:
            problem = ValidationProblem(
                field='files',
                message='Files should be present'
            )
            return UploadOpenHousePhotos(result=problem)

        current_user = info.context.user

        rex_showing = current_user.showing_query(showing_id).first()
        if not rex_showing or not rex_showing.open_house_guid:
            problem = NotFoundProblem(
                entity='Open House',
                message='Open House not found.'
            )
            return UploadOpenHousePhotos(result=problem)

        files_to_send = {}
        upload_timestamp = int(datetime.utcnow().timestamp())
        for i, file_to_send in enumerate(files):
            extension = file_to_send.mimetype.split('/')[-1]
            new_name = f'{i}-{upload_timestamp}'
            new_filename = f'{new_name}.{extension}'
            file_to_send.name = new_name
            file_to_send.filename = new_filename
            files_to_send[f'photo{i}'] = file_to_send

        data = {
            'password': PASSWORD,
            'open_house_guid': rex_showing.open_house_guid,
        }
        api_result = rex_paperless_signin_api.call_upload_photos(data=data, files=files_to_send)

        if not api_result.get('content', {}).get('success'):
            result = UploadOpenHousePhotosNotOk(
                message='REX API call was unsuccessful'
            )
            return UploadOpenHousePhotos(result=result)

        return UploadOpenHousePhotos(
            result=to_graphql_showing(rex_showing)
        )
