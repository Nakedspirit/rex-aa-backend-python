import graphene
from graphene import relay

from rex_backend.apis.rex_paperless_signin import rex_paperless_signin_api, PASSWORD
from rex_backend.graphql.adapters import to_graphql_showing
from rex_backend.graphql.sub_resolvers.phone_number import resolve_phone_number
from rex_backend.graphql.types import NotFoundProblem, ValidationProblem
from rex_backend.repositories.rex_showing_repo import RexShowingRepo
from rex_backend.helpers import is_valid_email


class AddVisitorOk(graphene.ObjectType):
    visitor_id = graphene.ID()


class AddVisitorNotOk(graphene.ObjectType):
    message = graphene.NonNull(graphene.String)


class NoPhoneNumberProblem(graphene.ObjectType):
    message = graphene.NonNull(graphene.String)


class AddVisitorResult(graphene.Union):
    class Meta:
        types = (NotFoundProblem, ValidationProblem, NoPhoneNumberProblem, AddVisitorOk, AddVisitorNotOk)


class AddVisitor(relay.ClientIDMutation):
    class Input:
        showing_id = graphene.ID(required=True)
        name = graphene.String(required=True)
        email = graphene.String(required=True)
        phone_number = graphene.String(required=True)

    result = graphene.NonNull(AddVisitorResult)

    @staticmethod
    def mutate_and_get_payload(root, info, showing_id, name, email, phone_number):
        if not is_valid_email(email):
            return AddVisitor(
                result=ValidationProblem(
                    field='email',
                    message='Email is not valid'
                )
            )

        rex_showing_id = int(showing_id)
        rex_showing = RexShowingRepo.load_rex_showing(rex_showing_id)
        if not rex_showing or not rex_showing.open_house_guid:
            return AddVisitor(
                result=NotFoundProblem(
                    entity='Open House',
                    message='Open House not found.'
                )
            )

        graphql_showing = to_graphql_showing(rex_showing)
        open_house_phone_number = resolve_phone_number(graphql_showing, info)
        if not open_house_phone_number:
            return AddVisitor(
                result=NoPhoneNumberProblem(
                    message="There is a problem with Open House phone number. Please contact REX Operations for further instructions."
                )
            )

        data_to_send = {
            "password": PASSWORD,
            "open_house_guid": rex_showing.open_house_guid,
            "open_house_phone_number": open_house_phone_number,
            "phone_number": phone_number,
            "email": email,
            "name": name,
        }
        api_response = rex_paperless_signin_api.call_add_visitor(json=data_to_send)

        data_to_send.pop('email')
        data_to_send.pop('name')
        api_response = rex_paperless_signin_api.call_sign_visitor(json=data_to_send)

        is_success = bool(api_response.get('content', {}).get('success'))
        if is_success:
            visitor_id = api_response.get('content', {}).get('id')
            result = AddVisitorOk(
                visitor_id=visitor_id,
            )
        else:
            result = AddVisitorNotOk(
                message='REX sign visitor API endpoint returned False.'
            )

        return AddVisitor(result=result)
