import graphene
from graphene import relay


class FinishShowing(relay.ClientIDMutation):
    class Input:
        showing_id = graphene.ID(required=True)

    result = graphene.Boolean()

    @staticmethod
    def mutate_and_get_payload(cls, root, info, **input):
        return FinishShowing(result=False)
