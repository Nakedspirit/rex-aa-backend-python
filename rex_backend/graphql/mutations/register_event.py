import graphene
from graphene import relay


from rex_backend.application import app
from rex_backend.exceptions import AuthException
from rex_backend.graphql.enums import EventName
from rex_backend.register_event import EventDispatcher


class RegisterEvent(relay.ClientIDMutation):
    class Input:
        event_name = EventName(required=True)
        object_id = graphene.ID()
        payload = graphene.String(
            description="It should contain JSON as a string")

    result = graphene.Boolean()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        validate_token(info.context.headers['Authorization'])

        dispatcher = EventDispatcher(**input)
        result = dispatcher()

        return RegisterEvent(result=result)


def validate_token(token):
    token = str(token).split(' ')[-1].strip()
    valid_token = app.config['REGISTER_EVENT_SECRET_KEY']
    if not valid_token or token != valid_token:
        raise AuthException("JWT payload is invalid")
