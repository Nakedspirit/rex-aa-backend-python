import graphene
from graphene import relay
from rex_backend.graphql.types import ShowingReport, NotFoundProblem, ValidationProblem
from rex_backend.graphql.enums import ShowingReportStatus as ShowingReportStatusEnum
from rex_backend.models.base import ShowingReport as ShowingReportModel, ShowingReportStatus
from rex_backend.application import db
from rex_backend.graphql.adapters import to_graphql_showing, to_graphql_showing_report
from rex_backend.application import rex_scheduler_api_service
from rex_backend.utils.tz import tznow


class SubmitReportResult(graphene.Union):
    class Meta:
        types = (ShowingReport, NotFoundProblem, ValidationProblem)


class SubmitReport(relay.ClientIDMutation):
    class Input:
        showing_id = graphene.ID(required=True)
        status = ShowingReportStatusEnum(required=True)
        status_comment = graphene.String()

    result = graphene.Field(SubmitReportResult)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        current_user = info.context.user
        rex_showing_id = int(input.get('showing_id'))

        rex_showing = current_user.showing_query(rex_showing_id).first()
        if not rex_showing or rex_showing.status not in ('Scheduled', 'Completed'):
            return SubmitReport(result=NotFoundProblem(entity='showing', message='Showing not found'))

        if rex_showing.status == 'Scheduled':
            result = rex_scheduler_api_service.call_complete_endpoint(rex_showing.id)
            if result.get('status') != 200 or result.get('content', {}).get('success') is False:
                return SubmitReport(result=ValidationProblem(field='', message="Can't complete showing"))

        showing_report_obj = load_report(rex_showing.id)
        if not showing_report_obj:
            showing_report_obj = ShowingReportModel(rex_showing_id=rex_showing.id, user_id=current_user.id)

        showing_report_obj.status = getattr(ShowingReportStatus, input.get('status'))
        showing_report_obj.status_comment = input.get('status_comment')
        showing_report_obj.status_added_at = tznow()

        session = db.session
        session.add(showing_report_obj)
        session.commit()

        showing_report_graphql_obj = to_graphql_showing_report(showing_report_obj)
        showing_report_graphql_obj.showing = to_graphql_showing(rex_showing)
        return SubmitReport(result=showing_report_graphql_obj)


def load_report(rex_showing_id):
    return ShowingReportModel.query.filter(ShowingReportModel.rex_showing_id == rex_showing_id).first()
