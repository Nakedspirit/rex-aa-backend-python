import graphene
from graphene import relay
from rex_backend.graphql.types import SignInOk, ValidationProblem
from rex_backend.graphql.enums import OauthIdentityProvider
from rex_backend.interactors import SignInOauthInteractor
from rex_backend.exceptions import InvalidOauthToken, NotFoundRexAssociatedAgentException


class SignInOauthResult(graphene.Union):
    class Meta:
        types = (SignInOk, ValidationProblem)


class SignInOauth(relay.ClientIDMutation):
    class Input:
        oauth_provider = OauthIdentityProvider()
        oauth_user_id = graphene.String(required=True, deprecation_reason='useless field for the backend')
        oauth_access_token = graphene.String(required=True, deprecation_reason='we are going to use idToken instead')
        expiration_date = graphene.String(required=False, deprecation_reason='useless field for the backend')
        oauth_id_token = graphene.String()
        device_id = graphene.String(required=True)

    result = graphene.Field(lambda: SignInOauthResult)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        try:
            raw_result = SignInOauthInteractor(need_logs=True).call(**input)
            result = SignInOk(
                jwt=raw_result['jwt'],
                is_developer=raw_result['is_developer'],
                disable_mutations=raw_result['disable_mutations'],
                demo_mode=raw_result['demo_mode']
            )
        except InvalidOauthToken as exc:
            result = ValidationProblem(field=exc.provider, message='Invalid OAuth token')
        except NotFoundRexAssociatedAgentException as exc:
            result = ValidationProblem(field=exc.field, message='Demo mode Rex user is not found')

        return SignInOauth(result=result)
