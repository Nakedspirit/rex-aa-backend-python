import logging

import graphene
from graphene import relay

from rex_backend.graphql.mutations.answer_report_questions import AnswerInput, AnswerReportQuestionsResult, Result
from rex_backend.interactors.complete_report_group import CompleteReportGroup
from rex_backend.interactors.send_answers_to_rex import SendAnswersToRex
from rex_backend.interactors.save_answers import SaveAnswers
from rex_backend.interactors.send_email_with_showing_report import SendEmailWithShowingReport
from rex_backend.exceptions import NotFoundException
from rex_backend.models.base import ReportGroup


logger = logging.getLogger('rex_backend')


class AnswerReportGroupQuestions(relay.ClientIDMutation):
    class Input:
        showing_report_id = graphene.ID(required=True)
        answers_to_questions = graphene.List(AnswerInput, required=True)
        group_title = graphene.String()

    result = AnswerReportQuestionsResult(deprecation_reason='We moved to REX API')

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        showing_report_id = input.get('showing_report_id')
        group_title = input.get('group_title')
        group = cls._load_group(showing_report_id, group_title)
        showing_report = group.showing_report

        SaveAnswers().call(
            showing_report=showing_report,
            answers_data=input.get('answers_to_questions')
        )
        CompleteReportGroup().call(
            report_group=group
        )
        SendAnswersToRex().call(
            showing_report=showing_report,
            current_user=info.context.user
        )
        SendEmailWithShowingReport().call(
            showing_report=showing_report
        )

        return AnswerReportGroupQuestions(result=Result(success=True))

    @classmethod
    def _load_group(сls, showing_report_id, group_title):
        group = ReportGroup.query.filter(ReportGroup.showing_report_id == showing_report_id) \
                                 .filter(ReportGroup.title == group_title) \
                                 .first()

        if not group:
            raise NotFoundException('ReportGroup', f'{group_title} not found')

        return group
