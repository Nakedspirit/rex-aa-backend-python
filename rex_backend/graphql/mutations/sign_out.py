import graphene
from graphene import relay


from rex_backend.application import db
from rex_backend.utils.tz import tznow


class SignOut(relay.ClientIDMutation):
    result = graphene.Boolean()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        identity = info.context.identity
        identity.sign_out_at = tznow()

        device = info.context.device

        session = db.session
        session.add(identity)
        session.delete(device)
        session.commit()

        return SignOut(result=True)
