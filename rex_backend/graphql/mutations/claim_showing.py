import graphene
from graphene import relay

from rex_backend.graphql.types import Showing
from rex_backend.application import rex_scheduler_api_service
from rex_backend.graphql.adapters import to_graphql_showing
from rex_backend.repositories.rex_showing_repo import RexShowingRepo
from rex_backend.application import sentry_sdk
from rex_backend.graphql.data_loaders import init_data_loaders


class InvalidRequestProblem(graphene.ObjectType):
    showing_id = graphene.NonNull(graphene.ID)
    message = graphene.String()


class AlreadyClaimedProblem(graphene.ObjectType):
    showing_id = graphene.NonNull(graphene.ID)
    message = graphene.String()
    claimed_at = graphene.types.datetime.DateTime()


class ClaimShowingResult(graphene.Union):
    class Meta:
        types = (Showing, AlreadyClaimedProblem, InvalidRequestProblem)


class ClaimShowing(relay.ClientIDMutation):
    class Input:
        showing_id = graphene.ID(required=True)
        slots_indexes_to_claim = graphene.List(graphene.Int)

    result = graphene.Field(lambda: ClaimShowingResult)

    @classmethod
    @init_data_loaders
    def mutate_and_get_payload(cls, root, info, **input):
        rex_showing_id = int(input.get('showing_id'))
        slots_indexes_to_claim = input.get('slots_indexes_to_claim')
        current_user = info.context.user

        rex_member = RexShowingRepo.load_rex_member(rex_showing_id, current_user.rex_users_ids)
        if not rex_member:
            msg = f'No rex_memeber for rex_showing({rex_showing_id}) and rex_users_ids({current_user.rex_users_ids})'
            sentry_sdk.capture_message(msg)
            return ClaimShowing(result=InvalidRequestProblem(
                showing_id=rex_showing_id,
                message="Don't know how to send your request"
            ))

        rex_event_to_send = make_rex_event(rex_member, slots_indexes_to_claim)
        rex_api_result = rex_scheduler_api_service.call_main_endpoint(rex_event_to_send)

        content = rex_api_result.get('content', {})
        if content.get('success') is False:
            msg = 'Bad REX API response, message:{}'.format(content.get('message'))
            sentry_sdk.capture_message(msg)

        rex_showing = RexShowingRepo.load_rex_showing(rex_showing_id)
        if not rex_showing:
            return ClaimShowing(result=InvalidRequestProblem(
                showing_id=rex_showing_id,
                message='Showing not found.\nRefresh and try again.'
            ))

        if rex_showing.claimed_by_id is None:
            return ClaimShowing(result=InvalidRequestProblem(
                showing_id=rex_showing_id,
                message='A problem occured. Try again.\nIf this persists, contact the\noperations team.'
            ))

        if int(rex_showing.claimed_by_id) not in current_user.rex_users_ids:
            return ClaimShowing(result=AlreadyClaimedProblem(
                showing_id=rex_showing_id,
                message='Showing already claimed by another AA',
                claimed_at=rex_showing.claimed_at
            ))

        return ClaimShowing(result=to_graphql_showing(rex_showing))


def make_rex_event(rex_member, slots_indexes_to_claim=None):
    action = 'email' if rex_member.medium == 'email' else 'text'
    if not slots_indexes_to_claim:
        slots_indexes_to_claim = '0'
    else:
        slots_indexes_to_claim = set(filter(lambda x: int(x) > 0, slots_indexes_to_claim))
        slots_indexes_to_claim = ','.join([str(slot) for slot in slots_indexes_to_claim])
    return {
        action: {
            "Body": slots_indexes_to_claim,
            "From": rex_member.user_address,
            "To": rex_member.bot_address
        }
    }
