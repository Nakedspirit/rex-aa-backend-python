from rex_backend.utils.auto_module_loader import AutoModuleLoader, ClassConvention

# The code below automatically load all classes for you
# Just use `form rex_backend.mutation import YourMutation`
loader = AutoModuleLoader(__name__, convention=ClassConvention())
loader.load_objects_into_current_package()
loader.write_objects_into_all_variable()
