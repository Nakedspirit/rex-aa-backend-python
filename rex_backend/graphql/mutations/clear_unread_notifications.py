import graphene
from graphene import relay


from rex_backend.application import db
from rex_backend.models.base import Device as DeviceModel


class ClearUnreadNotifications(relay.ClientIDMutation):
    class Input:
        device_id = graphene.String(required=True)

    result = graphene.Boolean()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        device_id = input.get('device_id')
        device = load_device(device_id)
        device.badge_count = 0
        db.session.add(device)
        db.session.commit()

        return ClearUnreadNotifications(result=True)


def load_device(device_id):
    return DeviceModel.query.filter(DeviceModel.id == device_id).first()
