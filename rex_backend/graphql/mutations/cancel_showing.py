import graphene
from graphene import relay


from rex_backend.application import rex_scheduler_api_service
from rex_backend.application import sentry_sdk
from rex_backend.graphql.mutations.hide_showing import HideShowing


class CancelShowing(relay.ClientIDMutation):
    class Input:
        showing_id = graphene.ID(required=True)
        comment = graphene.String()

    result = graphene.Boolean()

    @staticmethod
    def mutate_and_get_payload(root, info, **input):
        current_user = info.context.user
        rex_showing_id = input.get('showing_id')

        rex_command_to_send = make_rex_command(current_user.contact_email, rex_showing_id, 'cancel')
        rex_api_result = rex_scheduler_api_service.call_main_endpoint(rex_command_to_send)

        content = rex_api_result.get('content', {})
        if content.get('success') is False:
            msg = 'Bad REX API response, message:{}'.format(content.get('message'))
            sentry_sdk.capture_message(msg)

        if rex_api_result.get('status') != 200 or not content.get('success'):
            return CancelShowing(result=False)

        HideShowing.mutate_and_get_payload(root, info, **input)
        return CancelShowing(result=True)


def make_rex_command(from_address, rex_showing_id, body):
    return {
        'command': {
            "From": from_address,
            "conversationId": rex_showing_id,
            "Body": body
        }
    }
