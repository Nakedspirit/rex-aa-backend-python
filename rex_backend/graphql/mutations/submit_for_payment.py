import graphene
from graphene import relay


from rex_backend.utils.tz import tznow
from rex_backend.application import db
from rex_backend.models.base import ShowingReportStatus
from rex_backend.graphql.types import ShowingReport as ShowingReportGraphql
from rex_backend.graphql.resolvers import resolve_showing_report
from rex_backend.repositories.showing_report_repo import ShowingReportRepo
from rex_backend.graphql.data_loaders import init_data_loaders


class SubmitForPayment(relay.ClientIDMutation):
    class Input:
        showing_id = graphene.ID(required=True)

    result = graphene.Field(ShowingReportGraphql)
    err_message = graphene.String()

    @classmethod
    @init_data_loaders
    def mutate_and_get_payload(cls, root, info, **input):
        current_user = info.context.user
        rex_showing_id = int(input.get('showing_id'))

        showing_report = ShowingReportRepo.load_showing_report(rex_showing_id, current_user.id)
        if not showing_report:
            return SubmitForPayment(result=None, err_message='Showing Report not found')

        rex_showing = showing_report.rex_showing
        err_message = None

        if showing_report.status == ShowingReportStatus.buyer_not_appeared:
            showing_report = cls.submit_for_payment(showing_report)

        # Check for Open House
        if rex_showing.open_house_id and showing_report.status == ShowingReportStatus.ok and showing_report.is_questions_completed:
            showing_report = cls.submit_for_payment(showing_report)
        else:
            err_message = "This Open House can't be sended for payment"

        # Check for Showing
        if showing_report.status == ShowingReportStatus.ok and showing_report.is_questions_completed and showing_report.is_call_buyer_completed:
            showing_report = cls.submit_for_payment(showing_report)
        else:
            err_message = "This Showing can't be sended for payment"

        showing_report_graphql_obj = resolve_showing_report(None, info, showing_report.rex_showing_id)
        return SubmitForPayment(result=showing_report_graphql_obj, err_message=err_message)

    @staticmethod
    def submit_for_payment(showing_report):
        showing_report.submitted_for_payment_at = tznow()
        session = db.session
        session.add(showing_report)
        session.commit()

        return showing_report
