import logging

import graphene
from graphene import relay
from sqlalchemy.exc import IntegrityError

from rex_backend.application import db
from rex_backend.models.base import UserAction, UserActionName


logger = logging.getLogger('rex_backend')


class HideShowing(relay.ClientIDMutation):
    class Input:
        showing_id = graphene.ID(required=True)

    result = graphene.Boolean()

    @staticmethod
    def mutate_and_get_payload(root, info, **input):
        current_user = info.context.user
        showing_id = input.get('showing_id')

        action = UserAction(
            user=current_user,
            name=UserActionName.hide_showing.value,
            object_id=showing_id
        )
        db.session.add(action)

        try:
            db.session.commit()
            result = True
        except IntegrityError as exc:
            logger.error(exc)
            result = False

        return HideShowing(result=result)
