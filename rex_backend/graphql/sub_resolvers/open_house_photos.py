from rex_backend.graphql.enums import ShowingTypeEnum
from rex_backend.apis.rex_paperless_signin import rex_paperless_signin_api, PASSWORD
from rex_backend.apis import HttpAPIException


def resolve_open_house_photos(open_house, info):
    if open_house.type != ShowingTypeEnum.OPEN_HOUSE.value:
        return []

    try:
        api_result = rex_paperless_signin_api.call_get_photos(
            params={
                'open_house_guid': open_house.guid,
                'password': PASSWORD,
            }
        )
    except HttpAPIException:
        return []

    links = api_result.get('content', {}).get('links', [])
    return links
