import logging

from rex_backend.interactors.create_groups_for_showing_report import CreateGroupsForShowingReport
from rex_backend.models.base import ReportGroupStatus
from rex_backend.interactors.save_questions import SaveQuestions
from rex_backend.helpers import flat_list
from rex_backend.apis.rex_questions import rex_questions_api, RexGroup


logger = logging.getLogger('rex_backend')

SHOWING_TYPE_TO_TOOL = {
    'OPEN_HOUSE': 'openhouse',
    'REGULAR': 'postshowings',
}


def resolve_groups(showing_report, info):
    tool = SHOWING_TYPE_TO_TOOL.get(showing_report.showing.type.value)
    api_result = rex_questions_api.call_get_questions(params={'tool': tool})
    raw_groups = api_result.get('content', {}).get('result', {}).get('pages', [])
    if len(raw_groups) == 0:
        return []

    rex_groups = RexGroup.init_many(raw_groups)
    rex_groups_titles = [rex_group.title for rex_group in rex_groups]

    logger.info(
        'Resolving groups for'
        f' ShowingReport({showing_report.id}) of'
        f' Showing({showing_report.showing.id}, {showing_report.showing.type.value})'
        f' and groups titles are {rex_groups_titles}'
    )

    groups = CreateGroupsForShowingReport().call(
        showing_report_id=showing_report.id,
        groups_titles=rex_groups_titles
    )

    rex_questions = flat_list([rex_group.questions for rex_group in rex_groups])
    SaveQuestions().call(
        rex_questions=rex_questions
    )

    completed_groups_dict = dict([(group.title, True) for group in groups if group.status == ReportGroupStatus.completed])
    return [rex_group.to_graphql(is_completed=completed_groups_dict.get(rex_group.title)) for rex_group in rex_groups]
