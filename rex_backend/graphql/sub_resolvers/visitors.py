import logging
from datetime import datetime

from rex_backend.exceptions import RexException
from rex_backend.graphql.enums import ShowingTypeEnum
from rex_backend.apis.rex_paperless_signin import rex_paperless_signin_api
from rex_backend.models.rex_showings import RexOpenHouse


logger = logging.getLogger('rex_backend')


class InvalidShowingTypeException(RexException):
    def __init__(self, showing):
        self.showing = showing

    def __str__(self):
        return f"Showing(id:{self.showing.id}) has invalid type {self.showing.type}"


def resolve_visitors(showing, info):
    if showing.type != ShowingTypeEnum.OPEN_HOUSE:
        raise InvalidShowingTypeException(showing)

    open_house = RexOpenHouse.query.filter(RexOpenHouse.showing_id == showing.id).first()
    if not open_house:
        raise InvalidShowingTypeException(showing)

    response = rex_paperless_signin_api.call_get_visitors(params={'open_house_guid': open_house.guid})

    if response.status_code != 200:
        logger.warn(f'REX visitors API on request "{response.url}" returned status:"{response.status_code}" text:"{response.text }"')
        raise RexException('REX visitors API got bad response')

    json_content = response.json()

    if not json_content.get('success'):
        logger.warn(f'REX visitors API on request "{response.url}" returned json:"{json_content}"')
        return []

    visitors = json_content.get('signins', [])

    for visitor in visitors:
        visited_at = visitor['created']
        if visited_at:
            visitor['visited_at'] = datetime.strptime(visited_at, '%a, %d %b %Y %H:%M:%S %Z')

    return visitors
