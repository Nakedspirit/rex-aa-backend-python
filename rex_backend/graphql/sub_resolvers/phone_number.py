from rex_backend.graphql.enums import ShowingTypeEnum
from rex_backend.apis.rex_paperless_signin import rex_paperless_signin_api
from rex_backend.apis import HttpAPIException


def resolve_phone_number(showing, info):
    if showing.type != ShowingTypeEnum.OPEN_HOUSE.value:
        return None

    try:
        api_result = rex_paperless_signin_api.call_get_phone_number(params={'listing_id': showing.listing_id})
    except HttpAPIException:
        return None

    phone_number = api_result.get('content', {}).get('phone_number', None)
    return phone_number
