from time import time as timer
import logging
from flask import request
from sentry_sdk import capture_exception


from rex_backend.interactors.jwt_authorization import JwtAuthorization
from rex_backend.interactors.link_app_user_to_rex_users import LinkUsersInteractor


logger = logging.getLogger('rex_backend')


class JWTAuthorizationMiddleware(object):
    SCHEMA_ROOTS = ["Mutation", "Query", "Subscription"]
    SKIPPED = ["signInOauth", "IntrospectionQuery", "__schema", "registerEvent"]

    def resolve(self, next, root, info, **args):
        root_name = info.parent_type.name
        if not self.is_schema_root(root_name):
            return next(root, info, **args)

        field_name = info.field_name
        if self.can_skip(field_name):
            return next(root, info, **args)

        auth_header = request.headers.get("Authorization", "")
        jwt = auth_header.split(' ')[-1]

        auth_result = JwtAuthorization().call(
            jwt=jwt,
            field_name=field_name
        )

        user = auth_result.get('user')
        is_demo_mode = auth_result.get('is_demo_mode')

        info.context.user = user
        info.context.identity = user.current_identity
        info.context.device = user.current_device

        if not is_demo_mode:
            LinkUsersInteractor().call(
                user=user,
                phone_number=user.contact_phone_number,
                email=user.contact_email
            )

        return next(root, info, **args)

    def is_schema_root(self, root_name):
        return root_name in self.SCHEMA_ROOTS

    def can_skip(self, field_name):
        return field_name in self.SKIPPED


class LogForRegisterEventMiddleware(object):
    def resolve(self, next, root, info, **args):
        field_name = info.field_name
        if field_name != 'registerEvent':
            return next(root, info, **args)

        result = next(root, info, **args)
        if isinstance(result.value, Exception):
            result_to_log = f'{result.value.__class__}: {str(result.value)}'
        else:
            result_to_log = result.value.__dict__

        msg = f'''
        Called registerEvent Mutation
        ---
        From: {info.context.remote_addr}
        Authorization header present: {info.context.headers.has_key('Authorization')  }
        Query: {info.context.values.get('query')}
        ---
        Result of call: {result_to_log}
        '''
        print(msg)

        return result


class LogResolvingTimeMiddleware(object):
    def resolve(self, next, root, info, **args):
        start = timer()
        return_value = next(root, info, **args)
        duration = timer() - start
        logger.info("{parent_type}.{field_name}: {duration} ms".format(
            parent_type=root._meta.name if root and hasattr(root, '_meta') else '',
            field_name=info.field_name,
            duration=round(duration * 1000, 2)
        ))
        return return_value


class SentryMiddleware(object):
    def resolve(self, next, root, info, **args):
        result = next(root, info, **args)

        if isinstance(result.value, Exception):
            capture_exception(result.value)

        return result
