from collections import defaultdict

from promise import Promise
from promise.dataloader import DataLoader

from rex_backend.models.rex_listings import RexListingImage, RexFileInfo


class PhotosUrlsLoader(DataLoader):
    def batch_load_fn(self, ids):
        query = RexListingImage.query.with_entities(
            RexListingImage.listing_id,
            RexFileInfo.public_url,
        ).join(
            RexFileInfo, RexFileInfo.id == RexListingImage.image_file_id
        ).filter(
            RexListingImage.listing_id.in_(ids)
        )
        images_data = query.all()

        images_dict = defaultdict(list)
        for image_data in images_data:
            images_dict[image_data.listing_id].append(image_data.public_url)

        return Promise.resolve(
            [images_dict.get(listing_id, [])
             for listing_id in ids]
        )
