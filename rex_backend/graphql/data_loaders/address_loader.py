from promise import Promise
from promise.dataloader import DataLoader

from rex_backend.models.rex_listings import RexListing, RexAddress


class AddressLoader(DataLoader):
    def batch_load_fn(self, ids):
        addresses_data = RexAddress.query.with_entities(
            RexAddress,
            RexListing.id.label('listing_id'),
        ).join(
            RexListing, RexListing.address_id == RexAddress.id
        ).filter(
            RexListing.id.in_(ids)
        ).all()

        addresses_hash = {address_data[1]: address_data[0]
                          for address_data in addresses_data}

        return Promise.resolve(
            [self.address_to_string(addresses_hash.get(id))
             for id in ids]
        )

    @staticmethod
    def address_to_string(address):
        address_points = [
            address.address_line,
            address.city,
            address.state,
        ]
        address_points = [p for p in address_points if p]
        return ', '.join(address_points)
