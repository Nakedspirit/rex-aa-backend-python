from promise import Promise
from promise.dataloader import DataLoader

from rex_backend.repositories.rex_showing_repo import RexShowingRepo
from rex_backend.graphql.adapters import to_user_info_graphql_type


class SellerForShowingLoader(DataLoader):
    def batch_load_fn(self, keys):
        rex_users = RexShowingRepo.load_showing_users(keys, 'Seller')
        users_infos = dict()
        for rex_user in rex_users:
            user_info = to_user_info_graphql_type(rex_user)
            users_infos[rex_user.rex_showing_id] = user_info

        return Promise.resolve([users_infos.get(key) for key in keys])
