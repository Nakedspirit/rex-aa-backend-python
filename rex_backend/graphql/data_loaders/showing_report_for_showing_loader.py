from promise import Promise
from promise.dataloader import DataLoader

from rex_backend.models.base import ShowingReport as ShowingReportModel
from rex_backend.graphql.adapters import to_graphql_showing_report


class ShowingReportForShowingLoader(DataLoader):
    def batch_load_fn(self, graphql_showings):
        graphql_showings_dict = dict(((sh.id, sh) for sh in graphql_showings))
        ids = graphql_showings_dict.keys()

        showing_reports = ShowingReportModel.query \
                                            .filter(ShowingReportModel.rex_showing_id.in_(ids)) \
                                            .all()
        graphql_showing_reports = dict()
        for showing_report in showing_reports:
            rex_showing_id = showing_report.rex_showing_id

            graphql_showing = graphql_showings_dict.get(rex_showing_id)
            graphql_showing_report = to_graphql_showing_report(showing_report)
            graphql_showing_report.showing = graphql_showing

            graphql_showing_reports[graphql_showing.id] = graphql_showing_report

        return Promise.resolve(
            [graphql_showing_reports.get(sh_id) for sh_id in ids]
        )
