from promise import Promise
from promise.dataloader import DataLoader

from rex_backend.models.rex_listings import RexListing
from rex_backend.graphql.adapters import to_graphql_listing


class ListingLoader(DataLoader):
    def batch_load_fn(self, ids):
        listings = {listing.id: listing
                    for listing in RexListing.query.filter(RexListing.id.in_(ids)).all()}

        return Promise.resolve([to_graphql_listing(listings.get(listing_id))
                                for listing_id in ids])
