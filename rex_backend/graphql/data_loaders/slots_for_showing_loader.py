from collections import defaultdict
from promise import Promise
from promise.dataloader import DataLoader

from rex_backend.models.rex_showings import RexShowingTimeSlot
from rex_backend.graphql.adapters import to_slot_graphql_type


class SlotsForShowingLoader(DataLoader):
    def batch_load_fn(self, keys):
        rex_slots = self.load_time_slots(keys)
        slots = defaultdict(list)
        for rex_slot in rex_slots:
            slot = to_slot_graphql_type(rex_slot)
            slots[rex_slot.conversation_id].append(slot)

        return Promise.resolve([self.__add_indexes(slots.get(showing_id, [])) for showing_id in keys])

    def load_time_slots(self, showing_ids):
        return RexShowingTimeSlot.query \
                                 .filter(RexShowingTimeSlot.conversation_id.in_(showing_ids)) \
                                 .all()

    def __add_indexes(self, slots):
        slots.sort(key=lambda obj: obj.id)
        for i, slot in enumerate(slots):
            slot.index = i + 1
        return slots
