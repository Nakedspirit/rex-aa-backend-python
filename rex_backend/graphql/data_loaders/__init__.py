import functools

from graphql.execution.base import ResolveInfo

from rex_backend.helpers import camel_to_snake
from .buyer_for_showing_loader import BuyerForShowingLoader
from .seller_for_showing_loader import SellerForShowingLoader
from .showing_report_for_showing_loader import ShowingReportForShowingLoader
from .slots_for_showing_loader import SlotsForShowingLoader
from .listing_loader import ListingLoader
from .address_loader import AddressLoader
from .photos_urls_loader import PhotosUrlsLoader


LOADERS = [
    SlotsForShowingLoader,
    ShowingReportForShowingLoader,
    BuyerForShowingLoader,
    SellerForShowingLoader,
    ListingLoader,
    AddressLoader,
    PhotosUrlsLoader,
]


# Decorator for initializing dataloaders for your graphql resolver
def init_data_loaders(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        for arg in args:
            if isinstance(arg, ResolveInfo):
                info = arg
                break

        if not info:
            raise Exception(f'object of type ResolveInfo not found in args {args}')

        for loader_class in LOADERS:
            name = camel_to_snake(loader_class.__name__)
            setattr(info.context, name, loader_class())
        return func(*args, **kwargs)
    return wrapper
