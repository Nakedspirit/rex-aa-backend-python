import graphene
from graphene import relay

from .connections import ShowingConnection
from .types import Showing, Listing, ShowingReport, ClaimedSinceLastVisit, ListingComment, UserStat
from .enums import UserStatEnum
from .resolvers import resolve_unclaimed_showings, resolve_upcoming_showings, resolve_listing, resolve_completed_showings, resolve_showing, resolve_showing_report, resolve_claimed_since_last_visit, resolve_listing_comments, resolve_leaderboard, resolve_gigs
from .resolvers.visitor_questions import resolve_visitor_questions, VisitorQuestionsResult


class Query(graphene.ObjectType):
    unclaimed_showings = relay.ConnectionField(
        ShowingConnection,
        resolver=resolve_unclaimed_showings
    )
    upcoming_showings = relay.ConnectionField(
        ShowingConnection,
        resolver=resolve_upcoming_showings
    )
    completed_showings = relay.ConnectionField(
        ShowingConnection,
        is_report_completed=graphene.Boolean(),
        resolver=resolve_completed_showings
    )
    gigs = relay.ConnectionField(
        ShowingConnection,
        max_meeting_datetime_of_showing=graphene.types.datetime.DateTime(description='default is now + 24 hours'),
        max_meeting_datetime_of_open_house=graphene.types.datetime.DateTime(description='default is now + 72 hours'),
        resolver=resolve_gigs
    )

    leaderboard = graphene.List(
        UserStat,
        stat_name=graphene.NonNull(UserStatEnum),
        date_end=graphene.types.datetime.Date(description='default is today'),
        date_start=graphene.types.datetime.Date(description='default is dateEnd - 30 days'),
        resolver=resolve_leaderboard
    )

    showing = graphene.Field(
        Showing,
        id=graphene.ID(),
        resolver=resolve_showing
    )
    listing = graphene.Field(
        Listing,
        id=graphene.ID(),
        resolver=resolve_listing
    )
    listingComments = graphene.Field(
        graphene.List(ListingComment),
        id=graphene.ID(),
        resolver=resolve_listing_comments
    )
    showing_report = graphene.Field(
        ShowingReport,
        showing_id=graphene.ID(),
        resolver=resolve_showing_report
    )

    claimed_since_last_visit = graphene.Field(
        ClaimedSinceLastVisit,
        resolver=resolve_claimed_since_last_visit
    )

    visitor_questions = graphene.Field(
        VisitorQuestionsResult,
        showing_id=graphene.ID(),
        visitor_id=graphene.ID(),
        resolver=resolve_visitor_questions
    )
