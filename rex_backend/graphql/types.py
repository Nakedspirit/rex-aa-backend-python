import graphene
from graphene.types.datetime import DateTime
from .enums import ShowingTypeEnum, ShowingStatus, ShowingReportStatus, UserStatEnum
from rex_backend.graphql.sub_resolvers.resolve_groups import resolve_groups
from rex_backend.graphql.sub_resolvers.visitors import resolve_visitors
from rex_backend.graphql.sub_resolvers.phone_number import resolve_phone_number
from rex_backend.graphql.sub_resolvers.open_house_photos import resolve_open_house_photos


class ValidationProblem(graphene.ObjectType):
    field = graphene.NonNull(graphene.String)
    message = graphene.NonNull(graphene.String)


class NotFoundProblem(graphene.ObjectType):
    entity = graphene.NonNull(graphene.String)
    message = graphene.NonNull(graphene.String)


class SignInOk(graphene.ObjectType):
    jwt = graphene.NonNull(graphene.String)
    is_developer = graphene.Boolean()
    disable_mutations = graphene.Boolean()
    demo_mode = graphene.Boolean()


class Showing(graphene.ObjectType):
    id = graphene.NonNull(graphene.ID)
    guid = graphene.ID()
    listing_id = graphene.NonNull(graphene.ID)
    type = graphene.NonNull(ShowingTypeEnum)
    status = ShowingStatus()
    slots = graphene.List(graphene.NonNull(lambda: Slot))
    duration = graphene.Int()
    buyer = graphene.Field(lambda: UserInfo)
    seller = graphene.Field(lambda: UserInfo)
    listing = graphene.Field(lambda: Listing)
    showing_report = graphene.Field(lambda: ShowingReport)

    # Open House special fields
    phone_number = graphene.String(
        resolver=resolve_phone_number,
        description='Open House phone number. This is synchronous query. Query it only for the one Showing.'
    )
    visitors = graphene.List(
        lambda: Visitor,
        resolver=resolve_visitors,
        description='Open House visitors. This is synchronous query. Query it only for the one Showing.'
    )
    photos = graphene.List(
        graphene.String,
        resolver=resolve_open_house_photos,
        description="Open House photos. This is synchronous query. Query it only for the one Showing."
    )

    showing_instructions = graphene.String()
    need_seller_available = graphene.Boolean()
    need_rex_showing_representative = graphene.Boolean()
    is_outside_agent = graphene.Boolean()

    final_showing_time = DateTime()
    created_at = DateTime()
    claimed_at = DateTime()
    submitted_for_payment_at = DateTime()

    def resolve_listing(self, info):
        loader = info.context.listing_loader
        return loader.load(self.listing_id)

    def resolve_slots(self, info):
        loader = info.context.slots_for_showing_loader
        return loader.load(self.id)

    def resolve_buyer(self, info):
        loader = info.context.buyer_for_showing_loader
        return loader.load(self.id)

    def resolve_seller(self, info):
        loader = info.context.seller_for_showing_loader
        return loader.load(self.id)

    def resolve_showing_report(self, info):
        loader = info.context.showing_report_for_showing_loader
        return loader.load(self)


class Slot(graphene.ObjectType):
    id = graphene.NonNull(graphene.ID)
    index = graphene.NonNull(graphene.Int)
    time = graphene.NonNull(graphene.types.datetime.DateTime)
    selected = graphene.Boolean()
    showing = graphene.NonNull(lambda: Showing)


class Listing(graphene.ObjectType):
    id = graphene.NonNull(graphene.ID)
    ac = graphene.String()
    address = graphene.String()
    description = graphene.String()
    fireplace = graphene.String()
    heating = graphene.String()
    hoafee = graphene.Float()
    house_type = graphene.String()
    lot_square_foot = graphene.Float()
    price_per_square_foot = graphene.Float()
    square_foot = graphene.Float()
    number_of_baths = graphene.Int()
    number_of_beds = graphene.Int()
    parking_spaces = graphene.String()
    parking_type = graphene.String()
    pool = graphene.String()
    stories = graphene.Float()
    year_built = graphene.Int()
    total_price = graphene.Float()
    photos = graphene.List(graphene.String)
    latitude = graphene.Float()
    longitude = graphene.Float()

    def resolve_address(self, info):
        loader = info.context.address_loader
        return loader.load(self.id)

    def resolve_photos(self, info):
        loader = info.context.photos_urls_loader
        return loader.load(self.id)

    def resolve_price_per_square_foot(self, info):
        if self.total_price and self.square_foot:
            return self.total_price / self.square_foot


class ListingComment(graphene.ObjectType):
    question = graphene.NonNull(graphene.String)
    answer = graphene.String()


class ShowingReport(graphene.ObjectType):
    id = graphene.NonNull(graphene.ID)
    showing = graphene.NonNull(lambda: Showing)

    status = graphene.NonNull(ShowingReportStatus)
    status_comment = graphene.String()
    status_added_at = graphene.types.datetime.DateTime()

    is_questions_completed = graphene.Boolean()
    is_call_buyer_completed = graphene.Boolean()
    is_ready_for_payment = graphene.Boolean()
    groups = graphene.List(lambda: QuestionGroup, resolver=resolve_groups)

    submitted_for_payment_at = graphene.types.datetime.DateTime()

    def resolve_is_ready_for_payment(self, info):
        if self.status == ShowingReportStatus.buyer_not_appeared:
            return True

        return self.status == ShowingReportStatus.ok.value and self.is_questions_completed


class QuestionGroup(graphene.ObjectType):
    id = graphene.NonNull(graphene.ID, deprecation_reason='We moved to REX API')
    title = graphene.NonNull(graphene.String)
    order = graphene.Int()
    is_completed = graphene.Boolean()
    questions = graphene.List(lambda: ReportQuestion)


class QuestionOption(graphene.ObjectType):
    text = graphene.NonNull(graphene.String)
    value = graphene.NonNull(graphene.String)
    selected = graphene.Boolean(deprecation_reason='We moved to REX API')


class MultipleQuestion(graphene.ObjectType):
    id = graphene.NonNull(graphene.ID)
    title = graphene.String()
    is_required = graphene.Boolean()
    order = graphene.Int()
    options = graphene.List(QuestionOption)


class SingularQuestion(graphene.ObjectType):
    id = graphene.NonNull(graphene.ID)
    title = graphene.String()
    is_required = graphene.Boolean()
    order = graphene.Int()
    options = graphene.List(QuestionOption)


class TextQuestion(graphene.ObjectType):
    id = graphene.NonNull(graphene.ID)
    title = graphene.String()
    is_required = graphene.Boolean()
    order = graphene.Int()
    placeholder = graphene.String(deprecation_reason='We moved to REX API')
    response = graphene.String()


class ReportQuestion(graphene.Union):
    class Meta:
        types = (MultipleQuestion, SingularQuestion, TextQuestion)

    def resolve_type(self, info):
        return self.type


class Question(graphene.Union):
    class Meta:
        types = (MultipleQuestion, SingularQuestion, TextQuestion)

    def resolve_type(self, info):
        return self.type


class UserStat(graphene.ObjectType):
    user_name = graphene.NonNull(graphene.String)
    name = graphene.NonNull(UserStatEnum)
    value = graphene.NonNull(graphene.Int)
    position = graphene.NonNull(graphene.Int)
    position_change = graphene.Int()
    is_current_user = graphene.Boolean()


class UserInfo(graphene.ObjectType):
    id = graphene.NonNull(graphene.ID)
    first_name = graphene.String()
    last_name = graphene.String()
    email = graphene.String()
    phone_number = graphene.String()
    user_picture = graphene.String()
    role = graphene.String()


class ClaimedSinceLastVisit(graphene.ObjectType):
    last_visit_at = DateTime()
    amount_of_claimed = graphene.Int()


class Visitor(graphene.ObjectType):
    id = graphene.NonNull(graphene.ID)
    name = graphene.String(required=True)
    email = graphene.String()
    phone_number = graphene.String()
    visited_at = graphene.DateTime()
