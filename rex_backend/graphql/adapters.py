from rex_backend.models.base import User
from .enums import ShowingTypeEnum
from .types import Showing, Listing, ShowingReport, UserInfo, Slot
from rex_backend.utils.tz import set_tz


def to_graphql_showing(rex_showing):
    tz = rex_showing.timezone if hasattr(rex_showing, 'timezone') else None
    created_at = rex_showing.created_at
    claimed_at = rex_showing.claimed_at if hasattr(rex_showing, 'claimed_at') else None

    final_showing_time = None
    if rex_showing.final_meeting_time and tz:
        final_showing_time = set_tz(rex_showing.final_meeting_time, tz)

    showing_type = ShowingTypeEnum.REGULAR
    if hasattr(rex_showing, 'open_house_id') and rex_showing.open_house_id:
        showing_type = ShowingTypeEnum.OPEN_HOUSE

    duration = None
    if hasattr(rex_showing, 'open_house_duration') and rex_showing.open_house_duration:
        duration = rex_showing.open_house_duration

    guid = None
    if hasattr(rex_showing, 'open_house_guid'):
        guid = rex_showing.open_house_guid

    return Showing(
        id=rex_showing.id,
        guid=guid,
        listing_id=int(rex_showing.data.get('listingId')),
        type=showing_type,
        status=rex_showing.status.upper(),
        showing_report=None,
        showing_instructions=rex_showing.data.get('agentsShowingInstructions'),
        need_seller_available=rex_showing.data.get('needSellerAvailable'),
        need_rex_showing_representative=rex_showing.data.get('needRexShowingRepresentative'),
        is_outside_agent=rex_showing.data.get('isOutsideAgent'),
        created_at=created_at,
        claimed_at=claimed_at,
        final_showing_time=final_showing_time,
        submitted_for_payment_at=None,
        duration=duration,
    )


def to_graphql_showing_report(showing_report):
    return ShowingReport(
        id=showing_report.id,
        status=showing_report.status.name,
        status_comment=showing_report.status_comment,
        status_added_at=showing_report.status_added_at,
        submitted_for_payment_at=showing_report.submitted_for_payment_at,
        is_questions_completed=showing_report.is_questions_completed,
        is_call_buyer_completed=showing_report.is_call_buyer_completed,
    )


def to_user_info_graphql_type(rex_user):
    if isinstance(rex_user, User):
        return UserInfo(
            id=rex_user.rex_user_id,
            first_name=rex_user.first_name,
            last_name=rex_user.last_name,
            email=rex_user.contact_email,
            phone_number=rex_user.contact_phone_number,
            role=''
        )

    return UserInfo(
        id=rex_user.id,
        first_name=rex_user.first_name,
        last_name=rex_user.last_name,
        email=rex_user.email_address,
        phone_number=rex_user.phone_number,
        role=rex_user.role.lower()
    )


def to_slot_graphql_type(rex_time_slot):
    selected = None if rex_time_slot.is_rejected is None else (not rex_time_slot.is_rejected)
    return Slot(
        id=rex_time_slot.id,
        time=rex_time_slot.start,
        selected=selected
    )


def to_graphql_listing(rex_listing):
    if not rex_listing:
        return None

    return Listing(
        id=rex_listing.id,
        ac=rex_listing.air_conditioning_type,
        description=rex_listing.description,
        fireplace=rex_listing.fireplace_indicator_flag,
        heating=rex_listing.heating_type,
        hoafee=rex_listing.hoa_fee,
        house_type=rex_listing.property_type,
        lot_square_foot=rex_listing.land_square_footage,
        square_foot=rex_listing.living_square_feet,
        number_of_baths=rex_listing.total_baths,
        number_of_beds=rex_listing.bedrooms,
        parking_spaces=rex_listing.parking_spaces,
        parking_type=rex_listing.parking_type_code,
        pool=rex_listing.pool_flag,
        stories=rex_listing.stories,
        year_built=rex_listing.year_built,
        total_price=rex_listing.price,
        photos=[image.file_info.public_url for image in rex_listing.images],  # MOVE INTO LOADER
        latitude=rex_listing.latitude,
        longitude=rex_listing.longitude,
    )
