from datetime import datetime, timedelta


from rex_backend.utils.tz import tznow
from rex_backend.interactors.leaderboard import Leaderboard


def resolve_leaderboard(_, info, stat_name, date_start=None, date_end=None):
    current_user = info.context.user
    date_end = date_end if date_end else datetime.date(tznow())
    date_start = date_start if date_start else date_end - timedelta(days=30)

    if date_start > date_end:
        raise Exception('dateStart can not be greater than dateEnd')

    return Leaderboard().call(
        current_user=current_user,
        stat_name=stat_name,
        date_start=date_start,
        date_end=date_end,
    )
