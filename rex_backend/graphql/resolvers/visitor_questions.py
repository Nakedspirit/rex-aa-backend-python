import graphene


from rex_backend.graphql.data_loaders import init_data_loaders
from rex_backend.graphql.types import Question, NotFoundProblem
from rex_backend.models.rex_listings import RexListing
from rex_backend.apis.rex_questions import rex_questions_api, RexQuestionsContainer, make_source_for_visitors_questions, TOOL


class VisitorQuestionsOk(graphene.ObjectType):
    questions = graphene.List(Question)


class VisitorQuestionsNotOk(graphene.ObjectType):
    message = graphene.NonNull(graphene.String)


class VisitorQuestionsResult(graphene.Union):
    class Meta:
        types = (NotFoundProblem, VisitorQuestionsNotOk, VisitorQuestionsOk)


@init_data_loaders
def resolve_visitor_questions(_, info, showing_id, visitor_id):
    current_user = info.context.user

    rex_showing = current_user.showing_query(showing_id).first()
    if not rex_showing or not rex_showing.open_house_guid:
        return NotFoundProblem(
            entity='Open House',
            message='Open House not found.'
        )

    rex_listing_id = int(rex_showing.data.get('listingId'))
    rex_listing = RexListing.query.filter(RexListing.id == rex_listing_id).first()
    if not rex_listing:
        return NotFoundProblem(
            entity='Listing',
            message='Listing not found.'
        )

    try:
        questions = get_questions(rex_listing.guid, showing_id, visitor_id)
    except BadAPIResponse as exception:
        return VisitorQuestionsNotOk(
            message=exception.message
        )

    return VisitorQuestionsOk(
        questions=[question.to_graphql() for question in questions]
    )


class BadAPIResponse(Exception):
    def __init__(self, message):
        self.message = message


def get_questions(rex_listing_guid, showing_id, visitor_id):
    tool = TOOL.get('VISITOR_QUESTIONS')
    questions_api_result = rex_questions_api.call_get_questions(params={
        'tool': tool
    })

    if not questions_api_result.get('content', {}).get('success'):
        raise BadAPIResponse('REX API returned bad response.')

    questions_container = RexQuestionsContainer(questions_api_result)

    answers_api_result = rex_questions_api.call_get_questions_answers(params={
        'listing_guid': rex_listing_guid,
        'tool': tool,
        'source': make_source_for_visitors_questions(showing_id, visitor_id),
    })

    if not answers_api_result.get('content', {}).get('success'):
        raise BadAPIResponse('REX API returned bad response.')

    questions_container.process_answers(answers_api_result)

    return questions_container.questions
