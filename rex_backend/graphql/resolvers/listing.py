from rex_backend.models.rex_listings import RexListing
from rex_backend.graphql.data_loaders import init_data_loaders
from rex_backend.graphql.adapters import to_graphql_listing


@init_data_loaders
def resolve_listing(_, info, id):
    rex_listing = load_listing(id)
    if rex_listing:
        return to_graphql_listing(rex_listing)


def load_listing(id):
    return RexListing.query \
                     .filter(RexListing.id == id) \
                     .first()
