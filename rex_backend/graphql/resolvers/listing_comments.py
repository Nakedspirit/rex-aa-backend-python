from .listing import load_listing
from rex_backend.apis.rex_questions import rex_questions_api


def resolve_listing_comments(_, info, id):
    listing = load_listing(id)
    if not listing:
        return []

    data_of_agents_questions = rex_questions_api.call_get_questions_answers(params={
        'tool': 'aasurvey',
        'listing_guid': listing.guid
    })
    data_of_seller_questions = rex_questions_api.call_get_questions_answers(params={
        'tool': 'sellersurvey',
        'listing_guid': listing.guid
    })

    agent_elems = sort_agents_elems(data_of_agents_questions.get('content', {}).get('result', []))
    seller_elems = data_of_seller_questions.get('content', {}).get('result', [])
    all_elems = [item for item in (agent_elems + seller_elems) if item]

    return elems_to_graphql_dicts(all_elems)


def sort_agents_elems(elems):
    special_elems = {
        'lockbox_code': None,
        'showing_restrictions': None,
        'access_information': None
    }
    another_elems = []
    for elem in elems:
        elem_name = elem.get('name')
        if elem_name in special_elems:
            special_elems[elem_name] = elem
        else:
            another_elems.append(elem)

    return [
        special_elems.get('lockbox_code'),
        special_elems.get('showing_restrictions'),
        special_elems.get('access_information'),
        *another_elems
    ]


def elems_to_graphql_dicts(elems):
    if not elems:
        return []

    def to_graphql_listing_comment(listing_comment):
        answers = listing_comment.get('answer') or []
        first_answer = answers[0] if len(answers) else {}
        answer = first_answer.get('answer_text', '')
        return {
            'question': listing_comment.get('question'),
            'answer': answer
        }

    return list(map(to_graphql_listing_comment, elems))
