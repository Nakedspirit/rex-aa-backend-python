from rex_backend.models.rex_showings import RexShowing
from rex_backend.models.rex_listings import RexListing
from ._shared import apply_pagination_and_load
from rex_backend.graphql.data_loaders import init_data_loaders
from rex_backend.graphql.adapters import to_graphql_showing, to_graphql_listing
from rex_backend.repositories.showing_report_repo import ShowingReportRepo


@init_data_loaders
def resolve_completed_showings(_, info, **kwargs):
    current_user = info.context.user

    if not current_user.is_ready_to_make_queries:
        return []

    completed_showing_reports = ShowingReportRepo.load_completed_showing_reports()
    completed_rex_showings_ids = [showing_report.rex_showing_id for showing_report in completed_showing_reports]

    is_report_completed = kwargs.get('is_report_completed')
    current_user = info.context.user
    query = current_user.completed_showings_query()

    if is_report_completed:
        query = query.filter(RexShowing.id.in_(completed_rex_showings_ids))
    else:
        if len(completed_rex_showings_ids) > 0:
            query = query.filter(RexShowing.id.notin_(completed_rex_showings_ids))

    rex_showings = apply_pagination_and_load(query, kwargs.get('first'), kwargs.get('last'))

    if len(rex_showings) == 0:
        return []

    # TODO: rewrite the code below
    ids_dict = dict(
        [(rex_showing.id, int(rex_showing.data.get('listingId')))
         for rex_showing in rex_showings
         if rex_showing.data.get('listingId')]
    )

    rex_listing_ids = ids_dict.values()
    rex_listings = RexListing.query.filter(RexListing.id.in_(rex_listing_ids)).all()
    rex_listings_dict = dict([(int(rex_listing.id), rex_listing) for rex_listing in rex_listings])

    rex_showing_id_rex_listing_dict = {}
    for rex_showing_id, rex_listing_id in ids_dict.items():
        rex_showing_id = int(rex_showing_id)
        rex_listing_id = int(rex_listing_id)
        rex_listing = rex_listings_dict.get(rex_listing_id)
        if rex_listing:
            rex_showing_id_rex_listing_dict[rex_showing_id] = rex_listing

    graphql_showings = []
    for rex_showing in rex_showings:
        rex_listing = rex_showing_id_rex_listing_dict.get(rex_showing.id)
        if rex_listing:
            graphql_showing = to_graphql_showing(rex_showing)
            graphql_showing.listing = to_graphql_listing(rex_listing)
            graphql_showings.append(graphql_showing)

    return graphql_showings
