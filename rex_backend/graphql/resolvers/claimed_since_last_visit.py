from rex_backend.graphql.types import ClaimedSinceLastVisit
from rex_backend.repositories.rex_showing_repo import RexShowingRepo
from rex_backend.models.rex_showings import RexBlockAttribute
from rex_backend.utils.tz import tznow


def resolve_claimed_since_last_visit(_, info, **kwargs):
    last_visit_at = info.context.device.closed_at
    if last_visit_at is None:
        last_visit_at = info.context.identity.sign_out_at

    if not last_visit_at:
        return ClaimedSinceLastVisit(
            last_visit_at=None,
            amount_of_claimed=0
        )

    amount_of_claimed = 0
    rex_users_ids = info.context.user.rex_users_ids
    if rex_users_ids:
        amount_of_claimed = already_claimed_count(
            rex_users_ids,
            last_visit_at
        )

    return ClaimedSinceLastVisit(
        last_visit_at=last_visit_at,
        amount_of_claimed=amount_of_claimed
    )


def already_claimed_count(rex_users_ids, last_visit_at):
    return int(
        RexShowingRepo.rex_already_claimed_showings_for_user_query(rex_users_ids)
        .filter(RexBlockAttribute.created_at < tznow())
        .filter(RexBlockAttribute.created_at >= last_visit_at)
        .count()
    )
