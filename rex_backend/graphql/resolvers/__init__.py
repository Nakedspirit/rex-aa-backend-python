from rex_backend.utils.auto_module_loader import AutoModuleLoader, DefaultConvention


class ResolverConvetion(DefaultConvention):
    '''
    Defines which python object to load from python module
    Example: from `unclaimed_showings.py` it will load `resolve_unclaimed_showings`
    '''
    def apply(self, module):
        module_name = super().apply(module)
        return f'resolve_{module_name}'


# The code below automatically load all resolvers for you
# Just use `form rex_backend.resolvers import your_resolver`
loader = AutoModuleLoader(__name__, convention=ResolverConvetion())
loader.load_objects_into_current_package()
loader.write_objects_into_all_variable()
