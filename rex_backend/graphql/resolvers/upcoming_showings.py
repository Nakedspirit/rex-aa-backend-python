from ._shared import apply_pagination_and_load
from rex_backend.graphql.data_loaders import init_data_loaders
from rex_backend.graphql.adapters import to_graphql_showing
from rex_backend.wrappers.extended_rex_showing import ExtendedRexShowing


@init_data_loaders
def resolve_upcoming_showings(_, info, **kwargs):
    current_user = info.context.user

    if not current_user.is_ready_to_make_queries:
        return []

    query = current_user.upcoming_showings_query()
    query = current_user.apply_hide_showings_condition(query)
    rex_showings = apply_pagination_and_load(query, kwargs.get('first'), kwargs.get('last'))
    rex_showings = [rex_showing
                    for rex_showing in ExtendedRexShowing.init_from_list(rex_showings)
                    if rex_showing.rex_listing_id]

    return [to_graphql_showing(rex_showing) for rex_showing in rex_showings]
