def apply_pagination_and_load(query, first=None, last=None):
    offset = count_offset_by_first(first)
    if offset:
        query = query.offset(offset)

    limit = count_limit_by_first_and_last(first, last)
    if limit:
        query = query.limit(limit)

    return query.all()


def count_offset_by_first(first):
    return None if first is None else int(first) - 1


def count_limit_by_first_and_last(first, last):
    if last is None or last < 1:
        return None

    first = 1 if first is None or first > 1 else int(first)
    return last - (first - 1)
