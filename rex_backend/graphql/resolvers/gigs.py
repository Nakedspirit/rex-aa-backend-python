import datetime

from rex_backend.graphql.data_loaders import init_data_loaders
from rex_backend.graphql.adapters import to_graphql_showing
from rex_backend.wrappers.extended_rex_showing import ExtendedRexShowing


@init_data_loaders
def resolve_gigs(_, info, max_meeting_datetime_of_showing=None, max_meeting_datetime_of_open_house=None):
    current_user = info.context.user

    if not current_user.is_ready_to_make_queries:
        return []

    max_meeting_datetime_of_showing = init_datetime_param(max_meeting_datetime_of_showing, hours=24)
    max_meeting_datetime_of_open_house = init_datetime_param(max_meeting_datetime_of_open_house, hours=72)

    rex_showings = current_user.gigs_query(
        max_meeting_datetime_of_showing=max_meeting_datetime_of_showing,
        max_meeting_datetime_of_open_house=max_meeting_datetime_of_open_house,
    ).all()

    uniq_rex_showings = []
    seen = set()
    for showing in rex_showings:
        if showing.id not in seen:
            seen.add(showing.id)
            uniq_rex_showings.append(showing)

    rex_showings = [rex_showing
                    for rex_showing in ExtendedRexShowing.init_from_list(uniq_rex_showings)
                    if rex_showing.rex_listing_id]

    return [to_graphql_showing(rex_showing) for rex_showing in rex_showings]


def init_datetime_param(dt, hours=24):
    if dt:
        return dt

    return datetime.datetime.now() + datetime.timedelta(hours=hours)
