from rex_backend.graphql.data_loaders import init_data_loaders
from rex_backend.graphql.adapters import to_graphql_showing


@init_data_loaders
def resolve_showing(_, info, id):
    current_user = info.context.user
    rex_showing = load_showing(id, current_user)
    if rex_showing:
        return to_graphql_showing(rex_showing)


def load_showing(showing_id, current_user):
    return current_user.showing_query(showing_id).first()
