from rex_backend.application import app
from ._shared import apply_pagination_and_load
from rex_backend.graphql.data_loaders import init_data_loaders
from rex_backend.graphql.adapters import to_graphql_showing
from rex_backend.wrappers.extended_rex_showing import ExtendedRexShowing


DAYS_TO_SHOW = int(app.config['DAYS_TO_SHOW_ALREADY_CLAIMED'])


@init_data_loaders
def resolve_unclaimed_showings(_, info, **kwargs):
    current_user = info.context.user

    if not current_user.is_ready_to_make_queries:
        return []

    query = current_user.unclaimed_showings_query()
    query = current_user.apply_hide_showings_condition(query)
    rex_showings = apply_pagination_and_load(query, kwargs.get('first'), kwargs.get('last'))
    not_filtered_showings = [rex_showing
                             for rex_showing in ExtendedRexShowing.init_from_list(rex_showings)
                             if rex_showing.rex_listing_id]

    showings = []
    for showing in not_filtered_showings:
        claimed_by_current_user = showing.is_claimed_by(current_user)
        claimed_recently = showing.is_claimed_recently(days=DAYS_TO_SHOW)

        if showing.status == 'INITIALIZED':
            showings.append(showing)
            continue

        if showing.status == 'PENDING' and claimed_by_current_user:
            showings.append(showing)
            continue

        if showing.status == 'PENDING' and not claimed_by_current_user and claimed_recently:
            showing.status = 'ALREADY_CLAIMED'
            showings.append(showing)
            continue

        if showing.status in ('FAILED', 'CANCELLED') and claimed_by_current_user and claimed_recently:
            showings.append(showing)
            continue

    return [to_graphql_showing(showing) for showing in showings]
