from rex_backend.graphql.data_loaders import init_data_loaders
from rex_backend.graphql.adapters import to_graphql_showing_report, to_graphql_showing


@init_data_loaders
def resolve_showing_report(_, info, showing_id):
    current_user = info.context.user
    showing_report = load_showing_report(current_user, showing_id)
    if not showing_report:
        return None

    rex_showing = showing_report.rex_showing
    if not rex_showing:
        return None

    showing_report = to_graphql_showing_report(showing_report)
    showing_report.showing = to_graphql_showing(rex_showing)

    return showing_report


def load_showing_report(current_user, showing_id):
    return current_user.showing_report_query(showing_id).first()
