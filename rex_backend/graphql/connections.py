from graphene import relay
from .types import Showing, UserStat


class ShowingConnection(relay.Connection):
    class Meta:
        node = Showing


class UserStatsConnection(relay.Connection):
    class Meta:
        node = UserStat
