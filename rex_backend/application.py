import os
import logging
import logging.config
import sentry_sdk


from flask import Flask
from sentry_sdk.integrations.flask import FlaskIntegration


from rex_backend.utils.db import DatabaseManager
from rex_backend.utils.jwt import JwtService
from rex_backend.utils.rex_scheduler_api import RexSchedulerApi
from rex_backend.utils.google_oauth_service import GoogleOAuthService
from rex_backend.utils.google_table_service import GoogleTableService
from rex_backend.models.base import BaseModel
from rex_backend.models.rex_showings import RexShowingsBaseModel
from rex_backend.models.rex_listings import RexListingsBaseModel
from rex_backend.sentry_config import sentry_before_send_hook


def init_db(app, database_url: str, base_model) -> DatabaseManager:
    db = DatabaseManager(database_url, base_model)
    db.connect()
    app.db = app

    @app.teardown_appcontext
    def shutdown_session(error):
        if not app.config['TESTING']:
            db.close_session()
        return error

    return db


def create_app() -> Flask:
    app = Flask(__name__)

    app.config.from_pyfile('settings/common.py')
    try:
        app.config.from_envvar('APP_SETTINGS_FILE')
    except RuntimeError:
        pass

    logging.config.dictConfig(app.config['LOGGING'])
    app.logger.debug('Welcome to rex_backend! Running app...')

    return app


is_debugging_env = os.environ.get('DEBUG') is not None
if is_debugging_env:
    import ptvsd

    # Allow VSCode attach a debugger on this port
    # make sure docker-compose opens this port:
    # - ports: 3005:3005
    try:
        ptvsd.enable_attach(address=('0.0.0.0', 3005), redirect_output=True)
    except Exception as exc:
        print(exc)

    # Pause the program until a remote debugger is attached
    # ptvsd.wait_for_attach()

is_testing_env = os.environ.get('TESTING') is not None

sentry_connection = None if is_testing_env else os.environ.get('SENTRY_CONNECTION')
sentry_sdk.init(
    dsn=sentry_connection,
    integrations=[FlaskIntegration()],
    before_send=sentry_before_send_hook
)

app: Flask = create_app()
logger = app.logger

db = init_db(app, app.config['DATABASE_URL'], BaseModel)
rex_showings_db = init_db(
    app,
    app.config['REX_SHOWINGS_DATABASE_URL'],
    RexShowingsBaseModel
)
rex_listings_db = init_db(
    app,
    app.config['REX_LISTINGS_DATABASE_URL'],
    RexListingsBaseModel
)


jwt_service = JwtService(app)
google_oauth_service = GoogleOAuthService(
    app.config['GOOGLE_OAUTH_WEB_CLIENT_IDS'],
    app.config['GOOGLE_OAUTH_DOMAIN_ALLOWED_TO_SIGNIN']
)
rex_scheduler_api_service = RexSchedulerApi(
    app.config['REX_SCHEDULER_API_URL'],
    app.config['REX_SCHEDULER_API_KEY']
)
google_table_service = GoogleTableService(
    app.config['GOOGLE_CLOUD_BASE64_CERT'],
    app.config['GOOGLE_SPREADSHEET_ID'],
    app.config['GOOGLE_SPREADSHEET_WHITELIST_RANGE']
)

from rex_backend.views import *  # noqa: F403, F401

import rex_backend.profile_sql_queries  # noqa: F401
