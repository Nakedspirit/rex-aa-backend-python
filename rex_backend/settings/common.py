import os


from rex_backend.helpers import env_var_to_bool, make_connection_string, env_to_list


# Basic Application
ENV_NAME = os.getenv('ENV_NAME', 'local').lower()
TESTING = env_var_to_bool(os.getenv('TESTING'))
TIMEZONE = 'UTC'
ENABLE_PLAYGROUND = env_var_to_bool(os.environ.get('ENABLE_PLAYGROUND', ''))
DEBUG = env_var_to_bool(os.environ.get('DEBUG', ''))
JWT_SECRET_KEY = os.environ.get('JWT_SECRET_KEY')
REGISTER_EVENT_SECRET_KEY = os.environ.get('REGISTER_EVENT_SECRET_KEY')
FIREBASE_CM_BASE64_CERT = os.environ.get('FIREBASE_CM_BASE64_CERT')
FIREBASE_CM_FAKE_MODE = env_var_to_bool(os.environ.get('FIREBASE_CM_FAKE_MODE', False))
DAYS_TO_SHOW_ALREADY_CLAIMED = os.environ.get('DAYS_TO_SHOW_ALREADY_CLAIMED') or 1
DEMO_REX_USER_EMAIL = os.environ.get('DEMO_REX_USER_EMAIL')
HOURS_TO_SHOW_SHOWINGS_IN_DEMO_MODE = os.environ.get('HOURS_TO_SHOW_SHOWINGS_IN_DEMO_MODE') or 12


# Google OAuth / Sign in OAuth
google_oauth_web_client_ids = os.environ.get('GOOGLE_OAUTH_WEB_CLIENT_IDS')
if type(google_oauth_web_client_ids) == str:
    GOOGLE_OAUTH_WEB_CLIENT_IDS = [client_id for client_id in google_oauth_web_client_ids.split(',') if client_id]
else:
    GOOGLE_OAUTH_WEB_CLIENT_IDS = []
GOOGLE_OAUTH_DOMAIN_ALLOWED_TO_SIGNIN = os.environ.get('GOOGLE_OAUTH_DOMAIN_ALLOWED_TO_SIGNIN')


# Google Table Service / Whitelisting feature
GOOGLE_CLOUD_BASE64_CERT = os.environ.get('GOOGLE_CLOUD_BASE64_CERT')
GOOGLE_SPREADSHEET_ID = os.environ.get('GOOGLE_SPREADSHEET_ID')
GOOGLE_SPREADSHEET_WHITELIST_RANGE = os.environ.get('GOOGLE_SPREADSHEET_WHITELIST_RANGE')
GOOGLE_SPREADSHEET_DEVELOPER_RANGE = os.environ.get('GOOGLE_SPREADSHEET_DEVELOPER_RANGE')


# AWS Simple Email Service
AWS_SES_ACCESS_KEY = os.environ.get('AWS_SES_ACCESS_KEY')
AWS_SES_SECRET_KEY = os.environ.get('AWS_SES_SECRET_KEY')
AWS_SES_REGION = os.environ.get('AWS_SES_REGION')
AWS_SES_SENDER = os.environ.get('AWS_SES_SENDER')
DEFAULT_RECEIVER = os.environ.get('DEFAULT_RECEIVER')
EMAILS_BCC = env_to_list(os.environ.get('EMAILS_BCC'))
AVOID_REAL_USERS_EMAILS = env_var_to_bool(os.environ.get('AVOID_REAL_USERS_EMAILS'))


# REX APIs
REX_SCHEDULER_API_URL = os.environ.get('REX_SCHEDULER_API_URL')
REX_SCHEDULER_API_KEY = os.environ.get('REX_SVC_SCHEDULER_API_CLIENT_KEY') or os.environ.get('REX_SCHEDULER_API_KEY')
REX_LISTING_API_URL = os.environ.get('REX_LISTING_API_URL')
REX_SVC_RESPONSIBILITIES_URL = os.environ.get('REX_SVC_RESPONSIBILITIES_URL')
REX_SVC_RESPONSIBILITIES_INTERNAL_TOKEN = os.environ.get('REX_SVC_RESPONSIBILITIES_INTERNAL_TOKEN')
REX_PAPERLESS_SIGNIN_URL = os.environ.get('REX_PAPERLESS_SIGNIN_URL')
REX_PAPERLESS_SIGNIN_PASSWORD = os.environ.get('REX_PAPERLESS_SIGNIN_PASSWORD')
REX_BONOBONO_API_URL = os.environ.get('REX_BONOBONO_API_URL')


# Showings DB environment variables
rex_postgres_host = os.environ.get("REX_DB_DATA_DB_HOST")
rex_postgres_port = os.environ.get("REX_DB_DATA_DB_PORT")

rex_scheduler_user = os.environ.get("REX_DB_SCHEDULER_USER")
rex_scheduler_password = os.environ.get("REX_DB_SCHEDULER_PASSWORD")
rex_scheduler_db_name = os.environ.get("REX_DB_SCHEDULER_PATH")

rex_showings_database_url = os.environ.get('TESTING_REX_SHOWINGS_DATABASE_URL') if TESTING else os.environ.get('REX_SHOWINGS_DATABASE_URL')
if rex_showings_database_url:
    REX_SHOWINGS_DATABASE_URL = rex_showings_database_url
else:
    REX_SHOWINGS_DATABASE_URL = make_connection_string(
        "postgresql",
        rex_scheduler_user,
        rex_scheduler_password,
        rex_postgres_host,
        rex_postgres_port,
        rex_scheduler_db_name
    )


# Main Application DB Environment variables
rex_gigapp_db_name = os.environ.get("REX_DB_GIGAPPDB_PATH")
database_url = os.environ.get('TESTING_DATABASE_URL') if TESTING else os.environ.get('DATABASE_URL')
if database_url:
    DATABASE_URL = database_url
else:
    DATABASE_URL = make_connection_string(
        "postgresql",
        rex_scheduler_user,
        rex_scheduler_password,
        rex_postgres_host,
        rex_postgres_port,
        rex_gigapp_db_name
    )


# Listings DB environment variables
rex_listings_host = os.environ.get("REX_DB_REXDB_HOST")
rex_listings_port = os.environ.get("REX_DB_REXDB_PORT")
rex_listings_scheme = os.environ.get("REX_DB_REXDB_SCHEME")
rex_listings_user = os.environ.get("REX_DB_REXDB_USER")
rex_listings_password = os.environ.get("REX_DB_REXDB_PASSWORD")
rex_listings_db_name = os.environ.get("REX_DB_REXDB_PATH")

rex_listings_database_url = os.environ.get("REX_LISTINGS_DATABASE_URL")

if rex_listings_database_url:
    REX_LISTINGS_DATABASE_URL = rex_listings_database_url
else:
    REX_LISTINGS_DATABASE_URL = make_connection_string(
        rex_listings_scheme,
        rex_listings_user,
        rex_listings_password,
        rex_listings_host,
        rex_listings_port,
        rex_listings_db_name
    )


# Logger
LOGGER_NAME = "rex_backend"
LOGGING = {
    'version': 1,
    'root': {
        'level': 'INFO',
        'handlers': ['console'],
    },
    'formatters': {
        'simple': {
            'format': '[%(levelname)s] [%(asctime)s] %(module)s [%(name)s] %(message)s'
        },
        'verbose': {
            'format': '[%(levelname)s] [%(module)s] [%(pathname)s:%(lineno)d] [%(asctime)s] [%(name)s] %(message)s'
        },
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
            'stream': 'ext://sys.stdout'
        },
    },
    'loggers': {
        'rex_backend': {
            'level': 'INFO',
            'handlers': ['console'],
            'propagate': False
        },
    }
}
