from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Boolean, DateTime, BigInteger, DECIMAL, Float, Integer, String, Text, ForeignKey
from sqlalchemy.dialects.mysql import DOUBLE
from sqlalchemy.orm import relationship
from sqlalchemy_mixins import ReprMixin, SerializeMixin


Base = declarative_base()


class RexListingsBaseModel(Base, ReprMixin, SerializeMixin):
    __abstract__ = True
    pass


class RexListing(RexListingsBaseModel):
    __tablename__ = "listing"

    id = Column(BigInteger, autoincrement=True, primary_key=True)
    guid = Column(String(45))
    status = Column(String(45))
    listing_type = Column(String(45))
    property_type = Column(String(45))
    description = Column(String(3000))
    bedrooms = Column(Integer)
    total_baths = Column(DECIMAL(7, 2))
    living_square_feet = Column(Integer)
    total_rooms = Column(Integer)
    year_built = Column(Integer)
    price = Column(Integer)
    land_square_footage = Column(Integer)
    land_front_footage = Column(Integer)
    land_depth_Footage = Column(Integer)
    parking_spaces = Column(Integer)
    parking_type_code = Column(String(45))
    garage_parking_square_feet = Column(Integer)
    fireplace_indicator_flag = Column(Boolean)
    fireplace_number = Column(Integer)
    heating_type = Column(String(45))
    air_conditioning_type = Column(String(45))
    pool_flag = Column(Boolean)
    pool_code = Column(String(45))
    latitude = Column(DOUBLE)
    longitude = Column(DOUBLE)
    map_zoom = Column(Integer)
    rex_url = Column(String(100))
    hoa_fee = Column(Float)
    hoa_variable = Column(Boolean)
    stories = Column(DECIMAL(5, 2))
    created = Column(DateTime)
    updated = Column(DateTime)

    address_id = Column(BigInteger, ForeignKey('address.id'))

    address = relationship("RexAddress")
    images = relationship("RexListingImage")

    def __repr__(self):
        return "<RexListing(id='%s' address_id='%s'>" % (self.id, self.address_id)


class RexAddress(RexListingsBaseModel):
    __tablename__ = "address"

    id = Column(BigInteger, autoincrement=True, primary_key=True)
    guid = Column(String(40))
    address_line = Column(Text)
    city = Column(String(200))
    state = Column(String(2))
    postal_code = Column(String(10))
    display_address_line = Column(Text)
    display_city = Column(String(200))
    display_state = Column(String(2))
    display_postal_code = Column(String(10))
    created_at = Column('created', DateTime)
    upated_at = Column('updated', DateTime)

    def __repr__(self):
        return "<RexAddress(id='%s' display_address_line='%s')>" % (self.id, self.display_address_line)


class RexListingImage(RexListingsBaseModel):
    __tablename__ = 'listing_image'

    id = Column(BigInteger, autoincrement=True, primary_key=True)
    guid = Column(String(45))
    published_status = Column(Boolean)
    description = Column(String(1000))
    listing_image_order = Column(Integer)
    created_at = Column('created', DateTime)
    upated_at = Column('updated', DateTime)

    listing_id = Column(BigInteger, ForeignKey('listing.id'))
    image_file_id = Column(BigInteger, ForeignKey('file_info.id'))

    file_info = relationship("RexFileInfo")

    def __repr__(self):
        return "<RexAddress(id='%s' listing_id='%s' image_file_id='%s')>" % (self.id, self.listing_id, self.image_file_id)


class RexFileInfo(RexListingsBaseModel):
    __tablename__ = 'file_info'

    id = Column(BigInteger, autoincrement=True, primary_key=True)
    guid = Column(String(40))
    public_url = Column(Text)

    def __repr__(self):
        return "<RexFileInfo(id='%s' public_url='%s')>" % (self.id, self.public_url)


class RexQuestion(RexListingsBaseModel):
    __tablename__ = 'listing_questions'
    __repr_attrs__ = ['id', 'guid', 'type']

    id = Column(BigInteger, autoincrement=True, primary_key=True)
    guid = Column(String(45))
    group_id = Column(BigInteger)
    name = Column(String(100))
    question = Column(String(500))
    is_multi = Column(Boolean)
    type = Column('type', String)
    stage_needed_for = Column(String)
    enum_csv = Column(String(1000))
    source = Column(String(100))
    current = Column(Boolean)
    required = Column(Boolean)
    created_at = Column('created', DateTime)
    updated_at = Column('updated', DateTime)


class RexAnswerSet(RexListingsBaseModel):
    __tablename__ = 'listing_answers_set'

    id = Column(BigInteger, autoincrement=True, primary_key=True)
    question_id = Column(BigInteger, ForeignKey(RexQuestion.id))
    listing_id = Column(BigInteger, ForeignKey(RexListing.id))


class RexAnswer(RexListingsBaseModel):
    __tablename__ = 'listing_answers'
    __repr_attrs__ = ['id', 'showing_id', 'agent_email']

    id = Column(BigInteger, autoincrement=True, primary_key=True)
    guid = Column(String(45))
    answer_set_id = Column(BigInteger)
    answer_text = Column(String)
    answer_int = Column(Integer)
    answer_date = Column(DateTime)
    answer_bool = Column(Boolean)
    showing_id = Column('source', String(100))
    agent_email = Column('recorded_by', String(100))
    created_at = Column('created', DateTime)
    updated_at = Column('updated', DateTime)
