import enum
import logging
from datetime import datetime

import phonenumbers
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, DateTime, Date, ForeignKey, Integer, BIGINT, String, Enum, Boolean
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import relationship
from sqlalchemy_mixins import ReprMixin, SerializeMixin
from sqlalchemy.schema import UniqueConstraint
from sentry_sdk import capture_exception

from rex_backend.repositories.rex_showing_repo import RexShowingRepo


logger = logging.getLogger('rex_backend')
Base = declarative_base()


class BaseModel(Base, ReprMixin, SerializeMixin):
    __abstract__ = True


class User(BaseModel):
    __tablename__ = 'users'
    __repr_attrs__ = ['rex_agent_id', 'contact_email', 'contact_phone_number']

    id = Column(Integer, primary_key=True)
    rex_user_id = Column(Integer, nullable=True, index=True)  # Field is deprecated, use rex_users_ids instead
    rex_agent_id = Column(Integer, nullable=True, unique=True)
    first_name = Column(String)
    last_name = Column(String)
    contact_phone_number = Column(String)
    contact_email = Column(String)
    avatar_url = Column(String)
    created_at = Column(DateTime(timezone=True), nullable=False, default=datetime.utcnow)
    updated_at = Column(DateTime(timezone=True), nullable=False, default=datetime.utcnow, onupdate=datetime.utcnow)

    auth_identities = relationship("GoogleIdentity", backref="users")
    devices = relationship("Device", backref="users")
    links_to_rex_users = relationship("LinkToRexUser", backref="users")
    showing_reports = relationship("ShowingReport", back_populates="user")
    actions = relationship("UserAction", back_populates="user")

    @property
    def rex_users_ids(self):
        return [link.rex_user_id
                for link in self.links_to_rex_users
                if link.rex_user_id]

    @property
    def phone_number_obj(self):
        try:
            return phonenumbers.parse(self.contact_phone_number)
        except phonenumbers.phonenumberutil.NumberParseException as exc:
            logging.error(exc)
            capture_exception(exc)
            return None

    @property
    def national_phone_number(self):
        if self.phone_number_obj is None:
            return None

        return phonenumbers.format_number(
            self.phone_number_obj,
            phonenumbers.PhoneNumberFormat.NATIONAL
        )

    @property
    def contact(self):
        return self.contact_phone_number or self.contact_email

    @property
    def fullname(self):
        return f'{self.first_name} {self.last_name}'


class Device(BaseModel):
    __tablename__ = 'devices'
    __repr_attrs__ = ['id', 'user_id', 'push_notification_token']

    id = Column(String, nullable=False, primary_key=True)
    push_notification_token = Column(String, nullable=True, index=True)
    badge_count = Column(Integer, nullable=False, default=0)
    closed_at = Column(DateTime(timezone=True), nullable=True)

    user_id = Column(Integer, ForeignKey(User.id), nullable=False)
    user = relationship(User)


class GoogleIdentity(BaseModel):
    __tablename__ = 'auth_identities'
    __repr_attrs__ = ['type', 'payload']

    id = Column(Integer, primary_key=True)
    status = Column(String, default="active", nullable=False, index=True)
    type = Column(String, nullable=False, index=True)
    payload = Column(JSONB, default={}, nullable=False, index=True)

    sign_in_at = Column(DateTime(timezone=True), nullable=True)
    sign_out_at = Column(DateTime(timezone=True), nullable=True)

    user_id = Column(Integer, ForeignKey(User.id))
    user = relationship(User)

    def __init__(self, **data):
        data['type'] = 'GoogleIdentity'
        super().__init__(**data)

    @property
    def oauth_user_id(self):
        return self.payload.get('oauth_user_id')

    @property
    def oauth_access_token(self):
        return self.payload.get('oauth_access_token')

    @property
    def oauth_access_token_secret(self):
        return self.payload.get('oauth_access_token_secret')

    @property
    def expiration_date(self):
        return self.payload.get('expiration_date')


class LinkToRexUser(BaseModel):
    __tablename__ = 'links_to_rex_users'
    __repr_attrs__ = ['user_id', 'rex_user_id', 'phone_number']

    rex_user_id = Column(Integer, primary_key=True, autoincrement=False)
    phone_number = Column(String, index=True)
    email = Column(String, index=True)

    user_id = Column(Integer, ForeignKey(User.id), nullable=False)
    user = relationship(User)


class UserStat(BaseModel):
    COMPLETED_SHOWINGS_AMOUNT = 'completed_showings_amount'
    CONVERTED_TO_ESCROW_AMOUNT = 'converted_to_escrow_amount'

    __tablename__ = 'user_stats'
    __repr_attrs__ = ['user_id', 'name', 'value']

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, index=True)
    value = Column(Integer, default=0)

    date = Column(Date, nullable=False, default=datetime.utcnow)

    user_id = Column(Integer, ForeignKey(User.id), nullable=False)
    user = relationship(User)

    __table_args__ = (
        UniqueConstraint('user_id', 'date', 'name', name='_user_stat_for_day'),
    )


class ShowingReportStatus(enum.Enum):
    buyer_not_appeared = 0
    agent_could_not_make_it = 1
    canceled = 2
    ok = 3


class ShowingReport(BaseModel):
    __tablename__ = 'showing_reports'
    __repr_attrs__ = ['rex_showing_id', 'user_id']

    id = Column(Integer, primary_key=True)
    rex_showing_id = Column(BIGINT, nullable=False, unique=True)
    status = Column(Enum(ShowingReportStatus), nullable=False, index=True)
    status_comment = Column(String)
    status_added_at = Column(DateTime)

    is_questions_completed = Column(Boolean, default=False)
    is_call_buyer_completed = Column(Boolean, default=False)
    is_photo_submitted = Column(Boolean, default=False)

    submitted_for_payment_at = Column(DateTime, nullable=True)

    user_id = Column(BIGINT, ForeignKey(User.id), nullable=False)
    user = relationship(User, lazy='joined', back_populates='showing_reports')

    report_groups = relationship("ReportGroup", back_populates="showing_report")
    answers = relationship('ReportAnswer', back_populates='showing_report')

    @property
    def rex_showing(self):
        if not hasattr(self, '_rex_showing'):
            self._rex_showing = RexShowingRepo.rex_showing_query(self.rex_showing_id).first()

        return self._rex_showing


class ReportGroupStatus(Enum):
    initialized = 'initialized'
    completed = 'completed'


class ReportGroup(BaseModel):
    __tablename__ = 'report_groups'
    __repr_attrs__ = ['title', 'showing_report_id', 'status']
    __table_args__ = (
        UniqueConstraint('title', 'showing_report_id', name='_uniq_group_of_questions'),
    )

    id = Column(Integer, primary_key=True)
    title = Column(String, nullable=False)
    status = Column(String, nullable=True, index=True)

    showing_report_id = Column(BIGINT, ForeignKey(ShowingReport.id), nullable=False)
    showing_report = relationship(ShowingReport, back_populates='report_groups')


class ReportQuestionOption(object):
    def __init__(self, text, value, selected=False):
        self.text = text
        self.value = value
        self.selected = False

    def __eq__(self, other):
        if type(self) == type(other):
            return self.value == self.other
        return self.value == str(other)

    def __str__(self):
        return self.text

    def __repr__(self):
        return f'{self.__class__} value: {self.value}, text: {self.text}, selected: {self.selected}'


class ReportQuestion(BaseModel):
    __tablename__ = 'report_questions'
    __repr_attrs__ = ['id', 'title', 'rex_question_guid']

    id = Column(Integer, primary_key=True)
    rex_question_guid = Column(String, unique=True)
    title = Column(String, nullable=False)
    data = Column(JSONB, default={}, nullable=False, index=True)

    def __init__(self, **kwargs):
        self.answer_value = ''
        super().__init__(**kwargs)

    @property
    def original_data(self):
        return self.data.get('original_data', {})

    @property
    def is_required(self):
        return self.data.get('is_required', False)

    @property
    def raw_options(self):
        return self.data.get('options', [])

    @property
    def options(self):
        if not hasattr(self, '_options'):
            self._options = [ReportQuestionOption(*option) for option in self.raw_options]
        return self._options

    def answers_for(self, showing_report_id):
        def query():
            return ReportAnswer.query.filter(ReportAnswer.rex_question_guid == self.rex_question_guid) \
                                     .filter(ReportAnswer.showing_report_id == showing_report_id) \
                                     .all()

        return query()


class ReportAnswer(BaseModel):
    __tablename__ = 'report_answers'
    __repr_attrs__ = ['id', 'rex_question_guid', 'values']
    __table_args__ = (
        UniqueConstraint('rex_question_guid', 'showing_report_id', name='_uniq_showing_report_answer'),
    )

    id = Column(Integer, primary_key=True)
    rex_question_guid = Column(String, nullable=False)
    value = Column(String, nullable=True)
    data = Column(JSONB, default={}, nullable=False, index=True)

    showing_report_id = Column(ForeignKey(ShowingReport.id), nullable=False)
    showing_report = relationship(ShowingReport, back_populates='answers')

    @property
    def values(self):
        return self.data.get('values', [])


class UserActionName(enum.Enum):
    hide_showing = 'hide_showing'


class UserAction(BaseModel):
    __tablename__ = 'user_actions'
    __repr_attrs__ = ['name', 'user_id', 'object_id']
    __table_args__ = (
        UniqueConstraint('name', 'user_id', 'object_id', name='_uniq_user_action'),
    )

    id = Column(BIGINT, primary_key=True)
    name = Column(String, nullable=False, index=True)
    user_id = Column(Integer, ForeignKey(User.id), nullable=False)
    object_id = Column(Integer, nullable=True, index=True)
    created_at = Column(DateTime, nullable=False, default=datetime.utcnow)
    data = Column(JSONB, default={}, nullable=False)

    user = relationship(User, back_populates='actions')
