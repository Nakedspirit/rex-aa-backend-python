from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, JSON, Boolean, DateTime, Enum, Integer, String, false, text, true, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy_mixins import ReprMixin, SerializeMixin


Base = declarative_base()


class RexShowingsBaseModel(Base, ReprMixin, SerializeMixin):
    __abstract__ = True
    pass


VOTE_OPTIONS = ('Accept', 'Reject')


class RexAssociatedAgent(RexShowingsBaseModel):
    __tablename__ = "associate_agents"
    __repr_attrs__ = ['id', 'email', 'phone_number', 'name']

    id = Column(Integer, autoincrement=True, primary_key=True)
    email = Column(String, unique=True)
    phone_number = Column(String, unique=True)
    name = Column(String)
    address = Column(String)

    def get_attrs_for_app_user(self):
        first_name, last_name, *_skip = self.name.split(' ') + [None, None]
        return {
            'rex_agent_id': self.id,
            'contact_email': self.email,
            'contact_phone_number': self.phone_number,
            'first_name': first_name,
            'last_name': last_name
        }


class RexUser(RexShowingsBaseModel):
    __tablename__ = "autoschedule_users"
    __repr_attrs__ = ['id', 'phone_number', 'email_address', 'default_medium']

    id = Column(Integer, autoincrement=True, primary_key=True)
    email_address = Column(String, unique=True)
    phone_number = Column(String, unique=True)
    default_medium = Column(String)
    first_name = Column(String)
    middle_name = Column(String)
    last_name = Column(String)
    default_role = Column(String)
    created_at = Column(DateTime, server_default=text('NOW()'))

    def get_attrs_for_app_user(self):
        return {
            'rex_user_id': self.id,
            'contact_email': self.email_address,
            'contact_phone_number': self.phone_number,
            'first_name': self.first_name,
            'last_name': self.last_name
        }


class RexBlock(RexShowingsBaseModel):
    __tablename__ = "scheduler_blocks"
    __repr_attrs__ = ['id', 'type', 'conversation_id']

    id = Column(Integer, autoincrement=True, primary_key=True)
    name = Column(String, nullable=True)
    block_type = Column('type', String)
    index_block = Column(Integer)  # position of block in conversation
    is_active = Column(Boolean, server_default=true())
    needs_confirmation = Column(Boolean, server_default=false())  # whether or not the users in this block need confirmation text (e.g., are you sure you want to select... ?)
    allow_revote = Column(Boolean, server_default=true())  # allow participants to revote
    always_notify = Column(Boolean, server_default=false())  # notify of all failures and cancellations, even if this block has not been contacted yet
    created_at = Column(DateTime, server_default=text('NOW()'))

    conversation_id = Column(ForeignKey("scheduler_requests.id"), nullable=False)

    attributes = relationship("RexBlockAttribute")


class RexBlockAttribute(RexShowingsBaseModel):
    __tablename__ = "scheduler_block_attributes"
    __repr_attrs__ = ['id', 'block_id', 'key', 'value']

    id = Column(Integer, autoincrement=True, primary_key=True)
    key = Column(String)
    value = Column(String)
    created_at = Column(DateTime, server_default=text('NOW()'))

    block_id = Column(ForeignKey("scheduler_blocks.id"), nullable=False)


class RexBlockUser(RexShowingsBaseModel):
    __tablename__ = "scheduler_blocks_users"
    __repr_attrs__ = ['id', 'bot_address', 'user_address', 'block_id', 'user_id']

    id = Column(Integer, autoincrement=True, primary_key=True)
    bot_address = Column('bot_address', String)  # email address or phone number
    user_address = Column(String)
    medium = Column(String)  # email or phone
    role = Column(String)  # NOTE: not currently populated/used
    has_responded = Column(Boolean)  # whether or not user has responded in this channel
    index_member = Column(Integer)  # position of member in block

    is_redirect = Column(Boolean, server_default=false())  # TODO: DEFINITELY move or redefine. Controls whether messages to this bot address from this user are processed by the bot (True means send to bot, don't process)
    is_active = Column(Boolean, server_default=true())  # whether or not this channel is currently being used
    is_confirmed = Column(Boolean, server_default=false())  # whether or not user has confirmed response
    is_removed = Column(Boolean, server_default=false())  # whether or not user has been removed from block; should not receive any further communication
    last_response = Column(String)  # TODO: should this be a message_id instead?
    hook_receive = Column(String)  # TODO: DEFINITELY move or redefine. Invoke hook if is_redirect below is set.
    created_at = Column(DateTime, server_default=text('NOW()'))

    block_id = Column(ForeignKey("scheduler_blocks.id"), nullable=False)
    user_id = Column(ForeignKey("autoschedule_users.id"), nullable=False)
    # channel_id = Column(ForeignKey("autoschedule_channels.id"), nullable=False)


class RexShowingTimeSlot(RexShowingsBaseModel):
    __tablename__ = "scheduler_options"
    __repr_attrs__ = ['id', 'start', 'is_rejected']

    id = Column(Integer, autoincrement=True, primary_key=True)
    start = Column(DateTime)
    end = Column(DateTime)
    is_rejected = Column(Boolean, server_default=false())
    created_at = Column(DateTime, server_default=text('NOW()'))

    conversation_id = Column(ForeignKey('scheduler_requests.id'), nullable=True)
    showing = relationship("RexShowing", back_populates="time_slots")


class RexShowing(RexShowingsBaseModel):
    STATUSES = {
        'SCHEDULED': 'Scheduled',
        'PENDING': 'Pending',
        'EXPIRED': 'Expired',
        'FAILED': 'Failed',
        'CANCELLED': 'Cancelled',
        'COMPLETED': 'Completed',
        'ARCHIVED': 'Archived',
    }

    __tablename__ = 'scheduler_requests'
    __repr_attrs__ = ['id', 'status']

    id = Column(Integer, autoincrement=True, primary_key=True)
    name = Column(String, nullable=True)
    data = Column(JSON)

    status = Column(Enum(*STATUSES.values(), name='status'))  # !!!!!
    options = Column(String)  # list of isoformat strings lol TODO: change to proper table of options

    expires_by_in_minutes = Column(Integer)
    final_meeting_time = Column(DateTime)
    cancelled_by = Column(ForeignKey("autoschedule_users.id"), nullable=True)
    index_current_member = Column(Integer)  # which user within that block the bot is currently waiting on
    index_current_block = Column(Integer)  # which block the bot is currently waiting on
    is_active = Column(Boolean)
    is_archived = Column(Boolean)
    timezone = Column(String, server_default='UTC')
    created_at = Column(DateTime, server_default=text('NOW()'))

    initiator_id = Column(ForeignKey("autoschedule_users.id"), nullable=False)
    reschedule_id = Column(ForeignKey("scheduler_requests.id"), nullable=True)

    blocks = relationship("RexBlock")
    time_slots = relationship("RexShowingTimeSlot", back_populates="showing")
    # votes = relationship("RexShowingVote")
    open_house = relationship('RexOpenHouse', uselist=False, lazy='selectin', back_populates='showing')


class RexShowingVote(RexShowingsBaseModel):
    __tablename__ = "scheduler_votes"
    __repr_attrs__ = ['id', 'vote', 'member_id', 'option_id']

    id = Column(Integer, autoincrement=True, primary_key=True)
    vote = Column(Enum(*VOTE_OPTIONS, name='vote_options'))
    created_at = Column(DateTime, server_default=text('NOW()'))

    member_id = Column(ForeignKey("scheduler_blocks_users.id"), nullable=True)
    option_id = Column(ForeignKey("scheduler_options.id"), nullable=True)


class RexChannel(RexShowingsBaseModel):
    __tablename__ = "autoschedule_channels"

    id = Column(Integer, autoincrement=True, primary_key=True)
    bot_address = Column(String)
    user_address = Column(String)

    medium = Column(String)  # email or phone
    data = Column(JSON)

    is_active = Column(Boolean, server_default=true())
    is_redirect = Column(Boolean, server_default=false())
    hook_receive = Column(String)
    created_at = Column(DateTime, server_default=text('NOW()'))

    user_id = Column(ForeignKey(RexUser.id), nullable=False)


class RexOpenHouse(RexShowingsBaseModel):
    __tablename__ = "open_houses"
    __repr_attrs__ = ['id', 'showing_id', 'listing_id', 'css_user_id']

    id = Column(Integer, primary_key=True)
    guid = Column('dino_open_house_guid', String)
    created_at = Column(DateTime)
    duration = Column(Integer)

    css_user_id = Column(Integer)
    listing_id = Column(Integer)

    showing_id = Column('scheduler_request_id', Integer, ForeignKey(RexShowing.id))
    showing = relationship(RexShowing, back_populates='open_house')
