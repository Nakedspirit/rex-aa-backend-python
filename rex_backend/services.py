from rex_backend.application import app
from rex_backend.utils.aws_ses import AWSSES
from rex_backend.utils.rest_api import RestAPI
from rex_backend.utils.firebase_cloud_messaging import FirebaseCloudMessaging


aws_ses_service = AWSSES(
    app.config['AWS_SES_ACCESS_KEY'],
    app.config['AWS_SES_SECRET_KEY'],
    app.config['AWS_SES_REGION'],
    app.config['AWS_SES_SENDER']
)

rest_api_service = RestAPI()

firebase_cm_service = FirebaseCloudMessaging(
    app.config['FIREBASE_CM_BASE64_CERT'],
    fake_mode=app.config['FIREBASE_CM_FAKE_MODE']
)
