from rex_backend.models.base import User, LinkToRexUser


class UserRepo:
    @classmethod
    def load_by_rex_users_ids(cls, rex_users_ids):
        return User.query.join(LinkToRexUser, LinkToRexUser.user_id == User.id) \
                         .filter(LinkToRexUser.rex_user_id.in_(rex_users_ids)) \
                         .distinct(User.id) \
                         .all()
