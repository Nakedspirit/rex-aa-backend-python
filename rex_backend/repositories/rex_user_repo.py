from sqlalchemy import or_

from rex_backend.models.rex_showings import RexUser


class RexUserRepo:
    @staticmethod
    def load_rex_users_by_phone_number_or_email(phone_number, email=None):
        if email:
            condition = or_(
                RexUser.phone_number == phone_number,
                RexUser.email_address == email
            )
        else:
            condition = RexUser.phone_number == phone_number

        return RexUser.query.filter(condition).all()
