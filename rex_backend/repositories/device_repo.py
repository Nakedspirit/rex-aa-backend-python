from rex_backend.application import app
from rex_backend.models.base import Device, User, LinkToRexUser


class DeviceRepo:
    @classmethod
    def query_devices_by_rex_user_ids(cls, rex_user_ids):
        return Device.query \
                     .join(User, User.id == Device.user_id) \
                     .join(LinkToRexUser, LinkToRexUser.user_id == User.id) \
                     .filter(LinkToRexUser.rex_user_id.in_(rex_user_ids))

    @classmethod
    def query_devices_by_user_email(cls, user_email):
        return Device.query \
                     .join(User, User.id == Device.user_id) \
                     .filter(User.contact_email == user_email)

    @classmethod
    def load_demo_user_devices_with_push_tokens(cls):
        email = app.config['DEMO_REX_USER_EMAIL']
        devices = cls.query_devices_by_user_email(email).filter(Device.push_notification_token.isnot(None)).all()
        return [device for device in devices if device]

    @classmethod
    def load_devices_by_rex_user_ids_with_push_tokens(cls, rex_user_ids):
        return cls.query_devices_by_rex_user_ids(rex_user_ids) \
                  .filter(Device.push_notification_token.isnot(None)) \
                  .all()

    @classmethod
    def load_devices_by_rex_user_id_with_push_token(cls, rex_user_id):
        return cls.query_devices_by_rex_user_ids([rex_user_id]) \
                  .filter(Device.push_notification_token.isnot(None)) \
                  .all()
