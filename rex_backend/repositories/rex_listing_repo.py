from rex_backend.application import rex_listings_db


class RexListingRepo:
    @classmethod
    def load_seller(cls, listing_id):
        args = {'listing_id': int(listing_id)}
        query = (
            'SELECT listing.id AS listing_id, user.id AS user_id, person.id AS person_id, person.first, person.last, person.email as email'
            ' FROM listing'
            ' INNER JOIN user ON user.id = listing.seller_user_id'
            ' INNER JOIN person ON person.id = user.person_id'
            ' WHERE listing.id = :listing_id'
        )
        result_proxy = rex_listings_db.session.execute(query, args)
        return result_proxy.first()
