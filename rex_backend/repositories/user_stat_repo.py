from sqlalchemy import func, desc


from rex_backend.models.base import UserStat


class UserStatRepo:

    @classmethod
    def query_user_stats_for_period(cls, stat_name, from_date, to_date):
        fields_to_select = [
            UserStat.user_id,
            func.sum(UserStat.value).label('value')
        ]

        return UserStat.query \
            .with_entities(*fields_to_select) \
            .filter(UserStat.name == stat_name) \
            .filter(UserStat.date >= from_date) \
            .filter(UserStat.date <= to_date) \
            .group_by(UserStat.user_id) \
            .order_by(desc('value'))

    @classmethod
    def count_user_position_in_period(cls, user_id, stat_name, from_date, to_date):
        user_stat = cls.query_user_stats_for_period(stat_name, from_date, to_date) \
                       .filter(UserStat.user_id == user_id) \
                       .first()
        if not user_stat:
            return None

        sql_query = '''
        SELECT count(*)
        FROM (SELECT
                user_stats.user_id as user_id,
                sum(user_stats.value) as value
            FROM user_stats
            WHERE user_stats.name = :stat_name
                AND user_stats.date >= :from_date
                AND user_stats.date <= :to_date
                AND user_stats.user_id != :user_id
            GROUP BY user_stats.user_id) as agregated_stats
        WHERE agregated_stats.value > :value
        '''
        result = UserStat.query.session.execute(sql_query, {
            'stat_name': UserStat.COMPLETED_SHOWINGS_AMOUNT,
            'from_date': from_date,
            'to_date': to_date,
            'user_id': user_id,
            'value': user_stat.value
        })
        return result.first()[0] + 1
