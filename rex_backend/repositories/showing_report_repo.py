from sqlalchemy import or_, and_
from rex_backend.models.base import ShowingReport, ShowingReportStatus, ReportAnswer


class ShowingReportRepo:
    @staticmethod
    def showing_report_query(rex_showing_id):
        return ShowingReport.query.filter(ShowingReport.rex_showing_id == rex_showing_id)

    @staticmethod
    def showing_report_answers_query(showing_report_id):
        return ReportAnswer.query.filter(ReportAnswer.showing_report_id == showing_report_id)

    @staticmethod
    def completed_showing_reports_query():
        return ShowingReport.query \
                            .filter(
                                or_(
                                    and_(
                                        ShowingReport.status == ShowingReportStatus.ok,
                                        ShowingReport.is_questions_completed == True, # noqa E712
                                        ShowingReport.submitted_for_payment_at.isnot(None)
                                    ),
                                    and_(
                                        ShowingReport.status == ShowingReportStatus.buyer_not_appeared,
                                        ShowingReport.submitted_for_payment_at.isnot(None)
                                    ),
                                    ShowingReport.status == ShowingReportStatus.agent_could_not_make_it
                                )
                            )

    @classmethod
    def showing_report_by_user_query(cls, rex_showing_id, user_id):
        return cls.showing_report_query(rex_showing_id).filter(ShowingReport.user_id == user_id)

    @classmethod
    def load_showing_report_answers(cls, showing_report_id):
        return cls.showing_report_answers_query(showing_report_id).all()

    @classmethod
    def load_showing_report(cls, rex_showing_id, user_id):
        return cls.showing_report_by_user_query(rex_showing_id, user_id).first()

    @classmethod
    def load_completed_showing_reports(cls):
        return cls.completed_showing_reports_query().all()
