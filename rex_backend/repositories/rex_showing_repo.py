from sqlalchemy import and_, or_
from rex_backend.models.rex_showings import RexUser, RexShowing, RexBlock, RexBlockUser, RexBlockAttribute, RexOpenHouse


class RexShowingRepo:
    STANDARD_SHOWING_FIELDS = [
        RexShowing.id,
        RexShowing.status,
        RexShowing.data,
        RexShowing.final_meeting_time,
        RexShowing.timezone,
        RexShowing.created_at,
    ]

    OPEN_HOUSE_FIELDS = [
        RexOpenHouse.id.label('open_house_id'),
        RexOpenHouse.guid.label('open_house_guid'),
        RexOpenHouse.created_at.label('open_house_created_at'),
        RexOpenHouse.css_user_id.label('open_house_css_user_id'),
        RexOpenHouse.listing_id.label('open_house_listing_id'),
        RexOpenHouse.showing_id.label('open_house_showing_id'),
        RexOpenHouse.duration.label('open_house_duration'),
    ]

    @staticmethod
    def validate_rex_users_ids(rex_users_ids):
        if not rex_users_ids or len(rex_users_ids) == 0:
            raise ValueError(f"rex_users_ids can't be empty, rex_users_ids: {rex_users_ids}")

    @staticmethod
    def validate_statuses(statuses):
        if not statuses:
            raise ValueError(f"statuses can't be empty, statuses: {statuses}")

        for status in statuses:
            if status not in RexShowing.STATUSES.values():
                raise ValueError(f"status is invalid, status({status}) not one of {RexShowing.STATUSES.values()}")

    @classmethod
    def query_showings(cls):
        fields_to_select = [
            *cls.STANDARD_SHOWING_FIELDS,
            *cls.OPEN_HOUSE_FIELDS,
            RexBlockAttribute.created_at.label('claimed_at'),
            RexBlockAttribute.value.label('claimed_by_id'),
        ]
        return RexShowing.query \
                         .with_entities(*fields_to_select) \
                         .outerjoin(RexOpenHouse, RexOpenHouse.showing_id == RexShowing.id) \
                         .outerjoin(RexBlock, and_(RexBlock.conversation_id == RexShowing.id, RexBlock.block_type == 'firstcomefirstserve')) \
                         .outerjoin(RexBlockUser, RexBlockUser.block_id == RexBlock.id) \
                         .outerjoin(RexBlockAttribute, and_(RexBlockAttribute.block_id == RexBlock.id, RexBlockAttribute.key == 'first_user_id')) \
                         .order_by(RexShowing.id.desc()) \
                         .distinct(RexShowing.id)

    @classmethod
    def query_gigs(cls, max_meeting_datetime_of_showing, max_meeting_datetime_of_open_house):
        fields_to_select = [
            *cls.STANDARD_SHOWING_FIELDS,
            *cls.OPEN_HOUSE_FIELDS,
            RexBlockAttribute.created_at.label('claimed_at'),
            RexBlockAttribute.value.label('claimed_by_id'),
        ]
        return RexShowing.query \
                         .with_entities(*fields_to_select) \
                         .outerjoin(RexOpenHouse, RexOpenHouse.showing_id == RexShowing.id) \
                         .outerjoin(RexBlock, and_(RexBlock.conversation_id == RexShowing.id, RexBlock.block_type == 'firstcomefirstserve')) \
                         .outerjoin(RexBlockUser, RexBlockUser.block_id == RexBlock.id) \
                         .outerjoin(RexBlockAttribute, and_(RexBlockAttribute.block_id == RexBlock.id, RexBlockAttribute.key == 'first_user_id')) \
                         .order_by(RexShowing.final_meeting_time.asc()) \
                         .filter(
                             and_(
                                 RexShowing.status == RexShowing.STATUSES['SCHEDULED'],
                                 or_(
                                     max_meeting_datetime_of_showing > RexShowing.final_meeting_time,
                                     and_(
                                         RexOpenHouse.id.isnot(None),
                                         max_meeting_datetime_of_open_house > RexShowing.final_meeting_time,
                                     )
                                 )
                             )
                         )

    @classmethod
    def rex_showing_query(cls, showing_id):
        return cls.query_showings().filter(RexShowing.id == int(showing_id))

    @classmethod
    def query_showings_by_statuses(cls, statuses=[]):
        cls.validate_statuses(statuses)

        if len(statuses) > 1:
            conditional = RexShowing.status.in_(statuses)
        else:
            conditional = RexShowing.status == statuses[0]

        return cls.query_showings().filter(conditional)

    @classmethod
    def query_unclaimed_showings_by_rex_users_ids(cls, rex_users_ids):
        cls.validate_rex_users_ids(rex_users_ids)

        if len(rex_users_ids) > 1:
            condition = RexBlockUser.user_id.in_(rex_users_ids)
        else:
            condition = RexBlockUser.user_id == rex_users_ids[0]

        return cls.query_showings_by_statuses(statuses=['Pending', 'Cancelled', 'Failed']).filter(condition)

    @classmethod
    def rex_unclaimed_showings_query(cls):
        fields_to_select = [
            *cls.STANDARD_SHOWING_FIELDS,
            *cls.OPEN_HOUSE_FIELDS,
            RexBlockAttribute.value.label('claimed_by_id'),
            RexBlockAttribute.created_at.label('claimed_at'),
        ]
        return RexShowing.query \
                        .with_entities(*fields_to_select) \
                        .outerjoin(RexOpenHouse, RexOpenHouse.showing_id == RexShowing.id) \
                        .join(RexBlock, and_(RexBlock.conversation_id == RexShowing.id, RexBlock.block_type == 'firstcomefirstserve')) \
                        .join(RexBlockUser, RexBlockUser.block_id == RexBlock.id) \
                        .outerjoin(RexBlockAttribute, and_(RexBlockAttribute.block_id == RexBlock.id, RexBlockAttribute.key == 'first_user_id')) \
                        .filter(RexShowing.status == 'Pending') \
                        .filter(
                            or_(
                                RexBlockUser.user_id != None,  # noqa: E712
                                RexBlockAttribute.value != None
                            )
                        ).distinct(RexShowing.id) \
                        .order_by(RexShowing.id.desc())

    @classmethod
    def rex_upcoming_showings_query(cls):
        fields_to_select = [
            *cls.STANDARD_SHOWING_FIELDS,
            *cls.OPEN_HOUSE_FIELDS,
            RexBlockAttribute.created_at.label('claimed_at'),
        ]
        return RexShowing.query \
                         .with_entities(*fields_to_select) \
                         .outerjoin(RexOpenHouse, RexOpenHouse.showing_id == RexShowing.id) \
                         .join(RexBlock, RexBlock.conversation_id == RexShowing.id) \
                         .join(RexBlockAttribute, RexBlockAttribute.block_id == RexBlock.id) \
                         .filter(RexBlock.block_type == 'firstcomefirstserve') \
                         .filter(RexShowing.status.in_(['Pending', 'Scheduled'])) \
                         .filter(RexBlockAttribute.key == 'first_user_id') \
                         .distinct(RexShowing.id) \
                         .group_by(RexShowing.id, RexOpenHouse.id, RexBlockAttribute.created_at) \
                         .order_by(RexShowing.id.desc())

    @classmethod
    def query_upcoming_showings_by_rex_users_ids(cls, rex_users_ids):
        cls.validate_rex_users_ids(rex_users_ids)
        return cls.rex_upcoming_showings_query().filter(RexBlockAttribute.value.in_(rex_users_ids))

    @classmethod
    def rex_already_claimed_showings_for_user_query(cls, rex_users_ids):
        fields_to_select = [
            *cls.STANDARD_SHOWING_FIELDS,
            *cls.OPEN_HOUSE_FIELDS,
            RexBlockAttribute.value.label('claimed_by_id'),
            RexBlockAttribute.created_at.label('claimed_at'),
        ]
        query = RexShowing.query \
                          .with_entities(*fields_to_select) \
                          .outerjoin(RexOpenHouse, RexOpenHouse.showing_id == RexShowing.id) \
                          .join(RexBlock, and_(RexBlock.conversation_id == RexShowing.id, RexBlock.block_type == 'firstcomefirstserve')) \
                          .join(RexBlockUser, RexBlockUser.block_id == RexBlock.id) \
                          .outerjoin(RexBlockAttribute, and_(RexBlockAttribute.block_id == RexBlock.id, RexBlockAttribute.key == 'first_user_id')) \
                          .filter(RexShowing.status == 'Pending') \
                          .distinct(RexShowing.id)

        if len(rex_users_ids) > 1:
            query = query.filter(RexBlockUser.user_id.in_([int(id) for id in rex_users_ids])) \
                         .filter(RexBlockAttribute.value.notin_([str(id) for id in rex_users_ids]))
        else:
            rex_user_id = rex_users_ids[0]
            query = query.filter(RexBlockUser.user_id == int(rex_user_id)) \
                         .filter(RexBlockAttribute.value != str(rex_user_id))
        return query

    @classmethod
    def rex_already_claimed_or_declined_showings_query(cls):
        fields_to_select = [
            *cls.STANDARD_SHOWING_FIELDS,
            *cls.OPEN_HOUSE_FIELDS,
            RexBlockAttribute.value.label('claimed_by_id'),
            RexBlockAttribute.created_at.label('claimed_at'),
        ]
        return RexShowing.query \
                        .with_entities(*fields_to_select) \
                        .outerjoin(RexOpenHouse, RexOpenHouse.showing_id == RexShowing.id) \
                        .join(RexBlock, and_(RexBlock.conversation_id == RexShowing.id, RexBlock.block_type == 'firstcomefirstserve')) \
                        .join(RexBlockUser, RexBlockUser.block_id == RexBlock.id) \
                        .outerjoin(RexBlockAttribute, and_(RexBlockAttribute.block_id == RexBlock.id, RexBlockAttribute.key == 'first_user_id')) \
                        .filter(RexShowing.status.in_(['Pending', 'Failed'])) \
                        .filter(
                            or_(
                                RexBlockUser.user_id != None,  # noqa: E712
                                RexBlockAttribute.value != None
                            )
                        ).distinct(RexShowing.id) \
                        .order_by(RexShowing.id.desc())

    @classmethod
    def rex_failed_or_claimed_showings_query(cls):
        fields_to_select = [
            *cls.STANDARD_SHOWING_FIELDS,
            *cls.OPEN_HOUSE_FIELDS,
            RexBlockAttribute.value.label('claimed_by_id'),
            RexBlockAttribute.created_at.label('claimed_at'),
        ]
        return RexShowing.query \
                         .with_entities(*fields_to_select) \
                         .outerjoin(RexOpenHouse, RexOpenHouse.showing_id == RexShowing.id) \
                         .join(RexBlock, and_(RexBlock.conversation_id == RexShowing.id, RexBlock.block_type == 'firstcomefirstserve')) \
                         .join(RexBlockUser, RexBlockUser.block_id == RexBlock.id) \
                         .join(RexBlockAttribute, and_(RexBlockAttribute.block_id == RexBlock.id, RexBlockAttribute.key == 'first_user_id')) \
                         .filter(or_(
                             and_(RexShowing.status == 'Pending', RexBlockAttribute.value.isnot(None)),
                             RexShowing.status == 'Failed'
                         )).filter(RexBlockUser.user_id.isnot(None)).distinct(RexShowing.id).order_by(RexShowing.id.desc())

    @classmethod
    def query_completed_showings(cls):
        fields_to_select = [
            *cls.STANDARD_SHOWING_FIELDS,
            *cls.OPEN_HOUSE_FIELDS,
            RexBlockAttribute.value.label('claimed_by_id'),
            RexBlockAttribute.created_at.label('claimed_at')
        ]
        return RexShowing.query \
                         .with_entities(*fields_to_select) \
                         .outerjoin(RexOpenHouse, RexOpenHouse.showing_id == RexShowing.id) \
                         .join(RexBlock, and_(RexBlock.conversation_id == RexShowing.id, RexBlock.block_type == 'firstcomefirstserve')) \
                         .join(RexBlockAttribute, and_(RexBlockAttribute.block_id == RexBlock.id, RexBlockAttribute.key == 'first_user_id')) \
                         .filter(RexShowing.status == 'Completed') \
                         .distinct(RexShowing.id) \
                         .order_by(RexShowing.id.desc())

    @classmethod
    def claimed_rex_showings_query(cls, rex_showing_id):
        fields_to_select = [
            *cls.STANDARD_SHOWING_FIELDS,
            RexBlockAttribute.created_at.label('claimed_at'),
            RexBlockAttribute.value.label('claimed_by_id')
        ]
        return RexShowing.query \
                         .with_entities(*fields_to_select) \
                         .join(RexBlock, RexBlock.conversation_id == RexShowing.id) \
                         .join(RexBlockAttribute, RexBlockAttribute.block_id == RexBlock.id) \
                         .filter(RexBlock.block_type == 'firstcomefirstserve') \
                         .filter(RexBlockAttribute.key == 'first_user_id')

    @classmethod
    def load_claimed_rex_showing(cls, rex_showing_id):
        return cls.claimed_rex_showings_query(rex_showing_id).first()

    @classmethod
    def load_rex_showing_by_user(cls, rex_showing_id, rex_users_ids):
        if not isinstance(rex_users_ids, list):
            rex_users_ids = [rex_users_ids]

        query = cls.claimed_rex_showings_query(rex_showing_id).filter(RexShowing.id == rex_showing_id)
        ids = [str(id) for id in rex_users_ids]
        query = query.filter(RexBlockAttribute.value.in_(ids))

        return query.first()

    @classmethod
    def query_rex_users_by_rex_showing(cls, rex_showing_id):
        return RexUser.query \
                      .join(RexBlockUser) \
                      .join(RexBlock) \
                      .join(RexShowing) \
                      .filter(RexBlock.block_type == 'firstcomefirstserve') \
                      .filter(RexShowing.id == rex_showing_id)

    @classmethod
    def load_rex_users_by_rex_showing(cls, rex_showing_id):
        return cls.query_rex_users_by_rex_showing(rex_showing_id).all()

    @classmethod
    def load_rex_member(cls, rex_showing_id, rex_users_ids):
        if not isinstance(rex_users_ids, list):
            rex_users_ids = [rex_users_ids]
        query = RexBlockUser.query \
                            .join(RexBlock) \
                            .join(RexShowing) \
                            .filter(RexShowing.id == rex_showing_id) \
                            .filter(RexBlockUser.user_id.in_(rex_users_ids))
        return query.first()

    @classmethod
    def load_rex_showing(cls, rex_showing_id):
        return cls.query_showings().filter(RexShowing.id == rex_showing_id).first()

    @classmethod
    def load_rex_open_house(cls, open_house_guid):
        return cls.query_showings().filter(RexOpenHouse.guid == open_house_guid).first()

    @classmethod
    def load_claiming_data_by_showins_ids(cls, rex_showings_ids):
        return RexBlockAttribute.query \
            .with_entities(
                RexShowing.id,
                RexBlockAttribute.created_at.label('claimed_at'),
                RexBlockAttribute.value.label('claimed_by_id')
            ) \
            .join(RexBlock, RexBlock.id == RexBlockAttribute.block_id) \
            .join(RexShowing, RexShowing.id == RexBlock.conversation_id) \
            .filter(RexBlockAttribute.key == 'first_user_id') \
            .filter(RexShowing.id.in_(rex_showings_ids)) \
            .distinct(RexShowing.id) \
            .all()

    @classmethod
    def query_showing_users(cls, showing_id, role):
        fields_to_select = [
            RexUser.id,
            RexUser.first_name,
            RexUser.last_name,
            RexUser.email_address,
            RexUser.phone_number,
            RexBlock.name.label('role'),
            RexBlock.conversation_id.label('rex_showing_id')
        ]
        return RexUser.query.with_entities(*fields_to_select) \
                            .join(RexBlockUser, RexBlockUser.user_id == RexUser.id) \
                            .join(RexBlock, RexBlock.id == RexBlockUser.block_id) \
                            .filter(RexBlock.name == role) \
                            .filter(RexBlock.conversation_id == int(showing_id))

    @classmethod
    def load_showing_users(cls, showing_ids, role):
        fields_to_select = [
            RexUser.id,
            RexUser.first_name,
            RexUser.last_name,
            RexUser.email_address,
            RexUser.phone_number,
            RexBlock.name.label('role'),
            RexBlock.conversation_id.label('rex_showing_id')
        ]
        return RexUser.query.with_entities(*fields_to_select) \
                            .join(RexBlockUser, RexBlockUser.user_id == RexUser.id) \
                            .join(RexBlock, RexBlock.id == RexBlockUser.block_id) \
                            .filter(RexBlock.name == role) \
                            .filter(RexBlock.conversation_id.in_(showing_ids)) \
                            .all()
