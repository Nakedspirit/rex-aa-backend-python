from rex_backend.models.base import UserAction, UserActionName


class UserActionRepo:
    @staticmethod
    def load_hide_showing_actions_by_user_id(user_id):
        return UserAction.query.filter(
            UserAction.user_id == user_id,
            UserAction.name == UserActionName.hide_showing.value
        ).all()
