import json
import logging

from sentry_sdk import capture_exception, capture_message

from . import HttpAPIException
from requests.exceptions import ConnectTimeout, RetryError


logger = logging.getLogger('rex_backend')


def default_rex_api_handler(api, endpoint, response):
    status = response.status_code

    if status != 200:
        msg = f'Request: {response.request.__dict__}, Response: {response.__dict__}'
        capture_message(msg)
        logger.critical(msg)

        exc_msg = f'{api.name} returned bad response. Status is {status}. Please try later or contact REX operations.'
        raise HttpAPIException(exc_msg)

    try:
        content = response.json()
    except AttributeError:
        content = {}
    except json.decoder.JSONDecodeError:
        content = {}

    return {'status': status, 'content': content}


def default_rex_api_exception_handler(api, endpoint, exception):
    capture_exception(exception)
    logger.critical(exception)

    if isinstance(exception, ConnectTimeout):
        msg = 'REX service is not responding. Please return to the previous screen and try again.'
        exception = HttpAPIException(msg)
    elif isinstance(exception, RetryError):
        msg = 'REX service is not responding. Please return to the previous screen and try again.'
        exception = HttpAPIException(msg)

    raise exception
