import requests
from requests.exceptions import RequestException
from urllib.parse import urljoin, urlparse


class HttpAPIException(Exception):
    pass


class NotFoundEndpointException(HttpAPIException):
    pass


class ReqiredParamsNotPresentException(HttpAPIException):
    def __init__(self, endpoint, required_param, given_params):
        self.endpoint = endpoint
        self.required_param = required_param
        self.given_params = given_params

    def __str__(self):
        return (
            f'For {self.endpoint!r} Called with params: {self.given_params}'
            f' but missing required param - "{self.required_param}".'
        )


class HttpAPI:
    def __init__(self, name, url, endpoints={}, default_params={}):
        self.validate_url(url)

        self.name = name
        self.url = url
        self.endpoints = endpoints
        self.default_params = default_params

    def register_endpoint(self, name, path, **kwargs):
        endpoint = HttpEndpoint(name, path, **kwargs)
        self.endpoints[endpoint.name] = endpoint
        return self

    def __getattr__(self, name):
        if not name.startswith('call_'):
            raise AttributeError

        endpoint_name = name.split('call_')[-1]
        endpoint = self.endpoints.get(endpoint_name)

        if not endpoint:
            raise NotFoundEndpointException(
                f'"{endpoint_name}" not declared in {self!r}'
            )

        endpoint.api = self

        return endpoint

    def __repr__(self):
        return f'HttpAPI({self.name}, {self.url}, endpoints={self.endpoints!r})'

    @staticmethod
    def validate_url(str_url):
        if str_url:
            url = urlparse(str_url)
            if bool(url.scheme) and bool(url.netloc):
                return

        raise HttpAPIException(f'Invalid url: "{str_url}"')


class HttpEndpoint:
    def __init__(self, name, path, method='get', required_params=[], timeout=10, handler=None, exception_handler=None):
        self.name = name
        self.path = path
        self.method = method
        self.required_params = required_params
        self.timeout = timeout
        self.handler = handler
        self.exception_handler = exception_handler

        self.api = None

    def __call__(self, **kwargs):
        http_method = getattr(self, self.method)
        return http_method(**kwargs)

    def get(self, params={}, headers={}):
        uri = urljoin(self.api.url, self.path)
        params = {**self.api.default_params, **params}

        self._check_given_params(params)

        try:
            response = requests.get(
                uri,
                params=params,
                headers=headers,
                timeout=self.timeout
            )
        except RequestException as exception:
            if callable(self.exception_handler):
                return self.exception_handler(self.api, self, exception)
            raise exception

        if callable(self.handler):
            response = self.handler(self.api, self, response)

        return response

    def post(self, data={}, json={}, headers={}, files={}):
        uri = urljoin(self.api.url, self.path)

        # self._check_given_params(params)

        response = self._process_request(
            lambda: requests.post(
                uri,
                files=files,
                data=data,
                json=json,
                headers=headers,
                timeout=self.timeout
            )
        )

        return response

    def __repr__(self):
        return f"HttpEndpoint(name='{self.name}', path='{self.path}', '{self.method}', required_params={self.required_params!r})"

    def _check_given_params(self, given_params):
        given_params_names = given_params.keys()
        for required_param in self.required_params:
            if required_param not in given_params_names:
                raise ReqiredParamsNotPresentException(self, required_param, given_params_names)

    def _process_request(self, request_callable):
        try:
            response = request_callable()
        except RequestException as exception:
            if callable(self.exception_handler):
                return self.exception_handler(self.api, self, exception)
            raise exception

        if callable(self.handler):
            response = self.handler(self.api, self, response)

        return response
