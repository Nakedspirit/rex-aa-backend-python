from collections import namedtuple

from . import HttpAPI
from rex_backend.application import app
from ._shared import default_rex_api_handler, default_rex_api_exception_handler
from rex_backend.helpers import flat_list


URL = app.config['REX_LISTING_API_URL']

TOOL = {
    'VISITOR_QUESTIONS': 'openhouseleads',
}

rex_questions_api = HttpAPI(
    'REX questions',
    URL,
)
rex_questions_api.register_endpoint(
    'get_questions',
    'questions/get-questions-survey',
    method='get',
    required_params=['tool'],
    handler=default_rex_api_handler,
    exception_handler=default_rex_api_exception_handler,
)
rex_questions_api.register_endpoint(
    'get_questions_answers',
    'questions/get-answers',
    method='get',
    required_params=['listing_guid', 'tool'],
    handler=default_rex_api_handler,
    exception_handler=default_rex_api_exception_handler,
)
rex_questions_api.register_endpoint(
    'add_answers',
    'questions/add-multiple-answers',
    method='post',
    handler=default_rex_api_handler,
    exception_handler=default_rex_api_exception_handler,
    timeout=10,
)


def make_source_for_visitors_questions(showing_id, signin_id):
    return f'gig-app;showing_id:{showing_id};signin_id:{signin_id}'


class RexQuestionsException(Exception):
    pass


class RexQuestionsContainer(object):
    def __init__(self, api_response):
        pages = api_response.get('content', {}).get('result', {}).get('pages')

        if not pages:
            raise RexQuestionsException('Invalid API response. Not found pages.')

        self.groups = RexGroup.init_many(pages)
        self.questions = flat_list([group.questions for group in self.groups])
        self.questions_dict = dict([(question.guid, question)
                                   for question in self.questions])

    def __repr__(self):
        return 'RexQuestions object - container with question from REX API'

    def get_question_by_guid(self, guid):
        return self.questions_dict.get(guid)

    def format_answers_to_send_to_rex(self, answers):
        correct_format_answers = {}
        for answer in answers:
            guid = answer.get('question_id')
            question = self.get_question_by_guid(guid)
            if not question:
                # log that theres no question with such id
                continue

            answers_to_question = answer.get('answers', [])

            if question.graphlq_type != 'MultipleQuestion':
                answers_to_question = answers_to_question[0] if len(answers_to_question) > 0 else None

            if answers_to_question:
                correct_format_answers[guid] = answers_to_question

        return correct_format_answers

    def process_answers(self, api_response):
        raw_questions = api_response.get('content', {}).get('result')

        if not raw_questions:
            raise RexQuestionsException('Invalid API response. Not found questions.')

        for raw_question in raw_questions:
            raw_answers = raw_question.get('answer')
            question_guid = raw_question['question_guid']
            question = self.questions_dict.get(question_guid)

            if not question or not raw_answers:
                continue

            question.answers = [RexAnswer(**raw_answer) for raw_answer in raw_answers]


RexOption = RexOption = namedtuple('RexOption', 'value text selected')
GraphqlQuestion = namedtuple('GraphqlQuestion', 'id title order is_required options type response')


class RexGroup:
    @classmethod
    def init_many(cls, raw_groups):
        return [cls(order=(i + 1), **raw_group) for i, raw_group in enumerate(raw_groups)]

    def __init__(self, title='', order=None, elements=[]):
        if not title or title.replace(' ', '') == '':
            title = f'title#{order}'

        self.title = title
        self.order = order
        self.questions = [RexQuestion(self, order=(i + 1), **element) for i, element in enumerate(elements)]

    def __repr__(self):
        return (
            f'{self.__class__}'
            f' title: "{self.title}"; order: {self.order}; elements_qty: {len(self.elements)};'
        )

    def to_graphql(self, is_completed=False):
        return {
            'id': self.order,
            'title': self.title,
            'order': self.order,
            'is_completed': bool(is_completed),
            'questions': [question.to_graphql() for question in self.questions],
        }


class RexQuestion:
    def __init__(self, group, order=None, **kwargs):
        self.group = group
        self.order = order
        self.name = kwargs.get('name')
        self.title = kwargs.get('title')
        self.guid = kwargs.get('valueName')
        self.is_required = kwargs.get('isRequired')
        self.type = kwargs.get('type')
        self.options = [RexOption(selected=False, **choice) for choice in kwargs.get('choices', [])]
        self.original_data = dict(kwargs)

        answers = kwargs.get('answers') or []
        self.answers = [RexAnswer(**answer) for answer in answers]

        assert self.name and self.title and self.guid

    def __repr__(self):
        return (
            f'{self.__class__}'
            f' name: "{self.name}"; guid: "{self.guid}"; title: "{self.title}"; type: {self.type};'
        )

    @property
    def graphlq_type(self):
        return {
            'radiogroup': 'SingularQuestion',
            'dropdown': 'SingularQuestion',
            'checkbox': 'MultipleQuestion'
        }.get(self.type) or 'TextQuestion'

    def to_graphql(self):
        response = None
        if self.answers:
            if self.graphlq_type == 'TextQuestion':
                response = self.answers[0].value
            elif self.graphlq_type == 'SingularQuestion':
                selected_value = self.answers[0].value
                for i, option in enumerate(self.options):
                    if option.value == selected_value:
                        self.options[i] = RexOption(
                            value=option.value,
                            text=option.text,
                            selected=True,
                        )
                        break
            elif self.graphlq_type == 'MultipleQuestion':
                for answer in self.answers:
                    for i, option in enumerate(self.options):
                        if option.value == answer.value:
                            self.options[i] = RexOption(
                                value=option.value,
                                text=option.text,
                                selected=True,
                            )
                            break

        return GraphqlQuestion(
            id=self.guid,
            order=self.order,
            title=self.title,
            is_required=bool(self.is_required),
            options=self.options,
            type=self.graphlq_type,
            response=response
        )


class RexAnswer:
    TYPES = {
        'text': 'text',
        'date': 'date',
        'int': 'int',
        'bool': 'bool',
    }

    def __init__(self, answer_text, answer_int, answer_date, answer_bool, source=None, recorded_by=None, guid=None):
        self.source = source
        self.recorded_by = recorded_by
        self.guid = guid

        value = None
        if answer_text:
            self.type = self.TYPES.get('text')
            value = answer_text
        elif answer_int:
            self.type = self.TYPES.get('int')
            value = answer_int
        elif answer_date:
            self.type = self.TYPES.get('date')
            value = answer_date
        elif answer_bool:
            self.type = self.TYPES.get('date')
            value = answer_date

        self.value = value

    def __repr__(self):
        return f'RexAnswer(type: {self.type}, value: {self.value})'
