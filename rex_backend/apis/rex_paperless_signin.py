from . import HttpAPI
from rex_backend.application import app
from ._shared import default_rex_api_handler, default_rex_api_exception_handler


URL = app.config['REX_PAPERLESS_SIGNIN_URL']
PASSWORD = app.config['REX_PAPERLESS_SIGNIN_PASSWORD']

rex_paperless_signin_api = HttpAPI(
    'REX paperless signin',
    URL,
    default_params={
        'password': PASSWORD
    }
)

rex_paperless_signin_api.register_endpoint(
    'get_phone_number',
    'api/phone-number',
    method='get',
    required_params=['password', 'listing_id'],
    handler=default_rex_api_handler,
    exception_handler=default_rex_api_exception_handler,
)

# VISITORS
rex_paperless_signin_api.register_endpoint(
    'get_visitors',
    'api/signins',
    method='get',
    required_params=['password', 'open_house_guid']
)

rex_paperless_signin_api.register_endpoint(
    'add_visitor',
    'api/add-user',
    method='post',
    required_params=['password', 'open_house_guid', 'open_house_phone_number', 'phone_number', 'email', 'name'],
    handler=default_rex_api_handler,
    exception_handler=default_rex_api_exception_handler,
)

rex_paperless_signin_api.register_endpoint(
    'sign_visitor',
    'api/add-signin',
    method='post',
    required_params=['password', 'open_house_guid', 'open_house_phone_number', 'phone_number', 'email'],
    handler=default_rex_api_handler,
    exception_handler=default_rex_api_exception_handler,
)

# PHOTOS
rex_paperless_signin_api.register_endpoint(
    'upload_photos',
    'api/photos',
    method='post',
    required_params=['password', 'open_house_guid'],
    handler=default_rex_api_handler,
    exception_handler=default_rex_api_exception_handler,
    timeout=30,
)

rex_paperless_signin_api.register_endpoint(
    'get_photos',
    'api/photos',
    method='get',
    required_params=['password', 'open_house_guid'],
    handler=default_rex_api_handler,
    exception_handler=default_rex_api_exception_handler,
    timeout=10,
)
