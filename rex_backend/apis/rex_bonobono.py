from . import HttpAPI
from rex_backend.application import app
from ._shared import default_rex_api_handler, default_rex_api_exception_handler


URL = app.config['REX_BONOBONO_API_URL']

rex_bonobono_api = HttpAPI(
    'REX bonobono API',
    URL,
)
rex_bonobono_api.register_endpoint(
    'get_leaderboard_stats',
    'associate_agents',
    method='get',
    required_params=['sort_by', 'date_start', 'date_end'],
    handler=default_rex_api_handler,
    exception_handler=default_rex_api_exception_handler,
    timeout=10,
)
