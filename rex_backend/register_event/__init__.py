import logging
import json


from .handlers import EVENT_HANDLERS


logger = logging.getLogger('rex_backend')


class NoEventHandlerException(Exception):
    '''
    It occurs when not handler for the event found
    '''
    def __init__(self, event):
        self.event = event

    def __str__(self):
        return f'{self.event.name} has not declared handler in EVENT_HANDLERS'


class EventDispatcher:
    '''
    It finds the right handler for the Event.
    Event handlers are declared in _handlers.py
    '''
    def __init__(self, **kwargs):
        self.event = Event(**kwargs)

    def __call__(self):
        return self.handler(event=self.event)

    @property
    def handler(self):
        event_name = self.event.name
        handler = EVENT_HANDLERS.get(event_name, None)

        if handler is None:
            raise NoEventHandlerException(self.event)

        return handler


class Event:
    '''
    Just a container with an event data
    '''
    def __init__(self, event_name, object_id, data='{}'):
        self.name = event_name
        self.object_id = object_id
        self.data = json.loads(data)

    def __repr__(self):
        return f'Event({self.name}, {self.name}, data="{self.data}")'
