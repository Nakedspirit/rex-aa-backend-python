import logging


from rex_backend.interactors.interactor import Interactor
from rex_backend.repositories.rex_showing_repo import RexShowingRepo
from ._shared import load_devices_by_rex_users_ids, increment_devices_badges_count, send_pushes_about_event


logger = logging.getLogger('rex_backend')


class ShowingConfirmedBySeller(Interactor):
    METHODS_TO_RUN = [
        'load_rex_showing',
        'load_devices',
        'send_pushes'
    ]
    REQUIRED_ARGS = [
        'event'
    ]

    def load_rex_showing(self, event):
        rex_showing = RexShowingRepo.load_rex_showing(event.object_id)

        if not rex_showing or rex_showing.status != 'Scheduled' or rex_showing.claimed_by_id is None:
            logger.warn(f'RexShowing = {rex_showing} # is none or not Scheduled or not claimed')
            self.finish(False)

        self.context.rex_showing = rex_showing

    def load_devices(self, event):
        rex_users_ids = [int(self.context.rex_showing.claimed_by_id)]
        devices = load_devices_by_rex_users_ids(rex_users_ids)

        if len(devices) == 0:
            logger.warn(f'Devices are empty')
            self.finish(False)

        increment_devices_badges_count(devices)

        self.context.devices = devices

    def send_pushes(self, event):
        send_pushes_about_event(
            event=event,
            rex_showing=self.context.rex_showing,
            devices=self.context.devices,
        )
        return True
