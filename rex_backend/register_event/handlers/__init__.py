from .showing_created import ShowingCreated
from .showing_claimed import ShowingClaimed
from .showing_confirmed_by_seller import ShowingConfirmedBySeller
from .showing_declined_by_seller import ShowingDeclinedBySeller
from .showing_cancelled import ShowingCancelled
from .signin_recorded import SigninRecorded


showing_created = ShowingCreated()
showing_claimed = ShowingClaimed()
showing_cancelled = ShowingCancelled()
confirmed_by_seller = ShowingConfirmedBySeller()
declined_by_seller = ShowingDeclinedBySeller()
signin_recorded = SigninRecorded()


EVENT_HANDLERS = {
    'SHOWING_CREATED': showing_created,
    'SHOWING_CLAIMED': showing_claimed,
    'SHOWING_CONFIRMED_BY_SELLER': confirmed_by_seller,
    'SHOWING_DECLINED_BY_SELLER': declined_by_seller,
    'SHOWING_CANCELLED': showing_cancelled,
    'OPEN_HOUSE_CREATED': showing_created,
    'OPEN_HOUSE_CLAIMED': showing_claimed,
    'OPEN_HOUSE_CONFIRMED_BY_SELLER': confirmed_by_seller,
    'OPEN_HOUSE_DECLINED_BY_SELLER': declined_by_seller,
    'OPEN_HOUSE_CANCELLED': showing_cancelled,
    'SIGNIN_RECORDED': signin_recorded,
}
