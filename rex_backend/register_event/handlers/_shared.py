import logging

from rex_backend.application import db
from rex_backend.repositories.rex_showing_repo import RexShowingRepo
from rex_backend.repositories.device_repo import DeviceRepo
from rex_backend.services import firebase_cm_service
from rex_backend.helpers import make_rex_showing_address


logger = logging.getLogger('rex_backend')


PUSH_NOTIFICATION_TITLES_FOR_EVENTS = {
    'SHOWING_CREATED': 'New Showing',
    'SHOWING_CLAIMED': 'Showing was claimed by a different Agent',
    'SHOWING_CONFIRMED_BY_SELLER': 'Showing confirmed by Seller',
    'SHOWING_DECLINED_BY_SELLER': 'Showing declined',
    'SHOWING_CANCELLED': 'Showing cancelled',
    'OPEN_HOUSE_CREATED': 'New Open House',
    'OPEN_HOUSE_CLAIMED': 'Open House was claimed by a different Agent',
    'OPEN_HOUSE_CONFIRMED_BY_SELLER': 'Open House confirmed by Seller',
    'OPEN_HOUSE_DECLINED_BY_SELLER': 'Open House declined',
    'OPEN_HOUSE_CANCELLED': 'Open House cancelled',
    'SIGNIN_RECORDED': 'Visitor added to Open House'
}


def load_associated_rex_users_ids_without_claimed_by_id(rex_showing):
    claimed_by_id = rex_showing.claimed_by_id
    rex_users = RexShowingRepo.load_rex_users_by_rex_showing(rex_showing.id)
    return [rex_user.id for rex_user in rex_users if str(rex_user.id) != claimed_by_id]


def load_devices_by_rex_users_ids(rex_users_ids):
    devices = []

    if len(rex_users_ids) > 0:
        devices += DeviceRepo.load_devices_by_rex_user_ids_with_push_tokens(rex_users_ids)

    devices += DeviceRepo.load_demo_user_devices_with_push_tokens()

    return devices


def increment_devices_badges_count(devices, num=1):
    for device in devices:
        device.badge_count += num
        db.session.add(device)

    db.session.commit()

    return devices


def send_pushes_about_event(event=None, rex_showing=None, devices=None):
    title = PUSH_NOTIFICATION_TITLES_FOR_EVENTS.get(event.name)
    address = make_rex_showing_address(rex_showing)

    body = f'{address}'
    data = {
        'type': event.name,
        'showingId': str(rex_showing.id),
        'listingId': str(rex_showing.data.get('listingId')),
        'claimedAt': str(rex_showing.claimed_at)
    }
    logger.info(
        'PushNotification('
        f' title: {title}'
        f' body: {body}'
        f' data: {data}'
        ')'
        f' sending to {devices}'
    )
    result = firebase_cm_service.send_messages(devices, title, body, data=data)
    logger.info(f'Firebase result: {result}')

    return result
