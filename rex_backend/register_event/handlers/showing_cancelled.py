import logging


from rex_backend.interactors.interactor import Interactor
from rex_backend.models.base import UserAction, UserActionName, User, LinkToRexUser
from rex_backend.repositories.rex_showing_repo import RexShowingRepo
from ._shared import load_devices_by_rex_users_ids, increment_devices_badges_count, send_pushes_about_event


logger = logging.getLogger('rex_backend')


class ShowingCancelled(Interactor):
    METHODS_TO_RUN = [
        'load_rex_showing',
        'load_devices',
        'send_pushes'
    ]
    REQUIRED_ARGS = [
        'event'
    ]

    def load_rex_showing(self, event):
        rex_showing_id = event.object_id
        rex_showing = RexShowingRepo.load_rex_showing(rex_showing_id)

        if not rex_showing or not rex_showing.claimed_by_id:
            logger.warn(f'RexShowing = {rex_showing} # is none or not claimed by anyone')
            self.finish(False)

        self.context.rex_showing = rex_showing

    def load_devices(self, event):
        rex_user_id = int(self.context.rex_showing.claimed_by_id)

        is_user_hide_showing = UserAction.query.join(
            User, User.id == UserAction.user_id
        ).join(
            LinkToRexUser, LinkToRexUser.user_id == User.id
        ).filter(
            UserAction.name == UserActionName.hide_showing.value,
            UserAction.object_id == int(event.object_id),
            LinkToRexUser.rex_user_id == rex_user_id
        ).first()

        if is_user_hide_showing:
            logger.warn('Stop processing ShowingCancelled interactor. The user hid this Showing.')
            self.finish(True)

        rex_users_ids = [rex_user_id]
        devices = load_devices_by_rex_users_ids(rex_users_ids)

        if len(devices) == 0:
            logger.warn(f'Devices are empty')
            self.finish(False)

        increment_devices_badges_count(devices)

        self.context.devices = devices

    def send_pushes(self, event):
        send_pushes_about_event(
            event=event,
            rex_showing=self.context.rex_showing,
            devices=self.context.devices,
        )
        return True
