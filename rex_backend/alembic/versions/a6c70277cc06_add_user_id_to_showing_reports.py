"""add user_id to showing_reports

Revision ID: a6c70277cc06
Revises: 2dfb40846797
Create Date: 2019-04-15 13:01:41.822028

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a6c70277cc06'
down_revision = '2dfb40846797'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('showing_reports', sa.Column('user_id', sa.BIGINT(), nullable=False))
    op.create_index(op.f('ix_showing_reports_user_id'), 'showing_reports', ['user_id'], unique=False)
    op.drop_index('ix_showing_reports_rex_showing_id', table_name='showing_reports')
    op.create_unique_constraint(None, 'showing_reports', ['rex_showing_id'])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'showing_reports', type_='unique')
    op.create_index('ix_showing_reports_rex_showing_id', 'showing_reports', ['rex_showing_id'], unique=True)
    op.drop_index(op.f('ix_showing_reports_user_id'), table_name='showing_reports')
    op.drop_column('showing_reports', 'user_id')
    # ### end Alembic commands ###
