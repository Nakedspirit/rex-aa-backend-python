"""add showing_reports

Revision ID: 2dfb40846797
Revises: 6576e7d74dc1
Create Date: 2019-04-11 06:03:32.452718

"""
import enum
from alembic import op
import sqlalchemy as sa


class ShowingReportStatus(enum.Enum):
    buyer_not_appeared = 0
    agent_could_not_make_it = 1
    canceled = 2
    ok = 3


# revision identifiers, used by Alembic.
revision = '2dfb40846797'
down_revision = '6576e7d74dc1'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('showing_reports',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('rex_showing_id', sa.BIGINT(), nullable=False),
        sa.Column('status', sa.Enum(ShowingReportStatus), nullable=False),
        sa.Column('status_comment', sa.String()),
        sa.Column('is_call_buyer_completed', sa.Boolean(), server_default='0'),
        sa.Column('is_external_feedback_form_completed', sa.Boolean(), server_default='0'),
        sa.Column('is_photo_submitted', sa.Boolean(), server_default='0'),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_showing_reports_rex_showing_id'), 'showing_reports', ['rex_showing_id'], unique=True)
    op.create_index(op.f('ix_showing_reports_status'), 'showing_reports', ['status'], unique=False)


def downgrade():
    op.drop_index(op.f('ix_showing_reports_rex_showing_id'), table_name='showing_reports')
    op.drop_index(op.f('ix_showing_reports_status'), table_name='showing_reports')
    op.drop_table('showing_reports')
