"""create report_groups table

Revision ID: 96e8de40b32c
Revises: 5186b409d71a
Create Date: 2019-09-10 14:48:10.187076

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '96e8de40b32c'
down_revision = '5186b409d71a'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('report_groups',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('title', sa.String(), nullable=False),
        sa.Column('status', sa.String(), nullable=True),
        sa.Column('showing_report_id', sa.BIGINT(), nullable=False),
        sa.ForeignKeyConstraint(['showing_report_id'], ['showing_reports.id'], ),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('title', 'showing_report_id', name='_uniq_group_of_questions')
    )
    op.create_index(op.f('ix_report_groups_status'), 'report_groups', ['status'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_report_groups_status'), table_name='report_groups')
    op.drop_table('report_groups')
    # ### end Alembic commands ###
