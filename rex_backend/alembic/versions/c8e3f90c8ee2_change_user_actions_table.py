"""change user_actions table

Revision ID: c8e3f90c8ee2
Revises: ea64af7558ab
Create Date: 2019-10-17 15:40:45.153863

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c8e3f90c8ee2'
down_revision = 'ea64af7558ab'
branch_labels = None
depends_on = None


def upgrade():
    op.execute(sa.table('user_actions').delete())

    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('user_actions', sa.Column('user_id', sa.Integer(), nullable=False))
    op.drop_constraint('_uniq_user_action', 'user_actions', type_='unique')
    op.create_unique_constraint('_uniq_user_action', 'user_actions', ['name', 'user_id', 'object_id'])
    op.drop_constraint('user_actions_device_id_fkey', 'user_actions', type_='foreignkey')
    op.create_foreign_key('user_actions_user_id_fkey', 'user_actions', 'users', ['user_id'], ['id'])
    op.drop_column('user_actions', 'device_id')
    # ### end Alembic commands ###


def downgrade():
    op.execute(sa.table('user_actions').delete())

    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('user_actions', sa.Column('device_id', sa.VARCHAR(), autoincrement=False, nullable=False))
    op.drop_constraint('user_actions_user_id_fkey', 'user_actions', type_='foreignkey')
    op.create_foreign_key('user_actions_device_id_fkey', 'user_actions', 'devices', ['device_id'], ['id'])
    op.drop_constraint('_uniq_user_action', 'user_actions', type_='unique')
    op.create_unique_constraint('_uniq_user_action', 'user_actions', ['name', 'device_id', 'object_id'])
    op.drop_column('user_actions', 'user_id')
    # ### end Alembic commands ###
