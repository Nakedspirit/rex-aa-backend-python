"""add is_questions_completed to showing_reports

Revision ID: 3b42eeb56c5f
Revises: 1138f6411e3e
Create Date: 2019-05-08 05:14:41.720824

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '3b42eeb56c5f'
down_revision = '1138f6411e3e'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('showing_reports', sa.Column('is_questions_completed', sa.Boolean(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('showing_reports', 'is_questions_completed')
    # ### end Alembic commands ###
