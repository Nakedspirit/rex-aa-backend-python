class RexException(Exception):
    pass


class AuthException(RexException):
    pass


class NotFoundException(RexException):
    def __init__(self, entity, message=None):
        self.entity = entity
        self.message = message

    def __str__(self):
        message = self.message or 'not found'
        return f'{self.entity} {message}'


class InvalidOauthToken(RexException):
    def __init__(self, provider, error, message):
        self.provider = provider
        self.error = error
        self.message = message


class NotFoundRexUser(RexException):
    def __init__(self, field, value, message=None):
        self.field = field
        self.value = value
        self.message = message


class NotFoundRexAssociatedAgentException(NotFoundRexUser):
    pass


class RestAPIWrongResponseException(RexException):
    def __init__(self, data):
        self.data = data
