import os


ENV_VARS = {
    'TESTING': 'True',
    'REGISTER_EVENT_SECRET_KEY': 'hello-ama-secret-token',
    'GOOGLE_CLOUD_BASE64_CERT': '',
    'GOOGLE_SPREADSHEET_ID': '',
    'GOOGLE_SPREADSHEET_WHITELIST_RANGE': '',
    'GOOGLE_SPREADSHEET_DEVELOPER_RANGE': '',
    'GOOGLE_OAUTH_WEB_CLIENT_IDS': 'test.apps.googleusercontent.com',
    'GOOGLE_OAUTH_DOMAIN_ALLOWED_TO_SIGNIN': 'gmail.com',
    'FIREBASE_CM_FAKE_MODE': 'True',
    'REX_PAPERLESS_SIGNIN_URL': 'http://valid.url',
    'REX_LISTING_API_URL': 'http://valid.url',
    'REX_BONOBONO_API_URL': 'http://bonobono'
}

for key, value in ENV_VARS.items():
    os.environ[key] = value
