from collections import namedtuple


from rex_backend.application import jwt_service


def dict2obj(given_dict):
    return namedtuple('Struct', given_dict.keys())(*given_dict.values())


def make_jwt_with_device(user_and_identity_orm_obj):
    user = user_and_identity_orm_obj['user']
    identity = user_and_identity_orm_obj['identity']
    device = user_and_identity_orm_obj['device']
    other_attributes = {'device_id': device.id}
    return jwt_service.jwt_for_user(user, identity, other_attributes)


def make_jwt_without_device(user_and_identity_orm_obj):
    user = user_and_identity_orm_obj['user']
    identity = user_and_identity_orm_obj['identity']
    return jwt_service.jwt_for_user(user, identity)


def graphql_request(client, jwt, query):
    authorization = f"Bearer {jwt}"

    response = client.post(
        '/graphql',
        data = { 'query': query },
        headers= { 'Authorization': authorization }
    )
    assert response.json, 'response should contain JSON'
    return response.json
