from rex_backend.wrappers.user import DemoUser
from rex_backend.tests.factories import ShowingReportFactory


def test_demo_user_completed_showings(session, current_user):
    ShowingReportFactory.create_batch(4, ready_for_payment=True)
    session.commit()

    demo_user = DemoUser(current_user)
    query = demo_user.completed_showings_query()
    
    str_query = str(query)
    assert 'NOT IN' in str_query


def test_demo_user_completed_showings_without_reports(session, current_user):
    demo_user = DemoUser(current_user)
    query = demo_user.completed_showings_query()
    
    str_query = str(query)
    assert 'NOT IN' not in str_query
