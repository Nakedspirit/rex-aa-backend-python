from collections import OrderedDict
from unittest import mock
from rex_backend.application import jwt_service
from rex_backend.models.rex_showings import RexUser


def test_query_not_real_field(graphql_client):
    graphql_response = graphql_client.execute('{ notRealField }')
    assert ('data' in graphql_response) == False
    assert ('errors' in graphql_response) == True

    error_message = graphql_response['errors'][0]['message']
    assert  error_message == 'Cannot query field "notRealField" on type "Query".'


@mock.patch('rex_backend.graphql.resolvers.showing.load_showing', return_value=None)
def test_graphql_middleware(mock_showing, client, valid_jwt):
    query = '''
        query {
          showing(id: 1) {
            id
          }
        }
    '''

    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': "Bearer {}".format(valid_jwt) })
    assert response.json.get('errors') is None


def test_graphql_middleware_where_not_permitted(client, valid_jwt, user_and_identity_orm_obj):
    query = '''
        mutation {
          completeCallBuyer(input: { showingReportId: 1 }) {
            result {
              id
            }
          }
        }
    '''
    user = user_and_identity_orm_obj['user']
    identity = user_and_identity_orm_obj['identity']
    device = user_and_identity_orm_obj['device']
    other_attributes = {'device_id': device.id, 'disable_mutations': True}
    valid_jwt = jwt_service.jwt_for_user(user, identity, other_attributes)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': "Bearer {}".format(valid_jwt) })
    assert response.json.get('errors') is not None
    assert response.json.get('errors')[0].get('message') == "You don't have permission for this action. \nContact REX operations."


def test_graphql_middleware_without_token(client):
    query = '''
        query {
          showing(id: 1) {
            id
          }
        }
    '''
    response = client.post('/graphql', data = { 'query': query })
    assert response.json['errors'] != None
    assert response.json['errors'][0]['message'] == "JWT payload is invalid"

def test_graphql_middleware_with_invalid_token(client):
    query = '''
        query {
          showing(id: 1) {
            id
          }
        }
    '''
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': "Bearer invalid"})
    assert response.json['errors'] != None
    assert response.json['errors'][0]['message'] == "JWT payload is invalid"
