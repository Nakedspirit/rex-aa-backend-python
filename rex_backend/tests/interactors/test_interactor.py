import pytest

from rex_backend.interactors.interactor import Interactor, ArgumentNotFoundException
from rex_backend.models.base import User


class TestInteractor(Interactor):
    METHODS_TO_RUN = [
        'first',
        'second',
        'third'
    ]

    def first(self, **kwargs):
        self.context.new_dict = {'a': 1, 'b': 2}

    def second(self, **kwargs):
        self.context.value = 'ok'
        self.context.value_bool = True
        self.context.object = User()

    def third(self, **kwargs):
        self.context.value = 'no'
        self.context.value_bool = False

        return 'Thanks for attention'


class ArgsInteractor(Interactor):
    METHODS_TO_RUN = [
        'say_hi'
    ]

    REQUIRED_ARGS = ['name', 'last_name']

    def say_hi(self, **kwargs):
        return f'Hi {kwargs.get("name")} {kwargs.get("last_name")}!'


class FinishInteractor(Interactor):
    METHODS_TO_RUN = ['first', 'second']
    REQUIRED_ARGS = ['finish']

    def first(self, **kwargs):
        if kwargs.get('finish'):
            self.finish('first')

    def second(self, **kwargs):
        return 'second'


def test_interactor_internal_logic(client):
    interactor = TestInteractor()
    result = interactor.call()
    assert result == 'Thanks for attention'


def test_interactor_internal_logic_with_enabled_logs(client):
    interactor = TestInteractor(need_logs=True)
    result = interactor.call()
    assert result == 'Thanks for attention'


def test_interactor_callable(client):
    interactor = TestInteractor()
    assert interactor() == 'Thanks for attention'


def test_required_args_feature_of_interactor(client):
    interactor = ArgsInteractor()

    assert interactor(name='John', last_name='Doe') == 'Hi John Doe!'
    assert interactor(name='John', last_name=False) == 'Hi John False!'

    with pytest.raises(ArgumentNotFoundException):
        interactor.call(name='John')
        
    with pytest.raises(ArgumentNotFoundException):
        interactor.call(last_name='Doe')

    with pytest.raises(ArgumentNotFoundException):
        interactor.call(name=None, last_name='Doe')


def test_finish_feature_of_interactor(client):
    interactor = FinishInteractor()
    assert interactor(finish=True) == 'first'
    assert interactor(finish=False) == 'second'