from unittest import mock

from rex_backend.application import db
from rex_backend.models.base import ReportGroup
from rex_backend.tests.factories import ShowingReportFactory, ReportGroupFactory
from rex_backend.interactors.create_groups_for_showing_report import CreateGroupsForShowingReport



def test_сreate_groups_for_showing_report(session):
    showing_report = ShowingReportFactory.create()
    session.commit()

    groups_titles = ['Group Title 1', 'Group Title 2']
    interactor = CreateGroupsForShowingReport()

    new_groups = interactor.call(
        showing_report_id=showing_report.id,
        groups_titles=groups_titles
    )
    assert new_groups

    assert ReportGroup.query.filter(ReportGroup.title == 'Group Title 1').first(), '1st group should exists'
    assert ReportGroup.query.filter(ReportGroup.title == 'Group Title 2').first(), '2st group should exists'


def test_сreate_groups_for_showing_report_with_existing_groups(session):
    showing_report = ShowingReportFactory.create()
    group1 = ReportGroupFactory.create(title='Group Title 1', showing_report=showing_report)
    group2 = ReportGroupFactory.create(title='Group Title 2', showing_report=showing_report)
    session.commit()

    existing_group_ids = [group1.id, group2.id]
    groups_titles = ['Group Title 1', 'Group Title 2']

    interactor = CreateGroupsForShowingReport()
    new_groups = interactor.call(
        showing_report_id=showing_report.id,
        groups_titles=groups_titles
    )
    assert new_groups

    group1 = ReportGroup.query.filter(ReportGroup.title == 'Group Title 1').first()
    assert group1, '1st group should exists'
    assert group1.id not in existing_group_ids


    group2 = ReportGroup.query.filter(ReportGroup.title == 'Group Title 2').first()
    assert group2, '2st group should exists'
    assert group2.id not in existing_group_ids


def test_сreate_groups_for_showing_report_with_completed_showing_report(session):
    showing_report = ShowingReportFactory.create(is_questions_completed=True)
    session.commit()

    groups_titles = ['Group Title 1', 'Group Title 2']

    interactor = CreateGroupsForShowingReport()
    new_groups = interactor.call(
        showing_report_id=showing_report.id,
        groups_titles=groups_titles
    )
    assert new_groups

    group1 = ReportGroup.query.filter(ReportGroup.title == 'Group Title 1').first()
    assert group1, '1st group should exists'
    assert group1.status == 'completed'
