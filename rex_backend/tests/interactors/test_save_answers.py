from unittest import mock

from rex_backend.application import db
from rex_backend.tests.factories.showing_report_factory import ShowingReportFactory
from rex_backend.tests.factories.report_answer_factory import ReportAnswerFactory
from rex_backend.models.base import ReportAnswer
from rex_backend.interactors.save_answers import SaveAnswers



def test_interactor(current_user):
    showing_report = ShowingReportFactory.create(
        ready_for_payment=True
    )
    answer = ReportAnswerFactory.create(
        rex_question_guid='test1',
        showing_report=showing_report,
        data={'answers': ['no']}
    )
    db.session.commit()

    interactor = SaveAnswers()
    interactor.call(
        showing_report=showing_report,
        answers_data=[
            { 'question_id': 'test1', 'answers': ['ok', 'go'] },
            { 'question_id': 'test2', 'answers': ['wooooooooo'] }
        ]
    )

    answer = ReportAnswer.query.filter(ReportAnswer.rex_question_guid == 'test1').first()
    assert answer  
    assert answer.values == ['ok', 'go']

    answer = ReportAnswer.query.filter(ReportAnswer.rex_question_guid == 'test2').first()
    assert answer
    assert answer.showing_report == showing_report