import datetime

import pytest
from unittest import mock

from rex_backend.application import db
from rex_backend.tests.factories import ShowingReportFactory, ReportQuestionFactory, ReportAnswerFactory, RexShowingFactory
from rex_backend.interactors.send_email_with_showing_report import SendEmailWithShowingReport
from rex_backend.tests.helpers import dict2obj
from rex_backend.utils.aws_ses import AWSSESRespose


fake_rex_listing = dict2obj({
    'id': 34507,
    'guid': '5211977afccf429c88c033d6ac34084e'
})

fake_rex_api_response = {
    'content': {
        'teams': {
            fake_rex_listing.guid: [
                {
                    'responsibility': 'CSS',
                    'email': 'test@test.test'
                }
            ]
        }
    }
}


fake_rex_seller = dict2obj({
    'email': 'test@test.test'
})


@mock.patch('rex_backend.repositories.rex_listing_repo.RexListingRepo.load_seller', return_value=fake_rex_seller)
@mock.patch('rex_backend.models.rex_listings.RexListing.query')
@mock.patch('rex_backend.interactors.send_email_with_showing_report.rest_api_service.get', return_value=fake_rex_api_response)
@mock.patch('rex_backend.utils.aws_ses.AWSSES.send_email', return_value=AWSSESRespose({'MessageId': 'ok'}))
def test_interactor(aws_ses_mock, rex_api_mock, rex_listing_mock, seller_mock,  session):
    rex_listing_mock.filter.return_value.first.return_value = fake_rex_listing
    showing_report = ShowingReportFactory.create(
        ready_for_payment=True
    )
    showing_report._rex_showing = RexShowingFactory.make(id=1)
    question = ReportQuestionFactory.create(
        rex_question_guid='test1'
    )
    question2 = ReportQuestionFactory.create(
        rex_question_guid='test2',
        data={}
    )
    answer = ReportAnswerFactory.create(
        showing_report=showing_report,
        rex_question_guid='test1'
    )
    answer2 = ReportAnswerFactory.create(
        showing_report=showing_report,
        rex_question_guid='test2',
        data={'values': ['just a text']}
    )
    db.session.commit()

    interactor = SendEmailWithShowingReport()
    interactor.call(showing_report=showing_report)

    assert interactor.context.email_message_for_seller.html
    assert 'Open House' not in interactor.context.email_message_for_seller.html
    assert 'Showing' in interactor.context.email_message_for_seller.html

    assert interactor.context.email_message_for_css
    assert 'Open House' not in interactor.context.email_message_for_css.html
    assert 'Showing' in interactor.context.email_message_for_css.html

    assert aws_ses_mock.called


@mock.patch('rex_backend.repositories.rex_listing_repo.RexListingRepo.load_seller', return_value=fake_rex_seller)
@mock.patch('rex_backend.models.rex_listings.RexListing.query')
@mock.patch('rex_backend.interactors.send_email_with_showing_report.rest_api_service.get', return_value=fake_rex_api_response)
@mock.patch('rex_backend.utils.aws_ses.AWSSES.send_email', return_value=AWSSESRespose({'MessageId': 'ok'}))
def test_interactor_for_open_house(aws_ses_mock, rex_api_mock, rex_listing_mock, seller_mock,  session):
    rex_listing_mock.filter.return_value.first.return_value = fake_rex_listing
    showing_report = ShowingReportFactory.create(
        ready_for_payment=True
    )
    showing_report._rex_showing = RexShowingFactory.make(id=1, open_house_id=1)
    question = ReportQuestionFactory.create(
        rex_question_guid='test1'
    )
    question2 = ReportQuestionFactory.create(
        rex_question_guid='test2',
        data={}
    )
    answer = ReportAnswerFactory.create(
        showing_report=showing_report,
        rex_question_guid='test1'
    )
    answer2 = ReportAnswerFactory.create(
        showing_report=showing_report,
        rex_question_guid='test2',
        data={'values': ['just a text']}
    )
    db.session.commit()

    interactor = SendEmailWithShowingReport()
    interactor.call(showing_report=showing_report)

    assert interactor.context.email_message_for_seller.html
    assert 'Open House' in interactor.context.email_message_for_seller.html

    assert interactor.context.email_message_for_css
    assert 'Open House' in interactor.context.email_message_for_css.html

    assert aws_ses_mock.called



@mock.patch('rex_backend.models.rex_listings.RexListing.query')
@mock.patch('rex_backend.interactors.send_email_with_showing_report.rest_api_service.get', return_value=fake_rex_api_response)
@mock.patch('rex_backend.utils.aws_ses.AWSSES.send_email', return_value=AWSSESRespose({'MessageId': 'ok'}))
def test_interactor_without_questions(aws_ses_mock, rex_api_mock, rex_listing_mock, session):
    showing_report = ShowingReportFactory.create(
        ready_for_payment=True
    )
    showing_report._rex_showing = RexShowingFactory.make(id=1)
    answer = ReportAnswerFactory.create(
        showing_report=showing_report,
        rex_question_guid='test1'
    )
    answer2 = ReportAnswerFactory.create(
        showing_report=showing_report,
        rex_question_guid='test2',
        data={'values': ['just a text']}
    )
    db.session.commit()

    interactor = SendEmailWithShowingReport()
    interactor.call(showing_report=showing_report)

    assert not aws_ses_mock.send_email.called



@mock.patch('rex_backend.repositories.rex_listing_repo.RexListingRepo.load_seller', return_value=fake_rex_seller)
@mock.patch('rex_backend.models.rex_listings.RexListing.query')
@mock.patch('rex_backend.interactors.send_email_with_showing_report.rest_api_service.get', return_value=fake_rex_api_response)
@mock.patch('rex_backend.utils.aws_ses.AWSSES.send_email', return_value=AWSSESRespose({'MessageId': 'ok'}))
def test_interactor_questions_filter_for_seller_email(aws_ses_mock, rex_api_mock, rex_listing_mock, seller_mock,  session):
    rex_listing_mock.filter.return_value.first.return_value = fake_rex_listing
    showing_report = ShowingReportFactory.create(
        ready_for_payment=True
    )
    showing_report._rex_showing = RexShowingFactory.make(id=1, open_house_id=1)
    question = ReportQuestionFactory.create(
        rex_question_guid='test1',
        data={'original_data': {'name': 'ps_aa_time_purchase'}}
    )
    question2 = ReportQuestionFactory.create(
        rex_question_guid='test2',
        data={}
    )
    answer = ReportAnswerFactory.create(
        showing_report=showing_report,
        rex_question_guid='test1'
    )
    answer2 = ReportAnswerFactory.create(
        showing_report=showing_report,
        rex_question_guid='test2',
        data={'values': ['just a text']}
    )
    db.session.commit()

    interactor = SendEmailWithShowingReport()
    interactor.call(showing_report=showing_report)

    assert interactor.context.email_message_for_seller.html
    assert 'test1' not in interactor.context.email_message_for_seller.html

    assert interactor.context.email_message_for_css
    assert 'test1' in interactor.context.email_message_for_css.html
