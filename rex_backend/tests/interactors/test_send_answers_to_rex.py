import datetime

from unittest import mock

from rex_backend.application import db
from rex_backend.interactors.send_answers_to_rex import SendAnswersToRex
from rex_backend.tests.factories.showing_report_factory import ShowingReportFactory
from rex_backend.tests.factories.report_answer_factory import ReportAnswerFactory
from rex_backend.tests.factories.report_question_factory import ReportQuestionFactory
from rex_backend.tests.mocks.apis.rex_questions import add_multiple_answers_response
from rex_backend.tests.helpers import dict2obj


FAKE_REX_LISTING = dict2obj({
    'id': 13805,
    'guid': '711124500d3c447f975490298570600f'
})

FAKE_REX_SHOWING = dict2obj({
    'id': '695',
    'status': 'Completed',
    'data': {"buyer_id": "", "listingId": 13805, "outside_agent_license_number": "", "streetAddress": "15 Santa Isabel", "city": "Rancho Santa Margarita", "state": "CA", "agentsShowingInstructions": "[Lock Box Code] 9774.", "rexUrl": "15-santa-isabel", "listingId": "939", "expiresByInHours": "2", "isArchived": "False", "buyer": "John Doe <+18553424739>", "css": "John YS <jys@rexhomes.com>", "hasBuyerAgent": "False", "createdAt": "04/01/19 10:06 AM", "status": "Pending"},
    'final_meeting_time': datetime.datetime(2019, 4, 1, 12, 0, 0),
    'timezone': 'UTC',
    'created_at': datetime.datetime(2019, 4, 1, 12, 0, 0),
    'claimed_at': datetime.datetime(2019, 4, 1, 12, 0, 0),
})


@mock.patch('rex_backend.wrappers.user.Agent.showing_query')
@mock.patch('rex_backend.models.rex_listings.RexListing.query')
@mock.patch('requests.post', return_value=add_multiple_answers_response())
def test_interactor(mock_api_response, mock_listing, mock_showing, current_user):
    mock_listing.filter.return_value.first.return_value = FAKE_REX_LISTING
    mock_showing.return_value.first.return_value = FAKE_REX_SHOWING

    showing_report = ShowingReportFactory.create(
        ready_for_payment=True,
        user=current_user
    )
    question = ReportQuestionFactory.create(
        rex_question_guid='test1',
        data={'original_data': {'type': 'checkbox'}}
    )
    question2 = ReportQuestionFactory.create(
        rex_question_guid='test2',
        data={'original_data': {'type': 'text'}}
    )
    answer = ReportAnswerFactory.create(
        rex_question_guid='test1',
        showing_report=showing_report,
        data={'values': ['no']}
    )
    answer2 = ReportAnswerFactory.create(
        rex_question_guid='test2',
        showing_report=showing_report,
        data={'values': ['oooooooh']}
    )
    db.session.commit()

    interactor = SendAnswersToRex()
    interactor.call(
        showing_report=showing_report,
        current_user=current_user
    )

    assert mock_listing.filter.return_value.first.called
    assert mock_showing.return_value.first.called
    assert mock_api_response.called
