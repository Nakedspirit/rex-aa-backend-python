from collections import namedtuple

from unittest import mock

from rex_backend.models.base import ReportQuestion
from rex_backend.interactors.save_questions import SaveQuestions
from rex_backend.tests.factories import ReportQuestionFactory
from rex_backend.apis.rex_questions import RexQuestion


test_rex_questions =[
    RexQuestion(None, **{
        'order': 1,
        'name': 'test 1',
        'title': 'TEST 1',
        'valueName': 'test1',
        'is_required': True,
        'type': 'text',
    }),
    RexQuestion(None, **{
        'order': 2,
        'name': 'test 2',
        'title': 'TEST 2',
        'valueName': 'test2',
        'is_required': False,
        'type': 'text',
    }),
]

def test_interactor(session):
    question = ReportQuestionFactory.create(
        rex_question_guid='test1',
        title='test 1'
    )
    session.commit()

    interactor = SaveQuestions()
    interactor.call(
        rex_questions=test_rex_questions
    )

    assert question.title == 'TEST 1'
    assert ReportQuestion.query.filter(ReportQuestion.title == 'TEST 2').first()