import os


import pytest
from sqlalchemy import create_engine


from rex_backend.application import app, rex_showings_db as db
from rex_backend.models.rex_showings import RexShowingsBaseModel as BaseModel
from rex_backend.tests.factories import RexAssociatedAgentFactory, RexUserFactory, RexBlockFactory, RexBlockAttributeFactory, RexBlockUserFactory, RexShowingTimeSlotFactory, RexShowingFactory, RexShowingVoteFactory, RexChannelFactory
from rex_backend.helpers import create_database_if_not_exists


# Initialize rex showings database for tests
test_rex_showings_database_url = os.environ.get('TESTING_REX_SHOWINGS_DATABASE_URL')
assert test_rex_showings_database_url == db.connection_string
assert test_rex_showings_database_url != os.environ.get('REX_SHOWINGS_DATABASE_URL')


# Create db if not exists
create_database_if_not_exists(test_rex_showings_database_url)


# Collect factories to assign sqlalchemy session to them
factories = [ 
    RexAssociatedAgentFactory, 
    RexUserFactory, 
    RexBlockFactory, 
    RexBlockAttributeFactory, 
    RexBlockUserFactory, 
    RexShowingTimeSlotFactory, 
    RexShowingFactory, 
    RexShowingVoteFactory, 
    RexChannelFactory
]


@pytest.fixture(scope='session')
def rex_showings_connection():
    
    BaseModel.metadata.create_all(db.engine)
    connection = db.engine.connect()
    yield connection
    connection.close()
    BaseModel.metadata.drop_all(db.engine)


@pytest.fixture(scope='function')
def rex_showings_session(rex_showings_connection):
    transaction = rex_showings_connection.begin()

    Session = db._session_factory
    session = Session(bind=rex_showings_connection)
    db._session = session

    for factory in factories:
        factory ._meta.sqlalchemy_session = session

    yield session

    transaction.rollback()
    session.close()
