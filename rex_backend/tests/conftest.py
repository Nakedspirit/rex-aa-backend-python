import os
import sys
import random

import pytest
from graphene.test import Client as GraphQLClient

# Importing ENV vars overridden for tests
import env

sys.path.append(os.path.abspath(os.path.curdir))
from rex_backend.application import app
from rex_backend.graphql.schema import rex_schema
from rex_backend.models.rex_showings import RexUser

# Test helpers
from helpers import *

# Fixtures for the application db
from db import connection, session
from db_rex_showings import rex_showings_connection, rex_showings_session


# Move this hack to another place
@pytest.fixture
def mock_rex_users_for_middleware():
    with mock.patch('rex_backend.repositories.rex_user_repo.RexUserRepo.load_rex_users_by_phone_number_or_email') as mock1:
        mock1.return_value = [
            RexUser(id=555, first_name="Demo", last_name="Mode", phone_number="+79631015924"),
            RexUser(id=81058, first_name="Test", email_address="test@testing.test")
        ]
        yield


@pytest.fixture
def client(session, mock_rex_users_for_middleware):
    client = app.test_client()
    yield client


@pytest.fixture
def graphql_client():
    client = GraphQLClient(rex_schema)
    yield client


# General fixures, used by the whole application
from fixtures import *
