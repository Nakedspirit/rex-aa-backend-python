import pytest

from rex_backend.apis import HttpAPI, HttpAPIException


def test_http_api_with_invalid_url():
    with pytest.raises(HttpAPIException, match='Invalid url: "invalid-url-argument"'):
        api = HttpAPI(
            'API for test',
            'invalid-url-argument'
        )


def test_http_api_with_valid_url():
    api = HttpAPI(
        'API for test',
        'http://valid-url'
    )
    assert api