import random
import datetime


import pytest
from unittest import mock


from rex_backend.models.rex_showings import RexShowing, RexShowingTimeSlot
from rex_backend.tests.conftest import dict2obj


@pytest.fixture
def mock_empty_rex_showings():
    with mock.patch('rex_backend.graphql.resolvers.unclaimed_showings.apply_pagination_and_load') as mock1:
        mock1.return_value = []
        yield


@pytest.fixture
def mock_rex_showings():
    with mock.patch('rex_backend.graphql.resolvers.unclaimed_showings.apply_pagination_and_load') as mock1:
        mock1.return_value = fake_rex_showings()
        with mock.patch('rex_backend.graphql.data_loaders.SlotsForShowingLoader.load_time_slots') as mock2:
            mock2.return_value = fake_time_slots()
            with mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_showing_users') as mock4:
                mock4.return_value = fake_buyers_and_sellers()
                yield


def fake_buyers_and_sellers():
    return [
        dict2obj({
            'id': 1,
            'first_name': 'John',
            'last_name': 'Doe',
            'email_address': None,
            'phone_number': '+11855342473',
            'role': 'Seller',
            'rex_showing_id': '694'
        }),
        dict2obj({
            'id': 2,
            'first_name': 'John',
            'last_name': 'Doe',
            'email_address': None,
            'phone_number': '+11855342473',
            'role': 'Buyer',
            'rex_showing_id': '695'
        })
    ]


def fake_rex_showings():
    return [
        dict2obj({
            'id': 695,
            'data': {"buyer_id": "", "outside_agent_license_number": "", "streetAddress": "15 Santa Isabel", "city": "Rancho Santa Margarita", "state": "CA", "agentsShowingInstructions": "[Lock Box Code] 9774.", "rexUrl": "15-santa-isabel", "listingId": "939", "expiresByInHours": "2", "isArchived": "False", "buyer": "John Doe <+18553424739>", "css": "John YS <jys@rexhomes.com>", "hasBuyerAgent": "False", "createdAt": "04/01/19 10:06 AM", "status": "Pending"},
            'created_at': datetime.datetime(2019, 4, 1, 12, 0, 0),
            'status': 'Pending',
            'claimed_at': datetime.datetime(2019, 4, 1, 12, 0, 0),
            'final_meeting_time': None,
            'timezone': 'UTC',
            'claimed_by_id': None
        }),
        dict2obj({
            'id': 694,
            'data': {"buyer_id": "", "outside_agent_license_number": "", "streetAddress": "15 Santa Isabel", "city": "Rancho Santa Margarita", "state": "CA", "agentsShowingInstructions": "[Lock Box Code] 9774.", "rexUrl": "15-santa-isabel", "listingId": "939", "expiresByInHours": "2", "isArchived": "False", "buyer": "John Doe <+18553424739>", "css": "John YS <jys@rexhomes.com>", "hasBuyerAgent": "False", "createdAt": "04/01/19 10:06 AM", "status": "Pending"},
            'created_at': datetime.datetime(2019, 4, 1, 12, 0, 0),
            'status': 'Pending',
            'claimed_at': datetime.datetime(2019, 4, 1, 12, 0, 0),
            'final_meeting_time': None,
            'timezone': 'UTC',
            'claimed_by_id': None
        })
    ]


def fake_time_slots():
    return [
        RexShowingTimeSlot(
            id = 15,
            start = datetime.datetime(2019, 4, 1, 12, 0, 0),
            end = None,
            is_rejected = False,
            conversation_id = 694,
            created_at = datetime.datetime(2019, 4, 1, 12, 0, 0)
        ),
        RexShowingTimeSlot(
            id = 14,
            start = datetime.datetime(2019, 4, 1, 12, 0, 0),
            end = None,
            is_rejected = False,
            conversation_id = 694,
            created_at = datetime.datetime(2019, 4, 1, 12, 0, 0)
        ),
        RexShowingTimeSlot(
            id = 16,
            start = datetime.datetime(2019, 4, 1, 12, 0, 0),
            end = None,
            is_rejected = False,
            conversation_id = 695,
            created_at = datetime.datetime(2019, 4, 1, 12, 0, 0)
        )
    ]
