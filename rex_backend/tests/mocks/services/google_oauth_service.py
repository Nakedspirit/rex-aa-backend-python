import pytest
from unittest import mock


@pytest.fixture
def mock_google_oauth_success_response(oauth_input_params, default_application_data):
    with mock.patch('rex_backend.utils.google_oauth_service.GoogleOAuthService.ask_google') as mock1:
        m = mock.MagicMock()
        m.status_code = 200
        m.json = lambda: success_json_response(
            oauth_input_params.get('oauth_user_id'),
            default_application_data.get('oauth_aud'),
            default_application_data.get('oauth_domain')
        )
        mock1.return_value = m
        yield


@pytest.fixture
def mock_google_oauth_failed_response():
    with mock.patch('rex_backend.utils.google_oauth_service.GoogleOAuthService.ask_google') as mock1:
        m = mock.MagicMock()
        m.status_code = 400
        m.json = (lambda: {
            "error": "invalid_token",
            "error_description": "Unknown token version 0 for token string: token-here"
        })
        mock1.return_value = m
        yield


@pytest.fixture
def mock_google_oauth_success_response_with_another_aud(oauth_input_params, default_application_data):
    with mock.patch('rex_backend.utils.google_oauth_service.GoogleOAuthService.ask_google') as mock1:
        m = mock.MagicMock()
        m.status_code = 200
        m.json = lambda: success_json_response(
            oauth_input_params.get('oauth_user_id'),
            'another-aud',
            default_application_data.get('oauth_domain')
        )
        yield


@pytest.fixture
def mock_google_oauth_success_response_with_another_domain(oauth_input_params, default_application_data):
    with mock.patch('rex_backend.utils.google_oauth_service.GoogleOAuthService.ask_google') as mock1:
        m = mock.MagicMock()
        m.status_code = 200
        m.json = lambda: success_json_response(
            oauth_input_params.get('oauth_user_id'),
            default_application_data.get('oauth_aud'),
            'another-domain.com'
        )
        mock1.return_value = m
        yield


@pytest.fixture
def mock_google_id_token_success_response(oauth_input_params, default_application_data):
    aud = default_application_data.get('oauth_aud')
    domain = default_application_data.get('oauth_domain')
    with mock.patch('rex_backend.utils.google_oauth_service.id_token.verify_oauth2_token') as mock1:
        mock1.return_value = {
            'aud': aud,
            'hd': domain,
            'sub': oauth_input_params.get('oauth_user_id'),
            'email': "test@{}".format(domain)
        }
        yield


def success_json_response(oauth_user_id, aud, domain):
    return {
        "issued_to": aud,
        "audience": aud,
        "user_id": oauth_user_id,
        "scope": "https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/plus.me",
        "expires_in": 2926,
        "email": "test@{}".format(domain),
        "verified_email": True,
        "access_type": "online"
    }
