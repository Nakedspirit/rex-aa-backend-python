import random
import datetime


import pytest
from unittest import mock


from rex_backend.tests.conftest import dict2obj
from rex_backend.models.rex_showings import RexShowing, RexShowingTimeSlot
from rex_backend.tests.mocks.query_unclaimed_showings import fake_buyers_and_sellers


@pytest.fixture
def mock_empty_showings():
    with mock.patch('rex_backend.graphql.resolvers.upcoming_showings.apply_pagination_and_load') as mock1:
        mock1.return_value = []
        yield


@pytest.fixture
def mock_showings():
    with mock.patch('rex_backend.graphql.resolvers.upcoming_showings.apply_pagination_and_load') as mock1:
        mock1.return_value = fake_rex_showings()
        yield


@pytest.fixture
def mock_time_slots():
    with mock.patch('rex_backend.graphql.data_loaders.SlotsForShowingLoader.load_time_slots') as mock1:
        mock1.return_value = fake_time_slots()
        yield


@pytest.fixture
def mock_buyers_and_sellers():
    with mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_showing_users') as mock1:
        mock1.return_value = fake_buyers_and_sellers()
        yield


def fake_rex_showings():
    rex_showings = [
        {
            'id': 695,
            'status': 'Scheduled',
            'data': {"buyer_id": "", "outside_agent_license_number": "", "streetAddress": "15 Santa Isabel", "city": "Rancho Santa Margarita", "state": "CA", "agentsShowingInstructions": "[Lock Box Code] 9774.", "rexUrl": "15-santa-isabel", "listingId": "939", "expiresByInHours": "2", "isArchived": "False", "buyer": "John Doe <+18553424739>", "css": "John YS <jys@rexhomes.com>", "hasBuyerAgent": "False", "createdAt": "04/01/19 10:06 AM", "status": "Pending"},
            'final_meeting_time': datetime.datetime(2019, 4, 1, 12, 0, 0),
            'timezone': 'UTC',
            'created_at': datetime.datetime(2019, 4, 1, 12, 0, 0),
            'claimed_at': datetime.datetime(2019, 4, 1, 12, 0, 0),
        },
        {
            'id': 694,
            'status': 'Pending',
            'data': {"buyer_id": "", "outside_agent_license_number": "", "streetAddress": "15 Santa Isabel", "city": "Rancho Santa Margarita", "state": "CA", "agentsShowingInstructions": "[Lock Box Code] 9774.", "rexUrl": "15-santa-isabel", "listingId": "939", "expiresByInHours": "2", "isArchived": "False", "buyer": "John Doe <+18553424739>", "css": "John YS <jys@rexhomes.com>", "hasBuyerAgent": "False", "createdAt": "03/29/19 12:13 AM", "status": "Pending"},
            'final_meeting_time': datetime.datetime(2019, 4, 1, 12, 0, 0),
            'timezone': 'UTC',
            'created_at': datetime.datetime(2019, 4, 1, 12, 0, 0),
            'claimed_at': datetime.datetime(2019, 4, 1, 12, 0, 0),
        }
    ]
    return [dict2obj(showing_dict) for showing_dict in rex_showings]


def fake_time_slots():
    return [
        RexShowingTimeSlot(
            id = 15,
            start = datetime.datetime(2019, 4, 1, 12, 0, 0),
            end = None,
            is_rejected = False,
            conversation_id = 694,
            created_at = datetime.datetime(2019, 4, 1, 12, 0, 0)
        ),
        RexShowingTimeSlot(
            id = 14,
            start = datetime.datetime(2019, 4, 1, 12, 0, 0),
            end = None,
            is_rejected = False,
            conversation_id = 694,
            created_at = datetime.datetime(2019, 4, 1, 12, 0, 0)
        ),
        RexShowingTimeSlot(
            id = 16,
            start = datetime.datetime(2019, 4, 1, 12, 0, 0),
            end = None,
            is_rejected = False,
            conversation_id = 695,
            created_at = datetime.datetime(2019, 4, 1, 12, 0, 0)
        )
    ]
