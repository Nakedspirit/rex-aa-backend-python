from unittest import mock


def questions_survey():
    m = mock.Mock()
    m.status_code = 200
    m.json.return_value = {'success': True, 'result': {'pages': [{'title': 'Showing Feedback', 'elements': [{'name': 'ps_buyer_email_address', 'title': "Buyer's Email Address", 'valueName': '62939552f2eb448d9c19fe90c38eaef3', 'isRequired': True, 'type': 'text'}, {'name': 'ps_property_price', 'title': 'How is this property priced in comparison to similar properties in the area?', 'valueName': '0893c42e954345bbbafc62bbb4a22763', 'isRequired': True, 'choices': [{'value': 'About Right', 'text': 'About Right'}, {'value': 'Somewhat Overpriced', 'text': 'Somewhat Overpriced'}, {'value': 'Substantially Overpriced', 'text': 'Substantially Overpriced'}], 'type': 'radiogroup'}, {'name': 'ps_curb_appeal', 'title': 'How was the curb appeal of this home?', 'valueName': '190786b3e9824dcfa5367452f1c900b5', 'isRequired': True, 'choices': [{'value': 'Good', 'text': 'Good'}, {'value': 'Fair', 'text': 'Fair'}, {'value': 'Poor', 'text': 'Poor'}], 'type': 'radiogroup'}, {'name': 'ps_interior_appearance', 'title': 'What was the appearance of the interior of the home?', 'valueName': '0d5a3953656a46dcb851eb37f72e73f0', 'isRequired': True, 'choices': [{'value': 'Very Well Kept', 'text': 'Very Well Kept'}, {'value': 'Average', 'text': 'Average'}, {'value': 'Improvement Needed', 'text': 'Improvement Needed'}], 'type': 'radiogroup'}, {'name': 'ps_features_updating', 'title': 'Any features in the home that need updating?', 'valueName': '9c9501e8e7d54241bd8cf1caff9369e0', 'isRequired': True, 'choices': [{'value': 'Flooring', 'text': 'Flooring'}, {'value': 'Appliances', 'text': 'Appliances'}, {'value': 'Kitchen', 'text': 'Kitchen'}, {'value': 'Bathrooms', 'text': 'Bathrooms'}, {'value': 'Landscaping', 'text': 'Landscaping'}, {'value': 'N/A', 'text': 'N/A'}, {'value': 'Other', 'text': 'Other'}], 'type': 'checkbox'}, {'name': 'ps_features_most_liked', 'title': 'What Features / Parts of the home were most liked?', 'valueName': 'd439bb33942340ed8ff15cc71693463a', 'isRequired': True, 'choices': [{'value': 'Location', 'text': 'Location'}, {'value': 'Layout/Flow', 'text': 'Layout/Flow'}, {'value': 'Backyard', 'text': 'Backyard'}, {'value': 'View', 'text': 'View'}, {'value': 'Bathrooms', 'text': 'Bathrooms'}, {'value': 'Kitchen', 'text': 'Kitchen'}, {'value': 'Dining Room', 'text': 'Dining Room'}, {'value': 'Family/Living Area', 'text': 'Family/Living Area'}, {'value': 'N/A', 'text': 'N/A'}, {'value': 'Other', 'text': 'Other'}], 'type': 'checkbox'}, {'name': 'ps_considering_offer', 'title': 'Is the buyer considering making an offer on this property?', 'valueName': '0471df523bf141a9a0e300d5fe3886f8', 'isRequired': True, 'choices': [{'value': 'Yes', 'text': 'Yes'}, {'value': 'Likely but not Positive', 'text': 'Likely but not Positive'}, {'value': 'Not Sure', 'text': 'Not Sure'}, {'value': 'Unlikely', 'text': 'Unlikely'}, {'value': 'Absolutely Not', 'text': 'Absolutely Not'}], 'type': 'dropdown'}, {'name': 'ps_buyer_rent_own', 'title': 'Does the buyer currently rent or own?', 'valueName': '9c4152cad50f45598987d8f213389c01', 'isRequired': True, 'choices': [{'value': 'Renter', 'text': 'Renter'}, {'value': 'Owner', 'text': 'Owner'}, {'value': 'Owner', 'text': 'Owner'}, {'value': 'Needs to Sell Current Home', 'text': 'Needs to Sell Current Home'}], 'type': 'radiogroup'}, {'name': 'ps_buyer_how_long', 'title': 'How long has the buyer been searching for a home?', 'valueName': 'a5c36c008ac8487fb0ca3b5231aac323', 'isRequired': True, 'choices': [{'value': 'Just Getting Started', 'text': 'Just Getting Started'}, {'value': 'More Than Half a Year', 'text': 'More Than Half a Year'}, {'value': 'Over a Year', 'text': 'Over a Year'}], 'type': 'radiogroup'}, {'name': 'ps_buyer_rex_services', 'title': 'Is buyer Interested in REX services?', 'valueName': '27e769f4074845bfa9f92a44e5ed2756', 'isRequired': True, 'choices': [{'value': 'Home Loans', 'text': 'Home Loans'}, {'value': 'Insurance Buyer', 'text': 'Insurance Buyer'}, {'value': 'Rebate', 'text': 'Rebate'}, {'value': 'Home Warranty', 'text': 'Home Warranty'}, {'value': 'Selling wtih REX', 'text': 'Selling wtih REX'}, {'value': 'N/A', 'text': 'N/A'}], 'type': 'checkbox'}, {'name': 'ps_buyer_summary', 'title': 'Can you provide a quick summary of this showing and the buyer?', 'valueName': 'c2802be4d0a24005b6bf2837731ad3de', 'isRequired': True, 'type': 'comment'}, {'name': 'ps_questions', 'title': 'Questions for further follow up', 'valueName': '7d78c030bec74a36878256e92eda6eeb', 'type': 'comment'}]}]}}
    return m


def questions_survey_for_open_house(status=200):
    m = mock.Mock()
    m.status_code = status
    m.json.return_value = {'success': True, 'result': {'pages': [{'title': ' ', 'elements': [{'name': 'oh_number_attendees', 'title': 'Number of Attendees', 'valueName': '2364d5c293bc4eb5b69c0ca23947ec0c', 'type': 'text', 'validators': [{'type': 'numeric'}]}, {'name': 'oh_buyer_likes', 'title': 'Main buyer likes (Select all that apply):', 'valueName': '4a33cc23a6c640e28113c27145cd74a6', 'choices': [{'value': 'Location (school district/neighborhood/nearby amenities)', 'text': 'Location (school district/neighborhood/nearby amenities)'}, {'value': ' Special Features (pool/garage/community amenities/ backyard)', 'text': ' Special Features (pool/garage/community amenities/ backyard)'}, {'value': 'Design/Layout', 'text': 'Design/Layout'}, {'value': 'Yard space', 'text': 'Yard space'}, {'value': 'Master suite', 'text': 'Master suite'}, {'value': 'Size of rooms', 'text': 'Size of rooms'}, {'value': 'Kitchen', 'text': 'Kitchen'}, {'value': 'Upgrades', 'text': 'Upgrades'}, {'value': 'View', 'text': 'View'}, {'value': 'Bathrooms', 'text': 'Bathrooms'}, {'value': 'Dining Room', 'text': 'Dining Room'}, {'value': 'Family/Living Area', 'text': 'Family/Living Area'}], 'type': 'checkbox'}, {'name': 'oh_price_feedback', 'title': 'What are the main thoughts on price?', 'valueName': 'd4f909c3142046de902e029354f123ac', 'choices': [{'value': 'Underpriced', 'text': 'Underpriced'}, {'value': 'About Right', 'text': 'About Right'}, {'value': 'Somewhat Overpriced', 'text': 'Somewhat Overpriced'}, {'value': 'Substantially Overpriced', 'text': 'Substantially Overpriced'}], 'type': 'radiogroup'}, {'name': 'oh_buyer_dislikes', 'title': 'Main buyer dislikes (Select all that apply):', 'valueName': '2c92455a508041d7b98537d95ec686c9', 'choices': [{'value': 'Lack of amenities', 'text': 'Lack of amenities'}, {'value': ' Lack of upgrades', 'text': ' Lack of upgrades'}, {'value': 'Age of home', 'text': 'Age of home'}, {'value': 'Neighborhood', 'text': 'Neighborhood'}, {'value': 'Design / Layout', 'text': 'Design / Layout'}, {'value': 'Cleanliness', 'text': 'Cleanliness'}, {'value': 'Size of home', 'text': 'Size of home'}, {'value': 'Size of rooms', 'text': 'Size of rooms'}, {'value': 'Kitchen size', 'text': 'Kitchen size'}, {'value': 'Exterior', 'text': 'Exterior'}], 'type': 'checkbox'}, {'name': 'oh_curb_appeal', 'title': 'How was the curb appeal of this home?', 'valueName': 'c08519d1d59f4e0d9d29594ca4f3358d', 'choices': [{'value': 'Good', 'text': 'Good'}, {'value': 'Fair', 'text': 'Fair'}, {'value': 'Poor', 'text': 'Poor'}], 'type': 'radiogroup'}, {'name': 'oh_needs_updates', 'title': 'What features in the home need updating? (Select all that apply):', 'valueName': 'b76d827788374cae9a82ec898523a9f4', 'choices': [{'value': 'Flooring', 'text': 'Flooring'}, {'value': 'Appliances', 'text': 'Appliances'}, {'value': 'Kitchen', 'text': 'Kitchen'}, {'value': 'Bathrooms', 'text': 'Bathrooms'}, {'value': 'Landscaping', 'text': 'Landscaping'}, {'value': 'N/A', 'text': 'N/A'}], 'type': 'checkbox'}, {'name': 'oh_additional_comments', 'title': 'Additional Comments', 'valueName': 'ed32e2583dcb4a0ca6dd1446ed60109e', 'type': 'comment'}]}]}}
    return m


def empty_questions_survey():
    m = mock.Mock()
    m.status_code = 200
    m.json.return_value = {"success": True, "result": {}}
    return m


def add_multiple_answers_response():
    m = mock.Mock()
    m.status_code = 200
    m.json.return_value = {"success": True, "result": True}
    return m


QUESTIONS_SURVEY_MOCK = questions_survey()
OPEN_HOUSE_QUESTIONS_MOCK = questions_survey_for_open_house()


visitor_questions_mock = mock.Mock()
visitor_questions_mock.status_code = 200
visitor_questions_mock.json.return_value = {
  "success": True,
  "result": [
    {
      "question_guid": "42761b79b5364185a9b9efe0a4ce6026",
      "name": "ohl_guest_name",
      "question": "Guest Name",
      "stage_needed_for": "SHOWINGS",
      "type": "text",
      "enum_csv": "",
      "is_multi": 0,
      "question_source": "aa",
      "validator_regex": None,
      "validator_error_message": None,
      "current": 1,
      "required": 1,
      "question_id": 240,
      "tool_name": "openhouseleads",
      "set_id": None,
      "answer": [
        {
          "answer_text": "test@test.test",
          "answer_int": None,
          "answer_date": None,
          "answer_bool": None,
          "source": "showing_id:1;visitor_id:1",
          "recorded_by": "kzvonov@rexhomes.com",
          "guid": "1012943688ff41f89c881e286bc9df9b"
        }
      ]
    },
    {
      "question_guid": "88dad6a885e2434eb8e0b0a0011fd8d2",
      "name": "ohl_guest_email",
      "question": "Guest Email",
      "stage_needed_for": "SHOWINGS",
      "type": "text",
      "enum_csv": "",
      "is_multi": 0,
      "question_source": "aa",
      "validator_regex": None,
      "validator_error_message": None,
      "current": 1,
      "required": 0,
      "question_id": 241,
      "tool_name": "openhouseleads",
      "set_id": None
    },
    {
      "question_guid": "07c2ea5ec38a42a383f8858f9f39dc98",
      "name": "ohl_guest_phone",
      "question": "Guest Phone Number",
      "stage_needed_for": "SHOWINGS",
      "type": "text",
      "enum_csv": "",
      "is_multi": 0,
      "question_source": "aa",
      "validator_regex": None,
      "validator_error_message": None,
      "current": 1,
      "required": 0,
      "question_id": 242,
      "tool_name": "openhouseleads",
      "set_id": None
    },
    {
      "question_guid": "ba9cf96fef8e480da33f8a2f105cfa56",
      "name": "ohl_guest_interested",
      "question": "Is the guest interested in this house?",
      "stage_needed_for": "SHOWINGS",
      "type": "text",
      "enum_csv": "Yes,No",
      "is_multi": 0,
      "question_source": "aa",
      "validator_regex": None,
      "validator_error_message": None,
      "current": 1,
      "required": 0,
      "question_id": 243,
      "tool_name": "openhouseleads",
      "set_id": None
    },
    {
      "question_guid": "bd1280d9f0fb45648efea6af557e3065",
      "name": "ohl_pre_qualified",
      "question": "Has the guest pre-qualified for financing?",
      "stage_needed_for": "SHOWINGS",
      "type": "text",
      "enum_csv": "Yes,No",
      "is_multi": 0,
      "question_source": "aa",
      "validator_regex": None,
      "validator_error_message": None,
      "current": 1,
      "required": 0,
      "question_id": 244,
      "tool_name": "openhouseleads",
      "set_id": None
    },
    {
      "question_guid": "b180f30606f643568818d983c742c475",
      "name": "ohl_selling",
      "question": "Might the guest consider selling with REX in the future?",
      "stage_needed_for": "SHOWINGS",
      "type": "text",
      "enum_csv": "Yes,No",
      "is_multi": 0,
      "question_source": "aa",
      "validator_regex": None,
      "validator_error_message": None,
      "current": 1,
      "required": 0,
      "question_id": 245,
      "tool_name": "openhouseleads",
      "set_id": None
    },
    {
      "question_guid": "9716120346b24182ac1bfa2c6d0becdd",
      "name": "ohl_make_offer",
      "question": "Is the guest interested in making an offer?",
      "stage_needed_for": "SHOWINGS",
      "type": "text",
      "enum_csv": "Yes,No",
      "is_multi": 0,
      "question_source": "aa",
      "validator_regex": None,
      "validator_error_message": None,
      "current": 1,
      "required": 0,
      "question_id": 246,
      "tool_name": "openhouseleads",
      "set_id": None
    },
    {
      "question_guid": "ada59bca120a45f98cd646ccda40cb41",
      "name": "ohl_buyer_rebate",
      "question": "Is the guest interested in the Buyer Rebate Program?",
      "stage_needed_for": "SHOWINGS",
      "type": "text",
      "enum_csv": "Yes,No",
      "is_multi": 0,
      "question_source": "aa",
      "validator_regex": None,
      "validator_error_message": None,
      "current": 1,
      "required": 0,
      "question_id": 248,
      "tool_name": "openhouseleads",
      "set_id": None
    },
    {
      "question_guid": "54cf47025e3f40de8398aa34535e272d",
      "name": "ohl_visted_homes",
      "question": "Has the guest visited other homes?",
      "stage_needed_for": "SHOWINGS",
      "type": "text",
      "enum_csv": "Yes,No",
      "is_multi": 0,
      "question_source": "aa",
      "validator_regex": None,
      "validator_error_message": None,
      "current": 1,
      "required": 0,
      "question_id": 249,
      "tool_name": "openhouseleads",
      "set_id": None
    },
    {
      "question_guid": "d767a806450248149357adaf716cd13c",
      "name": "ohl_time_shopping",
      "question": "How long has the guest been shopping for a home?",
      "stage_needed_for": "SHOWINGS",
      "type": "text",
      "enum_csv": "Just Started,0-3 months,3+ months",
      "is_multi": 0,
      "question_source": "aa",
      "validator_regex": None,
      "validator_error_message": None,
      "current": 1,
      "required": 0,
      "question_id": 250,
      "tool_name": "openhouseleads",
      "set_id": None,
      "answer": [
        {
          "answer_text": "Just Started",
          "answer_int": None,
          "answer_date": None,
          "answer_bool": None,
          "source": "showing_id:1;visitor_id:1",
          "recorded_by": "kzvonov@rexhomes.com",
          "guid": "a44be1fc169d496b97539311b7d5d8f0"
        }
      ]
    },
    {
      "question_guid": "c0b99df30f524b348656e72aa0c4b0b2",
      "name": "ohl_time_purchase",
      "question": "When is the guest looking to purchase their home?",
      "stage_needed_for": "SHOWINGS",
      "type": "text",
      "enum_csv": "Now,within 3 months,3+ months",
      "is_multi": 0,
      "question_source": "aa",
      "validator_regex": None,
      "validator_error_message": None,
      "current": 1,
      "required": 0,
      "question_id": 251,
      "tool_name": "openhouseleads",
      "set_id": None
    },
    {
      "question_guid": "cc93961f74804bffb0fa4ac6413559f2",
      "name": "ohl_interest_level",
      "question": "Interest Level (select all that apply)",
      "stage_needed_for": "SHOWINGS",
      "type": "text",
      "enum_csv": "None given,Not interested at all,Interested in this house,Very interested in this house, Interested in REX services (loans/insruance/etc)",
      "is_multi": 1,
      "question_source": "aa",
      "validator_regex": None,
      "validator_error_message": None,
      "current": 1,
      "required": 0,
      "question_id": 252,
      "tool_name": "openhouseleads",
      "set_id": None
    },
    {
      "question_guid": "d4f777bcd4754d78b34ccd5ac00b73de",
      "name": "ohl_notes",
      "question": "Notes",
      "stage_needed_for": "SHOWINGS",
      "type": "comment",
      "enum_csv": "",
      "is_multi": 0,
      "question_source": "aa",
      "validator_regex": None,
      "validator_error_message": None,
      "current": 1,
      "required": 0,
      "question_id": 253,
      "tool_name": "openhouseleads",
      "set_id": None
    }
  ]
}

VISITOR_QUESTIONS_MOCK = visitor_questions_mock


visitor_questions_bad_response_mock = mock.Mock()
visitor_questions_bad_response_mock.status_code = 200
visitor_questions_bad_response_mock.json.return_value = {"success": False, "result": []}
VISITOR_QUESTIONS_BAD_RESPONSE_MOCK = visitor_questions_bad_response_mock


OPENHOUSELEADS_QUESTIONS_MOCK = mock.Mock()
OPENHOUSELEADS_QUESTIONS_MOCK.status_code = 200
OPENHOUSELEADS_QUESTIONS_MOCK.json.return_value = {
  "success": True,
  "result": {
    "pages": [
      {
        "title": " ",
        "elements": [
          {
            "name": "ohl_guest_interested",
            "title": "Is the guest interested in this house?",
            "valueName": "ba9cf96fef8e480da33f8a2f105cfa56",
            "choices": [
              {
                "value": "Yes",
                "text": "Yes"
              },
              {
                "value": "No",
                "text": "No"
              }
            ],
            "type": "radiogroup"
          },
          {
            "name": "ohl_pre_qualified",
            "title": "Has the guest pre-qualified for financing?",
            "valueName": "bd1280d9f0fb45648efea6af557e3065",
            "choices": [
              {
                "value": "Yes",
                "text": "Yes"
              },
              {
                "value": "No",
                "text": "No"
              }
            ],
            "type": "radiogroup"
          },
          {
            "name": "ohl_selling",
            "title": "Might the guest consider selling with REX in the future?",
            "valueName": "b180f30606f643568818d983c742c475",
            "choices": [
              {
                "value": "Yes",
                "text": "Yes"
              },
              {
                "value": "No",
                "text": "No"
              }
            ],
            "type": "radiogroup"
          },
          {
            "name": "ohl_make_offer",
            "title": "Is the guest interested in making an offer?",
            "valueName": "9716120346b24182ac1bfa2c6d0becdd",
            "choices": [
              {
                "value": "Yes",
                "text": "Yes"
              },
              {
                "value": "No",
                "text": "No"
              }
            ],
            "type": "radiogroup"
          },
          {
            "name": "ohl_buyer_rebate",
            "title": "Is the guest interested in the Buyer Rebate Program?",
            "valueName": "ada59bca120a45f98cd646ccda40cb41",
            "choices": [
              {
                "value": "Yes",
                "text": "Yes"
              },
              {
                "value": "No",
                "text": "No"
              }
            ],
            "type": "radiogroup"
          },
          {
            "name": "ohl_visted_homes",
            "title": "Has the guest visited other homes?",
            "valueName": "54cf47025e3f40de8398aa34535e272d",
            "choices": [
              {
                "value": "Yes",
                "text": "Yes"
              },
              {
                "value": "No",
                "text": "No"
              }
            ],
            "type": "radiogroup"
          },
          {
            "name": "ohl_time_shopping",
            "title": "How long has the guest been shopping for a home?",
            "valueName": "d767a806450248149357adaf716cd13c",
            "choices": [
              {
                "value": "Just Started",
                "text": "Just Started"
              },
              {
                "value": "0-3 months",
                "text": "0-3 months"
              },
              {
                "value": "3+ months",
                "text": "3+ months"
              }
            ],
            "type": "radiogroup"
          },
          {
            "name": "ohl_time_purchase",
            "title": "When is the guest looking to purchase their home?",
            "valueName": "c0b99df30f524b348656e72aa0c4b0b2",
            "choices": [
              {
                "value": "Now",
                "text": "Now"
              },
              {
                "value": "within 3 months",
                "text": "within 3 months"
              },
              {
                "value": "3+ months",
                "text": "3+ months"
              }
            ],
            "type": "radiogroup"
          },
          {
            "name": "ohl_interest_level",
            "title": "Interest Level (select all that apply)",
            "valueName": "cc93961f74804bffb0fa4ac6413559f2",
            "choices": [
              {
                "value": "None given",
                "text": "None given"
              },
              {
                "value": "Not interested at all",
                "text": "Not interested at all"
              },
              {
                "value": "Interested in this house",
                "text": "Interested in this house"
              },
              {
                "value": "Very interested in this house",
                "text": "Very interested in this house"
              },
              {
                "value": " Interested in REX services (loans/insruance/etc)",
                "text": " Interested in REX services (loans/insruance/etc)"
              }
            ],
            "type": "checkbox"
          },
          {
            "name": "ohl_notes",
            "title": "Notes",
            "valueName": "d4f777bcd4754d78b34ccd5ac00b73de",
            "type": "comment"
          },
          {
            "name": "ohl_interested_insurance",
            "title": "Is the guest interested in insurance?\t",
            "valueName": "e614a1b7a2484fcaa18db995cde18be2",
            "choices": [
              {
                "value": "Yes",
                "text": "Yes"
              },
              {
                "value": "No",
                "text": "No"
              }
            ],
            "type": "radiogroup"
          },
          {
            "name": "ohl_interested_home_loans",
            "title": "Is the guest interested in home loans?",
            "valueName": "6f06d2eb0277494bba52f4e3f2a10392",
            "choices": [
              {
                "value": "Yes",
                "text": "Yes"
              },
              {
                "value": "No",
                "text": "No"
              }
            ],
            "type": "radiogroup"
          },
          {
            "name": "ohl_interested_consultation",
            "title": "Is the guest interested in a Buyer’s Consultation?",
            "valueName": "6f1a37f420e14917ae8dd4d7440eab24",
            "choices": [
              {
                "value": "Yes",
                "text": "Yes"
              },
              {
                "value": "No",
                "text": "No"
              }
            ],
            "type": "radiogroup"
          },
          {
            "name": "ohl_interested_home_services",
            "title": "Is the guest interested in home services?",
            "valueName": "f29a9eacba2043128cdd9e2e8d657f33",
            "choices": [
              {
                "value": "Yes",
                "text": "Yes"
              },
              {
                "value": "No",
                "text": "No"
              }
            ],
            "type": "radiogroup"
          }
        ]
      }
    ]
  }
}
