from unittest import mock


def leaderboard_api_response():
    m = mock.Mock()
    m.status_code = 200
    m.json.return_value = {'agents': [{'id': 81058, 'name': 'Makar Stetsenko', 'numShowingsEscrow': 0, 'numShowingsTotal': 13}, {'id': 81046, 'name': 'Peter Tcvetkov', 'numShowingsEscrow': 0, 'numShowingsTotal': 6}], 'dateEnd': 'Sun, 18 Aug 2019 00:00:00 GMT', 'dateStart': 'Thu, 01 Aug 2019 00:00:00 GMT', 'sortBy': 'numShowingsTotal', 'summary': {'numAgentsTotal': 2, 'numShowingsEscrow': 0, 'numShowingsTotal': 19}}
    return m


def empty_leaderboard_api_response():
    m = mock.Mock()
    m.status_code = 200
    m.json.return_value = {}
    return m