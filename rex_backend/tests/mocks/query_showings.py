import random
import datetime


import pytest
from unittest import mock


from rex_backend.tests.conftest import dict2obj
from rex_backend.models.rex_showings import RexShowing, RexShowingTimeSlot


@pytest.fixture
def mock_without_listing():
    with mock.patch('rex_backend.graphql.resolvers.showing.load_showing') as mock1:
        mock1.return_value = None
        yield

@pytest.fixture
def mock_showings():
    with mock.patch('rex_backend.graphql.resolvers.showing.load_showing') as mock1:
        mock1.return_value = fake_showing_data()
        yield


@pytest.fixture
def mock_time_slots():
    with mock.patch('rex_backend.graphql.data_loaders.SlotsForShowingLoader.load_time_slots') as mock1:
        mock1.return_value = fake_time_slots()
        yield


def fake_showing_data():
    return dict2obj({
        'id': 695,
        'status': 'Completed',
        'data': {"buyer_id": "", "outside_agent_license_number": "", "streetAddress": "15 Santa Isabel", "city": "Rancho Santa Margarita", "state": "CA", "agentsShowingInstructions": "[Lock Box Code] 9774.", "rexUrl": "15-santa-isabel", "listingId": 939, "expiresByInHours": "2", "isArchived": "False", "buyer": "John Doe <+18553424739>", "css": "John YS <jys@rexhomes.com>", "hasBuyerAgent": "False", "createdAt": "04/01/19 10:06 AM", "status": "Pending"},
        'final_meeting_time': datetime.datetime(2019, 4, 1, 12, 0, 0),
        'timezone': 'UTC',
        'created_at': datetime.datetime(2019, 4, 1, 12, 0, 0),
        'claimed_at': datetime.datetime(2019, 4, 1, 12, 0, 0),
    })


def fake_time_slots():
    return [
        RexShowingTimeSlot(
            id = 15,
            start = datetime.datetime(2019, 4, 1, 12, 0, 0),
            end = None,
            is_rejected = False,
            conversation_id = 694,
            created_at = datetime.datetime(2019, 4, 1, 12, 0, 0)
        ),
        RexShowingTimeSlot(
            id = 14,
            start = datetime.datetime(2019, 4, 1, 12, 0, 0),
            end = None,
            is_rejected = False,
            conversation_id = 694,
            created_at = datetime.datetime(2019, 4, 1, 12, 0, 0)
        ),
        RexShowingTimeSlot(
            id = 16,
            start = datetime.datetime(2019, 4, 1, 12, 0, 0),
            end = None,
            is_rejected = False,
            conversation_id = 695,
            created_at = datetime.datetime(2019, 4, 1, 12, 0, 0)
        )
    ]


def fake_buyers_and_sellers():
    return [
        dict2obj({
            'id': 1,
            'first_name': 'John',
            'last_name': 'Doe',
            'email_address': None,
            'phone_number': '+11855342473',
            'role': 'Seller',
            'rex_showing_id': '694'
        }),
        dict2obj({
            'id': 2,
            'first_name': 'John',
            'last_name': 'Doe',
            'email_address': None,
            'phone_number': '+11855342473',
            'role': 'Buyer',
            'rex_showing_id': '695'
        })
    ]
