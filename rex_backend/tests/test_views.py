from unittest import mock
from rex_backend.application import app


def test_playground_200_with_debug(client):
    with mock.patch.dict(app.config, {'ENABLE_PLAYGROUND': True}):
        assert app.config['ENABLE_PLAYGROUND']

        response = client.get("/playground")
        assert response.status_code == 200
        assert b"/graphql" in response.data


def test_playground_404_without_debug(client):
    with mock.patch.dict(app.config, {'ENABLE_PLAYGROUND': False}):
        assert app.config['ENABLE_PLAYGROUND'] is False

        response = client.get("/playground")
        assert response.status_code == 404
