import os
from datetime import datetime, timedelta


import pytest
from faker import Faker
from unittest import mock
from sqlalchemy import delete


from rex_backend.models.base import GoogleIdentity, User, Device, UserStat
from rex_backend.wrappers.user import DemoUser, Agent
from rex_backend.application import jwt_service
from rex_backend.tests.factories import UserFactory, GoogleIdentityFactory, DeviceFactory, UserStatFactory, LinkToRexUserFactory


fake = Faker()


@pytest.fixture
def default_application_data():
    return {
        'rex_user_id': 555,
        'rex_agent_id': 5555,
        'device_id': 'mobile-device-id',
        'email': 'test@test.test',
        'register_event_secret_key': os.environ.get('REGISTER_EVENT_SECRET_KEY'),
        'oauth_aud': os.environ.get('GOOGLE_OAUTH_WEB_CLIENT_IDS'),
        'oauth_domain': os.environ.get('GOOGLE_OAUTH_DOMAIN_ALLOWED_TO_SIGNIN')
    }


@pytest.fixture
def oauth_input_params():
    return {
        'oauth_user_id': fake.pystr(max_chars=20),
        'oauth_access_token': fake.pystr(max_chars=40),
        'expiration_date': str(fake.date_between(start_date="+1d", end_date="+30d"))
    }


@pytest.fixture
def rex_user_id(default_application_data):
    return default_application_data.get('rex_user_id')


@pytest.fixture
def default_rex_agent_id(default_application_data):
    return default_application_data.get('rex_agent_id')


@pytest.fixture
def mobile_device_id(default_application_data):
    return default_application_data.get('device_id')


@pytest.fixture
def default_email(default_application_data):
    return default_application_data.get('email')


@pytest.fixture
def valid_register_event_secret(default_application_data):
    return default_application_data.get('register_event_secret_key')


@pytest.fixture
def user_and_identity_orm_obj(session, default_application_data):
    user = UserFactory.create(
        contact_email=default_application_data.get('email'),
        rex_agent_id=default_application_data.get('rex_agent_id'),
        rex_user_id=default_application_data.get('rex_user_id')
    )
    LinkToRexUserFactory.create_batch(3, user=user)
    device = DeviceFactory.create(user=user, user_id=user.id)
    identity = GoogleIdentityFactory.create(user=user, user_id=user.id)
    session.commit()
    yield {'user': user, 'identity': identity, 'device': device}


@pytest.fixture
def current_user(user_and_identity_orm_obj):
    user = Agent(user_and_identity_orm_obj['user'])
    user.current_identity = user_and_identity_orm_obj['identity']
    user.current_device = user_and_identity_orm_obj['device']
    return user


@pytest.fixture
def current_demo_user(user_and_identity_orm_obj):
    user = DemoUser(user_and_identity_orm_obj['user'])
    user.current_identity = user_and_identity_orm_obj['identity']
    user.current_device = user_and_identity_orm_obj['device']
    return user

@pytest.fixture
def valid_jwt(user_and_identity_orm_obj):
    user = user_and_identity_orm_obj['user']
    identity = user_and_identity_orm_obj['identity']
    device = user_and_identity_orm_obj['device']
    other_attributes = {'device_id': device.id}
    return jwt_service.jwt_for_user(user, identity, other_attributes)


@pytest.fixture
def valid_jwt_for_demo_user(user_and_identity_orm_obj):
    user = user_and_identity_orm_obj['user']
    identity = user_and_identity_orm_obj['identity']
    device = user_and_identity_orm_obj['device']
    other_attributes = {'device_id': device.id, 'demo_mode': True}
    return jwt_service.jwt_for_user(user, identity, other_attributes)


# TODO: move mocks into mocks directory
@pytest.fixture
def mock_get_whitelisted_emails():
    with mock.patch('rex_backend.utils.google_table_service.GoogleTableService.get_whitelisted_emails') as mock1:
        mock1.return_value = ['test@gmail.com', 'test@test.com', 'test@test.test']
        yield


@pytest.fixture
def mock_get_whitelisted_emails_where_no_emails():
    with mock.patch('rex_backend.utils.google_table_service.GoogleTableService.get_whitelisted_emails') as mock1:
        mock1.return_value = []
        yield


@pytest.fixture
def mock_firebase_send_to_many_call():
    with mock.patch('rex_backend.utils.firebase_cloud_messaging.FirebaseCloudMessaging.send_message_to_many') as mock1:
        mock1.return_value = 'something'
        yield


@pytest.fixture
def mock_firebase_send_messages_call():
    with mock.patch('rex_backend.utils.firebase_cloud_messaging.FirebaseCloudMessaging.send_messages') as mock1:
        mock1.return_value = 'something'
        yield


@pytest.fixture
def mock_firebase_send():
    with mock.patch('rex_backend.utils.firebase_cloud_messaging.FirebaseCloudMessaging.send_message') as mock1:
        mock1.return_value = 'something'
        yield
