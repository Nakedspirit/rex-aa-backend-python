import factory


from rex_backend.models.rex_showings import RexAssociatedAgent


class RexAssociatedAgentFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = RexAssociatedAgent

    id = factory.Sequence(lambda n: '%s' % n)
    email = factory.Sequence(lambda n: 'test%s@test.test' % n)
    phone_number = factory.Faker('phone_number')
    name = factory.Faker('name')
