import factory


from rex_backend.models.rex_showings import RexShowingVote
# from rex_backend.tests.factories import RexBlockUserFactory, RexShowingTimeSlotFactory


class RexShowingVoteFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = RexShowingVote

    # id = factory.Sequence(lambda n: '%s' % n)
    # vote = factory.Sequence(lambda n: '%s' % n)
    # created_at = factory.Sequence(lambda n: '%s' % n)

    # member_id = factory.SubFactory(RexBlockUserFactory)
    # option_id = factory.SubFactory(RexShowingTimeSlotFactory)
