import factory
import factory.fuzzy


from rex_backend.models.base import UserAction, UserActionName
from .user_factory import UserFactory


class UserActionFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = UserAction

    class Params:
        hide_showing = factory.Trait(
            name=UserActionName.hide_showing.value,
        )

    name = factory.fuzzy.FuzzyChoice([
        UserActionName.hide_showing.value
    ])

    user = factory.SubFactory(UserFactory)
    object_id = factory.Sequence(lambda n: n)