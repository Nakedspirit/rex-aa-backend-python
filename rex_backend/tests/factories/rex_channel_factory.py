import factory


from rex_backend.models.rex_showings import RexChannel
# from rex_backend.tests.factories import RexUserFactory


class RexChannelFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = RexChannel

    # id = factory.Sequence(lambda n: '%s' % n)
    # bot_address = factory.Sequence(lambda n: '%s' % n)
    # user_address = factory.Sequence(lambda n: '%s' % n)
    # medium = factory.Sequence(lambda n: '%s' % n)
    # data = factory.Sequence(lambda n: '%s' % n)
    # hook_receive = factory.Sequence(lambda n: '%s' % n)
    # created_at = factory.Sequence(lambda n: '%s' % n)

    # user_id = factory.SubFactory(RexUserFactory)
