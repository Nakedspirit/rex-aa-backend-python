import factory


from rex_backend.models.base import ReportAnswer
from .showing_report_factory import ShowingReportFactory


class ReportAnswerFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = ReportAnswer
    
    rex_question_guid = factory.Sequence(lambda n: 'rex-guid-%i' % n)
    data = {'values': ['test1', 'test2']}

    showing_report = factory.SubFactory(ShowingReportFactory)
