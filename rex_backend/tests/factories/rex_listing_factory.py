import datetime

import factory

from rex_backend.tests.helpers import dict2obj


class RexListingFactory(factory.alchemy.SQLAlchemyModelFactory):
    # TODO: make it work
    def make(**kwargs):
        data = {
            'id': 1,
            'guid': 'ffff7f2f2cb446398b42405c1c2b04a4',
        }
        data = {**data, **kwargs}

        return dict2obj(data)