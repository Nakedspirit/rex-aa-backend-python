import factory

from .user_factory import UserFactory
from rex_backend.models.base import Device


class DeviceFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Device

    id = factory.Sequence(lambda n: 'device-id-%s' % n)
    push_notification_token =  factory.Faker('isbn13', separator="-")
    badge_count = 0
    closed_at = factory.Faker('date_time_this_year', before_now=True, after_now=False, tzinfo=None)

    user = factory.SubFactory(UserFactory)