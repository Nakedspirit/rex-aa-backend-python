import factory


from rex_backend.models.rex_showings import RexBlockAttribute
from .rex_block_factory import  RexBlockFactory


class RexBlockAttributeFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = RexBlockAttribute

    id = factory.Sequence(lambda n: '%s' % n)
    key = factory.Sequence(lambda n: '%s' % n)
    value = factory.Sequence(lambda n: '%s' % n)
    created_at = factory.Faker('date_time_this_year', before_now=True, after_now=False, tzinfo=None)

    block_id = factory.SubFactory(RexBlockFactory)
