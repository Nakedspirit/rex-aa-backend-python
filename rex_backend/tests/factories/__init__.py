from rex_backend.utils.auto_module_loader import AutoModuleLoader, ClassConvention

loader = AutoModuleLoader(__name__, convention=ClassConvention())
loader.load_objects_into_current_package()
loader.write_objects_into_all_variable()
