import factory


from rex_backend.models.base import User


class UserFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = User

    rex_user_id = factory.Sequence(lambda n: '%s' % n)
    rex_agent_id = factory.Sequence(lambda n: '%s' % n)
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    contact_phone_number = factory.Sequence(lambda n: '+15554440000%s' % n)
    contact_email = factory.Sequence(lambda n: 'test%s@test.test' % n)
    avatar_url = factory.Faker('file_path', extension='png')
    created_at = factory.Faker('date_time_this_year', before_now=True, after_now=False, tzinfo=None)
    updated_at = factory.Faker('date_time_this_year', before_now=True, after_now=False, tzinfo=None)