import factory
import factory.fuzzy


from rex_backend.utils.tz import tznow
from rex_backend.models.base import ShowingReport, ShowingReportStatus
from .user_factory import UserFactory


class ShowingReportFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = ShowingReport

    class Params:
        ready_for_payment = factory.Trait(
            status=ShowingReportStatus.ok,
            is_questions_completed=True,
            is_call_buyer_completed=True
        )

    status = factory.fuzzy.FuzzyChoice([
        ShowingReportStatus.buyer_not_appeared,
        ShowingReportStatus.agent_could_not_make_it,
        ShowingReportStatus.canceled,
        ShowingReportStatus.ok
    ])
    status_comment = 'just a status comment'
    status_added_at = factory.LazyFunction(tznow)
    is_questions_completed = False
    is_call_buyer_completed = False

    user = factory.SubFactory(UserFactory)
    rex_showing_id = factory.Sequence(lambda n: '%i' % n)

