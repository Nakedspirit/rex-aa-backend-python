import factory


from rex_backend.models.base import UserStat
from .user_factory import UserFactory


class UserStatFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = UserStat

    name = factory.Sequence(lambda n: '%s' % n)
    value = factory.Sequence(lambda n: '%s' % n)

    date = factory.Sequence(lambda n: '%s' % n)
    user_id = factory.Sequence(lambda n: '%s' % n)
