import factory

from .user_factory import UserFactory
from rex_backend.models.base import LinkToRexUser

class LinkToRexUserFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = LinkToRexUser

    rex_user_id = factory.Sequence(lambda n: n + 1)

    user = factory.SubFactory(UserFactory)

    @factory.lazy_attribute
    def email(self):
        return self.user.contact_email

    @factory.lazy_attribute
    def phone_number(self):
        return self.user.contact_phone_number
