import factory

from rex_backend.utils.tz import tznow
from rex_backend.models.rex_showings import RexOpenHouse


class RexOpenHouseFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = RexOpenHouse

    id = factory.Sequence(lambda n: n + 1)
    guid = factory.Sequence(lambda n: '%i' % n)
    created_at = factory.LazyFunction(tznow)

    css_user_id = factory.Sequence(lambda n: n + 1)
    listing_id = factory.Sequence(lambda n: n + 1)
    showing_id = factory.Sequence(lambda n: n + 1)