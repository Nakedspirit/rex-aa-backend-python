import factory


from rex_backend.models.base import ReportQuestion
from .showing_report_factory import ShowingReportFactory


class ReportQuestionFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = ReportQuestion
    
    rex_question_guid = factory.Sequence(lambda n: f'rex-guid-{n}')
    title = factory.Sequence(lambda n: f'Title {n}')
    data = {"options": [{"text": "Test 3", "value": "test3"}, {"text": "Test 2", "value": "test2"}, {"text": "Test 1", "value": "test1"}], "is_required": True}
