import datetime

import factory

from rex_backend.models.rex_showings import RexShowingTimeSlot


class RexShowingTimeSlotFactory(factory.alchemy.SQLAlchemyModelFactory):
    # TODO: make it work
    # class Meta:
    #     model = RexShowingTimeSlot

    # id = factory.Sequence(lambda n: '%s' % n)
    # start = factory.Sequence(lambda n: '%s' % n)
    # end = factory.Sequence(lambda n: '%s' % n)
    # is_rejected = factory.Sequence(lambda n: '%s' % n)
    # conversation_id = factory.Sequence(lambda n: '%s' % n)
    # created_at = factory.Sequence(lambda n: '%s' % n)

    def make(**kwargs):
        data = {
            'id': 1,
            'start': datetime.datetime(2019, 4, 1, 12, 0, 0),
            'end': None,
            'is_rejected': False,
            'conversation_id': 1,
            'created_at': datetime.datetime(2019, 4, 1, 12, 0, 0) 
        }

        return RexShowingTimeSlot(**{**data, **kwargs})
