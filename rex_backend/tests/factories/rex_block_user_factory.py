import factory


from rex_backend.models.rex_showings import RexBlockUser
# from r import RexUserFactory, RexBlockFactory


class RexBlockUserFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = RexBlockUser

    # user_address = factory.Sequence(lambda n: '%s' % n)
    # bot_address = factory.Sequence(lambda n: '%s' % n)
    # is_removed = factory.Sequence(lambda n: '%s' % n)
    # is_active = factory.Sequence(lambda n: '%s' % n)

    # user = factory.SubFactory(RexUserFactory)
    # block_id = factory.SubFactory(RexBlockFactory)
