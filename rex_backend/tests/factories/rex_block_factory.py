import factory


from rex_backend.models.rex_showings import RexBlock


class RexBlockFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = RexBlock
    
    id = factory.Sequence(lambda n: '%s' % n)
    name = factory.Faker('name')
    block_type = factory.Sequence(lambda n: '%s' % n)
    index_block = factory.Sequence(lambda n: '%s' % n)
    created_at = factory.Faker('date_time_this_year', before_now=True, after_now=False, tzinfo=None)
