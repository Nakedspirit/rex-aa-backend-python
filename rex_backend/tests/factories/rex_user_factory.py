import factory


from rex_backend.models.rex_showings import RexUser
from rex_backend.tests.helpers import dict2obj


class RexUserFactory(factory.alchemy.SQLAlchemyModelFactory):
    # TODO: make it work
    # class Meta:
    #     model = RexUser
    
    # id = factory.Sequence(lambda n: '%s' % n)
    # email_address = factory.Sequence(lambda n: 'test%s@test.test' % n)
    # phone_number = factory.Faker('phone_number')
    # first_name = factory.Faker('first_name')
    # middle_name = factory.Faker('middle_name')
    # last_name = factory.Faker('last_name')
    # created_at = factory.Faker('date_time_this_year', before_now=True, after_now=False, tzinfo=None)

    def make(**kwargs):
        data = {
            'id': 1,
            'first_name': 'John',
            'last_name': 'Doe',
            'email_address': None,
            'phone_number': '+11855342473',
            'role': 'Seller',
            'rex_showing_id': '694'
        }

        return dict2obj({**data, **kwargs})
