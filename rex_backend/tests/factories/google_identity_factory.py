import factory


from rex_backend.models.base import GoogleIdentity
from .user_factory import UserFactory


class GoogleIdentityFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = GoogleIdentity

    status = 'active'
    payload = {
        'oauth_user_id': factory.Faker('pystr', max_chars=20).generate(),
        'oauth_access_token': factory.Faker('pystr', max_chars=40).generate(),
        'expiration_date': str(factory.Faker('date_between', start_date="+1d", end_date="+30d").generate())
    }

    sign_in_at = factory.Faker('date_time_this_year', before_now=True, after_now=False, tzinfo=None)
    sign_out_at = factory.Faker('date_time_this_year', before_now=True, after_now=False, tzinfo=None)

    user = factory.SubFactory(UserFactory)

