import datetime

import factory

from rex_backend.models.rex_showings import RexShowing
# from rex_backend.tests.factories import RexBlockFactory, RexShowingTimeSlotFactory
from rex_backend.tests.helpers import dict2obj


class RexShowingFactory(factory.alchemy.SQLAlchemyModelFactory):
    # TODO: make it work
    def make(**kwargs):
        data = {
            'id': 695,
            'data': {"buyer_id": "", "outside_agent_license_number": "", "streetAddress": "15 Santa Isabel", "city": "Rancho Santa Margarita", "state": "CA", "agentsShowingInstructions": "[Lock Box Code] 9774.", "rexUrl": "15-santa-isabel", "listingId": "1", "expiresByInHours": "2", "isArchived": "False", "buyer": "John Doe <+18553424739>", "css": "John YS <jys@rexhomes.com>", "hasBuyerAgent": "False", "createdAt": "04/01/19 10:06 AM", "status": "Pending"},
            'status': 'Pending',
            'final_meeting_time': None,
            'timezone': 'UTC',
            'created_at': datetime.datetime(2019, 4, 1, 12, 0, 0),
            'claimed_at': None,
            'claimed_by_id': None,
            'open_house_id': None,
            'open_house_guid': None,
            'open_house_created_at': None,
            'open_house_css_user_id': None,
            'open_house_listing_id': None,
            'open_house_showing_id': None,
        }
        data = {**data, **kwargs}

        return dict2obj(data)



