import factory
import factory.fuzzy


from rex_backend.models.base import ReportGroup, ReportGroupStatus
from .showing_report_factory import ShowingReportFactory


class ReportGroupFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = ReportGroup

    title = factory.Sequence(lambda n: 'title-%i' % n)
    status = ReportGroupStatus.initialized
    
    showing_report = factory.SubFactory(ShowingReportFactory)

