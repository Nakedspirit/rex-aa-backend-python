import os


import pytest


from rex_backend.application import app, db
from rex_backend.models.base import BaseModel
from rex_backend.helpers import create_database_if_not_exists
from rex_backend.tests.factories import UserFactory, DeviceFactory, GoogleIdentityFactory, UserStatFactory, ShowingReportFactory, ReportAnswerFactory, ReportGroupFactory, ReportQuestionFactory, LinkToRexUserFactory, UserActionFactory


# Initialize application database for tests
testing_database_url = os.environ.get('TESTING_DATABASE_URL')
assert testing_database_url == db.connection_string
assert testing_database_url != os.environ.get('DATABASE_URL')


# Create db if not exists
create_database_if_not_exists(testing_database_url)


# Collect factories to assign sqlalchemy session to them
factories = [
    UserFactory,
    DeviceFactory,
    GoogleIdentityFactory,
    UserStatFactory,
    ShowingReportFactory,
    ReportAnswerFactory,
    ReportGroupFactory,
    ReportQuestionFactory,
    LinkToRexUserFactory,
    UserActionFactory,
]


@pytest.fixture(scope='session')
def connection():
    BaseModel.metadata.create_all(db.engine)
    connection = db.engine.connect()
    yield connection
    connection.close()
    BaseModel.metadata.drop_all(db.engine)


@pytest.fixture(scope='function')
def session(connection):
    transaction = connection.begin()

    Session = db._session_factory
    session = Session(bind=connection)
    db._session = session

    for factory in factories:
        factory._meta.sqlalchemy_session = session

    yield session

    transaction.rollback()
    session.close()
