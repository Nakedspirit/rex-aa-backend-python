from rex_backend.tests.factories import UserFactory


def test_user_phone_number():
    user = UserFactory.build(contact_phone_number='+15554440000')
    assert user.phone_number_obj
    assert user.national_phone_number == '(555) 444-0000'


def test_user_rus_phone_number():
    user = UserFactory.build(contact_phone_number='+79634440000')
    assert user.phone_number_obj
    assert user.national_phone_number == '8 (963) 444-00-00'


def test_user_without_phone_number():
    user = UserFactory.build(contact_phone_number=None)
    assert user.phone_number_obj is None
    assert user.national_phone_number is None