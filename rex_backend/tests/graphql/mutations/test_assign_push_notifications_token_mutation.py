import random
import datetime


import pytest
from unittest import mock
from requests.models import Response


from rex_backend.tests.conftest import dict2obj
from rex_backend.application import db
from rex_backend.models.base import GoogleIdentity, User, Device


def test_mutation(client, valid_jwt):
    authorization = "Bearer {}".format(valid_jwt)
    query = get_mutation('push-notifications-token')
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None

    result = response.json.get('data', {}).get('assignPushNotificationsToken', {}).get('result')
    assert result


def get_mutation(token):
    return '''mutation {{
      assignPushNotificationsToken(input: {{
          token: "{}"
      }}) {{
        result
      }}
    }}
    '''.format(token)
