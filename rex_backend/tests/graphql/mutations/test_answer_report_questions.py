import pytest
from unittest import mock


from rex_backend.application import db
from rex_backend.models.base import ShowingReportStatus
from rex_backend.tests.factories import ShowingReportFactory
from rex_backend.tests.graphql.queries.test_showing_report import FAKE_REX_SHOWING
from rex_backend.tests.helpers import dict2obj
from rex_backend.tests.mocks.apis.rex_questions import add_multiple_answers_response


FAKE_REX_LISTING = dict2obj({
    'id': 13805,
    'guid': '711124500d3c447f975490298570600f'
})


@mock.patch('rex_backend.wrappers.user.Agent.showing_query')
@mock.patch('rex_backend.models.rex_listings.RexListing.query')
@mock.patch('requests.post', return_value=add_multiple_answers_response())
def test_mutation(mock_api_response, mock_listing, mock_showing, client, current_user, valid_jwt):
    mock_listing.filter.return_value.first.return_value = FAKE_REX_LISTING
    mock_showing.return_value.first.return_value = FAKE_REX_SHOWING

    report = ShowingReportFactory.create(
        ready_for_payment=True, 
        rex_showing_id=FAKE_REX_SHOWING.id,
        user=current_user
    )
    db.session.commit()
    
    authorization = "Bearer {}".format(valid_jwt)
    query = get_mutation(report.id, {'62939552f2eb448d9c19fe90c38eaef3': ['hello!']})
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None

    result = response.json.get('data', {}).get('answerReportQuestions', {}).get('result')
    assert not result is None
    assert result['__typename'] == 'Result'
    assert result['success'] == True


def test_mutation_for_none_report(client, valid_jwt):
    authorization = "Bearer {}".format(valid_jwt)
    query = get_mutation(777, {})
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None

    result = response.json.get('data', {}).get('answerReportQuestions', {}).get('result')
    assert not result is None
    assert result['__typename'] == 'NotFoundProblem'
    assert result['entity'] == 'ShowingReport'


def get_mutation(showing_report_id, answers_to_questions):
    formatted_answers_to_questions = []
    for question_id, answers in answers_to_questions.items():
        answers = ','.join(['"{}"'.format(answer) for answer in answers])
        formatted_answers_to_questions.append(
            '{{ questionId: "{}", answers: [{}] }}'.format(question_id, answers)
        )
    formatted_answers_to_questions = ','.join(formatted_answers_to_questions)

    return '''mutation {{
        answerReportQuestions(input: {{
            showingReportId: {}
            answersToQuestions: [{}]
        }}) {{
            result {{
                ... on NotFoundProblem {{
                    __typename
                    entity
                    message
                }}
                ... on ValidationProblem {{
                    __typename
                    field
                    message
                }}
                ... on Result {{
                    __typename
                    success
                }}
            }}
        }}
    }}
    '''.format(showing_report_id, formatted_answers_to_questions)
