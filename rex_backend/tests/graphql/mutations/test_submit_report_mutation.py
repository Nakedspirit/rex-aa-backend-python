import random
import datetime


import pytest
from unittest import mock
from requests.models import Response


from rex_backend.tests.conftest import dict2obj
from rex_backend.models.base import ShowingReportStatus
from rex_backend.tests.factories import ShowingReportFactory
from rex_backend.application import db
from rex_backend.utils.tz import tznow


@pytest.fixture
def mock_rex_showing(rex_user_id):
    with mock.patch('rex_backend.wrappers.user.Agent.showing_query') as mock1:
        mock1.return_value.first.return_value = fake_rex_showing(rex_user_id)
        yield


@pytest.fixture
def mock_none_rex_showing(rex_user_id):
    with mock.patch('rex_backend.wrappers.user.Agent.showing_query') as mock1:
        mock1.return_value.first.return_value = None
        yield


@pytest.fixture
def mock_rex_response():
    with mock.patch('rex_backend.utils.rex_scheduler_api.RexSchedulerApi.call_complete_endpoint') as mock1:
        mock1.return_value = { 'status': 200, 'content': { 'success': True } }
        yield


@pytest.fixture
def mock_bad_rex_response():
    with mock.patch('rex_backend.utils.rex_scheduler_api.RexSchedulerApi.call_complete_endpoint') as mock1:
        mock1.return_value = { 'status': 400, 'content': { 'success': False } }
        yield


@pytest.fixture
def mock_rex_response_false():
    with mock.patch('rex_backend.utils.rex_scheduler_api.RexSchedulerApi.call_complete_endpoint') as mock1:
        mock1.return_value = { 'status': 200, 'content': { 'success': False } }
        yield


@pytest.fixture
def showing_report(current_user):
    showing_report = ShowingReportFactory.create(
        user_id=current_user.id,
        rex_showing_id=695,
        status=ShowingReportStatus.ok,
        status_comment='hey there',
        status_added_at=tznow()
    )
    db.session.commit()
    yield showing_report


def test_mutation(client, valid_jwt, mock_rex_showing, mock_rex_response):
    authorization = "Bearer {}".format(valid_jwt)
    query = get_mutation(695, 'ok', 'hello there')
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None

    result = response.json.get('data', {}).get('submitReport', {}).get('result')
    assert not result is None
    assert result['__typename'] == 'ShowingReport'
    assert result['showing']


def test_mutation_report_exists(client, valid_jwt, mock_rex_showing, mock_rex_response, showing_report):
    authorization = "Bearer {}".format(valid_jwt)
    query = get_mutation(695, 'ok', 'hello there')
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None

    result = response.json.get('data', {}).get('submitReport', {}).get('result')
    assert not result is None
    assert result['__typename'] == 'ShowingReport'
    assert result['showing']


def test_mutation_when_no_showing(client, valid_jwt, mock_none_rex_showing):
    authorization = "Bearer {}".format(valid_jwt)
    query = get_mutation(695, 'ok', 'hello there')
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None

    result = response.json.get('data', {}).get('submitReport', {}).get('result')
    assert not result is None
    assert result['__typename'] == 'NotFoundProblem'


def test_mutation_when_bad_rex_response(client, valid_jwt, mock_rex_showing, mock_bad_rex_response):
    authorization = "Bearer {}".format(valid_jwt)
    query = get_mutation(695, 'ok', 'hello there')
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None

    result = response.json.get('data', {}).get('submitReport', {}).get('result')
    assert not result is None
    assert result['__typename'] == 'ValidationProblem'


def test_mutation_when_rex_response_success_false(client, valid_jwt, mock_rex_showing, mock_rex_response_false):
    authorization = "Bearer {}".format(valid_jwt)
    query = get_mutation(695, 'ok', 'hello there')
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None

    result = response.json.get('data', {}).get('submitReport', {}).get('result')
    assert not result is None
    assert result['__typename'] == 'ValidationProblem'


def get_mutation(rex_showing_id, status, status_comment = ''):
    return '''mutation {{
      submitReport(input: {{
          showingId: {}
          status: {}
          statusComment: "{}"
      }}) {{
        result {{
          ... on ShowingReport {{
            __typename
            id
            status
            statusComment
            statusAddedAt
            showing {{
                id
            }}
          }}
          ... on NotFoundProblem {{
            __typename
            message
          }}
          ... on ValidationProblem {{
            __typename
            field
            message
          }}
        }}
      }}
    }}
    '''.format(rex_showing_id, status, status_comment)

def fake_rex_showing(rex_user_id):
    return dict2obj({
        'id': '695',
        'status': 'Scheduled',
        'data': {"buyer_id": "", "outside_agent_license_number": "", "streetAddress": "15 Santa Isabel", "city": "Rancho Santa Margarita", "state": "CA", "agentsShowingInstructions": "[Lock Box Code] 9774.", "rexUrl": "15-santa-isabel", "listingId": "939", "expiresByInHours": "2", "isArchived": "False", "buyer": "John Doe <+18553424739>", "css": "John YS <jys@rexhomes.com>", "hasBuyerAgent": "False", "createdAt": "04/01/19 10:06 AM", "status": "Pending"},
        'final_meeting_time': datetime.datetime(2019, 4, 1, 12, 0, 0),
        'timezone': 'UTC',
        'created_at': datetime.datetime(2019, 4, 1, 12, 0, 0),
        'claimed_at': datetime.datetime(2019, 4, 1, 12, 0, 0),
        'claimed_by_id': rex_user_id
    })
