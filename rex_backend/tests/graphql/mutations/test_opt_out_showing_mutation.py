import random
import datetime


import pytest
from unittest import mock
from requests.models import Response


from rex_backend.models.rex_showings import RexBlockUser
from rex_backend.repositories.user_action_repo import UserActionRepo


FAKE_REX_MEMBER = RexBlockUser(
    user_address='+1111111111',
    bot_address='+1222222222',
    is_removed=True,
    is_active=True
)


@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_rex_member', return_value=FAKE_REX_MEMBER)
@mock.patch('rex_backend.utils.rex_scheduler_api.RexSchedulerApi.call_main_endpoint', return_value={ 'status': 200, 'content': { 'success': True } })
def test_mutation(mock_rex_response, mock_rex_channel, client, valid_jwt, current_user):
    authorization = "Bearer {}".format(valid_jwt)
    query = get_mutation(695)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None

    result = response.json.get('data', {}).get('optOutShowing', {}).get('result')
    assert result == True

    actions = UserActionRepo.load_hide_showing_actions_by_user_id(current_user.id)
    assert actions, 'there should be one hide_showing action'
    assert len(actions) == 1


@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_rex_member', return_value=FAKE_REX_MEMBER)
@mock.patch('rex_backend.utils.rex_scheduler_api.RexSchedulerApi.call_main_endpoint', return_value={ 'status': 200, 'content': { 'success': False } })
def test_mutation_with_false_api_response(mock_rex_response, mock_rex_channel, client, valid_jwt, current_user):
    authorization = "Bearer {}".format(valid_jwt)
    query = get_mutation(695)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None

    result = response.json.get('data', {}).get('optOutShowing', {}).get('result')
    assert result == False

    actions = UserActionRepo.load_hide_showing_actions_by_user_id(current_user.id)
    assert not actions, 'there should not be hide_showing actions'


@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_rex_member', return_value=None)
def test_mutation_where_no_channel(mock_rex_channel, client, valid_jwt, current_user):
    authorization = "Bearer {}".format(valid_jwt)
    query = get_mutation(695)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None

    result = response.json.get('data', {}).get('optOutShowing', {}).get('result')
    assert result == False

    actions = UserActionRepo.load_hide_showing_actions_by_user_id(current_user.id)
    assert not actions, 'there should not be hide_showing actions'


def get_mutation(rex_showing_id):
    return '''mutation {{
      optOutShowing(input: {{
          showingId: {}
      }}) {{
        result
      }}
    }}
    '''.format(rex_showing_id)
