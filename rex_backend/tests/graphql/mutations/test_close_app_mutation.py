import random
import datetime


import pytest
from unittest import mock
from requests.models import Response


from rex_backend.tests.conftest import dict2obj, make_jwt_with_device
from rex_backend.application import db
from rex_backend.application import jwt_service
from rex_backend.models.base import GoogleIdentity, User, Device


def test_close_app_mutation(client, user_and_identity_orm_obj):
    device_id = user_and_identity_orm_obj['device'].id
    valid_jwt = make_jwt_with_device(user_and_identity_orm_obj)
    authorization = "Bearer {}".format(valid_jwt)
    query = "mutation { closeApp(input: {}) { result } }"
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None

    device = Device.query.filter(Device.id == device_id).first()
    assert device
    assert device.closed_at is not None