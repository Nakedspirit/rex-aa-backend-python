import random
import datetime


import pytest
from unittest import mock
from requests.models import Response


from rex_backend.tests.conftest import dict2obj
from rex_backend.models.rex_showings import RexBlockUser


@pytest.fixture
def mock_rex_channel():
    with mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_rex_member') as mock1:
        mock1.return_value = fake_rex_channel()
        yield

@pytest.fixture
def mock_none_rex_channel():
    with mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_rex_member') as mock1:
        mock1.return_value = None
        yield

@pytest.fixture
def mock_rex_response():
    with mock.patch('rex_backend.utils.rex_scheduler_api.RexSchedulerApi.call_main_endpoint') as mock1:
        mock1.return_value = fake_rex_response()
        yield

@pytest.fixture
def mock_rex_response_for_another_showing():
    with mock.patch('rex_backend.utils.rex_scheduler_api.RexSchedulerApi.call_main_endpoint') as mock1:
        mock1.return_value = fake_rex_response(66666666)
        yield

@pytest.fixture
def mock_bad_rex_response():
    with mock.patch('rex_backend.utils.rex_scheduler_api.RexSchedulerApi.call_main_endpoint') as mock1:
        mock1.return_value = fake_bad_rex_response()
        yield

@pytest.fixture
def mock_rex_showing(rex_user_id):
    with mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_rex_showing') as mock1:
        mock1.return_value = fake_rex_showing(rex_user_id)
        yield

@pytest.fixture
def mock_claimed_rex_showing():
    with mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_rex_showing') as mock1:
        mock1.return_value = fake_rex_showing(66666)
        yield

def test_mutation(client, valid_jwt, mock_rex_channel, mock_rex_response, mock_rex_showing):
    authorization = "Bearer {}".format(valid_jwt)
    query = get_mutation(695, [1])
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None

    result = response.json.get('data', {}).get('claimShowing', {}).get('result')
    assert not result is None
    assert result['__typename'] == 'Showing'


def test_mutation_where_no_channel(client, valid_jwt, mock_none_rex_channel):
    authorization = "Bearer {}".format(valid_jwt)
    query = get_mutation(695, [1])
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    result = response.json.get('data', {}).get('claimShowing', {}).get('result')

    assert response.json.get('errors') is None
    assert not result is None
    assert result['__typename'] == 'InvalidRequestProblem'


def test_mutation_where_no_rex_showing(client, valid_jwt, mock_rex_channel, mock_rex_response, mock_claimed_rex_showing):
    authorization = "Bearer {}".format(valid_jwt)
    query = get_mutation(695, [1])
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    result = response.json.get('data', {}).get('claimShowing', {}).get('result')

    assert response.json.get('errors') is None
    assert not result is None
    assert result['__typename'] == 'AlreadyClaimedProblem'


def get_mutation(rex_showing_id, slots_indexes_to_claim = []):
    return '''mutation {{
      claimShowing(input: {{
          showingId: {}
          slotsIndexesToClaim: [{}]
      }}) {{
        result {{
          ... on Showing {{
            __typename
            id
            status
            showingReport {{
              id
            }}
            showingInstructions
            needSellerAvailable
            needRexShowingRepresentative
            isOutsideAgent
            createdAt
            claimedAt
            finalShowingTime
            submittedForPaymentAt
          }}
          ... on InvalidRequestProblem {{
            __typename
            showingId
            message
          }}
          ... on AlreadyClaimedProblem {{
            __typename
            showingId
            message
            claimedAt
          }}
        }}
      }}
    }}
    '''.format(
        rex_showing_id,
        ','.join([str(slot) for slot in slots_indexes_to_claim])
    )



def fake_rex_response(rex_showing_id = '695'):
    return { 'status': 200, 'content': { 'success': True, 'converastionId': rex_showing_id } }

def fake_bad_rex_response():
    return { 'status': 400, 'content': { 'success': False } }

def fake_rex_channel():
    return RexBlockUser(
        user_address = '+1111111111',
        bot_address = '+1222222222'
    )

def fake_rex_showing(rex_user_id):
    return dict2obj({
        'id': '695',
        'status': 'Pending',
        'data': {"buyer_id": "", "outside_agent_license_number": "", "streetAddress": "15 Santa Isabel", "city": "Rancho Santa Margarita", "state": "CA", "agentsShowingInstructions": "[Lock Box Code] 9774.", "rexUrl": "15-santa-isabel", "listingId": "939", "expiresByInHours": "2", "isArchived": "False", "buyer": "John Doe <+18553424739>", "css": "John YS <jys@rexhomes.com>", "hasBuyerAgent": "False", "createdAt": "04/01/19 10:06 AM", "status": "Pending"},
        'final_meeting_time': datetime.datetime(2019, 4, 1, 12, 0, 0),
        'timezone': 'UTC',
        'created_at': datetime.datetime(2019, 4, 1, 12, 0, 0),
        'claimed_at': datetime.datetime(2019, 4, 1, 12, 0, 0),
        'claimed_by_id': rex_user_id
    })
