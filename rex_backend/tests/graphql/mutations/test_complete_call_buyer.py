import random
import datetime


import pytest
from unittest import mock
from requests.models import Response


from rex_backend.tests.conftest import dict2obj
from rex_backend.models.base import ShowingReportStatus
from rex_backend.application import db
from rex_backend.utils.tz import tznow
from rex_backend.tests.factories import ShowingReportFactory, RexShowingFactory


@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.rex_showing_query')
def test_mutation(mock_rex_showing, client, valid_jwt, current_user):
    rex_showing = RexShowingFactory.make(id=1)
    mock_rex_showing.return_value.first.return_value = rex_showing
    showing_report = ShowingReportFactory.create(
        rex_showing_id=rex_showing.id,
        user=current_user,
        status=ShowingReportStatus.ok,
        status_added_at = tznow()
    )
    db.session.commit()

    authorization = "Bearer {}".format(valid_jwt)
    query = get_mutation(showing_report.id)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None

    result = response.json.get('data', {}).get('completeCallBuyer', {}).get('result')
    assert result
    assert result['id'] == str(showing_report.id)


@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.rex_showing_query')
def test_mutation_without_showing_report(mock_rex_showing, client, valid_jwt):
    mock_rex_showing.return_value.first.return_value = RexShowingFactory.make(id=1)

    authorization = "Bearer {}".format(valid_jwt)
    query = get_mutation(666)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None

    result = response.json.get('data', {}).get('completeCallBuyer', {}).get('result')
    assert result is None


@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.rex_showing_query')
def test_mutation_with_showing_report_with_wrong_status(mock_rex_showing, client, valid_jwt, current_user):
    rex_showing = RexShowingFactory.make(id=1)
    mock_rex_showing.return_value.first.return_value = rex_showing

    showing_report = ShowingReportFactory.create(
        rex_showing_id=rex_showing.id,
        user=current_user,
        status=ShowingReportStatus.agent_could_not_make_it
    )
    db.session.commit()

    authorization = "Bearer {}".format(valid_jwt)
    query = get_mutation(showing_report.id)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None

    result = response.json.get('data', {}).get('completeCallBuyer', {}).get('result')
    assert result is None


def get_mutation(showing_report_id):
    return '''mutation {{
      completeCallBuyer(input: {{
          showingReportId: {}
      }}) {{
        result {{
            id
            status
        }}
      }}
    }}
    '''.format(showing_report_id)
