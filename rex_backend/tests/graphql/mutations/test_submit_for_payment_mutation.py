import random
import datetime


import pytest
from unittest import mock
from requests.models import Response


from rex_backend.tests.conftest import dict2obj
from rex_backend.models.base import ShowingReportStatus
from rex_backend.application import db
from rex_backend.utils.tz import tznow
from rex_backend.tests.factories import ShowingReportFactory, RexShowingFactory



@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.rex_showing_query')
def test_mutation(mock_rex_showing, client, current_user, valid_jwt):
    rex_showing = RexShowingFactory.make(
        id=1,
    )
    mock_rex_showing.return_value.first.return_value = rex_showing
    report = ShowingReportFactory.create(
            rex_showing_id=rex_showing.id,
            user=current_user,
            status=ShowingReportStatus.ok
    )
    db.session.commit()

    authorization = "Bearer {}".format(valid_jwt)
    query = get_mutation(rex_showing.id)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None
    result = response.json.get('data', {}).get('submitForPayment', {}).get('result')
    assert result


@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.rex_showing_query')
def test_mutation_without_showing_report(mock_rex_showing, client, valid_jwt):
    rex_showing = RexShowingFactory.make(
        id=1,
    )
    mock_rex_showing.return_value.first.return_value = rex_showing
    authorization = "Bearer {}".format(valid_jwt)
    query = get_mutation(rex_showing.id)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None
    result = response.json.get('data', {}).get('submitForPayment', {}).get('result')
    assert result is None


def get_mutation(rex_showing_id):
    return '''mutation {{
      submitForPayment(input: {{
          showingId: {}
      }}) {{
        result {{
            id
            status
        }}
      }}
    }}
    '''.format(rex_showing_id)
