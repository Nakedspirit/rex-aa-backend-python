import random
import datetime


import pytest
from unittest import mock
from requests.models import Response

from rex_backend.tests.conftest import dict2obj
from rex_backend.tests.factories import RexShowingFactory, LinkToRexUserFactory
from rex_backend.services import firebase_cm_service


@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_rex_open_house')
@mock.patch.object(firebase_cm_service, 'send_messages', return_value='mock is working')
def test_mutation_for_showing_confirmed_by_seller(mock_firebase_cm_service, mock_rex_showing, session, client, current_user, valid_register_event_secret):
    mock_rex_showing.return_value = RexShowingFactory.make(
        id=1,
        status='Scheduled',
        claimed_by_id='999',
        open_house_id=1,
    )
    LinkToRexUserFactory.create(
        user=current_user,
        rex_user_id=999
    )
    session.commit()

    authorization = "Bearer {}".format(valid_register_event_secret)
    query = get_mutation('SIGNIN_RECORDED')

    response = client.post('/graphql', data={'query': query}, headers={'Authorization': authorization})
    assert not response.json.get('errors')
    assert response.json.get('data', {}).get('registerEvent', {}).get('result') is True


@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_rex_open_house', return_value=None)
def test_mutation_for_showing_confirmed_by_seller_with_invalid_showing(mock_rex_showing, client, valid_register_event_secret):
    authorization = "Bearer {}".format(valid_register_event_secret)
    query = get_mutation('SIGNIN_RECORDED')
    response = client.post('/graphql', data={'query': query}, headers={'Authorization': authorization})
    assert not response.json.get('errors')
    assert response.json.get('data', {}).get('registerEvent', {}).get('result') is False


def get_mutation(event_name):
    return '''mutation Test{{
    registerEvent(input: {{
        eventName: {}
        objectId: {}
    }}) {{
        result
    }}
    }}
    '''.format(event_name, '"test_guid_1"')
