import random
import datetime

import pytest
from unittest import mock
from requests.models import Response

from rex_backend.tests.factories import RexShowingFactory, LinkToRexUserFactory, UserActionFactory
from rex_backend.services import firebase_cm_service


@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_rex_showing')
@mock.patch.object(firebase_cm_service, 'send_messages', return_value='mock is working')
def test_mutation_where_showing_cancelled_by_user(mock_firebase_cm_service, mock_rex_showing, session, client, current_user, valid_register_event_secret):
    mock_rex_showing.return_value = RexShowingFactory.make(
        id=1,
        status='Cancelled',
        claimed_at=datetime.datetime(2019, 4, 1, 12, 0, 0),
        claimed_by_id='999'
    )
    LinkToRexUserFactory.create(
        user=current_user,
        rex_user_id=999
    )
    UserActionFactory.create(
        hide_showing=True,
        user=current_user,
        object_id=1,
    )
    session.commit()

    authorization = "Bearer {}".format(valid_register_event_secret)
    query = get_mutation('SHOWING_CANCELLED')
    response = client.post('/graphql', data={'query': query}, headers={'Authorization': authorization})
    assert not response.json.get('errors')
    assert response.json.get('data', {}).get('registerEvent', {}).get('result') is True

@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_rex_showing')
@mock.patch.object(firebase_cm_service, 'send_messages', return_value='mock is working')
def test_mutation_where_user_not_cancelled_this_showing(mock_firebase_cm_service, mock_rex_showing, session, client, current_user, valid_register_event_secret):
    mock_rex_showing.return_value = RexShowingFactory.make(
        id=1,
        status='Cancelled',
        claimed_at=datetime.datetime(2019, 4, 1, 12, 0, 0),
        claimed_by_id='999'
    )
    LinkToRexUserFactory.create(
        user=current_user,
        rex_user_id=999
    )
    UserActionFactory.create(
        hide_showing=True,
        user=current_user,
        object_id=2,
    )
    session.commit()

    authorization = "Bearer {}".format(valid_register_event_secret)
    query = get_mutation('SHOWING_CANCELLED')
    response = client.post('/graphql', data={'query': query}, headers={'Authorization': authorization})
    assert not response.json.get('errors')
    assert response.json.get('data', {}).get('registerEvent', {}).get('result') is True


@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_rex_showing')
@mock.patch.object(firebase_cm_service, 'send_messages', return_value='mock is working')
def test_mutation(mock_firebase_cm_service, mock_rex_showing, session, client, current_user, valid_register_event_secret):
    mock_rex_showing.return_value = RexShowingFactory.make(
        id=1,
        status='Cancelled',
        claimed_at=datetime.datetime(2019, 4, 1, 12, 0, 0),
        claimed_by_id='999'
    )
    LinkToRexUserFactory.create(
        user=current_user,
        rex_user_id=999
    )
    session.commit()

    authorization = "Bearer {}".format(valid_register_event_secret)
    query = get_mutation('SHOWING_CANCELLED')
    response = client.post('/graphql', data={'query': query}, headers={'Authorization': authorization})
    assert not response.json.get('errors')
    assert response.json.get('data', {}).get('registerEvent', {}).get('result') is True

    query = get_mutation('OPEN_HOUSE_CANCELLED')
    response = client.post('/graphql', data={'query': query}, headers={'Authorization': authorization})
    assert not response.json.get('errors')
    assert response.json.get('data', {}).get('registerEvent', {}).get('result') is True

@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_rex_showing')
def test_mutation_with_showing_claimed_by_different_user(mock_rex_showing, client, current_user, valid_register_event_secret):
    claimed_by_id = 999
    assert claimed_by_id not in current_user.rex_users_ids, 'In the test rex_users_ids should not contain claimed_by_id'

    mock_rex_showing.return_value = RexShowingFactory.make(
        id=1,
        status='Cancelled',
        claimed_at=datetime.datetime(2019, 4, 1, 12, 0, 0),
        claimed_by_id=str(claimed_by_id)
    )

    authorization = "Bearer {}".format(valid_register_event_secret)
    query = get_mutation('SHOWING_CANCELLED')
    response = client.post('/graphql', data={'query': query}, headers={'Authorization': authorization})
    assert not response.json.get('errors')
    assert response.json.get('data', {}).get('registerEvent', {}).get('result') is False


@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_rex_showing', return_value=None)
def test_mutation_with_none_rex_showing(mock_rex_showing, client, valid_register_event_secret):
    authorization = "Bearer {}".format(valid_register_event_secret)
    query = get_mutation('SHOWING_CANCELLED')
    response = client.post('/graphql', data={'query': query}, headers={'Authorization': authorization})
    assert not response.json.get('errors')
    assert response.json.get('data', {}).get('registerEvent', {}).get('result') is False


def get_mutation(event_name):
    return '''mutation Test{{
    registerEvent(input: {{
        eventName: {}
        objectId: {}
    }}) {{
        result
    }}
    }}
    '''.format(event_name, 18)