import random
import datetime


import pytest
from unittest import mock
from requests.models import Response

from rex_backend.tests.conftest import dict2obj
from rex_backend.tests.factories import RexShowingFactory, DeviceFactory, LinkToRexUserFactory
from rex_backend.services import firebase_cm_service


@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_rex_showing')
@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_rex_users_by_rex_showing')
@mock.patch.object(firebase_cm_service, 'send_messages', return_value='mock is working')
def test_mutation_for_new_showing(mock_firebase_cm_service, mock_rex_users, mock_rex_showing, session, client, valid_register_event_secret):
    mock_rex_showing.return_value = RexShowingFactory.make(
        id=1,
        status='Pending',
        claimed_at=None,
        claimed_by_id=None
    )
    mock_rex_users.return_value = [
        dict2obj({'id': 1}),
        dict2obj({'id': 2}),
        dict2obj({'id': 999})
    ]

    device = DeviceFactory.create()
    LinkToRexUserFactory.create(
        user=device.user,
        rex_user_id=999
    )
    session.commit()

    authorization = "Bearer {}".format(valid_register_event_secret)
    query = get_mutation('SHOWING_CREATED')
    response = client.post('/graphql', data={'query': query}, headers={'Authorization': authorization})
    assert not response.json.get('errors')
    assert response.json.get('data', {}).get('registerEvent', {}).get('result') is True

    query = get_mutation('OPEN_HOUSE_CREATED')
    response = client.post('/graphql', data={'query': query}, headers={'Authorization': authorization})
    assert not response.json.get('errors')
    assert response.json.get('data', {}).get('registerEvent', {}).get('result') is True


@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_rex_showing', return_value=None)
def test_mutation_for_new_showing_where_showing_is_claimed(mock_rex_showing, client, valid_register_event_secret):
    authorization = "Bearer {}".format(valid_register_event_secret)
    query = get_mutation('SHOWING_CREATED')
    response = client.post('/graphql', data={'query': query}, headers={'Authorization': authorization})
    assert not response.json.get('errors')
    assert response.json.get('data', {}).get('registerEvent', {}).get('result') is False


def get_mutation(event_name):
    return '''mutation Test{{
    registerEvent(input: {{
        eventName: {}
        objectId: {}
    }}) {{
        result
    }}
    }}
    '''.format(event_name, 18)