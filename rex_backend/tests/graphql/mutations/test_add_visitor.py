from unittest import mock

from rex_backend.tests.factories import RexShowingFactory


FAKE_OPEN_HOUSE = RexShowingFactory.make(
    id=1,
    open_house_id=1,
    open_house_guid='test_guid_1'
)
FAKE_OPEN_HOUSE_NUMBER = '+12025550190'


def test_add_visitor_with_invalid_email(client, valid_jwt):
    query = get_mutation(
        showing_id=1,
        email='invalid-email'
    )
    authorization = f'Bearer {valid_jwt}'
    response = client.post(
        '/graphql', 
        data={'query': query}, 
        headers={'Authorization': authorization}
    )
    json = response.json
    assert not json.get('errors')
    assert json.get('data')

    result = json.get('data').get('addVisitor', {}).get('result')
    assert result == {'__typename': 'ValidationProblem', 'field': 'email', 'message': 'Email is not valid'}


@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_rex_showing', return_value=None)
def test_add_visitor_where_showing_not_found(mock_rex_showing, client, valid_jwt):
    wrong_id = 666
    query = get_mutation(showing_id=wrong_id)
    authorization = f'Bearer {valid_jwt}'
    response = client.post(
        '/graphql', 
        data={'query': query}, 
        headers={'Authorization': authorization}
    )
    json = response.json
    assert not json.get('errors')
    assert json.get('data')

    result = json.get('data').get('addVisitor', {}).get('result')
    assert result == {'__typename': 'NotFoundProblem', 'entity': 'Open House', 'message': 'Open House not found.'}


@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_rex_showing')
def test_add_visitor_where_showing_is_not_open_house(mock_rex_showing, client, valid_jwt):
    fake_rex_showing = RexShowingFactory.make(id=1)
    mock_rex_showing.return_value = fake_rex_showing

    query = get_mutation(showing_id=fake_rex_showing.id)
    authorization = f'Bearer {valid_jwt}'
    response = client.post(
        '/graphql', 
        data={'query': query}, 
        headers={'Authorization': authorization}
    )
    json = response.json
    assert not json.get('errors')
    assert json.get('data')

    result = json.get('data').get('addVisitor', {}).get('result')
    assert result == {'__typename': 'NotFoundProblem', 'entity': 'Open House', 'message': 'Open House not found.'}


@mock.patch('rex_backend.graphql.mutations.add_visitor.resolve_phone_number', return_value=None)
@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_rex_showing', return_value=FAKE_OPEN_HOUSE)
def test_add_visitor_where_open_house_has_not_phone_number(mock_resolve_phone_number, mock_rex_showing, client, valid_jwt):
    query = get_mutation(showing_id=FAKE_OPEN_HOUSE.id)
    authorization = f'Bearer {valid_jwt}'
    response = client.post(
        '/graphql', 
        data={'query': query}, 
        headers={'Authorization': authorization}
    )
    json = response.json
    assert not json.get('errors')
    assert json.get('data')

    result = json.get('data').get('addVisitor', {}).get('result')
    assert result == {'__typename': 'NoPhoneNumberProblem', 'message': 'There is a problem with Open House phone number. Please contact REX Operations for further instructions.'}


@mock.patch('rex_backend.graphql.mutations.add_visitor.resolve_phone_number', return_value=FAKE_OPEN_HOUSE_NUMBER)
@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_rex_showing', return_value=FAKE_OPEN_HOUSE)
@mock.patch('requests.post')
def test_add_visitor_where_api_response_is_failed(mock_requests_post, mock_resolve_phone_number, mock_rex_showing, client, valid_jwt):
    mock_add_visitor = mock.Mock()
    mock_add_visitor.status_code = 200
    mock_add_visitor.json.return_value = {'success': False}

    mock_sign_visitor = mock.Mock()
    mock_sign_visitor.status_code = 200
    mock_sign_visitor.json.return_value = {'success': False}

    mock_requests_post.side_effect = [
        mock_add_visitor,
        mock_sign_visitor,
    ]

    query = get_mutation(showing_id=FAKE_OPEN_HOUSE.id)
    authorization = f'Bearer {valid_jwt}'
    response = client.post(
        '/graphql', 
        data={'query': query}, 
        headers={'Authorization': authorization}
    )

    json = response.json
    assert not json.get('errors')
    assert json.get('data')

    result = json.get('data').get('addVisitor', {}).get('result')
    assert result is result == {'__typename': 'AddVisitorNotOk', 'message': 'REX sign visitor API endpoint returned False.'}


@mock.patch('rex_backend.graphql.mutations.add_visitor.resolve_phone_number', return_value=FAKE_OPEN_HOUSE_NUMBER)
@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_rex_showing', return_value=FAKE_OPEN_HOUSE)
@mock.patch('requests.post')
def test_add_visitor_where_api_response_is_success(mock_requests_post, mock_resolve_phone_number, mock_rex_showing, client, valid_jwt):
    mock_add_visitor = mock.Mock()
    mock_add_visitor.status_code = 200
    mock_add_visitor.json.return_value = {'success': False}

    mock_sign_visitor = mock.Mock()
    mock_sign_visitor.status_code = 200
    mock_sign_visitor.json.return_value = {'success': True, 'id': 1}

    mock_requests_post.side_effect = [
        mock_add_visitor,
        mock_sign_visitor,
    ]

    query = get_mutation(showing_id=FAKE_OPEN_HOUSE.id)
    authorization = f'Bearer {valid_jwt}'
    response = client.post(
        '/graphql', 
        data={'query': query}, 
        headers={'Authorization': authorization}
    )

    json = response.json
    assert not json.get('errors')
    assert json.get('data')

    result = json.get('data').get('addVisitor', {}).get('result')
    assert result == {'__typename': 'AddVisitorOk', 'visitorId': '1'}


def get_mutation(
    showing_id=None,
    name='John Doe',
    email='jdoe@fake.fake',
    phone_number='+12025550127'
):
    return f'''
    mutation {{
        addVisitor(input: {{
            showingId: {showing_id}
            name: "{name}"
            email: "{email}"
            phoneNumber: "{phone_number}"

        }}) {{
            result {{
                ... on NotFoundProblem {{
                    __typename
                    entity
                    message
                }}
                ... on ValidationProblem {{
                    __typename
                    field
                    message
                }}
                ... on NoPhoneNumberProblem {{
                    __typename
                    message
                }}
                ... on AddVisitorNotOk {{
                    __typename
                    message
                }}
                ... on AddVisitorOk {{
                    __typename
                    visitorId
                }}
            }}
        }}
    }}
    '''