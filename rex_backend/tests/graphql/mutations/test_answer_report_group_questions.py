import pytest
from unittest import mock


from rex_backend.application import db
from rex_backend.models.base import ShowingReportStatus
from rex_backend.tests.factories import ShowingReportFactory, ReportGroupFactory
from rex_backend.tests.graphql.queries.test_showing_report import FAKE_REX_SHOWING
from rex_backend.tests.helpers import dict2obj
from rex_backend.tests.mocks.apis.rex_questions import add_multiple_answers_response
from rex_backend.models.base import ReportGroup


FAKE_REX_LISTING = dict2obj({
    'id': 13805,
    'guid': '711124500d3c447f975490298570600f'
})


@mock.patch('rex_backend.wrappers.user.Agent.showing_query')
@mock.patch('rex_backend.models.rex_listings.RexListing.query')
@mock.patch('requests.post', return_value=add_multiple_answers_response())
@mock.patch('rex_backend.interactors.send_email_with_showing_report.SendEmailWithShowingReport.call', return_value=None)
@mock.patch('rex_backend.interactors.send_answers_to_rex.SendAnswersToRex.call', return_value=None)
def test_mutation(mock_send_answers_to_rex_interactor, mock_send_email_interactor, mock_api_response, mock_listing, mock_showing, client, current_user, valid_jwt):
    mock_listing.filter.return_value.first.return_value = FAKE_REX_LISTING
    mock_showing.return_value.first.return_value = FAKE_REX_SHOWING

    report = ShowingReportFactory.create(
        ready_for_payment=True, 
        rex_showing_id=FAKE_REX_SHOWING.id,
        user=current_user
    )
    report_group = ReportGroupFactory.create(
        title='test title',
        showing_report=report
    )
    db.session.commit()
    
    authorization = "Bearer {}".format(valid_jwt)
    query = get_mutation(report.id, report_group.title, {'62939552f2eb448d9c19fe90c38eaef3': ['hello!']})
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None

    assert mock_send_email_interactor.called
    assert mock_send_answers_to_rex_interactor.called

    result = response.json.get('data', {}).get('answerReportGroupQuestions', {}).get('result')
    assert not result is None
    assert result['__typename'] == 'Result'
    assert result['success'] == True

    assert report_group.status == 'completed'
    assert report.is_questions_completed


def get_mutation(showing_report_id, group_title, answers_to_questions):
    formatted_answers_to_questions = []
    for question_id, answers in answers_to_questions.items():
        answers = ','.join(['"{}"'.format(answer) for answer in answers])
        formatted_answers_to_questions.append(
            '{{ questionId: "{}", answers: [{}] }}'.format(question_id, answers)
        )
    formatted_answers_to_questions = ','.join(formatted_answers_to_questions)

    return '''mutation {{
        answerReportGroupQuestions(input: {{
            showingReportId: {},
            groupTitle: "{}"
            answersToQuestions: [{}]
        }}) {{
            result {{
                ... on NotFoundProblem {{
                    __typename
                    entity
                    message
                }}
                ... on ValidationProblem {{
                    __typename
                    field
                    message
                }}
                ... on Result {{
                    __typename
                    success
                }}
            }}
        }}
    }}
    '''.format(showing_report_id, group_title, formatted_answers_to_questions)