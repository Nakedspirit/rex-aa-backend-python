from unittest import mock

from rex_backend.tests.factories import RexShowingFactory, RexListingFactory
from rex_backend.tests.mocks.apis.rex_questions import VISITOR_QUESTIONS_BAD_RESPONSE_MOCK, VISITOR_QUESTIONS_MOCK, OPENHOUSELEADS_QUESTIONS_MOCK


QUESTIONS_API_RESPONSE = mock.Mock()
QUESTIONS_API_RESPONSE.status_code = 200
QUESTIONS_API_RESPONSE.json.return_value = {
    'success': True,
    'result': {
        'pages': [
            {
                'title': ' ',
                'elements': [
                    {
                        'name': 'test',
                        'title': 'test?',
                        'valueName': 'test',
                        'choices': [
                            {'value': 'Yes', 'text': 'Yes'},
                            {'value': 'No', 'text': 'No'}
                        ],
                        'type': 'radiogroup'
                    },
                    {
                        'name': 'test1',
                        'title': 'test1?',
                        'valueName': 'test1',
                        'choices': [
                            {'value': '1', 'text': '1'},
                            {'value': '2', 'text': '2'},
                            {'value': '3', 'text': '3'}
                        ],
                        'type': 'checkbox'
                    },
                ]
            }
        ]
    }
}


SEND_ANSWERS_RESPONSE = mock.Mock()
SEND_ANSWERS_RESPONSE.status_code = 200
SEND_ANSWERS_RESPONSE.json.return_value = {'success': True}


@mock.patch('rex_backend.wrappers.user.Agent.showing_query')
def test_without_rex_showing(mock_rex_showing, client, valid_jwt):
    showing_id = 1
    visitor_id = 1
    mock_rex_showing.return_value.first.return_value = None

    query = get_query(showing_id=showing_id, visitor_id=visitor_id)
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })

    json = response.json
    assert not json.get('errors')
    data = json.get('data', {}).get('answerVisitorQuestions', {}).get('result')

    assert data == {'__typename': 'NotFoundProblem', 'entity': 'Open House', 'message': 'Open House not found.'}


@mock.patch('rex_backend.wrappers.user.Agent.showing_query')
@mock.patch('rex_backend.models.rex_listings.RexListing.query')
def test_without_rex_listing(mock_rex_listing, mock_rex_showing, client, valid_jwt):
    showing_id = 1
    visitor_id = 1
    mock_rex_showing.return_value.first.return_value = RexShowingFactory.make(
        id=showing_id,
        open_house_id=1,
        open_house_guid='open_house_guid'
    )
    mock_rex_listing.filter.return_value.first.return_value = None

    query = get_query(showing_id=showing_id, visitor_id=visitor_id)
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })

    json = response.json
    assert not json.get('errors')
    data = json.get('data', {}).get('answerVisitorQuestions', {}).get('result')

    assert data == {'__typename': 'NotFoundProblem', 'entity': 'Listing', 'message': 'Listing not found.'}


@mock.patch('rex_backend.wrappers.user.Agent.showing_query')
@mock.patch('rex_backend.models.rex_listings.RexListing.query')
def test_mutation_without_answers_argument(mock_rex_listing, mock_rex_showing, client, valid_jwt):
    showing_id = 1
    visitor_id = 1
    mock_rex_showing.return_value.first.return_value = RexShowingFactory.make(
        id=showing_id,
        open_house_id=1,
        open_house_guid='open_house_guid'
    )
    mock_rex_listing.filter.return_value.first.return_value = RexListingFactory.make(id=1)

    query = get_query(showing_id=showing_id, visitor_id=visitor_id, answers='[]')
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })

    json = response.json
    assert not json.get('errors')
    data = json.get('data', {}).get('answerVisitorQuestions', {}).get('result')

    assert data == {'__typename': 'ValidationProblem', 'field': 'answersToQuestions', 'message': 'You did not provide answers'}


@mock.patch('rex_backend.wrappers.user.Agent.showing_query')
@mock.patch('rex_backend.models.rex_listings.RexListing.query')
@mock.patch('requests.post', return_value=SEND_ANSWERS_RESPONSE)
@mock.patch('requests.get')
def test_query(
    mock_requests_get,
    mock_requests_post,
    mock_rex_listing_query,
    mock_rex_showing,
    client,
    valid_jwt):
    showing_id = 1
    visitor_id = 1
    mock_rex_showing.return_value.first.return_value = RexShowingFactory.make(
        id=showing_id,
        open_house_id=1,
        open_house_guid='open_house_guid'
    )
    mock_rex_listing_query.filter.return_value.first.return_value = RexListingFactory.make(id=1)
    mock_requests_get.side_effect = [
        OPENHOUSELEADS_QUESTIONS_MOCK,
        OPENHOUSELEADS_QUESTIONS_MOCK,
        VISITOR_QUESTIONS_MOCK,
    ]

    query = get_query(showing_id=showing_id, visitor_id=visitor_id)
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })

    json = response.json
    assert not json.get('errors')
    result = json.get('data', {}).get('answerVisitorQuestions')

    del result['result']['questions'] # Remove question to easy compare
    assert result == {'result': {'__typename': 'AnswerVisitorQuestionsOk', 'success': True}}


def get_query(showing_id, visitor_id, answers='[{ questionId: "test", answers: ["ok", "not ok"] }, { questionId: "test1", answers: ["1", "2"] }]'):
    return f'''
    mutation {{
        answerVisitorQuestions(input: {{
            showingId: {showing_id}
            visitorId: {visitor_id}
            answersToQuestions: {answers}
        }}) {{
            result {{
                ... on NotFoundProblem {{
                    __typename
                    entity
                    message
                }}
                ... on ValidationProblem {{
                    __typename
                    field
                    message
                }}
                ... on AnswerVisitorQuestionsOk {{
                    __typename
                    success
                    questions {{
                        ... on MultipleQuestion {{
                            __typename
                            id
                        }}
                        ... on SingularQuestion {{
                            __typename
                            id
                        }}
                        ... on TextQuestion {{
                            __typename
                            id
                        }}
                    }}
                }}
                ... on AnswerVisitorQuestionsNotOk {{
                    __typename
                    message
                }}
            }}
        }}
    }}
    '''
