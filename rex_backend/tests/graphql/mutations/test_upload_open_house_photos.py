import io
import json

from unittest import mock

from rex_backend.tests.factories import RexShowingFactory, RexListingFactory


@mock.patch('rex_backend.wrappers.user.Agent.showing_query')
def test_where_no_open_house(mock_rex_showing, client, valid_jwt):
    mock_rex_showing.return_value.first.return_value = None

    file_to_upload = (io.BytesIO(b"content of file"), 'test.jpg')

    query = get_query(showing_id=1)
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post(
        '/graphql',
        data={
            'operations': json.dumps({
                'query': query,
                'variables': {'files': [None]},
            }),
            'map': json.dumps({'0': ['variables.files.0']}),
            '0': file_to_upload,
        },
        headers={
            'Content-Type': 'multipart/form-data',
            'Authorization': authorization,
        }
    )

    json_result = response.json
    assert not json_result.get('errors')

    data = json_result.get('data', {}).get('uploadOpenHousePhotos', {}).get('result')
    assert data == {'__typename': 'NotFoundProblem', 'entity': 'Open House', 'message': 'Open House not found.'}


@mock.patch('rex_backend.wrappers.user.Agent.showing_query')
def test_where_no_files(mock_rex_showing, client, valid_jwt):
    mock_rex_showing.return_value.first.return_value = RexShowingFactory.make(
        id=1,
        open_house_id=1,
        open_house_guid='guid',
    )

    query = get_query(showing_id=1)
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post(
        '/graphql',
        data={
            'operations': json.dumps({
                'query': query,
                'variables': {'files': []},
            }),
            'map': json.dumps({}),
        },
        headers={
            'Content-Type': 'multipart/form-data',
            'Authorization': authorization,
        }
    )

    json_result = response.json
    assert not json_result.get('errors')

    data = json_result.get('data', {}).get('uploadOpenHousePhotos', {}).get('result')
    assert data == {'__typename': 'ValidationProblem', 'field': 'files', 'message': 'Files should be present'}


@mock.patch('rex_backend.wrappers.user.Agent.showing_query')
@mock.patch('requests.post')
def test_mutation(mock_requests_post, mock_rex_showing, client, valid_jwt):
    mock_rex_showing.return_value.first.return_value = RexShowingFactory.make(
        id=1,
        open_house_id=1,
        open_house_guid='guid',
    )
    mock_response = mock.Mock()
    mock_response.status_code = 200
    mock_response.json.return_value = {"success": True}
    mock_requests_post.return_value = mock_response

    file_to_upload = (io.BytesIO(b"content of file"), 'test.jpg')

    query = get_query(showing_id=1)
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post(
        '/graphql',
        data={
            'operations': json.dumps({
                'query': query,
                'variables': {'files': [None]},
            }),
            'map': json.dumps({'0': ['variables.files.0']}),
            '0': file_to_upload,
        },
        headers={
            'Content-Type': 'multipart/form-data',
            'Authorization': authorization,
        }
    )

    json_result = response.json
    assert not json_result.get('errors')

    data = json_result.get('data', {}).get('uploadOpenHousePhotos', {}).get('result')
    assert data == {'__typename': 'Showing', 'id': '1'}


def get_query(showing_id):
    return f'''
    mutation ($files: [Upload]!) {{
        uploadOpenHousePhotos(input: {{
            showingId: {showing_id},
            files: $files
        }}) {{
            result {{
                ... on NotFoundProblem {{
                    __typename
                    entity
                    message
                }}
                ... on ValidationProblem {{
                    __typename
                    field
                    message
                }}
                ... on UploadOpenHousePhotosNotOk {{
                    __typename
                    message
                }}
                ... on Showing {{
                    __typename
                    id
                }}
            }}
        }}
    }}
    '''
