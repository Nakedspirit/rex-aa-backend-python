import random
import datetime


import pytest
from unittest import mock
from requests.models import Response


from rex_backend.tests.conftest import dict2obj, make_jwt_with_device, make_jwt_without_device
from rex_backend.application import db
from rex_backend.application import jwt_service
from rex_backend.models.base import GoogleIdentity, User, Device, LinkToRexUser


def test_mutation(client, user_and_identity_orm_obj):
    identity_id = user_and_identity_orm_obj['identity'].id
    valid_jwt = make_jwt_with_device(user_and_identity_orm_obj)
    authorization = "Bearer {}".format(valid_jwt)
    query = get_mutation()
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None

    result = response.json.get('data', {}).get('signOut', {}).get('result')
    assert result

    identity = GoogleIdentity.query.filter(GoogleIdentity.id == identity_id).first()
    assert identity
    assert identity.sign_out_at is not None


def test_mutation_where_jwt_without_device(client, user_and_identity_orm_obj):
    valid_jwt = make_jwt_without_device(user_and_identity_orm_obj)
    authorization = "Bearer {}".format(valid_jwt)
    query = get_mutation()
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors')
    assert response.json.get('errors')[0].get('message') == 'Invalid device'


def get_mutation():
    return "mutation { signOut(input: {}) { result } }"
