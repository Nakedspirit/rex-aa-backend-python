import json
import pytest
from unittest import mock


from rex_backend.models.rex_showings import RexUser, RexAssociatedAgent
from rex_backend.models.base import GoogleIdentity, User, Device, LinkToRexUser
from rex_backend.application import jwt_service
from rex_backend.tests.mocks.services.google_oauth_service import mock_google_oauth_success_response, mock_google_oauth_failed_response, mock_google_oauth_success_response_with_another_aud, mock_google_oauth_success_response_with_another_domain, mock_google_id_token_success_response


@pytest.fixture
def mock_agent():
    with mock.patch('rex_backend.interactors.sign_in_oauth.SignInOauthInteractor.load_rex_agent_by_email') as mock1:
        id = 4251
        mock1.return_value = RexAssociatedAgent(
            id=id,
            email='test@test.test',
            phone_number='210-884-3353',
            name='SVI Test JYS'
        )
        yield


@pytest.fixture
def mock_demo_agent():
    with mock.patch('rex_backend.interactors.sign_in_oauth.SignInOauthInteractor.load_demo_rex_agent_by_email') as mock1:
        id = 4251
        mock1.return_value = RexAssociatedAgent(
            id=id,
            email='test@test.test',
            phone_number='210-884-3353',
            name='SVI Test JYS'
        )
        yield


@pytest.fixture
def mock_none_agent():
    with mock.patch('rex_backend.interactors.sign_in_oauth.SignInOauthInteractor.load_rex_agent_by_email') as mock1:
        mock1.return_value = None
        yield


def test_mutation(client, oauth_input_params, mock_google_oauth_success_response, mock_agent, mock_get_whitelisted_emails_where_no_emails):
    device_id = 'device-id'
    with mock.patch('rex_backend.interactors.sign_in_oauth.SignInOauthInteractor.get_is_developer') as mock_dev:
        mock_dev.return_value = False
        mutation = get_mutation('google', oauth_input_params['oauth_user_id'], oauth_input_params['oauth_access_token'], device_id)
        response = client.post('/graphql', data = { 'query': mutation })
        result = response.json.get('data', {}).get('signInOauth', {}).get('result')

        assert response.status == '200 OK'
        assert result['__typename'] == 'SignInOk'
        assert result['jwt'] is not None

        assert result['isDeveloper'] == False
        assert result['disableMutations'] == True
        assert result['demoMode'] == False

        google_identity = GoogleIdentity.query.join(User).order_by(GoogleIdentity.id.desc()).first()
        assert google_identity.oauth_user_id == oauth_input_params.get('oauth_user_id')
        assert google_identity.sign_in_at is not None

        user = User.query.order_by(User.id.desc()).first()
        assert user
        assert user.contact_email == 'test@test.test'
        assert user == google_identity.user

        device = Device.query.filter(Device.user_id == user.id).first()
        assert device
        assert device.id == device_id


def test_mutation_with_existed_user(client, oauth_input_params, mock_google_oauth_success_response, mock_agent, mock_get_whitelisted_emails):
    oauth_user_id = oauth_input_params['oauth_user_id']
    device_id = 'device-id'
    with mock.patch('rex_backend.interactors.sign_in_oauth.SignInOauthInteractor.get_is_developer') as mock_dev:
        mock_dev.return_value = False
        mutation = get_mutation('google', oauth_user_id, oauth_input_params['oauth_access_token'], device_id)
        response = client.post('/graphql', data = { 'query': mutation })
        result = response.json.get('data', {}).get('signInOauth', {}).get('result')

        assert response.status == '200 OK'
        assert result['__typename'] == 'SignInOk'
        assert result['jwt'] is not None

        jwt = result['jwt']
        jwt_payload = jwt_service.decode(jwt)
        assert jwt_payload.get('disable_mutations') is None

        google_identity = GoogleIdentity.query.join(User).order_by(GoogleIdentity.id.desc()).first()
        assert google_identity.oauth_user_id == oauth_user_id
        assert google_identity.sign_in_at is not None

        user = User.query.order_by(User.id.desc()).first()
        assert user
        assert user.contact_email == 'test@test.test'
        assert user == google_identity.user

        device = Device.query.filter(Device.user_id == user.id).first()
        assert device
        assert device.id == device_id


def test_mutation_where_no_rex_agent(client, oauth_input_params, mock_google_oauth_success_response, mock_get_whitelisted_emails, mock_none_agent, mock_demo_agent):
    oauth_user_id = oauth_input_params['oauth_user_id']
    mutation = get_mutation('google', oauth_user_id, oauth_input_params['oauth_access_token'], 'device-id')
    response = client.post('/graphql', data={'query': mutation})
    result = response.json.get('data', {}).get('signInOauth', {}).get('result')

    assert response.status == '200 OK'
    assert result['__typename'] == 'SignInOk'
    assert result['jwt'] is not None

    jwt = result['jwt']
    jwt_payload = jwt_service.decode(jwt)
    assert jwt_payload.get('demo_mode') == True


def test_mutation_with_invalid_oauth_token(client, oauth_input_params, mock_google_oauth_failed_response):
    oauth_user_id = oauth_input_params['oauth_user_id']
    mutation = get_mutation('google', oauth_user_id, oauth_input_params['oauth_access_token'], 'device-id')
    response = client.post('/graphql', data = { 'query': mutation })
    result = response.json.get('data', {}).get('signInOauth', {}).get('result')

    assert response.status == '200 OK'
    assert result['__typename'] == 'ValidationProblem'
    assert result['field'] == 'google'
    assert result['message'] == 'Invalid OAuth token'


def test_mutation_where_email_not_in_whitelist(client, oauth_input_params, mock_get_whitelisted_emails_where_no_emails, mock_google_oauth_success_response, mock_agent):
    mutation = get_mutation('google', oauth_input_params['oauth_user_id'], oauth_input_params['oauth_access_token'], 'device-id')
    response = client.post('/graphql', data = { 'query': mutation })
    result = response.json.get('data', {}).get('signInOauth', {}).get('result')

    assert response.status == '200 OK'
    assert result['__typename'] == 'SignInOk'
    assert result['jwt'] is not None

    jwt = result['jwt']
    jwt_payload = jwt_service.decode(jwt)
    assert jwt_payload.get('disable_mutations') == True


def test_mutation_where_is_developer(client, oauth_input_params, mock_get_whitelisted_emails_where_no_emails, mock_agent, mock_google_oauth_success_response):
    oauth_user_id = oauth_input_params['oauth_user_id']
    with mock.patch('rex_backend.interactors.sign_in_oauth.SignInOauthInteractor.get_is_developer') as mock_dev:
        mock_dev.return_value = True
        mutation = get_mutation('google', oauth_user_id, oauth_input_params['oauth_access_token'], 'device-id')
        response = client.post('/graphql', data = { 'query': mutation })
        result = response.json.get('data', {}).get('signInOauth', {}).get('result')

        assert response.status == '200 OK'
        assert result['__typename'] == 'SignInOk'
        assert result['jwt'] is not None

        jwt = result['jwt']
        jwt_payload = jwt_service.decode(jwt)
        assert jwt_payload.get('disable_mutations') == True

        assert result['isDeveloper'] == True


def test_mutation_where_app_is_invalid(client, oauth_input_params, mock_google_oauth_success_response_with_another_aud, mock_get_whitelisted_emails_where_no_emails):
    oauth_user_id = oauth_input_params['oauth_user_id']
    mutation = get_mutation('google', oauth_user_id, oauth_input_params['oauth_access_token'], 'device-id')
    response = client.post('/graphql', data = { 'query': mutation })
    result = response.json.get('data', {}).get('signInOauth', {}).get('result')

    assert response.status == '200 OK'
    assert result['__typename'] == 'ValidationProblem'
    assert result['field'] == 'google'
    assert result['message'] == 'Invalid OAuth token'


def test_mutation_where_different_domain(client, oauth_input_params, mock_google_oauth_success_response_with_another_domain, mock_get_whitelisted_emails_where_no_emails):
    oauth_user_id = oauth_input_params['oauth_user_id']
    mutation = get_mutation('google', oauth_user_id, oauth_input_params['oauth_access_token'], 'device-id')
    response = client.post('/graphql', data = { 'query': mutation })
    result = response.json.get('data', {}).get('signInOauth', {}).get('result')

    assert response.status == '200 OK'
    assert result['__typename'] == 'ValidationProblem'
    assert result['field'] == 'google'
    assert result['message'] == 'Invalid OAuth token'


def test_mutation_with_id_token(client, oauth_input_params, mock_google_id_token_success_response, mock_get_whitelisted_emails, mock_agent):
    oauth_user_id = oauth_input_params['oauth_user_id']
    oauth_input_params['oauth_id_token'] = 'my-token-id'
    with mock.patch('rex_backend.interactors.sign_in_oauth.SignInOauthInteractor.get_is_developer') as mock_dev:
        mock_dev.return_value = False

        mutation = get_mutation_with_id_token('google', oauth_user_id, oauth_input_params['oauth_id_token'], 'device-id')
        response = client.post('/graphql', data = { 'query': mutation })
        result = response.json.get('data', {}).get('signInOauth', {}).get('result')

        assert response.status == '200 OK'
        assert result['__typename'] == 'SignInOk'
        assert result['jwt'] is not None



def get_mutation(provider, user_id, access_token, device_id):
     return """mutation {{
  signInOauth(input:{{
    oauthProvider: {provider}
    oauthUserId: "{user_id}"
    oauthAccessToken: "{access_token}"
    deviceId: "{device_id}"
  }}) {{
    result {{
      ... on SignInOk {{
        __typename
        jwt
        isDeveloper
        disableMutations
        demoMode
      }}
      ... on ValidationProblem {{
        __typename
        field
        message
      }}
    }}
  }}
}}
""".format(provider=provider, user_id=user_id, access_token=access_token, device_id=device_id)


def get_mutation_with_id_token(provider, user_id, id_token, device_id):
     return """mutation {{
  signInOauth(input:{{
    oauthProvider: {provider}
    oauthUserId: "{user_id}"
    oauthAccessToken: "test"
    oauthIdToken: "{id_token}"
    deviceId: "{device_id}"
  }}) {{
    result {{
      ... on SignInOk {{
        __typename
        jwt
        isDeveloper
        disableMutations
        demoMode
      }}
      ... on ValidationProblem {{
        __typename
        field
        message
      }}
    }}
  }}
}}
""".format(provider=provider, user_id=user_id, id_token=id_token, device_id=device_id)
