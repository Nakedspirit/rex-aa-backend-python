from rex_backend.tests.helpers import graphql_request


def test_mutation_hide_showing(client, valid_jwt):
    response = graphql_request(client, valid_jwt, get_mutation(1))
    assert not response.get('errors')

    result = response.get('data', {}).get('hideShowing')
    assert result.get('result') is True


def test_mutation_hide_showing_twice(client, valid_jwt):
    graphql_request(client, valid_jwt, get_mutation(1))
    response = graphql_request(client, valid_jwt, get_mutation(1))
    assert not response.get('errors')

    result = response.get('data', {}).get('hideShowing')
    assert result.get('result') is False


def get_mutation(showing_id):
    return f'''mutation Test{{
    hideShowing(input: {{
        showingId: {showing_id}
    }}) {{
        result
    }}
    }}
    '''