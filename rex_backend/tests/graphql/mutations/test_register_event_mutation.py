import random
import datetime


import pytest
from unittest import mock
from requests.models import Response
from rex_backend.tests.conftest import dict2obj
from rex_backend.application import db
from rex_backend.models.base import Device, UserStat


@pytest.fixture
def mock_completed_showing():
    with mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_rex_showing') as mock1:
        mock1.return_value = dict2obj({
            'id': 695,
            'status': 'Completed',
            'data': {"buyer_id": "", "outside_agent_license_number": "", "streetAddress": "15 Santa Isabel", "city": "Rancho Santa Margarita", "state": "CA", "agentsShowingInstructions": "[Lock Box Code] 9774.", "rexUrl": "15-santa-isabel", "listingId": "939", "expiresByInHours": "2", "isArchived": "False", "buyer": "John Doe <+18553424739>", "css": "John YS <jys@rexhomes.com>", "hasBuyerAgent": "False", "createdAt": "04/01/19 10:06 AM", "status": "Pending"},
            'final_meeting_time': datetime.datetime(2019, 4, 1, 12, 0, 0),
            'timezone': 'UTC',
            'created_at': datetime.datetime(2019, 4, 1, 12, 0, 0),
            'claimed_at': datetime.datetime(2019, 4, 1, 12, 0, 0),
            'claimed_by_id': 555
        })
        yield


def test_mutation_with_invalid_secret(client):
    authorization = "Bearer {}".format('invalid-secret-for-register-event-endpoint')
    query = get_mutation('SHOWING_CLAIMED')
    response = client.post('/graphql', data={'query': query}, headers={'Authorization': authorization})
    assert not response.json.get('data', {}).get('registerEvent')
    assert response.json.get('errors')[0].get('message') == 'JWT payload is invalid'


def get_mutation(event_name):
    return '''mutation Test{{
    registerEvent(input: {{
        eventName: {}
        objectId: {}
    }}) {{
        result
    }}
    }}
    '''.format(event_name, 18)
