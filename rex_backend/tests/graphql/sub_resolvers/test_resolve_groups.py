from unittest import mock

from rex_backend.models.base import ReportGroup, ReportQuestion
from rex_backend.graphql.sub_resolvers.resolve_groups import resolve_groups
from rex_backend.tests.factories import ShowingReportFactory, RexShowingFactory
from rex_backend.tests.mocks.apis.rex_questions import QUESTIONS_SURVEY_MOCK, OPEN_HOUSE_QUESTIONS_MOCK
from requests.exceptions import ConnectTimeout, RetryError
from rex_backend.apis import HttpAPIException
from rex_backend.graphql.adapters import to_graphql_showing_report, to_graphql_showing


@mock.patch('requests.get', return_value=QUESTIONS_SURVEY_MOCK)
def test_resolve_groups(mock_requests_get, session):
    showing_report = ShowingReportFactory.create()
    session.commit()

    graphql_showing_report = to_graphql_showing_report(showing_report)
    graphql_showing_report.showing = to_graphql_showing(RexShowingFactory.make(id=1))

    result = resolve_groups(graphql_showing_report, {})

    report_group = ReportGroup.query.filter(ReportGroup.title == 'Showing Feedback').first()
    assert report_group

    report_question = ReportQuestion.query.filter(ReportQuestion.rex_question_guid == '9c9501e8e7d54241bd8cf1caff9369e0').first()
    assert report_question.title == 'Any features in the home that need updating?'
    assert report_question.data.get('original_data') == {'name': 'ps_features_updating', 'title': 'Any features in the home that need updating?', 'type': 'checkbox', 'valueName': '9c9501e8e7d54241bd8cf1caff9369e0', 'isRequired': True, 'choices': [{'value': 'Flooring', 'text': 'Flooring'}, {'value': 'Appliances', 'text': 'Appliances'}, {'value': 'Kitchen', 'text': 'Kitchen'}, {'value': 'Bathrooms', 'text': 'Bathrooms'}, {'value': 'Landscaping', 'text': 'Landscaping'}, {'value': 'N/A', 'text': 'N/A'}, {'value': 'Other', 'text': 'Other'}]}


@mock.patch('requests.get', return_value=OPEN_HOUSE_QUESTIONS_MOCK)
def test_resolve_groups_for_open_house(mock_requests_get, session):
    showing_report = ShowingReportFactory.create()
    session.commit()

    graphql_showing_report = to_graphql_showing_report(showing_report)
    graphql_showing_report.showing = to_graphql_showing(RexShowingFactory.make(id=1, open_house_id=1))

    result = resolve_groups(graphql_showing_report, {})

    report_group = ReportGroup.query.filter(ReportGroup.title == 'title#1').first()
    assert report_group

    report_question = ReportQuestion.query.filter(ReportQuestion.rex_question_guid == '2364d5c293bc4eb5b69c0ca23947ec0c').first()
    assert report_question.title == 'Number of Attendees'
    assert report_question.data.get('original_data') == {'name': 'oh_number_attendees', 'title': 'Number of Attendees', 'valueName': '2364d5c293bc4eb5b69c0ca23947ec0c', 'type': 'text', 'validators': [{'type': 'numeric'}]}


@mock.patch('requests.get', side_effect=ConnectTimeout('test with ConnectTimeout exc'))
def test_resolve_groups_when_connect_timeout(mock_requests_get, session):
    showing_report = ShowingReportFactory.create()
    session.commit()

    graphql_showing_report = to_graphql_showing_report(showing_report)
    graphql_showing_report.showing = to_graphql_showing(RexShowingFactory.make(id=1))

    try:
        result = resolve_groups(graphql_showing_report, {})
        assert false, 'HttpAPIException should be thrown'
    except Exception as exc:
        assert isinstance(exc, HttpAPIException)
        assert str(exc) == 'REX service is not responding. Please return to the previous screen and try again.'


@mock.patch('requests.get', side_effect=RetryError('test with RetryError exc'))
def test_resolve_groups_when_retry_error(mock_requests_get, session):
    showing_report = ShowingReportFactory.create()
    session.commit()

    graphql_showing_report = to_graphql_showing_report(showing_report)
    graphql_showing_report.showing = to_graphql_showing(RexShowingFactory.make(id=1))

    try:
        result = resolve_groups(graphql_showing_report, {})
        assert false, 'HttpAPIException should be thrown'
    except Exception as exc:
        assert isinstance(exc, HttpAPIException)
        assert str(exc) == 'REX service is not responding. Please return to the previous screen and try again.'