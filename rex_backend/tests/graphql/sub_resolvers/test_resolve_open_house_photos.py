from unittest import mock

from rex_backend.graphql.sub_resolvers.open_house_photos import resolve_open_house_photos
from rex_backend.tests.factories import RexShowingFactory
from rex_backend.graphql.adapters import to_graphql_showing


@mock.patch('requests.get')
def test_resolve_open_house_photos(mock_requests_get, session):
    mock_response = mock.Mock()
    mock_response.status_code = 200
    mock_response.json.return_value = { "success": True, "links": [ "https://cdn.rexhomes.com/svi/qa/b5bef4d363394247b2ea0a1fba2ebde9/0.jpg"]}
    mock_requests_get.return_value = mock_response

    graphql_showing = to_graphql_showing(RexShowingFactory.make(
        id=1,
        open_house_id=1,
        open_house_guid='b5bef4d363394247b2ea0a1fba2ebde9',
    ))

    links = resolve_open_house_photos(graphql_showing, {})
    assert links, 'Should return array with links'


@mock.patch('requests.get')
def test_resolve_open_house_photos_where_API_returns_500(mock_requests_get, session):
    mock_response = mock.Mock()
    mock_response.status_code = 500
    mock_response.body = 'Internal Server Error'
    mock_requests_get.return_value = mock_response

    graphql_showing = to_graphql_showing(RexShowingFactory.make(
        id=1,
        open_house_id=1,
        open_house_guid='b5bef4d363394247b2ea0a1fba2ebde9',
    ))

    links = resolve_open_house_photos(graphql_showing, {})
    assert links == [], 'Should return empty links'