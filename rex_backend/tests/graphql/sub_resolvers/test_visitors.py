from unittest import mock

from rex_backend.tests.helpers import graphql_request
from rex_backend.tests.factories import RexShowingFactory, RexOpenHouseFactory
from rex_backend.apis.rex_paperless_signin import rex_paperless_signin_api


@mock.patch('rex_backend.graphql.resolvers.showing.load_showing')
@mock.patch('rex_backend.models.rex_showings.RexOpenHouse.query')
@mock.patch.object(rex_paperless_signin_api, 'call_get_visitors') 
def test_resolve_visitors(mock_call_get_visitors, mock_rex_open_house_query, mock_rex_showing, client, valid_jwt):
    mock_rex_showing.return_value = RexShowingFactory.make(
        id=1,
        open_house_id=1,
    )
    mock_rex_open_house_query.filter.first.return_value = RexOpenHouseFactory.build(
        showing_id=1,
        guid='8732cafacaff444198e36bf883d4ae8d'
    )
    request_mock = mock.Mock()
    request_mock.status_code = 200
    request_mock.json.return_value = {
        "success": True,
        "signins": [
            {
                "id": 1,
                "email": "kzvonov+1@rexhomes.com",
                "name": "Kirill Zvonov",
                "phone_number": "+15555555555",
                "created": "Wed, 30 Oct 2019 17:41:58 GMT",
            }
        ]
    }
    mock_call_get_visitors.return_value = request_mock

    query = get_query(1)
    response = graphql_request(client, valid_jwt, query)
    
    assert response.get('errors') is None
    assert response.get('data', {}).get('showing', {}).get('visitors')

    visitor = response.get('data', {}).get('showing', {}).get('visitors')[0]
    assert visitor['visitedAt'] == '2019-10-30T17:41:58'


@mock.patch('rex_backend.graphql.resolvers.showing.load_showing')
def test_resolve_visitors_for_regular_showing(mock_rex_showing, client, valid_jwt):
    mock_rex_showing.return_value = RexShowingFactory.make(
        id=1,
    )

    query = get_query(1)
    response = graphql_request(client, valid_jwt, query)

    assert response.get('errors')


@mock.patch('rex_backend.graphql.resolvers.showing.load_showing')
@mock.patch('rex_backend.models.rex_showings.RexOpenHouse.query')
def test_resolve_visitors_without_open_house(mock_rex_open_house_query, mock_rex_showing, client, valid_jwt):
    mock_rex_showing.return_value = RexShowingFactory.make(
        id=1,
    )
    mock_rex_open_house_query.filter.first.return_value = None

    query = get_query(1)
    response = graphql_request(client, valid_jwt, query)

    assert response.get('errors')


@mock.patch('rex_backend.graphql.resolvers.showing.load_showing')
@mock.patch('rex_backend.models.rex_showings.RexOpenHouse.query')
@mock.patch.object(rex_paperless_signin_api, 'call_get_visitors') 
def test_resolve_visitors_with_500_rex_response(mock_call_get_visitors, mock_rex_open_house_query, mock_rex_showing, client, valid_jwt):
    mock_rex_showing.return_value = RexShowingFactory.make(
        id=1,
        open_house_id=1,
    )
    mock_rex_open_house_query.filter.first.return_value = RexOpenHouseFactory.build(
        showing_id=1,
        guid='8732cafacaff444198e36bf883d4ae8d'
    )
    request_mock = mock.Mock()
    request_mock.status_code = 500
    request_mock.text = 'error'

    query = get_query(1)
    response = graphql_request(client, valid_jwt, query)
    
    assert response.get('errors')


def get_query(id):
    return f'''
    query {{
        showing(id: {id}) {{
            id
            visitors {{
                id
                name
                email
                phoneNumber
                visitedAt
            }}
        }}
    }}
    '''