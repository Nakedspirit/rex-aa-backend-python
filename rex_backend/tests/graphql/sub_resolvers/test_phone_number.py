from unittest import mock

from rex_backend.graphql.sub_resolvers.phone_number import resolve_phone_number
from rex_backend.tests.factories import RexShowingFactory, RexListingFactory
from rex_backend.graphql.adapters import to_graphql_showing, to_graphql_listing


@mock.patch('requests.get')
def test_resolve_phone_number(mock_requests_get, session):
    mock_response = mock.Mock()
    mock_response.status_code = 200
    mock_response.json.return_value = { "phone_number": "+18553152155", "success": True}
    mock_requests_get.return_value = mock_response


    graphql_showing = to_graphql_showing(RexShowingFactory.make(
        id=1,
        open_house_id=1,
    ))

    phone_number = resolve_phone_number(graphql_showing, {})

    assert phone_number, 'Should return phone number for Open House'


def test_resolve_phone_number_for_regular_showing(session):
    graphql_showing = to_graphql_showing(RexShowingFactory.make(id=1))

    phone_number = resolve_phone_number(graphql_showing, {})

    assert phone_number is None, 'Should not return phone number for Showing'