import random
import datetime

from unittest import mock

from rex_backend.models.rex_listings import RexListing, RexAddress, RexListingImage, RexFileInfo
from rex_backend.tests.helpers import dict2obj


FAKE_REX_LISTING = RexListing(
    id = 939,
    guid='711124500d3c447f975490298570600f',
    air_conditioning_type = 'CENTRAL',
    fireplace_indicator_flag = None,
    heating_type = 'FORCED_AIR',
    hoa_fee = 113.0,
    property_type = 'SINGLE',
    land_square_footage = 6110,
    living_square_feet = 1890,
    total_baths = 3.0,
    bedrooms = 3,
    parking_spaces = 3,
    parking_type_code = 'Garage',
    pool_flag = False,
    stories = 2.0,
    year_built = 1986,
    price = 780500,
)

FAKE_REX_ADDRESS = RexAddress(
    id = 795,
    city = 'Rancho John Doe',
    address_line = '42 unknown st',
    state = 'CA'
)


@mock.patch('rex_backend.graphql.resolvers.listing.load_listing', return_value=FAKE_REX_LISTING)
@mock.patch('rex_backend.models.rex_listings.RexAddress.query')
@mock.patch('rex_backend.models.rex_listings.RexListingImage.query')
def test_query(mock_rex_listing_image_query, mock_rex_address_query, mock_rex_listing, client, valid_jwt):
    mock_rex_listing_image_query.with_entities.return_value.join.return_value.filter.return_value.all.return_value = [
        dict2obj({'listing_id': FAKE_REX_LISTING.id, 'public_url': 'https://cdn.houseplansservices.com/product/p4pvnkmoahoo30m48busgglrug/w560x373.jpg?v=9'}),
        dict2obj({'listing_id': FAKE_REX_LISTING.id, 'public_url': 'https://cdn.houseplansservices.com/product/p4pvnkmoahoo30m48busgglrug/w560x373.jpg?v=9'})
    ]
    mock_rex_address_query.with_entities.return_value.join.return_value.filter.return_value.all.return_value = [
        (FAKE_REX_ADDRESS, FAKE_REX_LISTING.id)
    ]

    query = get_query(FAKE_REX_LISTING.id)
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })

    assert response.json.get('errors') is None
    returned_listing = response.json.get('data', {}).get('listing')
    assert not returned_listing is None
    assert returned_listing['address'] == '42 unknown st, Rancho John Doe, CA'
    assert len(returned_listing['photos']) == 2


@mock.patch('rex_backend.graphql.resolvers.listing.load_listing', return_value=None)
def test_query_without_listing(mock_rex_listing, client, valid_jwt):
    query = get_query(939)
    authorization = "Bearer {}".format(valid_jwt)

    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None
    returned_listing = response.json.get('data', {}).get('listing')
    assert returned_listing is None


def get_query(id):
    return '''{{
      listing(id: {}) {{
        id
        address
        totalPrice
        pricePerSquareFoot
        description
        numberOfBeds
        numberOfBaths
        squareFoot
        lotSquareFoot
        houseType
        stories
        yearBuilt
        hoafee
        parkingSpaces
        parkingType
        heating
        ac
        pool
        fireplace
        photos
        latitude
        longitude
      }}
    }}
    '''.format(id)
