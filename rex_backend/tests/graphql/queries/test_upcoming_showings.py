import datetime

from unittest import mock

from rex_backend.tests.factories.rex_showing_factory import RexShowingFactory


@mock.patch('rex_backend.graphql.resolvers.upcoming_showings.apply_pagination_and_load', return_value=[])
@mock.patch('rex_backend.graphql.data_loaders.SlotsForShowingLoader.load_time_slots', return_value=[])
@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_showing_users', return_value=[])
def test_query_without_showings(mock_showing_users, mock_showing_time_slots, mock_showings, client, valid_jwt):
    query = get_query()
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })

    assert response.status_code == 200
    assert response.json.get('errors') is None
    assert not response.json.get('data', {}).get('upcomingShowings') is None


@mock.patch('rex_backend.graphql.resolvers.upcoming_showings.apply_pagination_and_load')
@mock.patch('rex_backend.graphql.data_loaders.SlotsForShowingLoader.load_time_slots', return_value=[])
@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_showing_users', return_value=[])
def test_query(mock_showing_users, mock_showing_time_slots, mock_showings, client, current_user, valid_jwt):
    mock_showings.return_value = [
        RexShowingFactory.make(
            id=1,
            status='Pending',
            claimed_at=datetime.datetime.now(),
            claimed_by_id=current_user.id
        )
    ]

    query = get_query()
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.status_code == 200
    assert response.json.get('errors') is None

    edges = response.json.get('data', {}).get('upcomingShowings', {}).get('edges')
    assert edges

    showing = edges[0]['node']
    assert showing
    assert int(showing.get('id')) == 1
    assert showing.get('status') == 'PENDING'


@mock.patch('rex_backend.models.base.User.rex_users_ids', new_callable=mock.PropertyMock)
def test_query_where_user_has_no_rex_users_ids(mock_rex_users_ids, client, valid_jwt):
    mock_rex_users_ids.return_value = []

    query = get_query()
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.status_code == 200
    assert response.json.get('errors') is None

    edges = response.json.get('data', {}).get('upcomingShowings', {}).get('edges')
    assert len(edges) == 0


@mock.patch('rex_backend.graphql.resolvers.upcoming_showings.apply_pagination_and_load', return_value=[])
def test_query_for_demo_user(mock_showings, client, valid_jwt_for_demo_user):
    query = get_query()
    authorization = f'Bearer {valid_jwt_for_demo_user}'
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.status_code == 200
    assert response.json.get('errors') is None

    edges = response.json.get('data', {}).get('upcomingShowings', {}).get('edges')
    assert len(edges) == 0


def get_query():
    return '''{
        upcomingShowings {
          edges {
            cursor
            node {
              id
              type
              status
              slots {
                id
                time
              }
              buyer {
                firstName
              }
              seller {
                firstName
              }
              showingReport {
                id
              }
              showingInstructions
              needSellerAvailable
              needRexShowingRepresentative
              isOutsideAgent
              createdAt
              claimedAt
              finalShowingTime
              submittedForPaymentAt
            }
          }
          pageInfo {
            endCursor
            hasNextPage
            hasPreviousPage
            startCursor
          }
        }
      }
    '''
