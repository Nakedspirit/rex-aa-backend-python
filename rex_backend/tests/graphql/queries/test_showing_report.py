import random
import datetime
import random

import pytest
from unittest import mock


from rex_backend.tests.conftest import dict2obj
from rex_backend.models.base import ShowingReportStatus, User
from rex_backend.application import db
from rex_backend.utils.tz import tznow
from rex_backend.tests.mocks.query_upcoming_showings import fake_time_slots
from rex_backend.tests.factories import ShowingReportFactory, RexShowingFactory
from rex_backend.tests.mocks.apis.rex_questions import questions_survey, empty_questions_survey
from rex_backend.models.base import ReportGroup, ReportGroupStatus


FAKE_REX_SHOWING = dict2obj({
    'id': '695',
    'status': 'Completed',
    'data': {"buyer_id": "", "listingId": 13805, "outside_agent_license_number": "", "streetAddress": "15 Santa Isabel", "city": "Rancho Santa Margarita", "state": "CA", "agentsShowingInstructions": "[Lock Box Code] 9774.", "rexUrl": "15-santa-isabel", "listingId": "939", "expiresByInHours": "2", "isArchived": "False", "buyer": "John Doe <+18553424739>", "css": "John YS <jys@rexhomes.com>", "hasBuyerAgent": "False", "createdAt": "04/01/19 10:06 AM", "status": "Pending"},
    'final_meeting_time': datetime.datetime(2019, 4, 1, 12, 0, 0),
    'timezone': 'UTC',
    'created_at': datetime.datetime(2019, 4, 1, 12, 0, 0),
    'claimed_at': datetime.datetime(2019, 4, 1, 12, 0, 0),
})


@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.rex_showing_query')
@mock.patch('rex_backend.graphql.data_loaders.SlotsForShowingLoader.load_time_slots', return_value=fake_time_slots())
@mock.patch('requests.get', return_value=questions_survey())
@mock.patch('rex_backend.models.base.ShowingReport.rex_showing', new_callable=mock.PropertyMock)
def test_ready_for_payment_showing_report(mock_rex_showing, mock_api_respose, mock_time_slots, mock_showing, client, current_user, valid_jwt):
    mock_rex_showing.return_value.filter.return_value = FAKE_REX_SHOWING
    report = ShowingReportFactory.create(
        ready_for_payment=True,
        rex_showing_id=FAKE_REX_SHOWING.id,
        user=current_user
    )
    db.session.commit()
    mock_rex_showing.return_value = RexShowingFactory.make(id=1)

    query = get_query(FAKE_REX_SHOWING.id)
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None

    result = response.json.get('data', {}).get('showingReport')
    assert not result is None
    assert result['id']
    assert result['isReadyForPayment']
    assert result.get('groups')
    assert result.get('groups')[0].get('isCompleted')
    assert result.get('groups')[0].get('questions')[1]

    created_groups_num = ReportGroup.query.filter(ReportGroup.showing_report_id == report.id) \
                                          .filter(ReportGroup.status == ReportGroupStatus.completed) \
                                          .count()
    assert created_groups_num == 1


@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.rex_showing_query')
@mock.patch('rex_backend.graphql.data_loaders.SlotsForShowingLoader.load_time_slots', return_value=fake_time_slots())
@mock.patch('requests.get', return_value=empty_questions_survey())
@mock.patch('rex_backend.models.base.ShowingReport.rex_showing', new_callable=mock.PropertyMock)
def test_with_empty_questions(mock_rex_showing, mock_none_questions, mock_time_slots, mock_showing, client, current_user, valid_jwt):
    mock_rex_showing.return_value.filter.return_value = FAKE_REX_SHOWING
    report = ShowingReportFactory.create(
        ready_for_payment=True, 
        rex_showing_id=FAKE_REX_SHOWING.id,
        user=current_user
    )
    db.session.commit()
    mock_rex_showing.return_value = RexShowingFactory.make(id=1)

    query = get_query(FAKE_REX_SHOWING.id)
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None

    result = response.json.get('data', {}).get('showingReport')
    assert not result is None
    assert result['id']
    assert result['isReadyForPayment']
    assert not result.get('groups')


@mock.patch('rex_backend.graphql.data_loaders.SlotsForShowingLoader.load_time_slots', return_value=[])
@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.rex_showing_query')
def test_none_showing_report(mock_rex_showing, mock_time_slots, client, valid_jwt):
    mock_rex_showing.return_value.filter.return_value = None

    query = get_query(FAKE_REX_SHOWING.id)
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None

    result = response.json.get('data', {}).get('showingReport')
    assert result is None


@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.rex_showing_query')
def test_query_where_no_rex_showing(mock_rex_showing, client, valid_jwt):
    mock_rex_showing.return_value.filter.return_value = None

    query = get_query(FAKE_REX_SHOWING.id)
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None

    result = response.json.get('data', {}).get('showingReport')
    assert result is None


def get_query(id):
    return '''{{
      showingReport(showingId: {}) {{
        id
        status
        statusComment
        isReadyForPayment
        groups {{
            id
            title
            isCompleted
            questions {{
                ... on MultipleQuestion {{
                    __typename
                    id
                    title
                    isRequired
                    options {{
                        text
                        value
                        selected
                    }}
                }}
                ... on SingularQuestion {{
                    __typename
                    id
                    title
                    isRequired
                    options {{
                        text
                        value
                        selected
                    }}
                }}
                ... on TextQuestion {{
                    __typename
                    id
                    title
                    placeholder
                    response
                    isRequired
                }}
            }}
        }}
        showing {{
            id
            status
            slots {{
                id
                time
            }}
            showingInstructions
            needSellerAvailable
            needRexShowingRepresentative
            isOutsideAgent
            createdAt
            claimedAt
            finalShowingTime
            submittedForPaymentAt
        }}
      }}
    }}
    '''.format(id)
