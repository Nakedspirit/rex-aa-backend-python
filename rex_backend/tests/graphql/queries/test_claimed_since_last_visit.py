import random
import datetime


import pytest
from unittest import mock


from rex_backend.tests.conftest import dict2obj, make_jwt_with_device
from rex_backend.application import db
from rex_backend.application import jwt_service
from rex_backend.models.base import GoogleIdentity, User, Device


@pytest.fixture
def mock_already_claimed_count():
    with mock.patch('rex_backend.graphql.resolvers.claimed_since_last_visit.already_claimed_count') as mock1:
        mock1.return_value = 1
        yield


def test_query(client, user_and_identity_orm_obj, mock_already_claimed_count):
    device_id = user_and_identity_orm_obj['device'].id
    valid_jwt = make_jwt_with_device(user_and_identity_orm_obj)
    authorization = "Bearer {}".format(valid_jwt)

    query = "mutation { closeApp(input: {}) { result } }"
    client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })

    query = "query { claimedSinceLastVisit { lastVisitAt, amountOfClaimed } }"
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })

    assert response.json.get('errors') is None

    last_visit_at = response.json.get('data', {}).get('claimedSinceLastVisit', {}).get('lastVisitAt', None)

    assert last_visit_at

    last_visit_at = datetime.datetime.strptime(last_visit_at, '%Y-%m-%dT%H:%M:%S.%f%z')
    closed_at = Device.query.filter(Device.id == device_id).first().closed_at
    assert closed_at == last_visit_at


@mock.patch('rex_backend.models.base.User.rex_users_ids', new_callable=mock.PropertyMock)
def test_query_where_current_user_has_not_rex_users_ids(mock_rex_users_ids, client, current_user, valid_jwt):
    mock_rex_users_ids.return_value = []
    
    authorization = "Bearer {}".format(valid_jwt)
    query = "mutation { closeApp(input: {}) { result } }"
    client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })

    query = "query { claimedSinceLastVisit { lastVisitAt, amountOfClaimed } }"
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None

    last_visit_at = response.json.get('data', {}).get('claimedSinceLastVisit', {}).get('lastVisitAt', None)
    assert last_visit_at

    amount_of_claimed = response.json.get('data', {}).get('claimedSinceLastVisit', {}).get('amountOfClaimed', 0)
    assert amount_of_claimed == 0


@mock.patch('rex_backend.models.base.User.rex_users_ids', new_callable=mock.PropertyMock)
def test_query_where_current_user_rex_users_ids_is_none(mock_rex_users_ids, client, current_user, valid_jwt):
    mock_rex_users_ids.return_value = None
    
    authorization = "Bearer {}".format(valid_jwt)
    query = "mutation { closeApp(input: {}) { result } }"
    client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })

    query = "query { claimedSinceLastVisit { lastVisitAt, amountOfClaimed } }"
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None

    last_visit_at = response.json.get('data', {}).get('claimedSinceLastVisit', {}).get('lastVisitAt', None)
    assert last_visit_at

    amount_of_claimed = response.json.get('data', {}).get('claimedSinceLastVisit', {}).get('amountOfClaimed', 0)
    assert amount_of_claimed == 0

