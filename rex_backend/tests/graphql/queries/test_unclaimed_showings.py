import datetime

from unittest import mock

from rex_backend.tests.factories import UserActionFactory, RexShowingFactory, RexUserFactory, RexShowingTimeSlotFactory


@mock.patch('rex_backend.graphql.resolvers.unclaimed_showings.apply_pagination_and_load')
@mock.patch('rex_backend.graphql.data_loaders.SlotsForShowingLoader.load_time_slots', return_value=[])
@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_showing_users', return_value=[])
def test_query(showing_users_mock, showing_time_slots_mock, unclaimed_showings_mock, client, valid_jwt):
    unclaimed_showings_mock.return_value = [
        RexShowingFactory.make(id=1, status='Pending', claimed_at=None, claimed_by_id=None)
    ]

    query = get_query()
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.status_code == 200
    assert response.json.get('errors') is None
    assert response.json.get('data', {}).get('unclaimedShowings') is not None

    showing = response.json.get('data', {}).get('unclaimedShowings', {}).get('edges')[0].get('node')
    assert int(showing.get('id')) == 1
    assert showing.get('status') == 'INITIALIZED'


@mock.patch('rex_backend.graphql.resolvers.unclaimed_showings.apply_pagination_and_load')
@mock.patch('rex_backend.graphql.data_loaders.SlotsForShowingLoader.load_time_slots', return_value=[])
@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_showing_users', return_value=[])
def test_query_with_claimed_rex_showing(showing_users_mock, showing_time_slots_mock, unclaimed_showings_mock, client, current_user, valid_jwt):
    unclaimed_showings_mock.return_value = [
        RexShowingFactory.make(
            id=1,
            status='Pending',
            claimed_at=datetime.datetime.now(),
            claimed_by_id=current_user.rex_users_ids[0]
        )
    ]

    query = get_query()
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.status_code == 200
    assert response.json.get('errors') is None
    assert response.json.get('data', {}).get('unclaimedShowings') is not None

    showing = response.json.get('data', {}).get('unclaimedShowings', {}).get('edges')[0].get('node')
    assert int(showing.get('id')) == 1
    assert showing.get('status') == 'PENDING'


@mock.patch('rex_backend.graphql.resolvers.unclaimed_showings.apply_pagination_and_load')
@mock.patch('rex_backend.graphql.data_loaders.SlotsForShowingLoader.load_time_slots', return_value=[])
@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_showing_users', return_value=[])
def test_query_with_already_claimed_rex_showing(showing_users_mock, showing_time_slots_mock, unclaimed_showings_mock, client, current_user, valid_jwt):
    unclaimed_showings_mock.return_value = [
        RexShowingFactory.make(
            id=1,
            status='Pending',
            claimed_at=datetime.datetime.now(),
            claimed_by_id=666
        )
    ]

    query = get_query()
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.status_code == 200
    assert response.json.get('errors') is None
    assert response.json.get('data', {}).get('unclaimedShowings') is not None

    showing = response.json.get('data', {}).get('unclaimedShowings', {}).get('edges')[0].get('node')
    assert int(showing.get('id')) == 1
    assert showing.get('status') == 'ALREADY_CLAIMED'


@mock.patch('rex_backend.graphql.resolvers.unclaimed_showings.apply_pagination_and_load')
@mock.patch('rex_backend.graphql.data_loaders.SlotsForShowingLoader.load_time_slots', return_value=[])
@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_showing_users', return_value=[])
def test_query_with_old_already_claimed_rex_showing(showing_users_mock, showing_time_slots_mock, unclaimed_showings_mock, client, current_user, valid_jwt):
    unclaimed_showings_mock.return_value = [
        RexShowingFactory.make(
            id=1,
            status='Pending',
            claimed_at=datetime.datetime.now() - datetime.timedelta(days=3),
            claimed_by_id=666
        )
    ]

    query = get_query()
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.status_code == 200
    assert response.json.get('errors') is None
    assert response.json.get('data', {}).get('unclaimedShowings') is not None

    edges = response.json.get('data', {}).get('unclaimedShowings', {}).get('edges')
    assert bool(edges) is False


@mock.patch('rex_backend.graphql.resolvers.unclaimed_showings.apply_pagination_and_load')
@mock.patch('rex_backend.graphql.data_loaders.SlotsForShowingLoader.load_time_slots', return_value=[])
@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_showing_users', return_value=[])
def test_query_with_failed_rex_showing(showing_users_mock, showing_time_slots_mock, unclaimed_showings_mock, client, current_user, valid_jwt):
    unclaimed_showings_mock.return_value = [
        RexShowingFactory.make(
            id=1,
            status='Failed',
            claimed_at=datetime.datetime.now(),
            claimed_by_id=current_user.rex_users_ids[0]
        ),
        RexShowingFactory.make(
            id=2,
            status='Failed',
            claimed_at=datetime.datetime.now() - datetime.timedelta(days=2, hours=1),
            claimed_by_id=current_user.rex_users_ids[0]
        ),
        RexShowingFactory.make(
            id=3,
            status='Failed',
            claimed_at=datetime.datetime.now(),
            claimed_by_id=666
        )
    ]

    query = get_query()
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.status_code == 200
    assert response.json.get('errors') is None
    assert response.json.get('data', {}).get('unclaimedShowings') is not None

    edges = response.json.get('data', {}).get('unclaimedShowings', {}).get('edges', [])
    assert len(edges) == 1

    showing = edges[0].get('node', {})
    assert int(showing.get('id')) == 1
    assert showing.get('status') == 'FAILED'


@mock.patch('rex_backend.graphql.resolvers.unclaimed_showings.apply_pagination_and_load')
@mock.patch('rex_backend.graphql.data_loaders.SlotsForShowingLoader.load_time_slots', return_value=[])
@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_showing_users', return_value=[])
def test_query_with_canceled_rex_showing(showing_users_mock, showing_time_slots_mock, unclaimed_showings_mock, client, current_user, valid_jwt):
    unclaimed_showings_mock.return_value = [
        RexShowingFactory.make(
            id=1,
            status='Cancelled',
            claimed_at=datetime.datetime.now(),
            claimed_by_id=current_user.rex_users_ids[0]
        ),
        RexShowingFactory.make(
            id=2,
            status='Cancelled',
            claimed_at=datetime.datetime.now() - datetime.timedelta(days=2, hours=1),
            claimed_by_id=current_user.rex_users_ids[0]
        ),
        RexShowingFactory.make(
            id=3,
            status='Cancelled',
            claimed_at=datetime.datetime.now(),
            claimed_by_id=666
        )
    ]

    query = get_query()
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.status_code == 200
    assert response.json.get('errors') is None
    assert response.json.get('data', {}).get('unclaimedShowings') is not None

    edges = response.json.get('data', {}).get('unclaimedShowings', {}).get('edges', [])
    assert len(edges) == 1

    showing = edges[0].get('node', {})
    assert int(showing.get('id')) == 1
    assert showing.get('status') == 'CANCELLED'


@mock.patch('rex_backend.models.base.User.rex_users_ids', new_callable=mock.PropertyMock)
def test_query_current_user_has_not_rex_users_ids(mock_rex_users_ids, client, valid_jwt):
    mock_rex_users_ids.return_value = []

    query = get_query()
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None

    edges = response.json.get('data', {}).get('unclaimedShowings', {}).get('edges', [])
    assert len(edges) == 0


@mock.patch('rex_backend.graphql.resolvers.unclaimed_showings.apply_pagination_and_load')
@mock.patch('rex_backend.graphql.data_loaders.SlotsForShowingLoader.load_time_slots')
@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_showing_users')
def test_query_with_all_mocks(showing_users_mock, showing_time_slots_mock, unclaimed_showings_mock, client, valid_jwt):
    unclaimed_showings_mock.return_value = [
        RexShowingFactory.make(
            id=1,
            status='Pending',
        )
    ]
    showing_time_slots_mock.return_value = [
        RexShowingTimeSlotFactory.make()
    ]
    showing_users_mock.return_value = [
        RexUserFactory.make(
            id=1,
            role='Seller',
        ),
        RexUserFactory.make(
            id=1,
            role='Buyer',
        )
    ]

    query = get_query()
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.status_code == 200
    assert response.json.get('errors') is None
    assert response.json.get('data', {}).get('unclaimedShowings') is not None

    showing = response.json.get('data', {}).get('unclaimedShowings', {}).get('edges')[0].get('node')
    assert showing


@mock.patch('rex_backend.graphql.resolvers.unclaimed_showings.apply_pagination_and_load')
@mock.patch('rex_backend.graphql.data_loaders.SlotsForShowingLoader.load_time_slots', return_value=[])
@mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_showing_users', return_value=[])
def test_query_with_open_house_rex_showing(showing_users_mock, showing_time_slots_mock, unclaimed_showings_mock, client, current_user, valid_jwt):
    unclaimed_showings_mock.return_value = [
        RexShowingFactory.make(
            id=1,
            status='Pending',
            open_house_id=1
        )
    ]

    query = get_query()
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.status_code == 200
    assert response.json.get('errors') is None
    assert response.json.get('data', {}).get('unclaimedShowings') is not None

    showing = response.json.get('data', {}).get('unclaimedShowings', {}).get('edges')[0].get('node')
    assert showing
    assert int(showing.get('id')) == 1
    assert showing.get('type') == 'OPEN_HOUSE'


@mock.patch('rex_backend.graphql.resolvers.unclaimed_showings.apply_pagination_and_load', return_value=[])
def test_query_with_hidden_showings(showing_users_mock, session, client, current_user, valid_jwt):
    UserActionFactory.create(hide_showing=True, user=current_user)
    session.commit()

    query = get_query()
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.status_code == 200
    assert response.json.get('errors') is None
    assert response.json.get('data', {}).get('unclaimedShowings') is not None


@mock.patch('rex_backend.graphql.resolvers.unclaimed_showings.apply_pagination_and_load', return_value=[])
def test_query_with_three_hidden_showings(showing_users_mock, session, client, current_user, valid_jwt):
    UserActionFactory.create_batch(3, hide_showing=True, user=current_user)
    session.commit()

    query = get_query()
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.status_code == 200
    assert response.json.get('errors') is None
    assert response.json.get('data', {}).get('unclaimedShowings') is not None


@mock.patch('rex_backend.graphql.resolvers.unclaimed_showings.apply_pagination_and_load', return_value=[])
def test_query_with_three_hidden_showings_for_demo_user(showing_users_mock, session, client, current_demo_user, valid_jwt_for_demo_user):
    UserActionFactory.create_batch(3, hide_showing=True, user=current_demo_user)
    session.commit()

    query = get_query()
    authorization = f"Bearer {valid_jwt_for_demo_user}"
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.status_code == 200
    assert response.json.get('errors') is None
    assert response.json.get('data', {}).get('unclaimedShowings') is not None


@mock.patch('rex_backend.graphql.resolvers.unclaimed_showings.apply_pagination_and_load', return_value=[])
def test_query_for_demo_user(mock_showings, client, valid_jwt_for_demo_user):
    
    query = get_query()
    authorization = f'Bearer {valid_jwt_for_demo_user}'
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.status_code == 200
    assert response.json.get('errors') is None

    edges = response.json.get('data', {}).get('unclaimedShowings', {}).get('edges')
    assert len(edges) == 0


def get_query():
    return '''{
        unclaimedShowings {
          edges {
            cursor
            node {
              id
              type
              status
              slots {
                id
                time
                selected
              }
              showingReport {
                id
              }
              buyer {
                id
                firstName
                lastName
                email
                phoneNumber
              }
              seller {
                id
                firstName
                lastName
                email
                phoneNumber
              }
              showingInstructions
              needSellerAvailable
              needRexShowingRepresentative
              isOutsideAgent
              createdAt
              claimedAt
              finalShowingTime
              submittedForPaymentAt
            }
          }
          pageInfo {
            endCursor
            hasNextPage
            hasPreviousPage
            startCursor
          }
        }
      }
    '''
