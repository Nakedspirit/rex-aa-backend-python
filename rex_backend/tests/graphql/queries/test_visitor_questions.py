import datetime

from unittest import mock

from rex_backend.tests.factories import RexShowingFactory, RexListingFactory
from rex_backend.tests.mocks.apis.rex_questions import VISITOR_QUESTIONS_BAD_RESPONSE_MOCK, VISITOR_QUESTIONS_MOCK, OPENHOUSELEADS_QUESTIONS_MOCK


@mock.patch('rex_backend.wrappers.user.Agent.showing_query')
def test_query_without_showing(mock_rex_showing, client, valid_jwt):
    mock_rex_showing.return_value.first.return_value = None

    query = get_query(1, 1)
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })

    json = response.json
    result = json.get('data', {}).get('visitorQuestions')

    assert result == {'__typename': 'NotFoundProblem', 'entity': 'Open House', 'message': 'Open House not found.'}
    assert not json.get('errors')


@mock.patch('rex_backend.wrappers.user.Agent.showing_query')
@mock.patch('rex_backend.models.rex_listings.RexListing.query')
def test_query_without_listing(mock_rex_listing_query, mock_rex_showing, client, valid_jwt):
    showing_id = 1
    visitor_id = 1
    mock_rex_showing.return_value.first.return_value = RexShowingFactory.make(
        id=showing_id,
        open_house_id=1,
        open_house_guid='open_house_guid'
    )
    mock_rex_listing_query.filter.return_value.first.return_value = None

    query = get_query(showing_id=showing_id, visitor_id=visitor_id)
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })

    json = response.json
    result = json.get('data', {}).get('visitorQuestions')

    assert result == {'__typename': 'NotFoundProblem', 'entity': 'Listing', 'message': 'Listing not found.'}
    assert not json.get('errors')


@mock.patch('rex_backend.wrappers.user.Agent.showing_query')
@mock.patch('rex_backend.models.rex_listings.RexListing.query')
@mock.patch('requests.get')
def test_query_where_bad_api_response(requests_get_mock, mock_rex_listing_query, mock_rex_showing, client, valid_jwt):
    showing_id = 1
    visitor_id = 1
    mock_rex_showing.return_value.first.return_value = RexShowingFactory.make(
        id=showing_id,
        open_house_id=1,
        open_house_guid='open_house_guid'
    )
    mock_rex_listing_query.filter.return_value.first.return_value = RexListingFactory.make(id=1)
    requests_get_mock.return_value = VISITOR_QUESTIONS_BAD_RESPONSE_MOCK

    query = get_query(showing_id=showing_id, visitor_id=visitor_id)
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })

    json = response.json
    result = json.get('data', {}).get('visitorQuestions')

    assert result == {'__typename': 'VisitorQuestionsNotOk', 'message': 'REX API returned bad response.'}
    assert not json.get('errors')


@mock.patch('rex_backend.wrappers.user.Agent.showing_query')
@mock.patch('rex_backend.models.rex_listings.RexListing.query')
@mock.patch('requests.get')
def test_query(mock_requests_get, mock_rex_listing_query, mock_rex_showing, client, valid_jwt):
    showing_id = 1
    visitor_id = 1
    mock_rex_showing.return_value.first.return_value = RexShowingFactory.make(
        id=showing_id,
        open_house_id=1,
        open_house_guid='open_house_guid'
    )
    mock_rex_listing_query.filter.return_value.first.return_value = RexListingFactory.make(id=1)
    mock_requests_get.side_effect = [
        OPENHOUSELEADS_QUESTIONS_MOCK,
        VISITOR_QUESTIONS_MOCK,
    ]

    query = get_query(showing_id=showing_id, visitor_id=visitor_id)
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })

    json = response.json
    assert not json.get('errors')
    result = json.get('data', {}).get('visitorQuestions')

    assert result.get('__typename') == 'VisitorQuestionsOk'


def get_query(showing_id, visitor_id):
    return f'''
    query {{
        visitorQuestions(
            showingId: {showing_id}
            visitorId: {visitor_id}
        ) {{
            ... on NotFoundProblem {{
                __typename
                entity
                message
            }}
            ... on VisitorQuestionsNotOk {{
                __typename
                message
            }}
            ... on VisitorQuestionsOk {{
                __typename
                questions {{
                    ... on MultipleQuestion {{
                        id
                        order
                        title
                        isRequired
                        options {{
                            text
                            value
                            selected
                        }}
                    }}
                    ... on SingularQuestion {{
                        id
                        order
                        title
                        isRequired
                        options {{
                            text
                            value
                            selected
                        }}
                    }}
                    ... on TextQuestion {{
                        id
                        order
                        title
                        isRequired
                        response
                    }}
            
                }}
            }}
        }}
    }}
    '''