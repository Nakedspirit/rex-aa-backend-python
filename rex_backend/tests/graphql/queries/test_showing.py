import random
import datetime

from unittest import mock

from rex_backend.tests.conftest import dict2obj
from rex_backend.models.rex_listings import RexListing, RexAddress, RexListingImage, RexFileInfo
from rex_backend.tests.mocks.query_upcoming_showings import mock_buyers_and_sellers
from rex_backend.tests.mocks.query_showings import mock_showings, mock_time_slots, mock_without_listing
from rex_backend.tests.factories import RexShowingFactory, ShowingReportFactory
from rex_backend.tests.mocks.apis.rex_questions import OPEN_HOUSE_QUESTIONS_MOCK


FAKE_LISTING = RexListing(
    id = 939,
    guid='711124500d3c447f975490298570600f',
    address = RexAddress(
        id = 795,
        city = 'Rancho John Doe',
        address_line = '42 unknown st',
        state = 'CA'
    ),
    air_conditioning_type = 'CENTRAL',
    fireplace_indicator_flag = None,
    heating_type = 'FORCED_AIR',
    hoa_fee = 113.0,
    property_type = 'SINGLE',
    land_square_footage = 6110,
    living_square_feet = 1890,
    total_baths = 3.0,
    bedrooms = 3,
    parking_spaces = 3,
    parking_type_code = 'Garage',
    pool_flag = False,
    stories = 2.0,
    year_built = 1986,
    price = 780500,
    images = [
        RexListingImage(
            file_info = RexFileInfo(
                public_url = 'https://cdn.houseplansservices.com/product/p4pvnkmoahoo30m48busgglrug/w560x373.jpg?v=9'
            )
        )
    ]
)

FAKE_REX_ADDRESS = RexAddress(
    id = 795,
    city = 'Rancho John Doe',
    address_line = '42 unknown st',
    state = 'CA'
)

@mock.patch('rex_backend.models.rex_listings.RexListing.query')
@mock.patch('rex_backend.models.rex_listings.RexAddress.query')
def test_query(mock_rex_address_query, mock_rex_listing_query, client, valid_jwt, mock_showings, mock_time_slots, mock_buyers_and_sellers):
    mock_rex_listing_query.filter.return_value.all.return_value = [
        FAKE_LISTING
    ]
    mock_rex_address_query.with_entities.return_value.join.return_value.filter.return_value.all.return_value = [
        (FAKE_REX_ADDRESS, 939)
    ]

    query = get_query(939)
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.json.get('errors') is None
    returned = response.json.get('data', {}).get('showing')
    assert not returned is None


@mock.patch('rex_backend.models.rex_listings.RexListing.query')
def test_query_without_listing(mock_rex_listing_query, client, valid_jwt, mock_without_listing):
    mock_rex_listing_query.filter.return_value.all.return_value = []

    query = get_query(939)
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })

    assert response.json.get('errors') is None
    returned = response.json.get('data', {}).get('showing')
    assert returned is None


@mock.patch('rex_backend.wrappers.user.Agent.showing_query')
@mock.patch('requests.get', return_value=OPEN_HOUSE_QUESTIONS_MOCK)
def test_showing_with_showing_report_and_questions(
    mock_requests_get_for_questions,
    mock_rex_showing_query,
    session, client, valid_jwt
    ):
    mock_rex_showing_query.return_value.first.return_value = RexShowingFactory.make(
        id=1,
        open_house_id=1,
    )
    showing_report = ShowingReportFactory.create(
        rex_showing_id=1
    )
    session.commit()

    query = f'''
    query {{
        showing(id: {1}) {{
            id
            showingReport {{
                id
                groups {{
                    title
                    order
                    questions {{
                        __typename
                    }}
                }}
            }}
        }}
    }}
    '''
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': f'Bearer {valid_jwt}' })
    json = response.json
    assert not json.get('errors')

    data = json.get('data')
    expected_data = {
        'showing': {
            'id': '1', 
            'showingReport': {
                'id': str(showing_report.id), 
                'groups': [
                    {
                        'title': 'title#1', 
                        'order': 1, 
                        'questions': [
                            {'__typename': 'TextQuestion'},
                            {'__typename': 'MultipleQuestion'},
                            {'__typename': 'SingularQuestion'}, 
                            {'__typename': 'MultipleQuestion'}, 
                            {'__typename': 'SingularQuestion'}, 
                            {'__typename': 'MultipleQuestion'}, 
                            {'__typename': 'TextQuestion'}
                        ]
                    }
                ]
            }
        }
    }
    assert data == expected_data



def get_query(id):
    return '''{{
      showing(id: {}) {{
        id
        type
        status
        listing {{
          id
          address
        }}
        slots {{
          id
          time
        }}
        showingReport {{
          id
          status
        }}
        buyer {{
          firstName
        }}
        seller {{
          firstName
        }}
        showingInstructions
        needSellerAvailable
        needRexShowingRepresentative
        isOutsideAgent
        createdAt
        claimedAt
        finalShowingTime
        submittedForPaymentAt
      }}
    }}
    '''.format(id)
