import pytest
from unittest import mock


from rex_backend.tests.mocks.apis.leaderboard import leaderboard_api_response, empty_leaderboard_api_response
from requests.exceptions import ConnectTimeout


@mock.patch('requests.get', return_value=leaderboard_api_response())
def test_query_with_data(mock_leaderboard_api_response, client, valid_jwt):
    query = get_query('COMPLETED_SHOWINGS_AMOUNT', date_start='2019-07-01', date_end='2019-09-01')
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data={'query': query}, headers={'Authorization': authorization})
    assert response.json.get('errors') is None
    returned = response.json.get('data', {}).get('leaderboard')
    node = returned[0]
    assert node.get('name') == 'COMPLETED_SHOWINGS_AMOUNT'
    assert node.get('value') == 13
    assert node.get('position') == 1
    assert node.get('positionChange') == 0


@mock.patch('requests.get', return_value=leaderboard_api_response())
def test_query_with_none_date_start_and_date_end(mock_leaderboard_api_response, client, valid_jwt):
    query = get_query('COMPLETED_SHOWINGS_AMOUNT', date_start=None)
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data={'query': query}, headers={'Authorization': authorization})
    assert response.json.get('errors') is None
    returned = response.json.get('data', {}).get('leaderboard')
    node = returned[0]
    assert node.get('name') == 'COMPLETED_SHOWINGS_AMOUNT'
    assert node.get('value') == 13
    assert node.get('position') == 1
    assert node.get('positionChange') == 0


def test_query_with_date_start_gt_date_end(client, valid_jwt):
    query = get_query('COMPLETED_SHOWINGS_AMOUNT', date_start='2019-09-01', date_end='2019-08-01')
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data={'query': query}, headers={'Authorization': authorization})
    assert response.json.get('errors')
    assert response.json.get('errors')[0]['message'] == 'dateStart can not be greater than dateEnd'


@mock.patch('requests.get', side_effect=ConnectTimeout('Test'))
def test_query_when_timeout_exception_occurs(mock_leaderboard_api_response, client, valid_jwt):
    query = get_query('COMPLETED_SHOWINGS_AMOUNT')
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data={'query': query}, headers={'Authorization': authorization})
    assert response.json.get('errors') is not None
    assert response.json.get('errors')[0].get('message') == 'REX service is not responding. Please return to the previous screen and try again.'


@mock.patch('requests.get', return_value=empty_leaderboard_api_response())
def test_query_with_empty_data(mock_empty_leaderboard_api_response, client, valid_jwt):
    query = get_query('COMPLETED_SHOWINGS_AMOUNT')
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data={'query': query}, headers={'Authorization': authorization})
    assert response.json.get('errors') is None
    returned = response.json.get('data', {}).get('leaderboard')
    assert returned == []


@mock.patch('requests.get', return_value=leaderboard_api_response())
def test_query_for_escrow_showings(mock_leaderboard_api_response, client, valid_jwt):
    query = get_query('ESCROW_SHOWINGS_AMOUNT')
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data={'query': query}, headers={'Authorization': authorization})
    assert response.json.get('errors') is None
    returned = response.json.get('data', {}).get('leaderboard')
    assert returned


def get_query(stat_name, date_start='2019-09-01', date_end='2019-09-30'):
    if date_start is None:
        return """{{
        leaderboard(
            statName: {stat_name}
        ) {{
            userName
            name
            value
            position
            positionChange
            isCurrentUser
        }}
        }}
        """.format(stat_name=stat_name)

    return """{{
        leaderboard(
            statName: {stat_name}
            dateStart: "{date_start}"
            dateEnd: "{date_end}"
        ) {{
            userName
            name
            value
            position
            positionChange
            isCurrentUser
        }}
        }}
    """.format(stat_name=stat_name, date_start=date_start, date_end=date_end)
