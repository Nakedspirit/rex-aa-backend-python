import datetime

from unittest import mock

from rex_backend.tests.helpers import graphql_request
from rex_backend.tests.factories import RexShowingFactory
from rex_backend.graphql.enums import ShowingTypeEnum


@mock.patch('rex_backend.wrappers.user.Agent.gigs_query')
def test_query(mock_rex_showings, client, valid_jwt):
    mock_rex_showings.return_value.all.return_value = [
        RexShowingFactory.make(
            id=1,
            status='Scheduled',
            open_house_id=1,
            final_meeting_time=datetime.datetime.now(),
        ),
        RexShowingFactory.make(
            id=2,
            status='Scheduled',
            final_meeting_time=datetime.datetime.now(),
        )
    ]

    query = get_query()
    response = graphql_request(client, valid_jwt, query)

    assert response.get('errors') is None
    assert response.get('data', {}).get('gigs') is not None

    edges = response.get('data', {}).get('gigs', {}).get('edges')
    assert edges

    showing = edges[0].get('node')
    assert showing.get('type') == ShowingTypeEnum.OPEN_HOUSE.value

    showing = edges[1].get('node')
    assert showing.get('type') == ShowingTypeEnum.REGULAR.value


@mock.patch('rex_backend.wrappers.user.DemoUser.gigs_query')
def test_query_for_demo_user(mock_rex_showings, client, valid_jwt_for_demo_user):
    mock_rex_showings.return_value.all.return_value = []

    query = get_query()
    response = graphql_request(client, valid_jwt_for_demo_user, query)

    assert response.get('errors') is None
    assert response.get('data', {}).get('gigs') is not None


@mock.patch('rex_backend.models.base.User.rex_users_ids', new_callable=mock.PropertyMock)
def test_query_with_current_user_withour_rex_links(mock_rex_users_ids, client, valid_jwt):
    mock_rex_users_ids.return_value = []
    
    query = get_query()
    response = graphql_request(client, valid_jwt, query)

    assert response.get('errors') is None
    assert response.get('data', {}).get('gigs') is not None

    edges = response.get('data', {}).get('gigs', {}).get('edges')
    assert len(edges) == 0


def get_query():
    return '''
    query {
        gigs {
            edges {
                node {
                    id
                    type
                    status
                    showingInstructions
                    needSellerAvailable
                    needRexShowingRepresentative
                    isOutsideAgent
                    createdAt
                    claimedAt
                    finalShowingTime
                    submittedForPaymentAt
                }
            }
        }
    }
    '''