import random
import datetime

from unittest import mock

from rex_backend.tests.factories import RexShowingFactory, RexListingFactory


@mock.patch('rex_backend.graphql.resolvers.completed_showings.apply_pagination_and_load', return_value=[])
def test_query(mock_rex_showings, client, valid_jwt):
    query = get_query()
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })

    assert response.status_code == 200
    assert response.json.get('errors') is None
    assert not response.json.get('data', {}).get('completedShowings') is None


# @mock.patch('rex_backend.graphql.resolvers.completed_showings.apply_pagination_and_load')
# @mock.patch('rex_backend.graphql.data_loaders.SlotsForShowingLoader.load_time_slots', return_value=[])
# @mock.patch('rex_backend.repositories.rex_showing_repo.RexShowingRepo.load_showing_users', return_value=[])
# @mock.patch('rex_backend.models.rex_listings.RexListing.query')
# def test_query_with_rex_showings(mock_rex_listing_query, mock_rex_users, mock_rex_time_slots, mock_rex_showings, client, valid_jwt):
#     mock_rex_showings.return_value = [
#         RexShowingFactory.make(
#             id=1,
#             status='Completed',
#             open_house_id=1
#         ),
#         RexShowingFactory.make(
#             id=2,
#             status='Completed',
#         )
#     ]
#     mock_rex_listing_query.filter.return_value.all.return_value = [
#         RexListingFactory.make(id=1)
#     ]

#     query = get_query()
#     authorization = "Bearer {}".format(valid_jwt)
#     response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
#     assert response.status_code == 200
#     assert response.json.get('errors') is None
#     edges = response.json.get('data', {}).get('completedShowings', {}).get('edges')
#     assert not edges is None
#     assert len(edges) == 2

#     open_house_node = edges[0].get('node')
#     assert open_house_node.get('type') == 'OPEN_HOUSE'


@mock.patch('rex_backend.models.base.User.rex_users_ids', new_callable=mock.PropertyMock)
def test_query_with_current_user_without_rex_users_ids(mock_rex_users_ids, client, valid_jwt):
    mock_rex_users_ids.return_value = []
    
    query = get_query()
    authorization = "Bearer {}".format(valid_jwt)
    response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
    assert response.status_code == 200
    assert response.json.get('errors') is None

    edges = response.json.get('data', {}).get('completedShowings', {}).get('edges')
    assert len(edges) == 0


# @mock.patch('rex_backend.graphql.resolvers.completed_showings.apply_pagination_and_load')
# def test_with_showings_without_listing_id(mock_rex_showings, client, valid_jwt):
#     mock_rex_showings.return_value = [
#         RexShowingFactory.make(
#             id=1,
#             status='Completed',
#             open_house_id=1,
#             data={},
#         ),
#         RexShowingFactory.make(
#             id=2,
#             status='Completed',
#             data={},
#         )
#     ]

#     query = '''{
#         completedShowings {
#             edges {
#                 cursor
#                 node {
#                     id
#                 }
#             }
#         }
#     }
#     '''
#     authorization = "Bearer {}".format(valid_jwt)
#     response = client.post('/graphql', data = { 'query': query }, headers= { 'Authorization': authorization })
#     assert response.status_code == 200
#     assert response.json.get('errors') is None

#     edges = response.json.get('data', {}).get('completedShowings', {}).get('edges')
#     assert len(edges) == 0


def get_query():
    return '''{
        completedShowings {
          edges {
            cursor
            node {
              id
              type
              status
              slots {
                id
                time
              }
              showingReport {
                id
                status
              }
              buyer {
                firstName
              }
              seller {
                firstName
              }
              showingInstructions
              needSellerAvailable
              needRexShowingRepresentative
              isOutsideAgent
              createdAt
              claimedAt
              finalShowingTime
              submittedForPaymentAt
            }
          }
          pageInfo {
            endCursor
            hasNextPage
            hasPreviousPage
            startCursor
          }
        }
      }
    '''
