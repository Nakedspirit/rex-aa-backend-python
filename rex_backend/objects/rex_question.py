from collections import namedtuple


class RexGroup:
    @classmethod
    def init_many(cls, raw_groups):
        return [cls(order=(i + 1), **raw_group) for i, raw_group in enumerate(raw_groups)]

    def __init__(self, title='', order=None, elements=[]):
        if not title or title.replace(' ', '') == '':
            title = f'title#{order}'

        self.title = title
        self.order = order
        self.questions = [RexQuestion(self, order=(i + 1), **element) for i, element in enumerate(elements)]

    def __repr__(self):
        return (
            f'{self.__class__}'
            f' title: "{self.title}"; order: {self.order}; elements_qty: {len(self.elements)};'
        )

    def to_graphql(self, is_completed=False):
        return {
            'id': self.order,
            'title': self.title,
            'order': self.order,
            'is_completed': bool(is_completed),
            'questions': [question.to_graphql() for question in self.questions],
        }


RexOption = namedtuple('RexOption', 'value text')
GraphqlQuestion = namedtuple('GraphqlQuestion', 'id title order is_required options type')


class RexQuestion:
    def __init__(self, group, order=None, **kwargs):
        self.group = group
        self.order = order
        self.name = kwargs.get('name')
        self.title = kwargs.get('title')
        self.guid = kwargs.get('valueName')
        self.is_required = kwargs.get('isRequired')
        self.type = kwargs.get('type')
        self.options = [RexOption(**choice) for choice in kwargs.get('choices', [])]
        self.original_data = dict(kwargs)
        assert self.name and self.title and self.guid

    def __repr__(self):
        return (
            f'{self.__class__}'
            f' name: "{self.name}"; guid: "{self.guid}"; title: "{self.title}"; type: {self.type};'
        )

    @property
    def graphlq_type(self):
        return {
            'radiogroup': 'SingularQuestion',
            'dropdown': 'SingularQuestion',
            'checkbox': 'MultipleQuestion'
        }.get(self.type) or 'TextQuestion'

    def to_graphql(self):
        return GraphqlQuestion(
            id=self.guid,
            order=self.order,
            title=self.title,
            is_required=bool(self.is_required),
            options=self.options,
            type=self.graphlq_type
        )
