import logging
from sqlalchemy import create_engine, orm
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.exc import UnmappedClassError


logger = logging.getLogger('rex_backend')


class _QueryProperty:

    def __init__(self, sa):
        self.sa = sa

    def __get__(self, obj, type):
        try:
            mapper = orm.class_mapper(type)
            if mapper:
                return orm.Query(mapper, session=self.sa.session)
        except UnmappedClassError:
            return None


class DatabaseManager:

    def __init__(self, connection_string, base_model):
        self.engine = None
        self.connection_string = connection_string
        self.connection = None
        self.BaseModel = base_model
        self.BaseModel.query = _QueryProperty(self)
        self._session = None

    def connect(self):
        self.engine = create_engine(self.connection_string, pool_pre_ping=True)
        logger.info(f"Connected to database: {repr(self.engine.url)}")
        self._session_factory = sessionmaker(bind=self.engine)

    @property
    def session(self):
        if not self._session:
            self._session = self._session_factory()
        return self._session

    @property
    def metadata(self):
        return self.BaseModel.metadata

    def close_session(self):
        if self._session:
            self._session.close()
            self._session = None
