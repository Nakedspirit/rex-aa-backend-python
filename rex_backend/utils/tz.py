import datetime
import time


import pytz


TIMEZONE = 'UTC'


def tznow(tz=pytz.timezone(TIMEZONE)):
    return pytz.UTC.localize(datetime.datetime.utcnow()).astimezone(tz)


def tztoday(tz=pytz.timezone(TIMEZONE)):
    return tznow().date()


def tzaware(dt, tz=pytz.timezone(TIMEZONE)):
    return pytz.UTC.localize(dt).astimezone(tz)


_EPOCH = datetime.datetime(1970, 1, 1, tzinfo=pytz.utc)


def timestamp(dt):
    "Return POSIX timestamp as float"
    if dt.tzinfo is None:
        return int(time.mktime((dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second, -1, -1, -1)) + dt.microsecond / 1e6)
    else:
        return int((dt - _EPOCH).total_seconds())


def set_tz(dt, tz_str):
    tz = pytz.timezone(tz_str)
    return tz.localize(dt)
