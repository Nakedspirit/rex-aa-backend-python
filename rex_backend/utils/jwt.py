import jwt
import time
from rex_backend.utils.tz import tznow, tzaware
from datetime import datetime
from typing import Any

DAY_1_SECONDS = 60 * 60 * 24

META = {'exp': int(time.time() - DAY_1_SECONDS), 'iss': 'rex', 'aud': 'client'}


class JwtService:

    def __init__(self, app, secret_field='JWT_SECRET_KEY'):
        self.secret_field = secret_field
        self.app = app
        if not self.app.config.get(self.secret_field):
            raise Exception("Configuration error, no env variable JWT_SECRET_KEY")

    def jwt_for_user(self, user, identity, other_attributes={}):
        user_data = {
            'user_id': user.id,
            'identity_id': identity.id,
            'iss': 'rex',
            'aud': 'client'
            # 'exp': timestamp(tznow() + timedelta(+30))
        }
        for user_data_key in user_data.keys():
            if user_data_key in other_attributes:
                del other_attributes[user_data_key]

        user_data.update(other_attributes)
        return self.encode(user_data)

    def encode(self, payload) -> str:
        to_encode = dict(META)
        to_encode.update(payload)
        s = jwt.encode(payload, key=self.app.config[self.secret_field])
        return s.decode('utf-8')

    def decode(self, token) -> Any:
        return jwt.decode(token, key=self.app.config[self.secret_field], audience=META['aud'])

    def is_valid_payload(self, payload) -> bool:
        return ((payload.get('iss') == META['iss']) and (payload.get('aud') == META['aud']) and not self.is_expired_paypload(payload))

    def is_expired_paypload(self, payload) -> bool:
        expiration = payload.get('exp')
        if expiration is None:
            return False

        expiration = tzaware(datetime.utcfromtimestamp(expiration))
        now = tznow()
        return now > expiration
