def make_rex_showing_address(rex_showing):
    address_points = [
        rex_showing.data.get('streetAddress'),
        rex_showing.data.get('city'),
        rex_showing.data.get('state')
    ]
    filtered_address = [p for p in address_points if p]
    return ', '.join(filtered_address)
