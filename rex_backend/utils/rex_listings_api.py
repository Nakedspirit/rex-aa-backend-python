import json
import requests
from urllib.parse import urljoin


# TODO: remove this service and use HttAPI
class RexListingsAPI:
    def __init__(self, url):
        self.url = url

    def get_questions_and_answers(self, listing_guid):
        url = urljoin(
            self.url,
            'questions/get-answers?tool=aasurvey&listing_guid={}'.format(listing_guid)
        )
        response = requests.get(url)
        return self.__parse_response(response)

    def __parse_response(self, response):
        status = response.status_code

        try:
            content = response.json()
        except AttributeError:
            content = {}
        except json.decoder.JSONDecodeError:
            content = {}

        return {'status': status, 'content': content}
