import sys
import glob
import importlib
from os import path

from rex_backend.helpers import snake_to_camel


class DefaultConvention:
    """
    Convention class for AutoModuleLoader
    - accepts python module(`file_name.py`)
    - returns `file_name`
    """
    def apply(self, module):
        return AutoModuleLoader.get_py_module_name(module.__file__)


class ClassConvention(DefaultConvention):
    """
    Convention class for AutoModuleLoader
    - accepts python module(`file_name.py`)
    - returns `FileName`
    """
    def apply(self, module):
        module_name = super().apply(module)
        return snake_to_camel(module_name)


class AutoModuleLoader:
    """
    This is the helper for loading standard objects like GraphQL resolvers.
    For example you have the following structure:
    # resolvers/
    #   __init__.py
    #   resolve_user.py -> def resolve_user(...) ..

    To load `resolve_user` function from `resolvers` package you need to add to `__init__.py` below lines:
    # from .resolve_user import resolve_user
    #
    # __all__ = ['resolve_user']

    Now, imagine you have 10 functions like `resolve_user`, it's a pain in the ass, so you can use this service 
    to accomplish this task, the example is below:
    # from graphql_files_loader import GraphQLFilesLoader
    #
    # files_loader = GraphQLFilesLoader(__name__)
    # files_loader.load_graphql_objects_into_current_package()
    # files_loader.write_graphql_objects_into_all_variable()

    The service will load functions and add them to module, 
    !!! but you need to follow the convention, you can define you own
    !!! DefaultConvention works like so `file_name.py` should define `file_name` function!!!
    """  # noqa: W291
    def __init__(self, package_name, convention=DefaultConvention()):
        self.current_module = sys.modules[package_name]
        self.loaded_modules = []
        self.convention = convention

    def get_modules_paths_to_load(self):
        current_dir_name = path.dirname(self.current_module.__file__)
        modules_paths = glob.glob(
            path.join(
                current_dir_name,
                '*.py'
            )
        )

        for module_path in list(modules_paths):
            module_name = self.get_py_module_name(module_path)
            if not path.isfile(module_path) or module_name.startswith('_'):
                modules_paths.remove(module_path)

        return modules_paths

    def load_objects_into_current_package(self):
        modules_paths = self.get_modules_paths_to_load()

        for module_path in modules_paths:
            module_name = self.get_py_module_name(module_path)
            module = importlib.import_module('.' + module_name, package=self.current_module.__package__)
            obj_name_from_module = self.convention.apply(module)

            if obj_name_from_module not in dir(module):
                raise ImportError(f"Can't load '{obj_name_from_module}'' from {module_path}")

            obj_from_module = getattr(module, obj_name_from_module)
            setattr(
                self.current_module,
                obj_name_from_module,
                obj_from_module
            )
            self.loaded_modules.append(module)

        return self.loaded_modules

    def write_objects_into_all_variable(self):
        all_ = [path.basename(module.__file__)[:-3] for module in self.loaded_modules]
        setattr(self.current_module, '__all__', all_)

    @staticmethod
    def get_py_module_name(module_path):
        return path.basename(module_path)[:-3]
