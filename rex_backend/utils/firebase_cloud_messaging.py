import json
from base64 import b64decode

import logging
import firebase_admin
from firebase_admin.credentials import Certificate
from firebase_admin import auth
from firebase_admin import messaging


logger = logging.getLogger('rex_backend')


def base64_to_dict(base64_str):
    decoded_str = b64decode(base64_str).decode('UTF-8')
    json_str = decoded_str.replace("'", "\"")
    return json.loads(json_str)


class FirebaseCloudMessaging:
    def __init__(self, base64_cert, fake_mode=False):
        self.fake_mode = fake_mode
        if base64_cert:
            cert_hash = base64_to_dict(base64_cert)
            self.cred = Certificate(cert_hash)
            self.app = firebase_admin.initialize_app(self.cred)
        else:
            self.cred = None
            self.app = None
            logger.error('FirebaseCloudMessaging not initialized properly, base64_cert is empty.')

    def verify_push_notification_token(self, id_token):
        return auth.verify_id_token(id_token, app=self.app)

    def send_message(self, token, title, body, data={'sound': 'default'}):
        ntf = messaging.Notification(title=title, body=body)
        apns = messaging.APNSConfig(
            payload=messaging.APNSPayload(
                aps=messaging.Aps(sound='default')
            )
        )
        msg = messaging.Message(token=token, notification=ntf, apns=apns, data=data)
        try:
            return messaging.send(msg, app=self.app, dry_run=self.fake_mode)
        except Exception as e:
            print(e.detail.response.json())
            raise

    # TODO: rewrite this method. Reason - mixed levels of abstractions
    def send_messages(self, devices, title, body, data={}):
        ntf = messaging.Notification(title=title, body=body)
        messages = []
        for device in devices:
            apns = messaging.APNSConfig(
                payload=messaging.APNSPayload(
                    aps=messaging.Aps(badge=device.badge_count, sound='default'),
                ),
            )
            msg = messaging.Message(token=device.push_notification_token, notification=ntf, data=data, apns=apns)
            messages.append(msg)

        return messaging.send_all(messages, app=self.app, dry_run=self.fake_mode)

    def send_message_to_many(self, tokens, title, body, data={'sound': 'deault'}):
        if len(tokens) == 1:
            return self.send_message(tokens[0], title, body, data=data)

        ntf = messaging.Notification(title=title, body=body)
        apns = messaging.APNSConfig(
            payload=messaging.APNSPayload(
                aps=messaging.Aps(sound='default')
            )
        )
        msg = messaging.MulticastMessage(tokens=tokens, notification=ntf, apns=apns, data=data)
        return messaging.send_multicast(msg, app=self.app, dry_run=self.fake_mode)
