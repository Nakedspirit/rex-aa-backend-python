import json
import requests
from urllib.parse import urljoin


# TODO: rewrite this REX API call with HttpAPI
class RexSchedulerApi:
    def __init__(self, api_url, api_key):
        self.api_url = api_url
        self.api_key = api_key  # TODO we need to add a check for this key, it may be wrong and it'll be silent fall

    def call_main_endpoint(self, rex_event):
        response = requests.post(self.api_url, {
            "event": json.dumps(rex_event)
        }, params={'token': self.api_key})
        return self.__parse_response(response)

    def call_complete_endpoint(self, rex_showing_id):
        url = urljoin(self.api_url, 'conversation/{}/complete'.format(rex_showing_id))
        response = requests.get(str(url), params={'token': self.api_key})
        return self.__parse_response(response)

    def __parse_response(self, response):
        status = response.status_code

        try:
            content = response.json()
        except AttributeError:
            content = {}
        except json.decoder.JSONDecodeError:
            content = {}

        return {'status': status, 'content': content}
