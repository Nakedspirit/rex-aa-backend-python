class EmailMessage:
    def __init__(self, recipients='', subject='', html='', text='', default_recipients=[], bcc=[], charset='UTF-8'):
        self._recipients = recipients
        self.subject = subject
        self.html = html
        self.text = text
        self.default_recipients = default_recipients
        self.bcc = bcc
        self.charset = charset

    def __repr__(self):
        return (
            f'{self.__class__}'
            f' {self.data}'
        )

    @property
    def data(self):
        return {
            'recipients': self.recipients,
            'bcc': self.bcc,
            'subject': self.subject,
            'html?': bool(self.html),
            'text?': bool(self.text),
            'charset': self.charset
        }

    @property
    def recipients(self):
        recipients = self._recipients
        if not recipients:
            return self.default_recipients

        if type(recipients) is list:
            return recipients

        return [str(recipients)]
