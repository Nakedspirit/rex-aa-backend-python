import logging
import json
from urllib.parse import urljoin

import requests
from sentry_sdk import capture_exception, capture_message


logger = logging.getLogger('rex_backend')


class RestAPIException(Exception):
    pass


# TODO: remove this service and use HttAPI
class RestAPI:
    # TODO: accept main url as a param here
    def __init__(self, timeout=3, api_name='API', additional_err_msg=''):
        self.api_name = api_name
        self.timeout = timeout
        self.additional_err_msg = additional_err_msg

    def get(self, url, headers={}, params={}):
        url = self.__join_url(url)

        response = self.__make_request(
            lambda: requests.get(
                url,
                params=params,
                headers=headers,
                timeout=self.timeout
            )
        )

        return self.__parse_response(response)

    def post(self, url, data):
        url = self.__join_url(url)
        response = self.__make_request(lambda: requests.post(url, json=data, timeout=self.timeout))

        return self.__parse_response(response)

    def __join_url(self, url):
        if type(url) == list:
            domain = url[0]
            path = '/'.join(url[1:])
            url = urljoin(domain, path)
        return url

    def __make_request(self, request_func):
        try:
            response = request_func()
        except requests.exceptions.ConnectTimeout as exc:
            capture_exception(exc)
            logger.critical(exc)
            exc_msg = f'{self.api_name} call failed. Connection timeout.'
            exc_msg += f' {self.additional_err_msg}.' if self.additional_err_msg else ''
            raise RestAPIException(exc_msg)
        except requests.exceptions.RetryError as exc:
            capture_exception(exc)
            logger.critical(exc)
            exc_msg = f'{self.api_name} call failed. Max retries exceeded.'
            exc_msg += f' {self.additional_err_msg}.' if self.additional_err_msg else ''
            raise RestAPIException(exc_msg)

        return response

    def __parse_response(self, response):
        status = response.status_code

        if status != 200:
            msg = f'Request: {response.request.__dict__}, Response: {response.__dict__}'
            capture_message(msg)
            logger.critical(msg)
            exc_msg = f'{self.api_name} returned bad response. Status is {status}.'
            exc_msg += f' {self.additional_err_msg}.' if self.additional_err_msg else ''
            raise RestAPIException(exc_msg)

        try:
            content = response.json()
        except AttributeError:
            content = {}
        except json.decoder.JSONDecodeError:
            content = {}

        return {'status': status, 'content': content}
