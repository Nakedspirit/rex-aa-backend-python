from rex_backend.application import app


from google.oauth2 import service_account
from googleapiclient import discovery


import json
from base64 import b64decode


def get_developer_emails():
    service_key_env_var_name = app.config['GOOGLE_CLOUD_BASE64_CERT']
    spreadsheet_id = app.config['GOOGLE_SPREADSHEET_ID']
    developer_range = app.config['GOOGLE_SPREADSHEET_DEVELOPER_RANGE']
    scopes = ['https://www.googleapis.com/auth/spreadsheets.readonly']

    decoded_str = b64decode(service_key_env_var_name).decode('UTF-8')
    json_str = decoded_str.replace("'", "\"")
    info = json.loads(json_str)
    credentials = service_account.Credentials.from_service_account_info(
        info, scopes=scopes)
    service = discovery.build('sheets', 'v4', credentials=credentials)

    # Call the Sheets API
    request = service.spreadsheets().values().get(
        spreadsheetId=spreadsheet_id, range=developer_range)
    response = request.execute()
    developer_emails = response.get('values', [])
    developer_emails = [data[0] for data in developer_emails]
    return developer_emails
