import logging


from google.oauth2 import id_token
from google.auth.transport import requests as google_requests
import requests


from rex_backend.application import sentry_sdk
from rex_backend.exceptions import InvalidOauthToken


logger = logging.getLogger('rex_backend')
_request = google_requests.Request()


class GoogleOAuthService:
    def __init__(self, google_clients_ids, correct_domain):
        self.google_clients_ids = google_clients_ids
        self.correct_domain = correct_domain

    def get_token_info(self, oauth_token):
        response = self.ask_google(oauth_token)
        response_data = response.json()
        if response.status_code != 200:
            logger.warning(f'Bad google response for access token: {response_data}')
            raise InvalidOauthToken('google', response_data['error'], response_data['error_description'])

        if response_data['audience'] not in self.google_clients_ids:
            logger.warning(f"The token isn't correct for this app")
            raise InvalidOauthToken('google', 'Invalid token', "The token isn't correct for this app")

        email = response_data['email']
        if self.correct_domain != self.extract_domain(email):
            logger.warning(f"Invalid email's domain")
            raise InvalidOauthToken('google', 'Invalid token', "Invalid email's domain")

        return response_data

    def get_id_token_info(self, token):
        try:
            idinfo = id_token.verify_oauth2_token(token, _request)

            google_client_id = idinfo.get('aud')
            if google_client_id is None or google_client_id not in self.google_clients_ids:
                raise ValueError(f"Could not verify audience. idinfo: {idinfo}")
            # if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
            #     raise ValueError('Wrong issuer.')

            domain = idinfo.get('hd') or self.extract_domain(idinfo.get('email'))
            if domain is None or domain != self.correct_domain:
                raise ValueError(f"Wrong hosted domain. idinfo: {idinfo}")
            idinfo['user_id'] = idinfo['sub']
            return idinfo
        except ValueError as exc:
            sentry_sdk.capture_exception(exc)
            logger.warning(str(exc))
            raise InvalidOauthToken('google', 'Something wrong with the token', str(exc))

    def ask_google(self, oauth_token):
        params = {'access_token': oauth_token}
        return requests.get('https://www.googleapis.com/oauth2/v1/tokeninfo', params=params)

    def extract_domain(self, email):
        return email.split('@')[-1]
