import logging


import boto3
from botocore.exceptions import ClientError
from sentry_sdk import capture_exception


logger = logging.getLogger('rex_backend')


class AWSSESRespose(object):
    '''The wrapper for AWS Simple Email Service response'''
    def __init__(self, response):
        self.response = response

    @property
    def message_id(self):
        return self.response.get('MessageId')

    @property
    def error_message(self):
        return self.response.get('Error', {}).get('Message')

    def is_success(self):
        return bool(self.message_id)

    def is_failure(self):
        return not self.is_success()


class AWSSES(object):
    '''The wrapper for AWS Simple Email Service'''
    def __init__(self, access_key, secret_key, aws_region, sender_email, charset='UTF-8'):
        if access_key is None:
            logger.info('AWS SES client initialized without aws_access_key_id & aws_secret_access_key')
            self.client = boto3.client(
                'ses',
                region_name=aws_region
            )
        else:
            logger.info('AWS SES client initialized with ENV vars')
            self.client = boto3.client(
                'ses',
                aws_access_key_id=access_key,
                aws_secret_access_key=secret_key,
                region_name=aws_region
            )
        self.sender_email = sender_email

    def send_email(self, email_message):
        try:
            response = self.client.send_email(
                Source=self.sender_email,
                Destination={
                    'ToAddresses': email_message.recipients,
                    'BccAddresses': email_message.bcc
                },
                Message={
                    'Subject': {
                        'Charset': email_message.charset,
                        'Data': email_message.subject,
                    },
                    'Body': {
                        'Html': {
                            'Charset': email_message.charset,
                            'Data': email_message.html,
                        },
                        'Text': {
                            'Charset': email_message.charset,
                            'Data': email_message.text,
                        },
                    },
                },
            )
        except ClientError as e:
            capture_exception(e)
            err_msg = e.response['Error']['Message']
            logger.error(err_msg)
            response = e.response

        return AWSSESRespose(response)
