import logging


from google.oauth2 import service_account
from googleapiclient import discovery


from rex_backend.utils.firebase_cloud_messaging import base64_to_dict


logger = logging.getLogger('rex_backend')


class GoogleTableService:
    def __init__(self, base64_cert, spreadsheet_id, spreadsheet_range, scopes=['https://www.googleapis.com/auth/spreadsheets.readonly']):
        if base64_cert:
            json_cert = base64_to_dict(base64_cert)
            self.spreadsheet_id = spreadsheet_id
            self.spreadsheet_range = spreadsheet_range
            self.credentials = service_account.Credentials.from_service_account_info(json_cert, scopes=scopes)
            self.service = discovery.build('sheets', 'v4', credentials=self.credentials)
        else:
            logger.error('GoogleTableService not initialized properly, base64_cert is empty.')

    def get_whitelisted_emails(self):
        request = self.service.spreadsheets().values().get(
            spreadsheetId=self.spreadsheet_id,
            range=self.spreadsheet_range
        )
        response = request.execute()
        whitelisted_emails = response.get('values', [])
        whitelisted_emails = [item.strip() for sublist in whitelisted_emails for item in sublist]
        return whitelisted_emails
